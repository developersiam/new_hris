﻿using DomainModelHris;
using DomainModelPayroll;
using HRISSystem.Shared;
using HRISSystem.ViewModel;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HRISSystem.Form.TemporaryWorker
{
    /// <summary>
    /// Interaction logic for TemporaryWorkerPage.xaml
    /// </summary>
    public partial class TemporaryWorkerPage : Page
    {
        List<sp_GetWorkingDateToPayroll_Result> workList = new List<sp_GetWorkingDateToPayroll_Result>();
        public TemporaryWorkerPage()
        {
            InitializeComponent();
            LotCombobox.ItemsSource = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);
            LotCombobox.SelectedIndex = -1;
            DepartmentCombobox.ItemsSource = BindDepartment();
            DepartmentCombobox.SelectedIndex = -1;
            StafftypeCombobox.ItemsSource = BindStaffType();
            StafftypeCombobox.IsEnabled = false;
        }

        private List<Department> BindDepartment()
        {
            var employeeList = BusinessLayerServices.EmployeeBL().GetEmployeeListByStafftype(4); // Only temporary staff
            var groupDepartment = employeeList.GroupBy(g => new { g.DeptCode }).Select(s => new Department { DeptCode = s.Key.DeptCode }).ToList();
            var departmentList = new List<Department>();
            foreach (var item in groupDepartment)
            {
                var department = BusinessLayerServices.DeptCodeBL().GetSingleDepartment(item.DeptCode);
                departmentList.Add(department);
            }
            departmentList = departmentList.OrderBy(o => o.DeptName).ToList();
            return departmentList;
        }

        private List<StaffType> BindStaffType()
        {
            //var dept = SingletonConfiguration.getInstance().DepartmentCode.ToUpper();
            //var username = SingletonConfiguration.getInstance().Username.ToLower();
            var staffList = new List<StaffType>();
            staffList.Add(new StaffType { StaffTypeID = 4, Description = "พนักงานรายวันชั่วคราว" });
            StafftypeCombobox.ItemsSource = staffList;
            return staffList;
        }

        private void LotCombobox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                var selected = (Lot_Number)LotCombobox.SelectedItem;
                if (selected == null) return;
                LotFromDatePicker.SelectedDate = selected != null ? selected.Start_date : null;
                LotToDatePicker.SelectedDate = selected != null ? selected.Finish_date : null;
                SalaryPaidDatePicker.SelectedDate = selected != null ? selected.Salary_Paid_date : null;
                OTPaidDatePicker.SelectedDate = selected != null ? selected.Ot_Paid_date : null;
                Clear();

                if (selected.Lock_Hr != "2" && selected.Lock_Hr_Labor != "2")
                {
                    MessageBox.Show("ไม่สามารถบันทึกข้อมูลงวดนี้ได้ ฝ่ายทรัพยากรณ์บุคคลยังไม่ได้ทำการล็อคงวด", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    UpdateButton.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            DepartmentCombobox.SelectedIndex = -1;
            InformationDataGrid.ItemsSource = null;
            TotalIncomeTextBox.Text = "";
            TotalPNDTextBox.Text = "";
            TotalStudentLoanTextBox.Text = "";
            TotalSSOTextBox.Text = "";
            TotalDeductTextBox.Text = "";
            GrandTotalTextBox.Text = "";
            TotalNetSalaryTextBox.Text = "";
            TotalSalaryTextBox.Text = "";
            UpdateButton.IsEnabled = false;
            ReportButton.IsEnabled = false;
            ReCalculateButton.IsEnabled = false;
        }

        private void DepartmentCombobox_DropDownClosed(object sender, EventArgs e)
        {
            ReloadDatagrid("");
        }

        private void ReloadDatagrid(string mode)
        {
            var deptCode = (Department)DepartmentCombobox.SelectedItem;
            var fromDate = LotFromDatePicker.SelectedDate;
            var toDate = LotToDatePicker.SelectedDate;
            var lot = (Lot_Number)LotCombobox.SelectedItem;
            if (lot == null) throw new Exception("กรุณาระบุงวด");
            if (fromDate == null || toDate == null) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
            if (fromDate > toDate) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");

            var EmployeeWageList = new List<TemporaryWorkerWageVM>();

            // get all work date by lot & department
            var empbyDeptList = BusinessLayerServices.TemporaryWorkerBL().sp_GetTemporaryWorkerWage(fromDate.Value, toDate.Value, deptCode.DeptCode);
            // group employee from work date
            var groupEmployee = empbyDeptList.GroupBy(g => new { g.DEPT_CODE, g.DEPT_NAME, g.EMPLOYEE_ID, g.FNAME, g.LNAME, g.Stafftype, g.Position_ID })
                                             .Select(s => new sp_GetTemporaryWorkerWage_Result { DEPT_CODE = s.Key.DEPT_CODE, DEPT_NAME = s.Key.DEPT_NAME, EMPLOYEE_ID = s.Key.EMPLOYEE_ID, FNAME = s.Key.FNAME, LNAME = s.Key.LNAME, Stafftype = s.Key.Stafftype, Position_ID = s.Key.Position_ID }).ToList();
            // get all ot by lot & department
            var otList = BusinessLayerServices.OTBL().GetOTListbyDept(deptCode.DeptCode, fromDate.Value, toDate.Value);

            foreach (var emp in groupEmployee)
            {
                // get each employee work date
                var singleEmpList = empbyDeptList.Where(w => w.EMPLOYEE_ID == emp.EMPLOYEE_ID).ToList(); 
                // get each employee OT
                var employeeOTList = otList.Where(w => w.Employee_ID == emp.EMPLOYEE_ID).ToList();

                // calculate summary money
                var eachWageList = new List<TemporaryWorkerWageVM>();
                for (var day = fromDate.Value.Date; day.Date <= toDate.Value.Date; day = day.AddDays(1))
                {
                    var isWorking = singleEmpList.SingleOrDefault(s => s.CalendarDate == day);

                    if (isWorking != null)
                    {
                        var tempWageSetup = BusinessLayerServices.TemporaryWorkerBL().GetSingleWage(deptCode.DeptCode, emp.Position_ID, day); // get day wage setup
                        var eachOTList = employeeOTList.Where(w => w.OTDate == day); // get day OT

                        var eachWage = new TemporaryWorkerWageVM();
                        eachWage.CalendarDate = day;
                        var wage = (tempWageSetup == null ? 325 : tempWageSetup.Wage) * isWorking.NWORKINGDATE; // day wage 
                        eachWage.NetSalary = wage; // wage divide by working
                        var ot = eachOTList.Where(e => e.OTType == "N").Sum(e => e.NcountOT);
                        var _SumOT = ot == null ? 0 : ot * Convert.ToDecimal(HRISService.DataCorrector().OTRateCalculate(wage.ToString(), "4"));
                        eachWage.SumOT = _SumOT;
                        var oth = eachOTList.Where(e => e.OTType == "H").Sum(e => e.NcountOT);
                        var _SumOTH = oth == null ? 0 : oth * Convert.ToDecimal(HRISService.DataCorrector().OTHolidayCalculate(wage.ToString(), "4"));
                        eachWage.SumOTH = _SumOTH;
                        var othh = eachOTList.Where(e => e.OTType == "S").Sum(e => e.NcountOT);
                        var _SumOTHH = othh == null ? 0 : othh * Convert.ToDecimal(HRISService.DataCorrector().OTHolidayRateCalculate(wage.ToString(), "4"));
                        eachWage.SumOTHH = _SumOTHH;
                        var _TotalIncome = wage + _SumOT + _SumOTH + _SumOTHH;
                        eachWage.TotalIncome = _TotalIncome;
                        eachWageList.Add(eachWage);
                    }
                }

                // each employee summary
                var eachEmployeeWage = new TemporaryWorkerWageVM
                {
                    DEPT_CODE = emp.DEPT_CODE,
                    DEPT_NAME = emp.DEPT_NAME,
                    EMPLOYEE_ID = emp.EMPLOYEE_ID,
                    FNAME = emp.FNAME,
                    LNAME = emp.LNAME,
                    Stafftype = emp.Stafftype,
                    Position_ID = emp.Position_ID,
                    NWORKINGDATE = singleEmpList.Sum(e => e.NWORKINGDATE),
                    NetSalary = BusinessLayerServices.TemporaryWorkerBL().GetWagebyDateList(deptCode.DeptCode, emp.Position_ID, fromDate.Value, toDate.Value),
                    OTnormal = employeeOTList.Where(e => e.OTType == "N").Sum(e => e.NcountOT),
                    Holiday = employeeOTList.Where(e => e.OTType == "H").Sum(e => e.NcountOT),
                    OTHoliday = employeeOTList.Where(e => e.OTType == "S").Sum(e => e.NcountOT),
                    //OTSpecial = singleOTList.Where(e => e.OTType == "SP").Sum(e => e.NcountOT),
                    SumOT = eachWageList.Sum(e => e.SumOT),
                    SumOTH = eachWageList.Sum(e => e.SumOTH),
                    SumOTHH = eachWageList.Sum(e => e.SumOTHH),
                    //SumSpecial = 0,
                    TotalIncome = eachWageList.Sum(e => e.TotalIncome),
                    //CutWage = 0,
                    //NetIncome = 0
                };
                EmployeeWageList.Add(eachEmployeeWage);
            }
            //}

            InformationDataGrid.ItemsSource = null;
            InformationDataGrid.ItemsSource = workList;
            TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", EmployeeWageList.Count);
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ReportButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ReCalculateButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }
    }
}
