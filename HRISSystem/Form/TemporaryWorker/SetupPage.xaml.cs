﻿using DomainModelHris;
using DomainModelPayroll;
using HRISSystem.Shared;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HRISSystem.Form.TemporaryWorker
{
    /// <summary>
    /// Interaction logic for SetupPage.xaml
    /// </summary>
    public partial class SetupPage : Page
    {
        List<DomainModelHris.Employee> employeeList;
        public SetupPage()
        {
            InitializeComponent();
            employeeList = BusinessLayerServices.EmployeeBL().GetEmployeeListByStafftype(4); // Only temporary staff
            LotCombobox.ItemsSource = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);
            LotCombobox.SelectedIndex = -1;
            BindDepartment();
            BindPosition();
            StafftypeCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();

            var dept = SingletonConfiguration.getInstance().DepartmentCode.ToUpper();
            StafftypeCombobox.SelectedValue = "4";
            StafftypeCombobox.IsEnabled = dept == "IT";
        }

        private void BindDepartment()
        {
            try
            {
                var groupDepartment = employeeList.GroupBy(g => new { g.DeptCode }).Select(s => new Department { DeptCode = s.Key.DeptCode }).ToList();
                var departmentList = new List<Department>();
                foreach (var item in groupDepartment)
                {
                    var department = BusinessLayerServices.DeptCodeBL().GetSingleDepartment(item.DeptCode);
                    departmentList.Add(department);
                }
                departmentList = departmentList.OrderBy(o => o.DeptName).ToList();
                DepartmentCombobox.ItemsSource = departmentList;
                DepartmentCombobox.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void LotCombobox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                var selected = (Lot_Number)LotCombobox.SelectedItem;
                if (selected == null) return;
                LotFromDatePicker.SelectedDate = selected != null ? selected.Start_date : null;
                LotToDatePicker.SelectedDate = selected != null ? selected.Finish_date : null;
                ManualFromDatePicker.DisplayDateStart = selected != null ? selected.Start_date : null;
                ManualFromDatePicker.DisplayDateEnd = selected != null ? selected.Finish_date : null;
                ManualToDatePicker.DisplayDateStart = selected != null ? selected.Start_date : null;
                ManualToDatePicker.DisplayDateEnd = selected != null ? selected.Finish_date : null;
                ReloadDatagrid();
                //Clear();

                //if (selected.Lock_Hr != "2" && selected.Lock_Hr_Labor != "2")
                //{
                //    MessageBox.Show("ไม่สามารถบันทึกข้อมูลงวดนี้ได้ ฝ่ายทรัพยากรณ์บุคคลยังไม่ได้ทำการล็อคงวด", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    UpdateButton.IsEnabled = false;
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DepartmentCombobox_DropDownClosed(object sender, EventArgs e)
        {
            BindPosition();
            ReloadDatagrid();
        }

        private void BindPosition()
        {
            try
            {
                var dept = (Department)DepartmentCombobox.SelectedItem;
                //if (dept == null) throw new Exception("Please select department");
                if (dept != null)
                {
                    var groupPosition = employeeList.Where(w => w.DeptCode == dept.DeptCode).ToList()
                                                .GroupBy(g => new { g.Position_ID })
                                                .Select(s => new Position { Position_ID = s.Key.Position_ID }).ToList();
                    var positionList = new List<Position>();
                    foreach (var item in groupPosition)
                    {
                        var position = BusinessLayerServices.PositionBL().GetPositionByID(item.Position_ID);
                        positionList.Add(position);
                    }

                    PositionCombobox.ItemsSource = null;
                    PositionCombobox.ItemsSource = positionList;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadDatagrid()
        {
            try
            {
                var fromDate = LotFromDatePicker.SelectedDate;
                var toDate = LotToDatePicker.SelectedDate;
                var dept = (Department)DepartmentCombobox.SelectedItem;

                if (dept != null)
                {
                    var wageList = BusinessLayerServices.TemporaryWorkerBL().GetTemporaryWageListByDate(dept.DeptCode, fromDate.Value, toDate.Value);

                    InformationDataGrid.ItemsSource = null;
                    InformationDataGrid.ItemsSource = wageList;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var fromDate = ManualFromDatePicker.SelectedDate;
                    var toDate = ManualToDatePicker.SelectedDate;
                    var dept = (Department)DepartmentCombobox.SelectedItem;
                    var posi = (Position)PositionCombobox.SelectedItem;
                    var wageText = AmountTextBox.Text.Trim();

                    var wage = Convert.ToDecimal(wageText);

                    var newItemlist = new List<TemporaryWorkerWage>();
                    for (var day = fromDate.Value.Date; day.Date <= toDate.Value.Date; day = day.AddDays(1))
                    {
                        var newItem = new TemporaryWorkerWage
                        {
                            WageDate = day,
                            DepartmentCode = dept.DeptCode,
                            PositionID = posi.Position_ID,
                            Wage = wage,
                            Remark = RemarkTextBox.Text
                        };
                        newItemlist.Add(newItem);
                        BusinessLayerServices.TemporaryWorkerBL().AddTemporaryWage(newItem);
                    }
                    DataCorrectorStatic<TemporaryWorkerWage>.AddTransactionList("A", "Temporary wage setup", null, newItemlist);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LotCombobox.SelectedIndex = -1;
                LotFromDatePicker.SelectedDate = null;
                LotToDatePicker.SelectedDate = null;
                DepartmentCombobox.SelectedIndex = -1;
                PositionCombobox.SelectedIndex = -1;
                ManualFromDatePicker.SelectedDate = null;
                ManualToDatePicker.SelectedDate = null;
                AmountTextBox.Text = "";
                RemarkTextBox.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void StafftypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var selected = (TemporaryWorkerWage)InformationDataGrid.SelectedItem;
                    BusinessLayerServices.TemporaryWorkerBL().DeleteTemporaryWage(selected);
                    DataCorrectorStatic<TemporaryWorkerWage>.AddTransaction("D", "Temporary wage setup", null, selected);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
