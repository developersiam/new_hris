﻿using DomainModelHris;
using DomainModelPayroll;
using HRISSystem.Shared;
using HRISSystem.ViewModel;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HRISSystem.Form.ManualWorkDates
{
    /// <summary>
    /// Interaction logic for ManualDatePage.xaml
    /// </summary>
    public partial class ManualDatePage : Page
    {
        //List<ManualWorkingViewmodel> vmList;

        public ManualDatePage()
        {
            InitializeComponent();
            LotCombobox.ItemsSource = BusinessLayerServices.PayrollBL().GetAllLot();
            DepartmentCombobox.ItemsSource = BusinessLayerServices.DeptCodeBL().GetAllDepartment();
            DepartmentCombobox.SelectedIndex = -1;
            var staffList = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
            staffList.Insert(0, new StaffType { StaffTypeID = 0, Description = "ทั้งหมด" });
            StafftypeCombobox.ItemsSource = staffList;
            var sundayExList = new List<StaffType>();
            sundayExList.Add(new StaffType { StaffTypeID = 0, Description = "ไม่รวมวันอาทิตย์" });
            sundayExList.Add(new StaffType { StaffTypeID = 1, Description = "รวมวันอาทิตย์" });
            SundyExceptionCombobox.ItemsSource = sundayExList;
            SundyExceptionCombobox.SelectedIndex = 0;
        }

        private void ReloadDataGrid()
        {
            try
            {
                var dept = (Department)DepartmentCombobox.SelectedItem;
                var staff = (StaffType)StafftypeCombobox.SelectedItem;
                var sunEx = (StaffType)SundyExceptionCombobox.SelectedItem;
                var dateFrom = ManualFromDatePicker.SelectedDate;
                var dateTo = ManualToDatePicker.SelectedDate;
                var name = EmployeeNameFilterTextBox.Text.Trim();
                if (dept == null) throw new Exception("กรุณาระบุแผนก");
                if (staff == null) throw new Exception("กรุณาระบุประเภทพนักงาน");
                if (dateFrom == null || dateTo == null) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
                if (dateFrom > dateTo) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");

                var vmList = new List<ManualWorkingViewmodel>();
                var manualList = BusinessLayerServices.ManualWorkingBL().GetManualDateByDateAndDepartment(dateFrom.Value, dateTo.Value, dept.DeptCode);
                var employeeList = BusinessLayerServices.EmployeeBL().GetEmployeeListByDepartment(dept.DeptCode);

                for (var day = dateFrom.Value.Date; day.Date <= dateTo.Value.Date; day = day.AddDays(1))
                {
                    var dt = new DateTime(day.Year, day.Month, day.Day);

                    if (name != "") employeeList = employeeList.Where(w => w.Person.FirstNameTH.Contains(name)).ToList();
                    if (staff.StaffTypeID != 0) employeeList = employeeList.Where(w => w.Staff_type == staff.StaffTypeID).ToList();
                    foreach (var emp in employeeList)
                    {
                        var timeIn = manualList.FirstOrDefault(f => f.ManualWorkDateDate == day && f.ManualWorkDateType_ID == "A01" && f.Employee_ID == emp.Employee_ID);
                        var timeOut = manualList.FirstOrDefault(f => f.ManualWorkDateDate == day && f.ManualWorkDateType_ID == "A02" && f.Employee_ID == emp.Employee_ID);
                        var item = new ManualWorkingViewmodel
                        {
                            DayofWeek = dt.DayOfWeek.ToString(),
                            ManualDate = day,
                            EmployeeID = emp.Employee_ID,
                            Title = emp.Person.TitleName.TitleNameInitialTH,
                            Firstname = emp.Person.FirstNameTH,
                            Lastname = emp.Person.LastNameTH,
                            TimeIn = timeIn != null ? timeIn.Times : null,
                            TimeOut = timeOut != null ? timeOut.Times : null,
                        };
                        if (dt.DayOfWeek != DayOfWeek.Sunday) vmList.Add(item);
                        else if(sunEx.StaffTypeID == 1) vmList.Add(item);
                    }
                }

                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = vmList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DepartmentCombobox_DropDownClosed(object sender, EventArgs e)
        {
            if (DepartmentCombobox.SelectedItem != null) Clear();
        }

        private void StafftypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            if (ManualFromDatePicker.SelectedDate != null && ManualToDatePicker.SelectedDate != null && StafftypeCombobox.SelectedItem != null) ReloadDataGrid();
        }

        private void Clear()
        {
            EmployeeNameFilterTextBox.Text = "";
            ManualFromDatePicker.SelectedDate = null;
            ManualToDatePicker.SelectedDate = null;
            TimeInTextBox.Text = "";
            TimeOutTextBox.Text = "";
            StafftypeCombobox.SelectedIndex = -1;
            InformationDataGrid.ItemsSource = null;
            SundyExceptionCombobox.SelectedIndex = 0;
        }

        private void EmployeeNameFilterTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            ReloadDataGrid();
        }

        private void TimeInTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TimeInTextBox.Text = HRISService.DataCorrector().FormatTime(TimeInTextBox.Text);
        }

        private void TimeOutTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TimeOutTextBox.Text = HRISService.DataCorrector().FormatTime(TimeOutTextBox.Text);
        }

        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ManualFromDatePicker.SelectedDate != null && ManualToDatePicker.SelectedDate != null && StafftypeCombobox.SelectedItem != null) ReloadDataGrid();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDataGrid();
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var lot = (Lot_Number)LotCombobox.SelectedItem;
                if (lot.Lock_Acc == "2" || lot.Lock_Acc_Labor == "2") throw new Exception("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถแก้ไขงวดนี้ได้อีก");
                if (lot.Lock_Hr_Labor == "2") throw new Exception("ฝ่ายทรัพยากรบุคคลได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก");

                var dept = (Department)DepartmentCombobox.SelectedItem;
                var staff = (StaffType)StafftypeCombobox.SelectedItem;
                if (dept == null) throw new Exception("กรุณาระบุแผนก");
                if (staff == null) throw new Exception("กรุณาระบุประเภทพนักงาน");

                var validFrom = ManualFromDatePicker.SelectedDate.Value;
                var endDate = ManualToDatePicker.SelectedDate.Value;
                if (validFrom == null || endDate == null) throw new Exception("กรุณาระบุวันที่");
                if (validFrom > endDate) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
                if (validFrom < lot.Start_date || validFrom > lot.Finish_date) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
                if (endDate < lot.Start_date || endDate > lot.Finish_date) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");

                if (string.IsNullOrEmpty(TimeInTextBox.Text) ||string.IsNullOrEmpty(TimeOutTextBox.Text)) throw new Exception("กรุณาระบุเวลาให้ถูกต้อง");
                var timeIn = TimeSpan.Parse(TimeInTextBox.Text);
                var timeOut = TimeSpan.Parse(TimeOutTextBox.Text);
                if (timeIn > timeOut) throw new Exception("กรุณาระบุเวลาให้ถูกต้อง");
                var sunEx = (StaffType)SundyExceptionCombobox.SelectedItem;

                if (MessageBox.Show("ข้อมูลการลงเวลาของพนักงานที่อยู่ในรายการทั้งหมดจะถูกแทนที่ด้วยเวลาที่ระบุ ยืนยันบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var newItemList = new List<ManualWorkDate>();
                    var employeeList = BusinessLayerServices.EmployeeBL().GetEmployeeListByDepartment(DepartmentCombobox.SelectedValue.ToString());
                    if (staff.StaffTypeID != 0) employeeList = employeeList.Where(w => w.Staff_type == staff.StaffTypeID).ToList();

                    for (var day = validFrom.Date; day.Date <= endDate.Date; day = day.AddDays(1))
                    {
                        var dt = new DateTime(day.Year, day.Month, day.Day);
                        foreach (var emp in employeeList)
                        {
                            var manualIn = new ManualWorkDate
                            {
                                Employee_ID = emp.Employee_ID,
                                ManualWorkDateDate = day,
                                ManualWorkDateType_ID = "A01",
                                Times = TimeSpan.Parse(TimeInTextBox.Text),
                                Status = true,
                                ModifiedDate = DateTime.Now
                            };
                            var manualOut = new ManualWorkDate
                            {
                                Employee_ID = emp.Employee_ID,
                                ManualWorkDateDate = day,
                                ManualWorkDateType_ID = "A02",
                                Times = TimeSpan.Parse(TimeOutTextBox.Text),
                                Status = true,
                                ModifiedDate = DateTime.Now
                            };

                            if (dt.DayOfWeek != DayOfWeek.Sunday)
                            {
                                Update(manualIn, manualOut);
                                newItemList.Add(manualIn);
                                newItemList.Add(manualOut);
                            }
                            else if (sunEx.StaffTypeID == 1)
                            {
                                Update(manualIn, manualOut);
                                newItemList.Add(manualIn);
                                newItemList.Add(manualOut);
                            }
                        }
                    }

                    if (newItemList.Any())
                    {
                        DataCorrectorStatic<ManualWorkDate>.AddTransactionList("A", "Manual Work Date", null, newItemList);
                        MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        ReloadDataGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Update(ManualWorkDate manualIn, ManualWorkDate manualOut)
        {
            var timeIn = BusinessLayerServices.ManualWorkingBL().GetSingleManualWorkDate(manualIn.ManualWorkDateDate, "A01", manualIn.Employee_ID);
            var timeOut = BusinessLayerServices.ManualWorkingBL().GetSingleManualWorkDate(manualOut.ManualWorkDateDate, "A02", manualOut.Employee_ID);
            if (timeIn != null) BusinessLayerServices.ManualWorkingBL().DeleteManuaWorkingDate(manualIn);
            BusinessLayerServices.ManualWorkingBL().AddManuaWorkingDate(manualIn);
            if (timeOut != null) BusinessLayerServices.ManualWorkingBL().DeleteManuaWorkingDate(manualOut);
            BusinessLayerServices.ManualWorkingBL().AddManuaWorkingDate(manualOut);
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
            DepartmentCombobox.SelectedIndex = -1;
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                var oldItem = (ManualWorkingViewmodel)InformationDataGrid.SelectedItem;
                if (oldItem != null && (oldItem.TimeIn != null || oldItem.TimeOut != null))
                {
                    var lot = (Lot_Number)LotCombobox.SelectedItem;
                    if (lot.Lock_Acc == "2" || lot.Lock_Acc_Labor == "2") throw new Exception("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถแก้ไขงวดนี้ได้อีก");
                    if (lot.Lock_Hr_Labor == "2") throw new Exception("ฝ่ายทรัพยากรบุคคลได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก");

                    if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        var manual = BusinessLayerServices.ManualWorkingBL().GetManualDateByDateAndID(oldItem.ManualDate.Value, oldItem.EmployeeID);
                        var manualIn = manual.FirstOrDefault(f => f.ManualWorkDateType_ID == "A01");
                        if (manualIn != null)
                        {
                            BusinessLayerServices.ManualWorkingBL().DeleteManuaWorkingDate(manualIn);
                            DataCorrectorStatic<ManualWorkDate>.AddTransaction("D", "Manual Work Date", manualIn, null);
                        }

                        var manualOut = manual.FirstOrDefault(f => f.ManualWorkDateType_ID == "A02");
                        if (manualOut != null)
                        {
                            BusinessLayerServices.ManualWorkingBL().DeleteManuaWorkingDate(manualOut);
                            DataCorrectorStatic<ManualWorkDate>.AddTransaction("D", "Manual Work Date", manualOut, null);
                        }

                        MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        ReloadDataGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void LotCombobox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                Clear();
                DepartmentCombobox.SelectedIndex = -1;

                var selected = (Lot_Number)LotCombobox.SelectedItem;
                if (selected != null)
                {
                    ManualFromDatePicker.DisplayDateStart = selected.Start_date;
                    ManualFromDatePicker.DisplayDateEnd = selected.Finish_date;
                    ManualToDatePicker.DisplayDateStart = selected.Start_date;
                    ManualToDatePicker.DisplayDateEnd = selected.Finish_date;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SundyExceptionCombobox_DropDownClosed(object sender, EventArgs e)
        {
            if (ManualFromDatePicker.SelectedDate != null && ManualToDatePicker.SelectedDate != null && StafftypeCombobox.SelectedItem != null) ReloadDataGrid();
        }
    }
}
