﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HRISSystemBL;
using DomainModelHris;
using HRISSystem.ViewModel;

namespace HRISSystem.Form.Employees
{
    /// <summary>
    /// Interaction logic for SearchEmployee_Window.xaml
    /// </summary>
    public partial class SearchEmployee_Window : Window
    {
        //public EmployeeInformationViewModel selectedEmployee;
        List<Employee> employeeList;
        public Employee selectedEmployee;
        public SearchEmployee_Window()
        {
            InitializeComponent();
            SearchTypeCombobox.ItemsSource = BindSearchEmployeeTypeList();
            SearchTypeCombobox.SelectedIndex = 0;

            StatusCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffStatusList();
            StatusCombobox.SelectedValue = "1";

            var staffTypeList = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
            staffTypeList.Insert(0, new StaffType { StaffTypeID = 0, Description = "ทั้งหมด" });
            StaffTypeCombobox.ItemsSource = staffTypeList;
            StaffTypeCombobox.SelectedIndex = 0;

            employeeList = new List<Employee>();

            ReloadDatagrid("Reload");
        }

        public List<string> BindSearchEmployeeTypeList()
        {
            List<string> result = new List<string>();
            result.Add("ค้นหาจากชื่อ (TH)");
            result.Add("ค้นหาจากชื่อ (EN)");
            result.Add("ค้นหาจากนามสกุล (TH)");
            result.Add("ค้นหาจากนามสกุล (EN)");
            result.Add("ค้นหาจากรหัสพนักงาน");
            //result.Add("ค้นหาจากรหัส HR");
            //result.Add("ค้นหาจากรหัส Payroll");
            //result.Add("ค้นหาจากรหัสบัตรประชาชน");
            return result;
        }

        private void FilterCombobox_Changed(object sender, EventArgs e)
        {
            ReloadDatagrid("Reload");
        }

        private void GetEmployeeList()
        {
            try
            {
                var searchStatus = (StaffStatu)StatusCombobox.SelectedItem;
                var searchStaff = (StaffType)StaffTypeCombobox.SelectedItem;
                if (searchStaff == null || searchStatus == null) return;

                var indexStatus = searchStatus.StaffStatusID;
                var indexStaff = searchStaff.StaffTypeID;

                employeeList = new List<Employee>();
                employeeList = BusinessLayerServices.EmployeeBL().GetSearchedEmployee(indexStaff, indexStatus);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            ReloadDatagrid("Filter");
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid("Filter");
        }

        private void ReloadDatagrid(string Action)
        {
            try
            {
                if (Action == "Reload") GetEmployeeList();
                var searchTypeIndex = SearchTypeCombobox.SelectedIndex;
                var searchText = SearchTextBox.Text.Trim();
                var filterList = new List<Employee>();

                if (!string.IsNullOrEmpty(searchText))
                {
                    switch (searchTypeIndex)
                    {
                        case 0:
                            filterList = employeeList.Where(w => w.Person.FirstNameTH.Contains(searchText)).ToList();
                            break;
                        case 1:
                            filterList = employeeList.Where(w => w.Person.FirstNameEN.ToLower().Contains(searchText.ToLower())).ToList();
                            break;
                        case 2:
                            filterList = employeeList.Where(w => w.Person.LastNameTH.Contains(searchText)).ToList();
                            break;
                        case 3:
                            filterList = employeeList.Where(w => w.Person.LastNameEN.ToLower().Contains(searchText)).ToList();
                            break;
                        case 4:
                            filterList = employeeList.Where(w => w.Employee_ID.StartsWith(searchText)).ToList();
                            break;
                        default:
                            break;
                    }
                }
                else filterList = employeeList;

                EmployeeInformationDataGrid.ItemsSource = null;
                EmployeeInformationDataGrid.ItemsSource = filterList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            DataSelected();
        }

        private void EmployeeInformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left) DataSelected();
        }

        private void DataSelected()
        {
            try
            {
                var employee = (DomainModelHris.Employee)EmployeeInformationDataGrid.SelectedItem;
                if (employee != null) selectedEmployee = employee; //Define data to public model for Passing value to mainpage.
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchTypeCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SearchTextBox.Focus();
            if(SearchTextBox.Text != "") ReloadDatagrid("Filter");
        }
    }
}
