﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModelHris;
using HRISSystemBL;
using HRISSystem.Shared;

namespace HRISSystem.Form.Employees.Persons
{
    /// <summary>
    /// Interaction logic for WorkingHistory2_Page.xaml
    /// </summary>
    public partial class WorkingHistory2_Page : Page
    {
        string PersonID;
        public WorkingHistory2_Page(string personID)
        {
            InitializeComponent();
            PersonID = personID;
            ReloadDatagrid();
            AddButton.IsEnabled = !string.IsNullOrEmpty(PersonID);
            OriginalWorkIDTextBox.Text = BusinessLayerServices.OriginalWorkBL().GetNewOriginalWorkID();
        }

        private void ReloadDatagrid()
        {
            try
            {
                Clear();
                OriginalWorkingDataGrid.ItemsSource = null;
                OriginalWorkingDataGrid.ItemsSource = BusinessLayerServices.OriginalWorkBL().GetOriginalWorkingListByPersonID(PersonID);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(PersonID)) throw new Exception("กรุณาระบุ Person ID");
                    if (string.IsNullOrEmpty(OriginalWorkIDTextBox.Text)) throw new Exception("กรุณาระบุ Working ID");
                    if (string.IsNullOrEmpty(CompanyNameTextBox.Text)) throw new Exception("กรุณาระบุ Company");
                    if (string.IsNullOrEmpty(PositionTextBox.Text)) throw new Exception("กรุณาระบุ Position");
                    if (string.IsNullOrEmpty(StartDatePicker.Text) || string.IsNullOrEmpty(EndDatePicker.Text)) throw new Exception("กรุณาระบุวันที่");

                    var newItem = new OriginalWorkHistory
                    {
                        OrginalWork_ID = OriginalWorkIDTextBox.Text,
                        Person_ID = PersonID,
                        CompanyName = CompanyNameTextBox.Text,
                        PositionName = PositionTextBox.Text,
                        JoinDate = StartDatePicker.SelectedDate,
                        EndDate = EndDatePicker.SelectedDate,
                        Noted = RemarkTextBox.Text,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.OriginalWorkBL().AddOriginalWork(newItem);
                    DataCorrectorStatic<OriginalWorkHistory>.AddTransaction("A", "Person Original Working", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(PersonID)) throw new Exception("กรุณาระบุ Person ID");
                    if (string.IsNullOrEmpty(OriginalWorkIDTextBox.Text)) throw new Exception("กรุณาระบุ Working ID");
                    if (string.IsNullOrEmpty(CompanyNameTextBox.Text)) throw new Exception("กรุณาระบุ Company");
                    if (string.IsNullOrEmpty(PositionTextBox.Text)) throw new Exception("กรุณาระบุ Position");
                    if (string.IsNullOrEmpty(StartDatePicker.Text) || string.IsNullOrEmpty(EndDatePicker.Text)) throw new Exception("กรุณาระบุวันที่");

                    var oldItem = BusinessLayerServices.OriginalWorkBL().GetSingleOriginalWorkHistory(OriginalWorkIDTextBox.Text);
                    var newItem = new OriginalWorkHistory
                    {
                        OrginalWork_ID = oldItem.OrginalWork_ID,
                        Person_ID = PersonID,
                        CompanyName = CompanyNameTextBox.Text,
                        PositionName  =PositionTextBox.Text,
                        JoinDate = StartDatePicker.SelectedDate,
                        EndDate = EndDatePicker.SelectedDate,
                        Noted = RemarkTextBox.Text,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.OriginalWorkBL().UpdateOriginalWork(newItem);
                    DataCorrectorStatic<OriginalWorkHistory>.AddTransaction("U", "Person Original Working", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void OriginalWorkingDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BindSelectedData();
        }

        private void EditDetailButton_Click(object sender, RoutedEventArgs e)
        {
            BindSelectedData();
        }

        void BindSelectedData()
        {
            try
            {
                var selected = (OriginalWorkHistory)OriginalWorkingDataGrid.SelectedItem;
                if (selected != null)
                {
                    OriginalWorkIDTextBox.Text = selected.OrginalWork_ID;
                    CompanyNameTextBox.Text = selected.CompanyName;
                    PositionTextBox.Text = selected.PositionName;
                    StartDatePicker.SelectedDate = selected.JoinDate;
                    EndDatePicker.SelectedDate = selected.EndDate;
                    RemarkTextBox.Text = selected.Noted;
                    AddButton.IsEnabled = false;
                }
                UpdateButton.IsEnabled = selected != null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var oldItem = (OriginalWorkHistory)OriginalWorkingDataGrid.SelectedItem;
                if (oldItem != null)
                {
                    if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        BusinessLayerServices.OriginalWorkBL().DeleteOriginalWork(oldItem);
                        DataCorrectorStatic<OriginalWorkHistory>.AddTransaction("D", "Person Original Working", oldItem, null);
                        MessageBox.Show("แก้ไขข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        ReloadDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Clear()
        {
            try
            {
                OriginalWorkIDTextBox.Text = BusinessLayerServices.OriginalWorkBL().GetNewOriginalWorkID();
                CompanyNameTextBox.Text = "";
                PositionTextBox.Text = "";
                StartDatePicker.SelectedDate = null;
                EndDatePicker.SelectedDate = null;
                RemarkTextBox.Text = "";
                AddButton.IsEnabled = true;
                UpdateButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }
    }
}
