﻿using HRISSystemBL;
using System;
using System.Windows;
using System.Windows.Controls;

namespace HRISSystem.Form.Employees.Persons
{
    /// <summary>
    /// Interaction logic for Userinfo_Page.xaml
    /// </summary>
    public partial class Userinfo_Page : Page
    {
        string PersonID;
        public Userinfo_Page(string Person_ID)
        {
            InitializeComponent();
            PersonID = Person_ID;
            if (string.IsNullOrEmpty(PersonID)) return;
            Clear();
            ReloadData();
            //BindControl();
        }

        private void BindControl()
        {
            throw new NotImplementedException();
        }

        private void ReloadData()
        {
            try
            {
                var userinfo = BusinessLayerServices.TimeStecBL().GetUserInfoByPersonID(PersonID);
                UserIDTextBox.Text = userinfo.userid.ToString();
                BadgenumberTextBox.Text = userinfo.badgenumber;
                CardTextBox.Text = userinfo.Card;
                NameTextBox.Text = userinfo.name;
                LastnameTextBox.Text = userinfo.lastname;
                GenderTextBox.Text = userinfo.Gender;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(UserIDTextBox.Text)) throw new Exception("กรุณาระบุ UserID");
                    if (string.IsNullOrEmpty(BadgenumberTextBox.Text)) throw new Exception("กรุณาระบุ Badgenumber");

                    var userinfo = BusinessLayerServices.TimeStecBL().GetUserInfoByPersonID(PersonID);

                    if (CardTextBox.Text != "")
                    {
                        var existedCard = BusinessLayerServices.TimeStecBL().GetUserInfoByCard(CardTextBox.Text);
                        if (existedCard != null && existedCard.userid != userinfo.userid)
                        {
                            if (MessageBox.Show("หมายเลขบัตรนี้ " + existedCard.name + " " + existedCard.lastname + " ใช้งานอยู่ ยืนยันที่จะทำการแทนบัตรหรือไม่?", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                            {
                                existedCard.Card = "";
                                BusinessLayerServices.TimeStecBL().UpdateUserInfo(existedCard); // Clear card from previous user
                                userinfo.Card = CardTextBox.Text; // Update card to new user
                            }
                            else return;
                        }
                        else userinfo.Card = CardTextBox.Text;
                    }

                    userinfo.name = NameTextBox.Text;
                    userinfo.lastname = LastnameTextBox.Text;
                    BusinessLayerServices.TimeStecBL().UpdateUserInfo(userinfo);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Clear()
        {
            try
            {
                UserIDTextBox.Text = "";
                BadgenumberTextBox.Text = "";
                CardTextBox.Text = "";
                NameTextBox.Text = "";
                LastnameTextBox.Text = "";
                GenderTextBox.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }
    }
}
