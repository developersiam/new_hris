﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModelHris;
using HRISSystemBL;
using HRISSystem.Shared;

namespace HRISSystem.Form.Employees.Persons
{
    /// <summary>
    /// Interaction logic for IDCard_Page.xaml
    /// </summary>
    public partial class IDCard_Page : Page
    {
        string PersonID;
        public IDCard_Page(string Person_ID)
        {
            InitializeComponent();
            PersonID = Person_ID;
            if (string.IsNullOrEmpty(PersonID)) return;
            ReloadDatagrid();
            ProvinveCombobox.ItemsSource = BusinessLayerServices.AddressBL().GetAllProvince();
            //AddButton.IsEnabled = !string.IsNullOrEmpty(PersonID);
            //UpdateButton.IsEnabled = !string.IsNullOrEmpty(PersonID);
        }

        private void ReloadDatagrid()
        {
            try
            {
                Clear();
                IDCardDataGrid.ItemsSource = null;
                IDCardDataGrid.ItemsSource = BusinessLayerServices.IIDCardInfoBL().GetIDCardByPersonID(PersonID);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditDetailButton_Click(object sender, RoutedEventArgs e)
        {
            BindSelectedControl();
        }

        private void IDCardDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BindSelectedControl();
        }

        public void BindSelectedControl()
        {
            try
            {
                var selected = (IDCardInfo)IDCardDataGrid.SelectedItem;
                if (selected != null)
                {
                    IDCardTextBox.Text = selected.Card_ID;
                    IDCardTextBox.IsEnabled = false;
                    IssueDatePicker.SelectedDate = selected.IssuedDate;
                    ExpireDatePicker.SelectedDate = selected.ExpiredDate;
                    ExpireDatePicker.IsEnabled = false;
                    ProvinveCombobox.SelectedValue = selected.IssuedAtProvinceID;
                    DistrictCombobox.ItemsSource = null;
                    DistrictCombobox.ItemsSource = BusinessLayerServices.AddressBL().GetDistrictListByProvinceID(selected.IssuedAtProvinceID);
                    DistrictCombobox.SelectedValue = selected.IssuedAtDistrictID;
                    AddButton.IsEnabled = false;
                }
                UpdateButton.IsEnabled = selected != null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ProvinveCombobox_DropDownClosed(object sender, EventArgs e)
        {
            var province = (Province)ProvinveCombobox.SelectedItem;
            if (province == null) return;
            DistrictCombobox.ItemsSource = null;
            DistrictCombobox.ItemsSource = BusinessLayerServices.AddressBL().GetDistrictListByProvinceID(province.Province_ID);
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(IDCardTextBox.Text)) throw new Exception("กรุณาระบุเลขบัตรประชาชน");
                    if (string.IsNullOrEmpty(IssueDatePicker.Text) || string.IsNullOrEmpty(ExpireDatePicker.Text)) throw new Exception("กรุณาระบุวันที่");
                    if (string.IsNullOrEmpty(ProvinveCombobox.Text)) throw new Exception("กรุณาระบุจังหวัด");
                    if (string.IsNullOrEmpty(DistrictCombobox.Text)) throw new Exception("กรุณาระบุอำเภอ/เขต");

                    var item = BusinessLayerServices.IIDCardInfoBL().GetSingleIDCardInfo(IDCardTextBox.Text, ExpireDatePicker.SelectedDate.Value);
                    item.IssuedDate = IssueDatePicker.SelectedDate;
                    item.IssuedAtDistrictID = DistrictCombobox.SelectedValue.ToString();
                    item.IssuedAtProvinceID = ProvinveCombobox.SelectedValue.ToString();
                    item.ModifiedDate = DateTime.Now;

                    BusinessLayerServices.IIDCardInfoBL().Update(item);
                    //DataCorrectorStatic<IDCardInfo>.AddTransaction("U", "Person ID Card", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(PersonID)) throw new Exception("กรุณาระบุ Person ID");
                    //if (string.IsNullOrEmpty(IDCardTextBox.Text)) throw new Exception("กรุณาระบุเลขบัตรประชาชน");
                    var isIDCardCorrect = HRISService.DataCorrector().NumericCheck(IDCardTextBox.Text, 0, 13);
                    if (!isIDCardCorrect) throw new Exception("กรุณาระบุเลขบัตรประชาชนให้ถูกต้อง");
                    if (string.IsNullOrEmpty(IssueDatePicker.Text) || string.IsNullOrEmpty(ExpireDatePicker.Text)) throw new Exception("กรุณาระบุวันที่");
                    if (string.IsNullOrEmpty(ProvinveCombobox.Text)) throw new Exception("กรุณาระบุจังหวัด");
                    if (string.IsNullOrEmpty(DistrictCombobox.Text)) throw new Exception("กรุณาระบุอำเภอ/เขต");

                    var newItem = new IDCardInfo
                    {
                        Person_ID = PersonID,
                        Card_ID = IDCardTextBox.Text,
                        IssuedDate = IssueDatePicker.SelectedDate,
                        ExpiredDate = ExpireDatePicker.SelectedDate.Value,
                        IssuedAtDistrictID = DistrictCombobox.SelectedValue.ToString(),
                        IssuedAtProvinceID = ProvinveCombobox.SelectedValue.ToString(),
                        CreatedDate = DateTime.Now
                    };
                    BusinessLayerServices.IIDCardInfoBL().Add(newItem);
                    //DataCorrectorStatic<IDCardInfo>.AddTransaction("A", "Person ID Card", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var oldItem = (IDCardInfo)IDCardDataGrid.SelectedItem;
                    BusinessLayerServices.IIDCardInfoBL().Delete(oldItem);
                    //DataCorrectorStatic<IDCardInfo>.AddTransaction("D", "Person ID Card", oldItem, null);
                    MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Clear()
        {
            try
            {
                IDCardTextBox.Text = "";
                IDCardTextBox.IsEnabled = true;
                IssueDatePicker.SelectedDate = null;
                ExpireDatePicker.SelectedDate = null;
                ExpireDatePicker.IsEnabled = true;
                ProvinveCombobox.SelectedIndex = -1;
                DistrictCombobox.ItemsSource = null;
                DistrictCombobox.SelectedIndex = -1;
                AddButton.IsEnabled = true;
                UpdateButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }
    }
}
