﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DomainModelHris;
using HRISSystemBL;
using HRISSystem.Shared;

namespace HRISSystem.Form.Employees.Persons
{
    /// <summary>
    /// Interaction logic for Contact_Page.xaml
    /// </summary>
    public partial class Contact_Page : Page
    {
        string PersonID;
        public Contact_Page(string personID)
        {
            InitializeComponent();
            PersonID = personID;
            AddButton.IsEnabled = !string.IsNullOrEmpty(PersonID);
            if (string.IsNullOrEmpty(PersonID)) return;
            ReloadDatagrid();
            TitlenameCombobox.ItemsSource = BusinessLayerServices.PersonBL().GetAllTitlenameList();
            ContactIDTextBox.Text = BusinessLayerServices.ContactPersonInfoBL().GetNewContactInfoID();
        }

        private void ReloadDatagrid()
        {
            ClearControl();
            ContactDataGrid.ItemsSource = null;
            ContactDataGrid.ItemsSource = BusinessLayerServices.ContactPersonInfoBL().GetContactInfoListByPersonID(PersonID);
        }

        private void EditDetailButton_Click(object sender, RoutedEventArgs e)
        {
            BindSelectedData();
        }

        private void ContactDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BindSelectedData();
        }

        void BindSelectedData()
        {
            try
            {
                var selected = (ContactPersonInfo)ContactDataGrid.SelectedItem;
                if (selected != null)
                {
                    ContactIDTextBox.Text = selected.ContactPersonInfo_ID;
                    TitlenameCombobox.SelectedValue = selected.Title_ID;
                    FirstnameTextBox.Text = selected.FirstName;
                    LastnameTextBox.Text = selected.LastName;
                    RelationshipTextBox.Text = selected.Relation;
                    OccupyTextBox.Text = selected.Occupation;
                    WorkplaceTextBox.Text = selected.CompanyName;
                    PosisionTextBox.Text = selected.PositionName;
                    PhoneHomeTextBox.Text = selected.HomePhone;
                    PhoneTextBox.Text = selected.MobilePhone;
                    AddButton.IsEnabled = false;
                }
                UpdateButton.IsEnabled = selected != null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearControl()
        {
            ContactIDTextBox.Text = BusinessLayerServices.ContactPersonInfoBL().GetNewContactInfoID();
            TitlenameCombobox.Text = "";
            FirstnameTextBox.Text = "";
            LastnameTextBox.Text = "";
            RelationshipTextBox.Text = "";
            OccupyTextBox.Text = "";
            WorkplaceTextBox.Text = "";
            PosisionTextBox.Text = "";
            PhoneHomeTextBox.Text = "";
            PhoneTextBox.Text = "";
            AddButton.IsEnabled = true;
            UpdateButton.IsEnabled = false;
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(PersonID)) throw new Exception("กรุณาระบุ Person ID");
                    if (string.IsNullOrEmpty(ContactIDTextBox.Text)) throw new Exception("กรุณาระบุ Contact ID");
                    if (string.IsNullOrEmpty(TitlenameCombobox.Text) || string.IsNullOrEmpty(FirstnameTextBox.Text) || string.IsNullOrEmpty(LastnameTextBox.Text)) throw new Exception("กรุณาระบุชื่อ-นามสกุล");
                    if (string.IsNullOrEmpty(RelationshipTextBox.Text)) throw new Exception("กรุณาระบุ Relationship");

                    var oldItem = BusinessLayerServices.ContactPersonInfoBL().GetSingleContactPersonInfo(ContactIDTextBox.Text, PersonID);
                    var newItem = new ContactPersonInfo
                    {
                        ContactPersonInfo_ID = oldItem.ContactPersonInfo_ID,
                        Person_ID = oldItem.Person_ID,
                        Relation = RelationshipTextBox.Text,
                        Title_ID = TitlenameCombobox.SelectedValue.ToString(),
                        FirstName = FirstnameTextBox.Text,
                        LastName = LastnameTextBox.Text,
                        Occupation = OccupyTextBox.Text,
                        CompanyName = WorkplaceTextBox.Text,
                        PositionName = PosisionTextBox.Text,
                        HomePhone = PhoneHomeTextBox.Text,
                        MobilePhone = PhoneTextBox.Text,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.ContactPersonInfoBL().UpdateContactInfo(newItem);
                    DataCorrectorStatic<ContactPersonInfo>.AddTransaction("U", "Person Contact", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Contact_Window window = new Contact_Window();
                //window.PersonIDTextBox.Text = PersonID;
                //window.ShowDialog();
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(PersonID)) throw new Exception("กรุณาระบุ Person ID");
                    if (string.IsNullOrEmpty(ContactIDTextBox.Text)) throw new Exception("กรุณาระบุ Contact ID");
                    if (string.IsNullOrEmpty(TitlenameCombobox.Text) || string.IsNullOrEmpty(FirstnameTextBox.Text) || string.IsNullOrEmpty(LastnameTextBox.Text)) throw new Exception("กรุณาระบุชื่อ-นามสกุล");
                    if (string.IsNullOrEmpty(RelationshipTextBox.Text)) throw new Exception("กรุณาระบุ Relationship");

                    var newItem = new ContactPersonInfo
                    {
                        ContactPersonInfo_ID = ContactIDTextBox.Text,
                        Person_ID = PersonID,
                        Relation = RelationshipTextBox.Text,
                        Title_ID = TitlenameCombobox.SelectedValue.ToString(),
                        FirstName = FirstnameTextBox.Text,
                        LastName = LastnameTextBox.Text,
                        Occupation = OccupyTextBox.Text,
                        CompanyName = WorkplaceTextBox.Text,
                        PositionName = PosisionTextBox.Text,
                        HomePhone = PhoneHomeTextBox.Text,
                        MobilePhone = PhoneTextBox.Text,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.ContactPersonInfoBL().AddContactInfo(newItem);
                    DataCorrectorStatic<ContactPersonInfo>.AddTransaction("A", "Person Contact", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var oldItem = (ContactPersonInfo)ContactDataGrid.SelectedItem;
                    BusinessLayerServices.ContactPersonInfoBL().DeleteContactInfo(oldItem);
                    DataCorrectorStatic<ContactPersonInfo>.AddTransaction("D", "Person Contact", oldItem, null);
                    MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearControl();
        }
    }
}
