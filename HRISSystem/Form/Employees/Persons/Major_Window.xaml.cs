﻿using System;
using System.Windows;
using System.Windows.Input;
using DomainModelHris;
using HRISSystemBL;
using HRISSystem.Shared;

namespace HRISSystem.Form.Employees.Persons
{
    /// <summary>
    /// Interaction logic for Major_Window.xaml
    /// </summary>
    public partial class Major_Window : Window
    {
        public Major_Window()
        {
            InitializeComponent();
            ReloadDatagrid();
            EduQualificationCombobox.ItemsSource = BusinessLayerServices.EducationBL().GetAllEduQualificationList();
        }

        private void ReloadDatagrid()
        {
            try
            {
                ClearControl();
                MajorDataGrid.ItemsSource = null;
                MajorDataGrid.ItemsSource = BusinessLayerServices.EducationBL().GetAllMajorList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddEduQualificationButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                EduQualification_Window w = new EduQualification_Window();
                w.ShowDialog();
                EduQualificationCombobox.ItemsSource = null;
                EduQualificationCombobox.ItemsSource = BusinessLayerServices.EducationBL().GetAllEduQualificationList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RelationshipDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BindSelectedData();
        }

        private void EditDetailButton_Click(object sender, RoutedEventArgs e)
        {
            BindSelectedData();
        }

        private void BindSelectedData()
        {
            try
            {
                var selected = (Major)MajorDataGrid.SelectedItem;
                if (selected != null)
                {
                    MajorIDTextBox.Text = selected.Major_ID;
                    MajorIDTextBox.IsEnabled = false;
                    EduQualificationCombobox.SelectedValue = selected.EducationQualification_ID;
                    MajorTHTextBox.Text = selected.MajorTH;
                    MajorENTextBox.Text = selected.MajorEN;
                    MajorInitialTHTextBox.Text = selected.MajorInitialTH;
                    MajorInitialENTextBox.Text = selected.MajorInitialEN;
                }
                AddButton.IsEnabled = false;
                UpdateButton.IsEnabled = selected != null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(MajorIDTextBox.Text)) throw new Exception("กรุณาระบุ ID");
                    if (string.IsNullOrEmpty(EduQualificationCombobox.Text)) throw new Exception("กรุณาระบุวุฒิการศึกษา");
                    if (string.IsNullOrEmpty(MajorTHTextBox.Text)) throw new Exception("กรุณาระบุสาขาวิชา");

                    var oldItem = BusinessLayerServices.EducationBL().GetSingleMajor(MajorIDTextBox.Text);
                    var newItem = new Major
                    {
                        Major_ID = oldItem.Major_ID,
                        EducationQualification_ID = EduQualificationCombobox.SelectedValue.ToString(),
                        MajorTH = MajorTHTextBox.Text,
                        MajorEN = MajorENTextBox.Text,
                        MajorInitialTH = MajorInitialTHTextBox.Text,
                        MajorInitialEN = MajorInitialENTextBox.Text,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.EducationBL().UpdateEduMajor(newItem);
                    DataCorrectorStatic<Major>.AddTransaction("U", "Education Setting", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var oldItem = (Major)MajorDataGrid.SelectedItem;
                if (oldItem != null)
                {
                    if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        BusinessLayerServices.EducationBL().DeleteEduMajor(oldItem);
                        DataCorrectorStatic<Major>.AddTransaction("D", "Education Setting", oldItem, null);
                        MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        ReloadDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearControl()
        {
            try
            {
                MajorIDTextBox.Text = "";
                MajorIDTextBox.IsEnabled = true;
                EduQualificationCombobox.Text = "";
                MajorTHTextBox.Text = "";
                MajorENTextBox.Text = "";
                MajorInitialTHTextBox.Text = "";
                MajorInitialENTextBox.Text = "";
                AddButton.IsEnabled = true;
                UpdateButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearControl();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {

                    if (string.IsNullOrEmpty(MajorIDTextBox.Text)) throw new Exception("กรุณาระบุ ID");
                    if (string.IsNullOrEmpty(EduQualificationCombobox.Text)) throw new Exception("กรุณาระบุวุฒิการศึกษา");
                    if (string.IsNullOrEmpty(MajorTHTextBox.Text)) throw new Exception("กรุณาระบุสาขาวิชา");

                    var newItem = new Major
                    {
                        Major_ID = MajorIDTextBox.Text,
                        EducationQualification_ID = EduQualificationCombobox.SelectedValue.ToString(),
                        MajorTH = MajorTHTextBox.Text,
                        MajorEN = MajorENTextBox.Text,
                        MajorInitialTH = MajorInitialTHTextBox.Text,
                        MajorInitialEN = MajorInitialENTextBox.Text,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.EducationBL().AddEduMajor(newItem);
                    DataCorrectorStatic<Major>.AddTransaction("U", "Education Setting", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
