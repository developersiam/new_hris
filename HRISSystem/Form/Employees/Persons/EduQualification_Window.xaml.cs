﻿using System;
using System.Windows;
using System.Windows.Input;
using DomainModelHris;
using HRISSystemBL;
using HRISSystem.Shared;

namespace HRISSystem.Form.Employees.Persons
{
    /// <summary>
    /// Interaction logic for EduQualification_Window.xaml
    /// </summary>
    public partial class EduQualification_Window : Window
    {
        public EduQualification_Window()
        {
            InitializeComponent();
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                ClearControl();
                EducationQualificationDataGrid.ItemsSource = null;
                EducationQualificationDataGrid.ItemsSource = BusinessLayerServices.EducationBL().GetAllEduQualificationList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RelationshipDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BindSelectedData();
        }

        private void EditDetailButton_Click(object sender, RoutedEventArgs e)
        {
            BindSelectedData();
        }

        private void BindSelectedData()
        {
            try
            {
                var selected = (EducationQualification)EducationQualificationDataGrid.SelectedItem;
                if (selected != null)
                {
                    EducationQualificationIDTextBox.Text = selected.EducationQualification_ID;
                    EducationQualificationIDTextBox.IsEnabled = false;
                    EducationQualificaionTHTextBox.Text = selected.EducationQualificaionTH;
                    EducationQualificationENTextBox.Text = selected.EducationQualificationEN;
                    EducationQualificationInitialTHTextBox.Text = selected.EducationQualificationInitialTH;
                    EducationQualificationInitialENTextBox.Text = selected.EducationQualificationInitialEN;
                }
                AddButton.IsEnabled = false;
                UpdateButton.IsEnabled = selected != null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(EducationQualificationIDTextBox.Text)) throw new Exception("กรุณาระบุ ID");
                    if (string.IsNullOrEmpty(EducationQualificaionTHTextBox.Text)) throw new Exception("กรุณาระบุชื่อวุฒิการศึกษา");

                    var oldItem = BusinessLayerServices.EducationBL().GetSingleEducationQualification(EducationQualificationIDTextBox.Text);
                    var newItem = new EducationQualification
                    {
                        EducationQualification_ID = oldItem.EducationQualification_ID,
                        EducationQualificaionTH = EducationQualificaionTHTextBox.Text,
                        EducationQualificationEN = EducationQualificationENTextBox.Text,
                        EducationQualificationInitialTH = EducationQualificationInitialTHTextBox.Text,
                        EducationQualificationInitialEN = EducationQualificationInitialENTextBox.Text,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.EducationBL().UpdateEduQualification(newItem);
                    DataCorrectorStatic<EducationQualification>.AddTransaction("U", "Education Setting", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var oldItem = (EducationQualification)EducationQualificationDataGrid.SelectedItem;
                if (oldItem != null)
                {
                    if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        BusinessLayerServices.EducationBL().DeleteEduQualification(oldItem);
                        DataCorrectorStatic<EducationQualification>.AddTransaction("D", "Education Setting", oldItem, null);
                        MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        ReloadDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearControl()
        {
            try
            {
                EducationQualificationIDTextBox.Text = "";
                EducationQualificationIDTextBox.IsEnabled = true;
                EducationQualificaionTHTextBox.Text = "";
                EducationQualificationENTextBox.Text = "";
                EducationQualificationInitialTHTextBox.Text = "";
                EducationQualificationInitialENTextBox.Text = "";
                AddButton.IsEnabled = true;
                UpdateButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearControl();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(EducationQualificationIDTextBox.Text)) throw new NullReferenceException();
                    if (string.IsNullOrEmpty(EducationQualificaionTHTextBox.Text)) throw new NullReferenceException();

                    var newItem = new EducationQualification
                    {
                        EducationQualification_ID = EducationQualificationIDTextBox.Text,
                        EducationQualificaionTH = EducationQualificaionTHTextBox.Text,
                        EducationQualificationEN = EducationQualificationENTextBox.Text,
                        EducationQualificationInitialTH = EducationQualificationInitialTHTextBox.Text,
                        EducationQualificationInitialEN = EducationQualificationInitialENTextBox.Text,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.EducationBL().AddEduQualification(newItem);
                    DataCorrectorStatic<EducationQualification>.AddTransaction("A", "Education Setting", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
