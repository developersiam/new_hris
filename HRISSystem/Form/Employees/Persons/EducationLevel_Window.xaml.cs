﻿using System;
using System.Windows;
using System.Windows.Input;
using DomainModelHris;
using HRISSystemBL;
using HRISSystem.Shared;

namespace HRISSystem.Form.Employees.Persons
{
    /// <summary>
    /// Interaction logic for EducationLevel_Window.xaml
    /// </summary>
    public partial class EducationLevel_Window : Window
    {
        public EducationLevel_Window()
        {
            InitializeComponent();
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                EdulevelDataGrid.ItemsSource = null;
                EdulevelDataGrid.ItemsSource = BusinessLayerServices.EducationBL().GetAllEduLevelList();
                ClearControl();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearControl();
        }

        private void ClearControl()
        {
            try
            {
                EduLevelIDTextBox.Text = "";
                EduLevelIDTextBox.IsEnabled = true;
                InitialTHTextBox.Text = "";
                InitialENTextBox.Text = "";
                EducationLevelTHTextBox.Text = "";
                EducationLevelENTextBox.Text = "";
                AddButton.IsEnabled = true;
                UpdateButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(EduLevelIDTextBox.Text)) throw new Exception("กรุณาระบุ ID");
                    if (string.IsNullOrEmpty(EducationLevelTHTextBox.Text)) throw new Exception("กรุณาระบุชื่อระดับการศึกษา");

                    var newItem = new EducationLevel
                    {
                        EducationLevel_ID = EduLevelIDTextBox.Text,
                        EducationLevelInitialTH = InitialTHTextBox.Text,
                        EducationLevelInitialEN = InitialENTextBox.Text,
                        EducationLevelTH = EducationLevelTHTextBox.Text,
                        EducationLevelEN = EducationLevelENTextBox.Text,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.EducationBL().AddEduLevel(newItem);
                    DataCorrectorStatic<EducationLevel>.AddTransaction("A", "Education Setting", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var oldItem = (EducationLevel)EdulevelDataGrid.SelectedItem;
                if (oldItem != null)
                {
                    if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        BusinessLayerServices.EducationBL().DeleteEduLevel(oldItem);
                        DataCorrectorStatic<EducationLevel>.AddTransaction("D", "Education Setting", oldItem, null);
                        MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        ReloadDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditDetailButton_Click(object sender, RoutedEventArgs e)
        {
            BindSelectedData();
        }

        private void RelationshipDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BindSelectedData();
        }

        private void BindSelectedData()
        {
            try
            {
                var selected = (EducationLevel)EdulevelDataGrid.SelectedItem;
                if (selected != null)
                {
                    EduLevelIDTextBox.Text = selected.EducationLevel_ID;
                    EduLevelIDTextBox.IsEnabled = false;
                    InitialTHTextBox.Text = selected.EducationLevelInitialTH;
                    InitialENTextBox.Text = selected.EducationLevelInitialEN;
                    EducationLevelTHTextBox.Text = selected.EducationLevelTH;
                    EducationLevelENTextBox.Text = selected.EducationLevelEN;
                }
                AddButton.IsEnabled = false;
                UpdateButton.IsEnabled = selected != null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(EduLevelIDTextBox.Text)) throw new Exception("กรุณาระบุ ID");
                    if (string.IsNullOrEmpty(EducationLevelTHTextBox.Text)) throw new Exception("กรุณาระบุชื่อระดับการศึกษา");

                    var oldItem = BusinessLayerServices.EducationBL().GetSingleEducationLevel(EduLevelIDTextBox.Text);
                    var newItem = new EducationLevel
                    {
                        EducationLevel_ID = oldItem.EducationLevel_ID,
                        EducationLevelInitialTH = InitialTHTextBox.Text,
                        EducationLevelInitialEN = InitialENTextBox.Text,
                        EducationLevelTH = EducationLevelTHTextBox.Text,
                        EducationLevelEN = EducationLevelENTextBox.Text,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.EducationBL().UpdateEduLevel(newItem);
                    DataCorrectorStatic<EducationLevel>.AddTransaction("U", "Education Setting", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
