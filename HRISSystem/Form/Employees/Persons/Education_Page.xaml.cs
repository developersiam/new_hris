﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DomainModelHris;
using HRISSystemBL;
using HRISSystem.Shared;

namespace HRISSystem.Form.Employees.Persons
{
    /// <summary>
    /// Interaction logic for Education_Page.xaml
    /// </summary>
    public partial class Education_Page : Page
    {
        public Education_Page()
        {
            InitializeComponent();
        }

        string PersonID;
        public Education_Page(string personID)
        {
            InitializeComponent();
            PersonID = personID;
            AddButton.IsEnabled = !string.IsNullOrEmpty(PersonID);
            if (string.IsNullOrEmpty(PersonID)) return;
            ReloadDatagrid();
            BindCombobox();
        }

        private void BindCombobox()
        {
            try
            {
                EduLevelCombobox.ItemsSource = BusinessLayerServices.EducationBL().GetAllEduLevelList();
                InstitueCombobox.ItemsSource = BusinessLayerServices.EducationBL().GetAllInstituteList();
                EduQualificationCombobox.ItemsSource = BusinessLayerServices.EducationBL().GetAllEduQualificationList();
                MajorCombobox.ItemsSource = BusinessLayerServices.EducationBL().GetAllMajorList();
                CountryCombobox.ItemsSource = BusinessLayerServices.AddressBL().GetAllCountry();
                CountryCombobox.SelectedValue = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadDatagrid()
        {
            try
            {
                Clear();
                EducationDataGrid.ItemsSource = null;
                EducationDataGrid.ItemsSource = BusinessLayerServices.EducationBL().GetEducationListByPersonID(PersonID);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void GPATextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            GPATextBox.Text = HRISService.DataCorrector().FormatNumber(2, GPATextBox.Text, false);
        }

        private void AddEduLevelButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                EducationLevel_Window w = new EducationLevel_Window();
                w.ShowDialog();
                EduLevelCombobox.ItemsSource = null;
                EduLevelCombobox.ItemsSource = BusinessLayerServices.EducationBL().GetAllEduLevelList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddInstitueButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Institue_Window w = new Institue_Window();
                w.ShowDialog();
                InstitueCombobox.ItemsSource = null;
                InstitueCombobox.ItemsSource = BusinessLayerServices.EducationBL().GetAllInstituteList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddEduQualificationButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                EduQualification_Window w = new EduQualification_Window();
                w.ShowDialog();
                EduQualificationCombobox.ItemsSource = null;
                EduQualificationCombobox.ItemsSource = BusinessLayerServices.EducationBL().GetAllEduQualificationList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddMajorButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Major_Window w = new Major_Window();
                w.ShowDialog();
                MajorCombobox.ItemsSource = null;
                MajorCombobox.ItemsSource = BusinessLayerServices.EducationBL().GetAllMajorList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditDetailButton_Click(object sender, RoutedEventArgs e)
        {
            BindSelectedControl();
        }

        private void EducationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BindSelectedControl();
        }

        private void BindSelectedControl()
        {
            try
            {
                var selected = (Education)EducationDataGrid.SelectedItem;
                if (selected != null)
                {
                    EduLevelCombobox.SelectedValue = selected.EducationLevel_ID;
                    EduLevelCombobox.IsEnabled = false;
                    InstitueCombobox.SelectedValue = selected.Institue_ID;
                    CountryCombobox.SelectedValue = selected.Country_ID;
                    EduQualificationCombobox.SelectedValue = selected.EducationQualification_ID;
                    EduQualificationCombobox.IsEnabled = false;
                    MajorCombobox.SelectedValue = selected.Major_ID;
                    AdmissionDateDatePicker.SelectedDate = selected.AdmissionDate;
                    GraduatedDateDatePicker.SelectedDate = selected.GraduatedDate;
                    GPATextBox.Text = selected.GPA.ToString();
                    AddButton.IsEnabled = false;
                }
                UpdateButton.IsEnabled = selected != null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(PersonID)) throw new Exception("กรุณาระบุ Person ID");
                    if (string.IsNullOrEmpty(EduLevelCombobox.Text)) throw new Exception("กรุณาระบุ Education Level");
                    if (string.IsNullOrEmpty(InstitueCombobox.Text)) throw new Exception("กรุณาระบุ Institute");
                    if (string.IsNullOrEmpty(CountryCombobox.Text)) throw new Exception("กรุณาระบุ Country");
                    if (string.IsNullOrEmpty(EduQualificationCombobox.Text)) throw new Exception("กรุณาระบุ Education Qualification");
                    if (string.IsNullOrEmpty(MajorCombobox.Text)) throw new Exception("กรุณาระบุ Major");

                    var oldItem = BusinessLayerServices.EducationBL().GetSingleEducation(PersonID, EduLevelCombobox.SelectedValue.ToString(), EduQualificationCombobox.SelectedValue.ToString());
                    var newItem = new Education
                    {
                        EducationLevel_ID = oldItem.EducationLevel_ID,
                        Person_ID = oldItem.Person_ID,
                        EducationQualification_ID = oldItem.EducationQualification_ID,
                        Institue_ID = InstitueCombobox.SelectedValue.ToString(),
                        Country_ID = CountryCombobox.SelectedValue.ToString(),
                        AdmissionDate = AdmissionDateDatePicker.SelectedDate,
                        GraduatedDate = GraduatedDateDatePicker.SelectedDate,
                        GPA = string.IsNullOrEmpty(GPATextBox.Text) ? (decimal?)null : decimal.Parse(GPATextBox.Text),
                        Major_ID = MajorCombobox.SelectedValue.ToString(),
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.EducationBL().UpdateEducation(newItem);
                    DataCorrectorStatic<Education>.AddTransaction("U", "Person Education", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Education_Window w = new Education_Window();
                //w.PersonIDTextBox.Text = PersonID;
                //w.ShowDialog();
                //ReloadDatagrid();

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(PersonID)) throw new Exception("กรุณาระบุ Person ID");
                    if (string.IsNullOrEmpty(EduLevelCombobox.Text)) throw new Exception("กรุณาระบุ Education Level");
                    if (string.IsNullOrEmpty(InstitueCombobox.Text)) throw new Exception("กรุณาระบุ Institute");
                    if (string.IsNullOrEmpty(CountryCombobox.Text)) throw new Exception("กรุณาระบุ Country");
                    if (string.IsNullOrEmpty(EduQualificationCombobox.Text)) throw new Exception("กรุณาระบุ Education Qualification");
                    if (string.IsNullOrEmpty(MajorCombobox.Text)) throw new Exception("กรุณาระบุ Major");

                    var newItem = new Education
                    {
                        Person_ID = PersonID,
                        EducationLevel_ID = EduLevelCombobox.SelectedValue.ToString(),
                        Institue_ID = InstitueCombobox.SelectedValue.ToString(),
                        Country_ID = CountryCombobox.SelectedValue.ToString(),
                        EducationQualification_ID = EduQualificationCombobox.SelectedValue.ToString(),
                        Major_ID = MajorCombobox.SelectedValue.ToString(),
                        AdmissionDate = AdmissionDateDatePicker.SelectedDate,
                        GraduatedDate = GraduatedDateDatePicker.SelectedDate,
                        GPA = string.IsNullOrEmpty(GPATextBox.Text) ? (decimal?)null : decimal.Parse(GPATextBox.Text),
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.EducationBL().AddEducation(newItem);
                    DataCorrectorStatic<Education>.AddTransaction("A", "Person Education", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var oldItem = (Education)EducationDataGrid.SelectedItem;
                if (oldItem != null)
                {
                    if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        BusinessLayerServices.EducationBL().DeleteEducation(oldItem);
                        DataCorrectorStatic<Education>.AddTransaction("D", "Person Education", oldItem, null);
                        MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        ReloadDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Clear()
        {
            try
            {
                EduLevelCombobox.SelectedIndex = -1;
                EduLevelCombobox.IsEnabled = true;
                InstitueCombobox.SelectedIndex = -1;
                CountryCombobox.SelectedIndex = -1;
                EduQualificationCombobox.SelectedIndex = -1;
                EduQualificationCombobox.IsEnabled = true;
                MajorCombobox.SelectedIndex = -1;
                AdmissionDateDatePicker.SelectedDate = null;
                GraduatedDateDatePicker.SelectedDate = null;
                GPATextBox.Text = "";
                UpdateButton.IsEnabled = false;
                AddButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }
    }
}
