﻿using System;
using System.Windows;
using System.Windows.Controls;
using HRISSystemBL;
using DomainModelHris;
using HRISSystem.Shared;
using System.Windows.Input;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;


namespace HRISSystem.Form.Employees.Persons
{
    /// <summary>
    /// Interaction logic for Person_Page.xaml
    /// </summary>
    public partial class Person_Page : Page
    {
        //public EmployeeInformationViewModel selectedEmployee;

        string username = SingletonConfiguration.getInstance().Username.ToLower();
        string deptCode = SingletonConfiguration.getInstance().DepartmentCode;
        public Person_Page()
        {
            InitializeComponent();
        }

        private void BindCombobox()
        {
            if (TitleCombobox.Items.Count == 0) TitleCombobox.ItemsSource = BusinessLayerServices.PersonBL().GetAllTitlenameList();
            if (BloodCombobox.Items.Count == 0) BloodCombobox.ItemsSource = BusinessLayerServices.PersonBL().GetAllBloodList();
            if (GenderCombobox.Items.Count == 0) GenderCombobox.ItemsSource = BusinessLayerServices.PersonBL().GetAllGenderList();
            if (MaritalStatusCombobox.Items.Count == 0) MaritalStatusCombobox.ItemsSource = BusinessLayerServices.PersonBL().GetAllMaritalStatusList();
            if (ReligionCombobox.Items.Count == 0) ReligionCombobox.ItemsSource = BusinessLayerServices.PersonBL().GetAllReligionList();
            if (NationalityCombobox.Items.Count == 0) NationalityCombobox.ItemsSource = BusinessLayerServices.PersonBL().GetAllNationalityList();
            if (RaceCombobox.Items.Count == 0) RaceCombobox.ItemsSource = BusinessLayerServices.PersonBL().GetAllRaceList();
        }

        private void PersonTabcontrol_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var item = sender as TabControl;
                var selected = item.SelectedItem as TabItem;

                int TcPersonTabIndex = PersonTabcontrol.SelectedIndex;
                if ((sender as TabControl) != null)
                {
                    if ((sender as TabControl).SelectedIndex == 0) //ข้อมูลการศึกษา (Education)
                    {
                        PersonFrame.Navigate(new Education_Page(PersonIDTextBox.Text));
                    }
                    else if ((sender as TabControl).SelectedIndex == 1) //ข้อมูลบัตรประชาชน (ID Card)
                    {
                        PersonFrame.Navigate(new IDCard_Page(PersonIDTextBox.Text));
                    }
                    else if ((sender as TabControl).SelectedIndex == 2) //ที่อยู่ (Address)
                    {
                        PersonFrame.Navigate(new Address_Page(PersonIDTextBox.Text));
                    }
                    else if ((sender as TabControl).SelectedIndex == 3) //ข้อมูลบุคคลที่สามารถติดต่อได้ (Contact Person)
                    {
                        PersonFrame.Navigate(new Contact_Page(PersonIDTextBox.Text));
                    }
                    else if ((sender as TabControl).SelectedIndex == 4) //ข้อมูลประวัติบุคคลในครอบครัว (Family)
                    {
                        PersonFrame.Navigate(new Family_Page(PersonIDTextBox.Text));
                    }
                    else if ((sender as TabControl).SelectedIndex == 5) //ประวัติการทำงานที่ STEC (Working History)
                    {
                        PersonFrame.Navigate(new WorkingHistory_Page(PersonIDTextBox.Text));
                    }
                    else if ((sender as TabControl).SelectedIndex == 6) //ข้อมูลประวัติการทำงานจากบริษัทฯอื่น (Working History)
                    {
                        PersonFrame.Navigate(new WorkingHistory2_Page(PersonIDTextBox.Text));
                    }
                    else if ((sender as TabControl).SelectedIndex == 7) //Userinfo
                    {
                        PersonFrame.Navigate(new Userinfo_Page(PersonIDTextBox.Text));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void PersonIDTextBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left) Search();
        }

        private void Search()
        {
            try
            {
                SearchEmployee_Window window = new SearchEmployee_Window();
                window.ShowDialog();

                if (window.selectedEmployee.Staff_type == 1 && username == "m.tanainan")
                {
                    MessageBox.Show("Permission required for staff type.", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (window.selectedEmployee != null) BindSelectedPerson(window.selectedEmployee.Person_ID);
                UpdateButton.IsEnabled = !string.IsNullOrEmpty(PersonIDTextBox.Text) || window.selectedEmployee != null;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BindSelectedPerson(string PersonID)
        {
            try
            {
                BindCombobox();
                var item = BusinessLayerServices.PersonBL().GetPersonByPersonID(PersonID);

                PersonIDTextBox.Text = item.Person_ID;
                TitleCombobox.SelectedValue = item.TitleName_ID;
                NameTHTextBox.Text = item.FirstNameTH;
                LastnameTHTextBox.Text = item.LastNameTH;
                NameENTextBox.Text = item.FirstNameEN;
                LastnameENTextBox.Text = item.LastNameEN;
                EmailTextBox.Text = item.Email;
                DOBDatePicker.SelectedDate = item.DateOfBirth;
                WeightTextBox.Text = item.Weight == null ? "" : item.Weight.Value.ToString();
                HeightTextBox.Text = item.Height == null ? "" : item.Height.Value.ToString();
                BloodCombobox.SelectedValue = item.Blood_ID;
                GenderCombobox.SelectedValue = item.Gender_ID;
                MaritalStatusCombobox.SelectedValue = item.MaritalStatus_ID;
                ReligionCombobox.SelectedValue = item.Religion_ID;
                NationalityCombobox.SelectedValue = item.Nationality_ID;
                RaceCombobox.SelectedValue = item.Race_ID;
                PersonTabcontrol.SelectedIndex = 0; //Navigate to first tab
                PersonFrame.Navigate(new Education_Page(PersonIDTextBox.Text)); //Refresh first page
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PersonIDTextBox.Text = "";
                TitleCombobox.SelectedValue = -1;
                NameTHTextBox.Text = "";
                LastnameTHTextBox.Text = "";
                NameENTextBox.Text = "";
                LastnameENTextBox.Text = "";
                EmailTextBox.Text = "";
                DOBDatePicker.SelectedDate = null;
                WeightTextBox.Text = "";
                HeightTextBox.Text = "";
                BloodCombobox.SelectedValue = -1;
                GenderCombobox.SelectedValue = -1;
                MaritalStatusCombobox.SelectedValue = -1;
                ReligionCombobox.SelectedValue = -1;
                NationalityCombobox.SelectedValue = -1;
                RaceCombobox.SelectedValue = -1;
                PersonTabcontrol.SelectedIndex = 0; //Navigate to first tab
                PersonFrame.Navigate(new Education_Page(PersonIDTextBox.Text)); //Refresh first page
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Person_Window window = new Person_Window();
                window.ShowDialog();
                if (string.IsNullOrEmpty(window.recentlyAddPersonID)) return;
                BindSelectedPerson(window.recentlyAddPersonID);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void HeightTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            HeightTextBox.Text = HRISService.DataCorrector().FormatNumber(1, HeightTextBox.Text, true);
        }

        private void WeightTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            WeightTextBox.Text = HRISService.DataCorrector().FormatNumber(1, WeightTextBox.Text, true);
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(PersonIDTextBox.Text)) throw new Exception("กรุณาระบุ Person ID");
                    if (string.IsNullOrEmpty(TitleCombobox.Text)) throw new Exception("กรุณาระบุ Titile Name");
                    if (string.IsNullOrEmpty(NameTHTextBox.Text) || string.IsNullOrEmpty(LastnameTHTextBox.Text)) throw new Exception("กรุณาระบุชื่อนามสกุลของพนักงาน");
                    if (string.IsNullOrEmpty(NameENTextBox.Text) || string.IsNullOrEmpty(LastnameENTextBox.Text)) throw new Exception("กรุณาระบุชื่อนามสกุลของพนักงาน");
                    if (string.IsNullOrEmpty(BloodCombobox.Text)) throw new Exception("กรุณาระบุกรุ๊ปเลือด");
                    if (string.IsNullOrEmpty(GenderCombobox.Text)) throw new Exception("กรุณาระบุเพศ");
                    if (string.IsNullOrEmpty(MaritalStatusCombobox.Text)) throw new Exception("กรุณาระบุสถานะสมรส");
                    if (string.IsNullOrEmpty(NationalityCombobox.Text)) throw new Exception("กรุณาระบุสัญชาติ");
                    if (string.IsNullOrEmpty(RaceCombobox.Text)) throw new Exception("กรุณาระบุเชื้อชาติ");
                    if (string.IsNullOrEmpty(ReligionCombobox.Text)) throw new Exception("กรุณาระบุศาสนา");

                    var oldItem = BusinessLayerServices.PersonBL().GetPersonByPersonID(PersonIDTextBox.Text);
                    var newItem = new Person
                    {
                        Person_ID = oldItem.Person_ID,
                        TitleName_ID = TitleCombobox.SelectedValue.ToString(),
                        FirstNameTH = NameTHTextBox.Text,
                        LastNameTH = LastnameTHTextBox.Text,
                        FirstNameEN = NameENTextBox.Text,
                        LastNameEN = LastnameENTextBox.Text,
                        Email = EmailTextBox.Text,
                        DateOfBirth = DOBDatePicker.SelectedDate,
                        Height = string.IsNullOrEmpty(HeightTextBox.Text) ? (decimal?)null : decimal.Parse(HeightTextBox.Text),
                        Weight = string.IsNullOrEmpty(WeightTextBox.Text) ? (decimal?)null : decimal.Parse(WeightTextBox.Text),
                        Blood_ID = BloodCombobox.SelectedValue.ToString(),
                        Gender_ID = GenderCombobox.SelectedValue.ToString(),
                        MaritalStatus_ID = MaritalStatusCombobox.SelectedValue.ToString(),
                        Nationality_ID = NationalityCombobox.SelectedValue.ToString(),
                        Race_ID = RaceCombobox.SelectedValue.ToString(),
                        Religion_ID = ReligionCombobox.SelectedValue.ToString()
                    };
                    BusinessLayerServices.PersonBL().UpdatePerson(newItem);
                    DataCorrectorStatic<Person>.AddTransaction("U", "Person Setting", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show( ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void NationalityCombobox_DropDownOpened(object sender, EventArgs e)
        {
            if (NationalityCombobox.Items.Count == 0) return;
            var selected = (Nationality)NationalityCombobox.SelectedItem;
            if (selected == null)
            {
                NationalityCombobox.SelectedValue = "TH"; 
            }
        }

        private void RaceCombobox_DropDownOpened(object sender, EventArgs e)
        {
            if (RaceCombobox.Items.Count == 0) return;
            var selected = (Race)RaceCombobox.SelectedItem;
            if (selected == null)
            {
                RaceCombobox.SelectedValue = "099";
            }
        }
    }
}
