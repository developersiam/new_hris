﻿using System;
using System.Windows;
using System.Windows.Controls;
using DomainModelHris;
using HRISSystemBL;
using HRISSystem.Shared;

namespace HRISSystem.Form.Employees.Persons
{
    /// <summary>
    /// Interaction logic for Address_Page.xaml
    /// </summary>
    public partial class Address_Page : Page
    {
        string PersonID;
        public Address_Page(string Person_ID)
        {
            InitializeComponent();
            PersonID = Person_ID;
            //AddButton.IsEnabled = !string.IsNullOrEmpty(PersonID);
            if (string.IsNullOrEmpty(PersonID)) return;
            ReloadDatagrid();
            BindControl();
        }

        private void ReloadDatagrid()
        {
            Clear();
            AddressDataGrid.ItemsSource = null;
            AddressDataGrid.ItemsSource = BusinessLayerServices.AddressBL().GetAddressListByPersonID(PersonID);
        }

        private void BindControl()
        {
            AddressTypeCombobox.ItemsSource = BusinessLayerServices.AddressBL().GetAllAddressType();
            CountryCombobox.ItemsSource = BusinessLayerServices.AddressBL().GetAllCountry();
            ProvinceCombobox.ItemsSource = BusinessLayerServices.AddressBL().GetAllProvince();
        }

        private void EditDetailButton_Click(object sender, RoutedEventArgs e)
        {
            BindSelectedControl();
        }

        private void AddressDataGrid_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            BindSelectedControl();
        }

        private void BindSelectedControl()
        {
            try
            {
                var selected = (Address)AddressDataGrid.SelectedItem;
                if (selected != null)
                {
                    AddressTypeCombobox.SelectedValue = selected.AddressType_ID;
                    AddressTypeCombobox.IsEnabled = false;
                    HouseNoTextBox.Text = selected.HomeNumber;
                    MooTextBox.Text = selected.Moo == null ? "" : selected.Moo.Value.ToString();
                    ProjectTextBox.Text = selected.HousingProject;
                    RoadTextBox.Text = selected.Street;
                    SoiTextBox.Text = selected.Soi;
                    ProvinceCombobox.SelectedValue = selected.SubDistrict.District.Province.Province_ID;
                    DistrictCombobox.ItemsSource = null;
                    DistrictCombobox.ItemsSource = BusinessLayerServices.AddressBL().GetDistrictListByProvinceID(ProvinceCombobox.SelectedValue.ToString());
                    DistrictCombobox.SelectedValue = selected.SubDistrict.District.District_ID;
                    SubDistrictCombobox.ItemsSource = null;
                    SubDistrictCombobox.ItemsSource = BusinessLayerServices.AddressBL().GetSubDistrictListByDistrictID(DistrictCombobox.SelectedValue.ToString());
                    SubDistrictCombobox.SelectedValue = selected.SubDistrict_ID;
                    CountryCombobox.SelectedValue = selected.Country_ID;
                    PostcodeTextBox.Text = selected.Postcode == null ? "" : selected.Postcode.Value.ToString();
                    HomePhoneNumberTextBox.Text = selected.HomePhoneNumber;
                    AddButton.IsEnabled = false;
                }
                UpdateButton.IsEnabled = selected != null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(AddressTypeCombobox.Text)) throw new Exception("กรุณาระบุประเภทที่อยู่");
                    if (string.IsNullOrEmpty(HouseNoTextBox.Text)) throw new Exception("กรุณาระบุบ้านเลขที่");
                    if (string.IsNullOrEmpty(ProvinceCombobox.Text)) throw new Exception("กรุณาระบุจังหวัด");
                    if (string.IsNullOrEmpty(DistrictCombobox.Text)) throw new Exception("กรุณาระบุอำเภอ/เขต");
                    if (string.IsNullOrEmpty(SubDistrictCombobox.Text)) throw new Exception("กรุณาระบุตำบล/แขวง");
                    if (string.IsNullOrEmpty(CountryCombobox.Text)) throw new Exception("กรุณาระบุประเทศ");
                    //if (string.IsNullOrEmpty(PostcodeTextBox.Text)) throw new Exception("กรุณาระบุรหัสไปรษณี");
                    if (HomePhoneNumberTextBox.Text.Length > 10) throw new Exception("กรุณาระบุรหัสเบอร์โทรศัพท์ให้ถูกต้อง");

                    var oldItem = BusinessLayerServices.AddressBL().GetSingleAddress(AddressTypeCombobox.SelectedValue.ToString(), PersonID);
                    var newItem = new Address
                    {
                        Person_ID = oldItem.Person_ID,
                        AddressType_ID = oldItem.AddressType_ID,
                        HomeNumber = HouseNoTextBox.Text,
                        Moo = string.IsNullOrEmpty(MooTextBox.Text) ? (int?)null : int.Parse(MooTextBox.Text),
                        HousingProject = ProjectTextBox.Text,
                        Street = RoadTextBox.Text,
                        Soi = SoiTextBox.Text,
                        SubDistrict_ID = SubDistrictCombobox.SelectedValue.ToString(),
                        Postcode = string.IsNullOrEmpty(PostcodeTextBox.Text) ? (int?)null : int.Parse(PostcodeTextBox.Text),
                        Country_ID = CountryCombobox.SelectedValue.ToString(),
                        HomePhoneNumber = HomePhoneNumberTextBox.Text
                    };
                    BusinessLayerServices.AddressBL().UpdateAddress(newItem);
                    DataCorrectorStatic<Address>.AddTransaction("U", "Person Address", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void MooTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            MooTextBox.Text = HRISService.DataCorrector().FormatNumber(0, MooTextBox.Text, true);
        }

        private void PostcodeTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            PostcodeTextBox.Text = HRISService.DataCorrector().FormatNumber(0, PostcodeTextBox.Text, true);
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            //Address_Window window = new Address_Window();
            //window.PersonIDTextBox.Text = PersonID;
            //window.ShowDialog();
            //ReloadDatagrid();
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(PersonID)) throw new Exception("กรุณาระบุ Person ID");
                    if (string.IsNullOrEmpty(AddressTypeCombobox.Text)) throw new Exception("กรุณาระบุประเภทที่อยู่");
                    if (string.IsNullOrEmpty(HouseNoTextBox.Text)) throw new Exception("กรุณาระบุบ้านเลขที่");
                    if (string.IsNullOrEmpty(ProvinceCombobox.Text)) throw new Exception("กรุณาระบุจังหวัด");
                    if (string.IsNullOrEmpty(DistrictCombobox.Text)) throw new Exception("กรุณาระบุอำเภอ/เขต");
                    if (string.IsNullOrEmpty(SubDistrictCombobox.Text)) throw new Exception("กรุณาระบุตำบล/แขวง");
                    if (string.IsNullOrEmpty(CountryCombobox.Text)) throw new Exception("กรุณาระบุประเทศ");
                    //if (string.IsNullOrEmpty(PostcodeTextBox.Text)) throw new Exception("กรุณาระบุรหัสไปรษณี");

                    var newItem = new Address
                    {
                        Person_ID = PersonID,
                        AddressType_ID = AddressTypeCombobox.SelectedValue.ToString(),
                        HomeNumber = HouseNoTextBox.Text,
                        Moo = string.IsNullOrEmpty(MooTextBox.Text) ? (int?)null : int.Parse(MooTextBox.Text),
                        HousingProject = ProjectTextBox.Text,
                        Street = RoadTextBox.Text,
                        Soi = SoiTextBox.Text,
                        SubDistrict_ID = SubDistrictCombobox.SelectedValue.ToString(),
                        Postcode = string.IsNullOrEmpty(PostcodeTextBox.Text) ? (int?)null : int.Parse(PostcodeTextBox.Text),
                        Country_ID = CountryCombobox.SelectedValue.ToString(),
                        HomePhoneNumber = HomePhoneNumberTextBox.Text
                    };
                    BusinessLayerServices.AddressBL().AddAddress(newItem);
                    DataCorrectorStatic<Address>.AddTransaction("A", "Person Address", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ProvinceCombobox_DropDownClosed(object sender, EventArgs e)
        {
            var province = (Province)ProvinceCombobox.SelectedItem;
            if (province == null) return;
            DistrictCombobox.ItemsSource = null;
            DistrictCombobox.ItemsSource = BusinessLayerServices.AddressBL().GetDistrictListByProvinceID(province.Province_ID);
        }

        private void DistrictCombobox_DropDownClosed(object sender, EventArgs e)
        {
            var district = (District)DistrictCombobox.SelectedItem;
            if (district == null) return;
            SubDistrictCombobox.ItemsSource = null;
            SubDistrictCombobox.ItemsSource = BusinessLayerServices.AddressBL().GetSubDistrictListByDistrictID(district.District_ID);
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var oldItem = (Address)AddressDataGrid.SelectedItem;
                    BusinessLayerServices.AddressBL().DeleteAddress(oldItem);
                    DataCorrectorStatic<Address>.AddTransaction("D", "Person Address", oldItem, null);
                    MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Clear()
        {
            try
            {
                AddressTypeCombobox.SelectedIndex = -1;
                AddressTypeCombobox.IsEnabled = true;
                HouseNoTextBox.Text = "";
                MooTextBox.Text = "";
                ProjectTextBox.Text = "";
                RoadTextBox.Text = "";
                SoiTextBox.Text = "";
                ProvinceCombobox.Text = "";
                DistrictCombobox.ItemsSource = null;
                DistrictCombobox.Text = "";
                SubDistrictCombobox.ItemsSource = null;
                SubDistrictCombobox.Text = "";
                CountryCombobox.Text = "";
                PostcodeTextBox.Text = "";
                HomePhoneNumberTextBox.Text = "";
                AddButton.IsEnabled = true;
                UpdateButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }
    }
}
