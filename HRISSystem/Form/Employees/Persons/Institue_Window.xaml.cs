﻿using System;
using System.Windows;
using System.Windows.Input;
using DomainModelHris;
using HRISSystemBL;
using HRISSystem.Shared;

namespace HRISSystem.Form.Employees.Persons
{
    /// <summary>
    /// Interaction logic for Institue_Window.xaml
    /// </summary>
    public partial class Institue_Window : Window
    {
        public Institue_Window()
        {
            InitializeComponent();
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                InstituteDataGrid.ItemsSource = null;
                InstituteDataGrid.ItemsSource = BusinessLayerServices.EducationBL().GetAllInstituteList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RelationshipDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BindSelectedData();
        }

        private void EditDetailButton_Click(object sender, RoutedEventArgs e)
        {
            BindSelectedData();
        }

        private void BindSelectedData()
        {
            try
            {
                var selected = (Institute)InstituteDataGrid.SelectedItem;
                if (selected != null)
                {
                    InstituteIDTextBox.Text = selected.Institute_ID;
                    InstituteIDTextBox.IsEnabled = false;
                    InstituteNameTHTextBox.Text = selected.InstituteNameTH;
                    InstituteNameENTextBox.Text = selected.InstituteNameEN;
                    InstituteNameIntitialTHTextBox.Text = selected.InstituteNameIntitialTH;
                    InstituteNameIntitialENTextBox.Text = selected.InstituteNameIntitialEN;
                }
                AddButton.IsEnabled = false;
                UpdateButton.IsEnabled = selected != null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(InstituteIDTextBox.Text)) throw new NullReferenceException();
                    if (string.IsNullOrEmpty(InstituteNameTHTextBox.Text)) throw new NullReferenceException();

                    var oldItem = BusinessLayerServices.EducationBL().GetSingleInstitute(InstituteIDTextBox.Text);
                    var newItem = new Institute
                    {
                        Institute_ID = oldItem.Institute_ID,
                        InstituteNameTH = InstituteNameTHTextBox.Text,
                        InstituteNameEN = InstituteNameENTextBox.Text,
                        InstituteNameIntitialTH = InstituteNameIntitialTHTextBox.Text,
                        InstituteNameIntitialEN = InstituteNameIntitialENTextBox.Text,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.EducationBL().UpdateInstitute(newItem);
                    DataCorrectorStatic<Institute>.AddTransaction("U", "Education Setting", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                    ClearControl();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var oldItem = (Institute)InstituteDataGrid.SelectedItem;
                if (oldItem != null)
                {
                    if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        BusinessLayerServices.EducationBL().DeleteInstitute(oldItem);
                        DataCorrectorStatic<Institute>.AddTransaction("D", "Education Setting", oldItem, null);
                        MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        ReloadDatagrid();
                        ClearControl();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearControl()
        {
            try
            {
                InstituteIDTextBox.Text = "";
                InstituteIDTextBox.IsEnabled = true;
                InstituteNameTHTextBox.Text = "";
                InstituteNameENTextBox.Text = "";
                InstituteNameIntitialTHTextBox.Text = "";
                InstituteNameIntitialENTextBox.Text = "";
                AddButton.IsEnabled = true;
                UpdateButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearControl();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(InstituteIDTextBox.Text)) throw new NullReferenceException();
                    if (string.IsNullOrEmpty(InstituteNameTHTextBox.Text)) throw new NullReferenceException();

                    var newItem = new Institute
                    {
                        Institute_ID = InstituteIDTextBox.Text,
                        InstituteNameTH = InstituteNameTHTextBox.Text,
                        InstituteNameEN = InstituteNameENTextBox.Text,
                        InstituteNameIntitialTH = InstituteNameIntitialTHTextBox.Text,
                        InstituteNameIntitialEN = InstituteNameIntitialENTextBox.Text,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.EducationBL().AddInstitute(newItem);
                    DataCorrectorStatic<Institute>.AddTransaction("A", "Education Setting", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                    ClearControl();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(SearchTextBox.Text)) return;
                InstituteDataGrid.ItemsSource = null;
                InstituteDataGrid.ItemsSource = BusinessLayerServices.EducationBL().FilterInstituteList(SearchTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
