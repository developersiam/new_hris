﻿using HRISSystemBL;
using DomainModelHris;
using DomainModelPayroll;
using DomainModelTimeStec;
using System;
using System.Windows;
using HRISSystem.Shared;
using System.Linq;
using HRISSystem.Form.Employees.Employees;

namespace HRISSystem.Form.Employees.Persons
{
    /// <summary>
    /// Interaction logic for Person_Window.xaml
    /// </summary>
    public partial class Person_Window : Window
    {
        //public EmployeeInformationViewModel selectedEmployee;
        public string recentlyAddPersonID;
        public Person_Window()
        {
            InitializeComponent();
            BindControl();
        }

        private void BindControl()
        {
            try
            {
                PersonIDTextBox.Text = BusinessLayerServices.PersonBL().GetNewPersonID();
                EmployeeIDTextBox.Text = BusinessLayerServices.EmployeeBL().GetNewEmplyeeID();

                //var newUserinfo = BusinessLayerServices.TimeStecBL().GetNewUserInfo();
                //FingerIDTextBox.Text = newUserinfo.badgenumber;
                //UserinfoIDTextBox.Text = newUserinfo.userid.ToString();

                DepartmentCombobox.ItemsSource = BusinessLayerServices.DeptCodeBL().GetAllDepartment();
                TitleCombobox.ItemsSource = BusinessLayerServices.PersonBL().GetAllTitlenameList();
                TitleCombobox.SelectedIndex = 0;
                StaffStatusCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffStatusList();
                StaffStatusCombobox.SelectedValue = 1; // Alway Choose 'ทำงาน' at first
                StaffTypeCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void StartDatePicker_CalendarClosed(object sender, RoutedEventArgs e)
        {
            if (StartDatePicker.SelectedDate == null) StartDatePicker.SelectedDate = DateTime.Now;
        }

        private void EndDatePicker_CalendarClosed(object sender, RoutedEventArgs e)
        {
            if (EndDatePicker.SelectedDate == null) EndDatePicker.SelectedDate = DateTime.MaxValue;
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (StartDatePicker.SelectedDate == null) StartDatePicker.SelectedDate = DateTime.Now;
                if (EndDatePicker.SelectedDate == null) EndDatePicker.SelectedDate = DateTime.MaxValue;
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(PersonIDTextBox.Text)) throw new Exception("กรุณาระบุ Person ID");
                    var userInfo = BusinessLayerServices.TimeStecBL().GetUserInfoByFingerID(FingerIDTextBox.Text);
                    if (userInfo == null) throw new Exception("ไม่พบรหัส Fingerscan ID นี้ในระบบ กรุณาตรวจสอบ");
                    if (string.IsNullOrEmpty(EmployeeIDTextBox.Text)) throw new Exception("กรุณาระบุ Employee ID");
                    var employee = BusinessLayerServices.EmployeeBL().GetAvailableEmployeeByFingerID(FingerIDTextBox.Text);
                    if (employee.Any()) throw new Exception("Fingerscan ID: " + FingerIDTextBox.Text + " ถูกใช้งานแล้ว" + Environment.NewLine +
                                                            "โดย: " + employee.FirstOrDefault().Person.FirstNameTH + " " + employee.FirstOrDefault().Person.LastNameTH);
                    if (string.IsNullOrEmpty(DepartmentCombobox.Text)) throw new Exception("กรุณาระบุ Department");
                    if (string.IsNullOrEmpty(TitleCombobox.Text)) throw new Exception("กรุณาระบุ Titile Name");
                    if (string.IsNullOrEmpty(NameTHTextBox.Text)) throw new Exception("กรุณาระบุ Firstname");
                    if (string.IsNullOrEmpty(LastnameTHTextBox.Text)) throw new Exception("กรุณาระบุ Lastname");
                    if (string.IsNullOrEmpty(DOBDatePicker.Text)) throw new Exception("กรุณาระบุ Birth date");
                    if (string.IsNullOrEmpty(StaffTypeCombobox.Text)) throw new Exception("กรุณาระบุ Staff Type");
                    if (string.IsNullOrEmpty(StaffStatusCombobox.Text)) throw new Exception("กรุณาระบุ Staff Status");
                    if (string.IsNullOrEmpty(PositionTextBox.Text)) throw new Exception("กรุณาระบุ Position");
                    
                    UserinfoIDTextBox.Text = userInfo.userid.ToString();

                    AddHrisPerson();
                    AddHrisEmployee();
                    AddPayrollEmployee();
                    AddUserInfo();

                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    recentlyAddPersonID = PersonIDTextBox.Text;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddHrisPerson()
        {
            var newTem = new Person
            {
                Person_ID = PersonIDTextBox.Text,
                TitleName_ID = TitleCombobox.SelectedValue.ToString(),
                FirstNameTH = NameTHTextBox.Text,
                LastNameTH = LastnameTHTextBox.Text,
                FirstNameEN = NameENTextBox.Text,
                LastNameEN = LastnameENTextBox.Text,
                DateOfBirth = DOBDatePicker.SelectedDate
            };
            BusinessLayerServices.PersonBL().AddPerson(newTem);
            //DataCorrectorStatic<Person>.AddTransaction("A", "Add Person", null, newTem);
        }

        private void AddHrisEmployee()
        {
            var newItem = new DomainModelHris.Employee
            {
                Employee_ID = EmployeeIDTextBox.Text,
                Person_ID = PersonIDTextBox.Text,
                DeptCode = DepartmentCombobox.SelectedValue.ToString(),
                FingerScanID = FingerIDTextBox.Text,
                StartDate = StartDatePicker.SelectedDate,
                EndDate = EndDatePicker.SelectedDate,
                Staff_status = ((StaffStatu)StaffStatusCombobox.SelectedItem).StaffStatusID,
                Staff_type = ((StaffType)StaffTypeCombobox.SelectedItem).StaffTypeID,
                Noted = EmployeeIDTextBox.Text,
                Position_ID = PositionIDTextBox.Text,
                CreatedDate = DateTime.Now
            };
            BusinessLayerServices.EmployeeBL().AddEmployee(newItem);
            //DataCorrectorStatic<DomainModelHris.Employee>.AddTransaction("A", "Add Person", null, newItem);
        }

        private void AddPayrollEmployee()
        {
            var newItem = new DomainModelPayroll.Employee
            {
                Employee_id = EmployeeIDTextBox.Text,
                EmployeeID = EmployeeIDTextBox.Text,
                Bank_acc = BankAccountTextBox.Text
            };
            BusinessLayerServices.PayrollBL().AddPayrollEmployee(newItem);
            //DataCorrectorStatic<DomainModelPayroll.Employee>.AddTransaction("A", "Add Person", null, newItem);
        }

        private void AddUserInfo()
        {
            //var oldItem = BusinessLayerServices.TimeStecBL().GetUserInfoByFingerID(FingerIDTextBox.Text);
            var newItem = BusinessLayerServices.TimeStecBL().GetUserInfoByFingerID(FingerIDTextBox.Text);
            if (CardTextBox.Text != "")
            {
                var existedCard = BusinessLayerServices.TimeStecBL().GetUserInfoByCard(CardTextBox.Text);
                if (existedCard != null && existedCard.userid != newItem.userid)
                {
                    if (MessageBox.Show("หมายเลขบัตรนี้ " + existedCard.name + " " + existedCard.lastname + " ใช้งานอยู่ ยืนยันที่จะทำการแทนบัตรหรือไม่?", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        existedCard.Card = "";
                        BusinessLayerServices.TimeStecBL().UpdateUserInfo(existedCard); // Clear card from previous user
                        newItem.Card = CardTextBox.Text; // Update card to new user
                    }
                    else CardTextBox.Text = "";
                }
                else newItem.Card = CardTextBox.Text;
            }
            
            newItem.create_operator = SingletonConfiguration.getInstance().Username;
            newItem.create_time = DateTime.Now;
            newItem.userid = int.Parse(UserinfoIDTextBox.Text);
            newItem.defaultdeptid = BusinessLayerServices.TimeStecBL().GetTimeStecDepartmentID(DepartmentCombobox.SelectedValue.ToString()).DeptID;
            newItem.name = NameTHTextBox.Text;
            newItem.lastname = LastnameTHTextBox.Text;
            newItem.Privilege = 0;
            newItem.AccGroup = 1;
            newItem.TimeZones = "0001000100000000";
            newItem.Gender = TitleCombobox.Text == "นาย" ? "M" : "F";
            newItem.Birthday = DOBDatePicker.SelectedDate.GetValueOrDefault().Date;
            newItem.identitycard = EmployeeIDTextBox.Text;
            newItem.Hiredday = DateTime.Now;
            newItem.ATT = false;
            newItem.OverTime = false;
            newItem.Lunchduration = 1;
            newItem.Holiday = false;
            newItem.INLATE = 0;
            newItem.OutEarly = 0;
            newItem.photo = "";
            newItem.SEP = 1;
            newItem.OffDuty = 0;
            newItem.DelTag = 0;
            newItem.AutoSchPlan = 0;
            newItem.MinAutoSchInterval = 24;
            newItem.RegisterOT = 1;
            newItem.set_valid_time = false;
            newItem.isatt = true;
            newItem.status = 0;
            BusinessLayerServices.TimeStecBL().UpdateUserInfo(newItem);
            //DataCorrectorStatic<userinfo>.AddTransaction("U", "Add Person", oldItem, newItem);
        }

        private void SearchPositionButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var dept = (Department)DepartmentCombobox.SelectedItem;
                if (dept == null) throw new Exception("กรุณาเลือกแผนก");

                var p = new PositionWindow(dept.DeptCode);
                p.ShowDialog();
                if (p.selected == null) return;
                PositionIDTextBox.Text = p.selected.Position_ID;
                PositionTextBox.Text = p.selected.PositionNameTH;
                PositionTextBox.ToolTip = p.selected.PositionNameTH;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DepartmentCombobox_DropDownClosed(object sender, EventArgs e)
        {
            PositionIDTextBox.Text = "";
            PositionTextBox.Text = "";
            PositionTextBox.ToolTip = "";
        }
    }
}
