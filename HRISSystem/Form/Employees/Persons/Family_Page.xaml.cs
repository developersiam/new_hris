﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DomainModelHris;
using HRISSystemBL;
using HRISSystem.Shared;

namespace HRISSystem.Form.Employees.Persons
{
    /// <summary>
    /// Interaction logic for Family_Page.xaml
    /// </summary>
    public partial class Family_Page : Page
    {
        string PersonID;
        public Family_Page(string personID)
        {
            InitializeComponent();
            PersonID = personID;
            AddButton.IsEnabled = !string.IsNullOrEmpty(PersonID);
            if (string.IsNullOrEmpty(PersonID)) return;
            ReloadDatagrid();
            BindCombobox();
            FamilyIDTextBox.Text = BusinessLayerServices.FamilyBL().GetNewFamilyID();
        }

        private void ReloadDatagrid()
        {
            try
            {
                ClearControl();
                FamilyDataGrid.ItemsSource = null;
                FamilyDataGrid.ItemsSource = BusinessLayerServices.FamilyBL().GetFamilyListByPersonID(PersonID);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindCombobox()
        {
            try
            {
                TitlenameCombobox.ItemsSource = BusinessLayerServices.PersonBL().GetAllTitlenameList();
                RelationshipCombobox.ItemsSource = BusinessLayerServices.FamilyBL().GetAllRelationshipList();
                VitalstatusCombobox.ItemsSource = BusinessLayerServices.FamilyBL().GetAllVitalstatusList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(PersonID)) throw new Exception("กรุณาระบุ Person ID");
                    if (string.IsNullOrEmpty(FamilyIDTextBox.Text)) throw new Exception("กรุณาระบุ Family ID");
                    if (string.IsNullOrEmpty(TitlenameCombobox.Text) || string.IsNullOrEmpty(FirstnameTextBox.Text) || string.IsNullOrEmpty(LastnameTextBox.Text)) throw new Exception("กรุณาระบุชื่อ-นามสกุล");
                    if (string.IsNullOrEmpty(RelationshipCombobox.Text)) throw new Exception("กรุณาระบุความสัมพันธ์");
                    if (string.IsNullOrEmpty(VitalstatusCombobox.Text)) throw new Exception("กรุณาระบุสถานะ");

                    var oldItem = BusinessLayerServices.FamilyBL().GetSingleFamilyMember(FamilyIDTextBox.Text, PersonID);
                    var newItem = new FamilyMember
                    {
                        FamilyMember_ID = oldItem.FamilyMember_ID,
                        Person_ID = oldItem.Person_ID,
                        TitleName_ID = ((TitleName)TitlenameCombobox.SelectedItem).TitleName_ID,
                        FirstName = FirstnameTextBox.Text,
                        LastName = LastnameTextBox.Text,
                        FamilyMemberType_ID = ((FamilyMemberType)RelationshipCombobox.SelectedItem).FamilyMemberType_ID,
                        DateOfBirth = BirthDatePicker.SelectedDate,
                        VitalStatus_ID = ((VitalStau)VitalstatusCombobox.SelectedItem).VitalStatus_ID
                    };
                    BusinessLayerServices.FamilyBL().UpdateFamily(newItem);
                    DataCorrectorStatic<FamilyMember>.AddTransaction("U", "Person Family", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditDetailButton_Click(object sender, RoutedEventArgs e)
        {
            BindSelectedData();
        }

        private void FamilyDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BindSelectedData();
        }

        void BindSelectedData()
        {
            try
            {
                var selected = (FamilyMember)FamilyDataGrid.SelectedItem;
                if (selected != null)
                {
                    FamilyIDTextBox.Text = selected.FamilyMember_ID;
                    TitlenameCombobox.SelectedValue = selected.TitleName_ID;
                    FirstnameTextBox.Text = selected.FirstName;
                    LastnameTextBox.Text = selected.LastName;
                    RelationshipCombobox.SelectedValue = selected.FamilyMemberType_ID;
                    BirthDatePicker.SelectedDate = selected.DateOfBirth;
                    VitalstatusCombobox.SelectedValue = selected.VitalStatus_ID;
                    AddButton.IsEnabled = false;
                }
                UpdateButton.IsEnabled = selected != null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearControl()
        {
            FamilyIDTextBox.Text = BusinessLayerServices.FamilyBL().GetNewFamilyID();
            TitlenameCombobox.SelectedValue = -1;
            FirstnameTextBox.Text = "";
            LastnameTextBox.Text = "";
            RelationshipCombobox.SelectedValue = -1;
            BirthDatePicker.Text = "";
            VitalstatusCombobox.SelectedValue = -1;
            UpdateButton.IsEnabled = false;
            AddButton.IsEnabled = true;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Family_Window window = new Family_Window();
                //window.PersonIDTextBox.Text = PersonID;
                //window.ShowDialog();
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(PersonID)) throw new Exception("กรุณาระบุ Person ID");
                    if (string.IsNullOrEmpty(FamilyIDTextBox.Text)) throw new Exception("กรุณาระบุ Family ID");
                    if (string.IsNullOrEmpty(TitlenameCombobox.Text) || string.IsNullOrEmpty(FirstnameTextBox.Text) || string.IsNullOrEmpty(LastnameTextBox.Text)) throw new Exception("กรุณาระบุชื่อ-นามสกุล");
                    if (string.IsNullOrEmpty(RelationshipCombobox.Text)) throw new Exception("กรุณาระบุความสัมพันธ์");
                    if (string.IsNullOrEmpty(VitalstatusCombobox.Text)) throw new Exception("กรุณาระบุสถานะ");

                    var newItem = new FamilyMember
                    {
                        FamilyMember_ID = FamilyIDTextBox.Text,
                        Person_ID = PersonID,
                        TitleName_ID = ((TitleName)TitlenameCombobox.SelectedItem).TitleName_ID,
                        FirstName = FirstnameTextBox.Text,
                        LastName = LastnameTextBox.Text,
                        FamilyMemberType_ID = ((FamilyMemberType)RelationshipCombobox.SelectedItem).FamilyMemberType_ID,
                        DateOfBirth = BirthDatePicker.SelectedDate,
                        VitalStatus_ID = ((VitalStau)VitalstatusCombobox.SelectedItem).VitalStatus_ID
                    };
                    BusinessLayerServices.FamilyBL().AddFamily(newItem);
                    DataCorrectorStatic<FamilyMember>.AddTransaction("A", "Person Family", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddRelationButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Relationship_Window window = new Relationship_Window();
                window.ShowDialog();
                RelationshipCombobox.ItemsSource = BusinessLayerServices.FamilyBL().GetAllRelationshipList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var oldItem = (FamilyMember)FamilyDataGrid.SelectedItem;
                    BusinessLayerServices.FamilyBL().DeleteFamily(oldItem);
                    DataCorrectorStatic<FamilyMember>.AddTransaction("D", "Person Family", oldItem, null);
                    MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearControl();
        }
    }
}
