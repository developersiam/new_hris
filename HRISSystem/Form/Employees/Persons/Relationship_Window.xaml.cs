﻿using System;
using System.Windows;
using System.Windows.Input;
using DomainModelHris;
using HRISSystemBL;
using HRISSystem.Shared;

namespace HRISSystem.Form.Employees.Persons
{
    /// <summary>
    /// Interaction logic for Relationship_Window.xaml
    /// </summary>
    public partial class Relationship_Window : Window
    {
        public Relationship_Window()
        {
            InitializeComponent();
            RelationshipDataGrid.ItemsSource = BusinessLayerServices.FamilyBL().GetAllRelationshipList();
            FamilyMemberTypeIDTextBox.Text = BusinessLayerServices.FamilyBL().GetNewRelationshipID();
        }

        private void EditDetailButton_Click(object sender, RoutedEventArgs e)
        {
            BindSelectedData();
        }

        private void RelationshipDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BindSelectedData();
        }

        void BindSelectedData()
        {
            try
            {
                var selected = (FamilyMemberType)RelationshipDataGrid.SelectedItem;
                if (selected != null)
                {
                    FamilyMemberTypeIDTextBox.Text = selected.FamilyMemberType_ID;
                    FamilyMemberTypeTextBox.Text = selected.FamilyMemberType1;
                }
                UpdateButton.IsEnabled = selected != null;
                AddButton.IsEnabled = selected == null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearControl();
        }

        void ClearControl()
        {
            FamilyMemberTypeIDTextBox.Text = BusinessLayerServices.FamilyBL().GetNewRelationshipID();
            FamilyMemberTypeTextBox.Text = "";
            UpdateButton.IsEnabled = false;
            AddButton.IsEnabled = true;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(FamilyMemberTypeIDTextBox.Text)) throw new NullReferenceException();
                    if (string.IsNullOrEmpty(FamilyMemberTypeTextBox.Text)) throw new NullReferenceException();

                    var newItem = new FamilyMemberType
                    {
                        FamilyMemberType_ID = FamilyMemberTypeIDTextBox.Text,
                        FamilyMemberType1 = FamilyMemberTypeTextBox.Text,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.FamilyBL().AddRelationship(newItem);
                    DataCorrectorStatic<FamilyMemberType>.AddTransaction("A", "Family Setting", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    RelationshipDataGrid.ItemsSource = BusinessLayerServices.FamilyBL().GetAllRelationshipList();
                    ClearControl();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(FamilyMemberTypeIDTextBox.Text)) throw new NullReferenceException();
                    if (string.IsNullOrEmpty(FamilyMemberTypeTextBox.Text)) throw new NullReferenceException();

                    var oldItem = BusinessLayerServices.FamilyBL().GetSingleFamilyMemberType(FamilyMemberTypeIDTextBox.Text);
                    var newItem = new FamilyMemberType
                    {
                        FamilyMemberType_ID = oldItem.FamilyMemberType_ID,
                        FamilyMemberType1 = FamilyMemberTypeTextBox.Text,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.FamilyBL().UpdateRelationship(newItem);
                    DataCorrectorStatic<FamilyMemberType>.AddTransaction("U", "Family Setting", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    RelationshipDataGrid.ItemsSource = BusinessLayerServices.FamilyBL().GetAllRelationshipList();
                    ClearControl();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
