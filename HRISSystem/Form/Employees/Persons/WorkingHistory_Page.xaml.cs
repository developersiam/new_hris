﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModelHris;
using HRISSystemBL;

namespace HRISSystem.Form.Employees.Persons
{
    /// <summary>
    /// Interaction logic for WorkingHistory_Page.xaml
    /// </summary>
    public partial class WorkingHistory_Page : Page
    {
        string PersonID;
        public WorkingHistory_Page(string personID)
        {
            InitializeComponent();
            PersonID = personID;
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            WorkingHistoryDataGrid.ItemsSource = null;
            WorkingHistoryDataGrid.ItemsSource = BusinessLayerServices.EmployeeBL().GetEmployeeListByPersonID(PersonID);
        }
    }
}
