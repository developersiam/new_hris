﻿using DomainModelHris;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Form.Employees
{
    /// <summary>
    /// Interaction logic for SearchMultipleEmployeeWindow.xaml
    /// </summary>
    public partial class SearchMultipleEmployeeWindow : Window
    {
        public List<sp_OT_GetEmployeeInformation_Result> employeeList = new List<sp_OT_GetEmployeeInformation_Result>();

        public SearchMultipleEmployeeWindow()
        {
            InitializeComponent();
            DepartmentCombobox.ItemsSource = BusinessLayerServices.DeptCodeBL().GetAllDepartment();
            StatusCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffStatusList();
            StatusCombobox.SelectedValue = "1"; //สถานะพนักงาน "ทำงาน"
            StaffTypeCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
        }

        private void DepartmentCombobox_DropDownClosed(object sender, EventArgs e)
        {
            ReloadDatagrid();
        }

        private void StatusCombobox_DropDownClosed(object sender, EventArgs e)
        {
            ReloadDatagrid();
        }

        private void StaffTypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                string _Department = DepartmentCombobox.Text == "" ? "" : DepartmentCombobox.SelectedValue.ToString();
                string _Status = StatusCombobox.Text;
                string _StaffType = StaffTypeCombobox.Text;

                if (string.IsNullOrEmpty(_Department) && string.IsNullOrEmpty(_Status) && string.IsNullOrEmpty(_StaffType)) return;

                EmployeeInformationDataGrid.ItemsSource = null;
                EmployeeInformationDataGrid.ItemsSource = BusinessLayerServices.EmployeeBL().GetFilteredEmployeeList(_Department, _Status, _StaffType);

                SelectedDatagrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EmployeeInformationDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            SelectedDatagrid();
        }

        private void EmployeeInformationDataGrid_KeyUp(object sender, KeyEventArgs e)
        {
            SelectedDatagrid();
        }

        private void SelectedDatagrid()
        {
            try
            {
                int? selectedCount = EmployeeInformationDataGrid.SelectedItems.Count;
                SelectButton.Content = string.Format("เลือก ({0})", selectedCount);
                SelectButton.IsEnabled = selectedCount > 0 ? true : false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (var item in EmployeeInformationDataGrid.SelectedItems)
                {
                    employeeList.Add((sp_OT_GetEmployeeInformation_Result)item);
                }

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
