﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModelHris;
using HRISSystemBL;
using HRISSystem.Shared;

namespace HRISSystem.Form.Employees.Employees
{
    /// <summary>
    /// Interaction logic for PayrollExceptPage.xaml
    /// </summary>
    public partial class PayrollExceptPage : Page
    {
        string EmployeeID;
        public PayrollExceptPage()
        {
            InitializeComponent();
        }
        public PayrollExceptPage(string employeeID)
        {
            InitializeComponent();
            EmployeeID = employeeID;
            UpdateButton.IsEnabled = !string.IsNullOrEmpty(employeeID);
            if (string.IsNullOrEmpty(EmployeeID)) return;
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                informationDataGrid.ItemsSource = null;
                informationDataGrid.ItemsSource = BusinessLayerServices.PayrollFlagBL().GetPayrollFlagListByEmployeeID(EmployeeID);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(FromDatePicker.Text)) FromDatePicker.SelectedDate = DateTime.Now;
                if (string.IsNullOrEmpty(ToDatePicker.Text)) ToDatePicker.SelectedDate = DateTime.Now;
                if (string.IsNullOrEmpty(EmployeeID)) throw new Exception("กรุณาระบุรหัสพนักงาน");

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (FromDatePicker.SelectedDate > ToDatePicker.SelectedDate) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");

                    var validFrom = FromDatePicker.SelectedDate.Value;
                    var endDate = ToDatePicker.SelectedDate.Value;
                    var newItemList = new List<PayrollFlag>();

                    for (var day = validFrom.Date; day.Date <= endDate.Date; day = day.AddDays(1))
                    {
                        var item = new PayrollFlag
                        {
                            Employee_ID = EmployeeID,
                            ValidFrom = day,
                            ToPayrollFlag = true,
                            ModifiledDate = DateTime.Now
                        };
                        BusinessLayerServices.PayrollFlagBL().AddPayrollFlag(item);
                        newItemList.Add(item);
                    }
                    DataCorrectorStatic<PayrollFlag>.AddTransactionList("A", "Employee Payroll Except", null, newItemList);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var oldItem = (PayrollFlag)informationDataGrid.SelectedItem;
                if (oldItem != null)
                {
                    if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        BusinessLayerServices.PayrollFlagBL().DeletePayrollFlag(oldItem);
                        DataCorrectorStatic<PayrollFlag>.AddTransaction("D", "Employee Payroll Except", oldItem, null);
                        MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        ReloadDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
