﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DomainModelHris;
using HRISSystemBL;

namespace HRISSystem.Form.Employees.Employees
{
    /// <summary>
    /// Interaction logic for PositionWindow.xaml
    /// </summary>
    public partial class PositionWindow : Window
    {
        public Position selected;
        List<Position> deptFilteredList;

        public PositionWindow(string deptCode)
        {
            InitializeComponent();
            var departmentList = BusinessLayerServices.DeptCodeBL().GetAllDepartment();
            departmentList.Insert(0, new Department { DeptCode = "", DeptName = "All" });
            DepartmentCombobox.ItemsSource = departmentList;
            DepartmentCombobox.SelectedValue = deptCode;
            ReloadDatagrid();   
        }

        private void ReloadDatagrid()
        {
            try
            {
                var dept = (Department)DepartmentCombobox.SelectedItem;
                var positionList = BusinessLayerServices.PositionBL().GetAllList().OrderBy(o => o.PositionNameTH).ToList();
                deptFilteredList = new List<Position>();

                if (dept == null || dept.DeptCode == "") deptFilteredList = positionList;
                else deptFilteredList = deptFilteredList = positionList.Where(o => o.DeptCode == dept.DeptCode).ToList();

                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = deptFilteredList;
                DatagridItemCount();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DatagridItemCount()
        {
            int? selectedCount = InformationDataGrid.Items.Count;
            TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", selectedCount);
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                var search = SearchTextBox.Text.Trim();
                var textFilteredList = new List<Position>();

                if (string.IsNullOrEmpty(search)) textFilteredList = deptFilteredList;
                else textFilteredList = deptFilteredList.Where(o => o.PositionNameTH.StartsWith(search)).ToList();
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = textFilteredList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                selected = (Position)InformationDataGrid.SelectedItem;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                selected = (Position)InformationDataGrid.SelectedItem;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void DepartmentCombobox_DropDownClosed(object sender, EventArgs e)
        {
            ReloadDatagrid();
        }

        private void AddPositionButton_Click(object sender, RoutedEventArgs e)
        {
            var w = new References.PositionWindow();
            w.ShowDialog();
            ReloadDatagrid();
        }
    }
}
