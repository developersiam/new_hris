﻿using DomainModelHris;
using HRISSystemBL;
using DomainModelPayroll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HRISSystem.Shared;
using DomainModelTimeStec;

namespace HRISSystem.Form.Employees.Employees
{
    /// <summary>
    /// Interaction logic for MoveDepartmentPage.xaml
    /// </summary>
    public partial class MoveDepartmentPage : Page
    {
        public MoveDepartmentPage()
        {
            InitializeComponent();
        }

        private void PersonIDTextBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            Search();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        public void Search()
        {
            try
            {
                SearchEmployee_Window window = new SearchEmployee_Window();
                window.ShowDialog();
                if (window.selectedEmployee != null) BindSelectedEmployee(window.selectedEmployee.Employee_ID);
                UpdateButton.IsEnabled = !string.IsNullOrEmpty(EmployeeIDTextBox.Text) || window.selectedEmployee != null;
                SearchPositionButton.IsEnabled = !string.IsNullOrEmpty(EmployeeIDTextBox.Text) || window.selectedEmployee != null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindSelectedEmployee(string employee_ID)
        {
            try
            {
                BindCombobox();
                var i = BusinessLayerServices.EmployeeBL().GetEmployeeByEmployeeID(employee_ID);

                //Left Side
                EmployeeIDTextBox.Text = i.Employee_ID;
                PersonIDTextBox.Text = i.Person_ID;
                FingerIDTextBox.Text = i.FingerScanID;
                DepartmentCombobox.SelectedValue = i.DeptCode;
                TitleCombobox.SelectedValue = i.Person.TitleName_ID;
                NameTHTextBox.Text = i.Person.FirstNameTH;
                LastnameTHTextBox.Text = i.Person.LastNameTH;
                BankAccountTextBox.Text = BusinessLayerServices.PayrollBL().GetBankAccountByHrisID(i);
                StartDatePicker.SelectedDate = i.StartDate;
                EndDatePicker.SelectedDate = i.EndDate;
                StaffTypeCombobox.SelectedValue = i.Staff_type;
                StaffStatusCombobox.SelectedValue = i.Staff_status;
                EmailTextBox.Text = i.Email;
                PositionIDTextBox.Text = i.Position_ID;
                PositionTextBox.Text = i.Position.PositionNameTH;

                //Right side
                NewPersonIDTextBox.Text = PersonIDTextBox.Text;
                NewEmployeeIDTextBox.Text = BusinessLayerServices.EmployeeBL().GetNewEmplyeeID(); // Get new employee id
                NewFingerIDTextBox.Text = FingerIDTextBox.Text; // Use same finger code
                NewTitleCombobox.SelectedValue = TitleCombobox.SelectedValue;
                NewNameTHTextBox.Text = NameTHTextBox.Text;
                NewLastnameTHTextBox.Text = LastnameTHTextBox.Text;
                NewBankAccountTextBox.Text = BankAccountTextBox.Text;
                NewEmailTextBox.Text = EmailTextBox.Text;

                NewDepartmentCombobox.SelectedIndex = -1;
                NewPositionIDTextBox.Text = "";
                NewPositionTextBox.Text = "";
                NewStartDatePicker.SelectedDate = null;
                NewEndDatePicker.SelectedDate = null;
                NewStaffTypeCombobox.SelectedIndex = -1;
                NewStaffStatusCombobox.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindCombobox()
        {
            if (DepartmentCombobox.Items.Count == 0) DepartmentCombobox.ItemsSource = BusinessLayerServices.DeptCodeBL().GetAllDepartment();
            if (TitleCombobox.Items.Count == 0) TitleCombobox.ItemsSource = BusinessLayerServices.PersonBL().GetAllTitlenameList();
            if (StaffTypeCombobox.Items.Count == 0) StaffTypeCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
            if (StaffStatusCombobox.Items.Count == 0) StaffStatusCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffStatusList();

            if (NewDepartmentCombobox.Items.Count == 0) NewDepartmentCombobox.ItemsSource = BusinessLayerServices.DeptCodeBL().GetAllDepartment();
            if (NewTitleCombobox.Items.Count == 0) NewTitleCombobox.ItemsSource = BusinessLayerServices.PersonBL().GetAllTitlenameList();
            if (NewStaffTypeCombobox.Items.Count == 0) NewStaffTypeCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
            if (NewStaffStatusCombobox.Items.Count == 0) NewStaffStatusCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffStatusList();
            NewStaffStatusCombobox.SelectedValue = "1";
        }

        private void SearchPositionButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var dept = (Department)NewDepartmentCombobox.SelectedItem;
                if (dept == null) throw new Exception("กรุณาเลือกแผนก");

                var p = new PositionWindow(dept.DeptCode);
                p.ShowDialog();
                if (p.selected == null) return;
                NewPositionIDTextBox.Text = p.selected.Position_ID;
                NewPositionTextBox.Text = p.selected.PositionNameTH;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void NewStartDatePicker_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(NewStartDatePicker.Text)) NewStartDatePicker.SelectedDate = DateTime.Now;
        }

        private void NewEndDatePicker_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(NewEndDatePicker.Text)) NewEndDatePicker.SelectedDate = DateTime.MaxValue;
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(NewStartDatePicker.Text)) NewStartDatePicker.SelectedDate = DateTime.Now;
                if (string.IsNullOrEmpty(NewEndDatePicker.Text)) NewEndDatePicker.SelectedDate = DateTime.MaxValue;
                if (MessageBox.Show("ยืนยันการสินสุดสถานะพนักงานและย้ายแผนกหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(NewDepartmentCombobox.Text)) throw new Exception("กรุณาระบุ Department");
                    if (string.IsNullOrEmpty(NewPositionTextBox.Text)) throw new Exception("กรุณาระบุ Position");
                    if (string.IsNullOrEmpty(NewStartDatePicker.Text)) throw new Exception("กรุณาระบุวันที่เริ่มงาน");
                    if (string.IsNullOrEmpty(NewEndDatePicker.Text)) throw new Exception("กรุณาระบุวันที่สิ้นสุดการทำงาน");
                    if (string.IsNullOrEmpty(NewStaffTypeCombobox.Text)) throw new Exception("กรุณาระบุ Staff Type");

                    if (StaffStatusCombobox.SelectedValue.ToString() == "1") throw new Exception("สถานะพนักงานเติมยังไม่สิ้นสุด, กรุณาตรวจสอบ");
                    if (NewStartDatePicker.SelectedDate < EndDatePicker.SelectedDate) throw new Exception("วันที่เริ่มงานไม่ถูกต้อง, กรุณาตรวจสอบ");
                    if (NewStartDatePicker.SelectedDate > NewEndDatePicker.SelectedDate) throw new Exception("วันที่สิ้นสุดไม่ถูกต้อง, กรุณาตรวจสอบ");

                    UpdateHrisEmployee();
                    AddHrisEmployee();
                    UpdatePayrollEmployee();
                    UpdateUserInfo();
                    UpdateLeaveQuota();
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void UpdateLeaveQuota()
        {
            try
            {
                if (MessageBox.Show("ต้องการย้ายสิทธิ์การลาทั้งหมดมายังรหัสปัจจุบัน หรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var newEmp = NewEmployeeIDTextBox.Text;
                    var oldEmp = EmployeeIDTextBox.Text;
                    var oldLeave = BusinessLayerServices.LeaveTypeBL().GetAll_AvialableLeaveSetupList_ByEmployeeID(oldEmp);

                    foreach (var item in oldLeave)
                    {
                        var leave = new LeaveSetup();
                        leave.Employee_ID = newEmp;
                        leave.LeaveType_ID = item.LeaveType_ID;
                        leave.Year = item.Year;
                        leave.LeaveAmount = item.LeaveAmount;
                        leave.ValidFrom = item.ValidFrom;
                        leave.ValidTo = item.ValidTo;
                        leave.ModifiedDate = DateTime.Now;
                        leave.ModifiedBy = SingletonConfiguration.getInstance().Username;

                        BusinessLayerServices.LeaveTypeBL().AddLeaveSetup(leave);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void UpdateHrisEmployee()
        {
            //End Employee
            var oldItem = BusinessLayerServices.EmployeeBL().GetEmployeeByEmployeeID(EmployeeIDTextBox.Text);
            var newItem = BusinessLayerServices.EmployeeBL().GetEmployeeByEmployeeID(EmployeeIDTextBox.Text);
            newItem.EndDate = EndDatePicker.SelectedDate;
            newItem.Staff_status = 2; //สิ้นสุดสถานะพนักงาน
            newItem.ModifiedDate = DateTime.Now;
            BusinessLayerServices.EmployeeBL().UpdateEmployee(newItem);
            DataCorrectorStatic<DomainModelHris.Employee>.AddTransaction("U", "Move Employee", oldItem, newItem);
        }

        private void AddHrisEmployee()
        {
            //New Employee
            var newItem = new DomainModelHris.Employee
            {
                Employee_ID = NewEmployeeIDTextBox.Text,
                Noted = NewEmployeeIDTextBox.Text,
                Person_ID = NewPersonIDTextBox.Text,
                FingerScanID = NewFingerIDTextBox.Text,
                StartDate = NewStartDatePicker.SelectedDate,
                EndDate = NewEndDatePicker.SelectedDate,
                Email = NewEmailTextBox.Text,
                DeptCode = NewDepartmentCombobox.SelectedValue.ToString(),
                Staff_status = ((StaffStatu)NewStaffStatusCombobox.SelectedItem).StaffStatusID,
                Staff_type = ((StaffType)NewStaffTypeCombobox.SelectedItem).StaffTypeID,
                Position_ID = NewPositionIDTextBox.Text,
                CreatedDate = DateTime.Now,
                IsChangeType = ChangeTypeCheckbox.IsChecked.Value ? true : (bool?)null           
            };
            BusinessLayerServices.EmployeeBL().AddEmployee(newItem);
            DataCorrectorStatic<DomainModelHris.Employee>.AddTransaction("A", "Move Employee", null, newItem);
        }
        private void UpdatePayrollEmployee()
        {
            //Add Payroll Employee
            var newItem = new DomainModelPayroll.Employee
            {
                Employee_id = NewEmployeeIDTextBox.Text,
                EmployeeID = NewEmployeeIDTextBox.Text,
                Bank_acc = NewBankAccountTextBox.Text
            };
            BusinessLayerServices.PayrollBL().AddPayrollEmployee(newItem);
            DataCorrectorStatic<DomainModelPayroll.Employee>.AddTransaction("A", "Move Employee", null, newItem);
        }

        private void UpdateUserInfo()
        {
            //Update UserInfo
            var oldItem = BusinessLayerServices.TimeStecBL().GetUserInfoByFingerID(FingerIDTextBox.Text);
            var newItem = BusinessLayerServices.TimeStecBL().GetUserInfoByFingerID(FingerIDTextBox.Text);
            //var x = BusinessLayerServices.TimeStecBL().GetTimeStecDepartmentID(DepartmentCombobox.SelectedValue.ToString()).DeptID;
            newItem.defaultdeptid = BusinessLayerServices.TimeStecBL().GetTimeStecDepartmentID(NewDepartmentCombobox.SelectedValue.ToString()).DeptID;
            BusinessLayerServices.TimeStecBL().UpdateUserInfo(newItem);
            DataCorrectorStatic<userinfo>.AddTransaction("U", "Move Employee", oldItem, newItem);
        }

        private void NewDepartmentCombobox_DropDownClosed(object sender, EventArgs e)
        {
            //PositionIDTextBox.Text = "";
            //PositionTextBox.Text = "";
            //PositionTextBox.ToolTip = "";
        }

        private void NewStaffTypeCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var newStafftype = (StaffType)NewStaffTypeCombobox.SelectedItem;
            var oldStafftype = (StaffType)StaffTypeCombobox.SelectedItem;

            if(newStafftype == null || oldStafftype == null) return;

            ChangeTypeCheckbox.IsChecked = newStafftype.StaffTypeID != oldStafftype.StaffTypeID;
        }
    }
}
