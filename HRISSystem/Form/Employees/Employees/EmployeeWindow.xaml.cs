﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HRISSystemBL;
using DomainModelHris;
using DomainModelPayroll;
using HRISSystem.ViewModel;
using HRISSystem.Shared;
using DomainModelTimeStec;
using HRISSystem.Helper;

namespace HRISSystem.Form.Employees.Employees
{
    /// <summary>
    /// Interaction logic for EmployeeWindow.xaml
    /// </summary>
    public partial class EmployeeWindow : Window
    {
        public string recentlyAddEmployeeID;
        public EmployeeWindow()
        {
            InitializeComponent();
            DepartmentCombobox.ItemsSource = BusinessLayerServices.DeptCodeBL().GetAllDepartment();
            TitleCombobox.ItemsSource = BusinessLayerServices.PersonBL().GetAllTitlenameList();
            TitleCombobox.SelectedIndex = 0;
            StaffTypeCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
            StaffStatusCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffStatusList();
            StaffStatusCombobox.SelectedIndex = 0;
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void PersonIDTextBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left) Search();
        }

        private void Search()
        {
            try
            {
                var window = new SearchEmployee_Window();
                window.ShowDialog();
                BindSelectedEmployee(window.selectedEmployee);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindSelectedEmployee(DomainModelHris.Employee model)
        {
            bool isWorking = model != null || !string.IsNullOrEmpty(PersonIDTextBox.Text);
            if (model != null)
            {
                var employee = BusinessLayerServices.EmployeeBL().GetLastEmployeeByPersonID(model.Person_ID);
                isWorking = employee.EndDate > DateTime.Now || employee.Staff_status == 1; // Employee status checking

                PersonIDTextBox.Text = employee.Person_ID;
                EmployeeIDTextBox.Text = isWorking ? employee.Employee_ID : BusinessLayerServices.EmployeeBL().GetNewEmplyeeID();
                FingerIDTextBox.Text = employee.FingerScanID;
                DepartmentCombobox.SelectedValue = isWorking ? model.DeptCode : null;
                PositionIDTextBox.Text = isWorking ? employee.Position_ID : null;
                PositionTextBox.Text = isWorking ? employee.Position.PositionNameTH : null;
                TitleCombobox.SelectedValue = model.Person.TitleName_ID;
                NameTHTextBox.Text = model.Person.FirstNameTH;
                LastnameTHTextBox.Text = model.Person.LastNameTH;
                BankAccountTextBox.Text = BusinessLayerServices.PayrollBL().GetBankAccountByPersonID(employee.Person_ID);
                StartDatePicker.SelectedDate = isWorking ? employee.StartDate : null;
                EndDatePicker.SelectedDate = isWorking ? employee.EndDate : null;
                StaffTypeCombobox.SelectedValue = isWorking ? employee.Staff_type : null;
                //StaffStatusCombobox.SelectedValue = isWorking ? employee.Staff_status : null;

                if (isWorking) MessageBox.Show("ไม่สามารถสร้างพนักงานใหม่ได้ เพราะสถานะยังไม่สิ้นสุด", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            SearchPositionButton.IsEnabled = !isWorking;
            UpdateButton.IsEnabled = !isWorking;
        }

        private void SearchPositionButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var dept = (Department)DepartmentCombobox.SelectedItem;
                if (dept == null) throw new Exception("กรุณาเลือกแผนก");

                var p = new PositionWindow(dept.DeptCode);
                p.ShowDialog();
                if (p.selected == null) return;
                PositionIDTextBox.Text = p.selected.Position_ID;
                PositionTextBox.Text = p.selected.PositionNameTH;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void StartDatePicker_CalendarClosed(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(StartDatePicker.Text)) StartDatePicker.SelectedDate = DateTime.Now;
        }

        private void EndDatePicker_CalendarClosed(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(EndDatePicker.Text)) EndDatePicker.SelectedDate = DateTime.MaxValue;
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(StartDatePicker.Text))
                    StartDatePicker.SelectedDate = DateTime.Now;

                if (string.IsNullOrEmpty(EndDatePicker.Text))
                    EndDatePicker.SelectedDate = DateTime.MaxValue;

                if (MessageBoxHelper.Question("ต้องการบันทึกข้อมูลหรือไม่") == MessageBoxResult.No)
                    return;

                if (string.IsNullOrEmpty(PersonIDTextBox.Text))
                    throw new Exception("กรุณาระบุ Person ID");

                if (string.IsNullOrEmpty(EmployeeIDTextBox.Text))
                    throw new Exception("กรุณาระบุ Employee ID");

                if (string.IsNullOrEmpty(FingerIDTextBox.Text))
                    throw new Exception("กรุณาระบุ Finger ID");

                if (string.IsNullOrEmpty(DepartmentCombobox.Text))
                    throw new Exception("กรุณาระบุ Department");

                if (string.IsNullOrEmpty(TitleCombobox.Text))
                    throw new Exception("กรุณาระบุ Titile Name");

                if (string.IsNullOrEmpty(NameTHTextBox.Text))
                    throw new Exception("กรุณาระบุ Firstname");

                if (string.IsNullOrEmpty(LastnameTHTextBox.Text))
                    throw new Exception("กรุณาระบุ Lastname");

                if (string.IsNullOrEmpty(StaffTypeCombobox.Text))
                    throw new Exception("กรุณาระบุ Staff Type");

                if (string.IsNullOrEmpty(StaffStatusCombobox.Text))
                    throw new Exception("กรุณาระบุ Staff Status");

                if (string.IsNullOrEmpty(PositionIDTextBox.Text))
                    throw new Exception("กรุณาระบุ Position");

                if (DefaultShiftComboBox.SelectedIndex < 0)
                    throw new Exception("กรุณาระบุ Detault Shift");

                AddHrisEmployee();
                AddPayrollEmployee();
                UpdateUserInfo();

                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                recentlyAddEmployeeID = EmployeeIDTextBox.Text;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddHrisEmployee()
        {
            try
            {
                var newItem = new DomainModelHris.Employee
                {
                    Employee_ID = EmployeeIDTextBox.Text,
                    Person_ID = PersonIDTextBox.Text,
                    DeptCode = DepartmentCombobox.SelectedValue.ToString(),
                    FingerScanID = FingerIDTextBox.Text,
                    Noted = EmployeeIDTextBox.Text,
                    Position_ID = PositionIDTextBox.Text,
                    StartDate = StartDatePicker.SelectedDate,
                    EndDate = EndDatePicker.SelectedDate,
                    Staff_status = ((StaffStatu)StaffStatusCombobox.SelectedItem).StaffStatusID,
                    Staff_type = ((StaffType)StaffTypeCombobox.SelectedItem).StaffTypeID,
                    Shift_ID = ((Shift)DefaultShiftComboBox.SelectedItem).Shift_ID,
                    CreatedDate = DateTime.Now
                };

                BusinessLayerServices.EmployeeBL().AddEmployee(newItem);
                DataCorrectorStatic<DomainModelHris.Employee>.AddTransaction("A", "Add Employee", null, newItem);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddPayrollEmployee()
        {
            try
            {
                var newItem = new DomainModelPayroll.Employee
                {
                    Employee_id = EmployeeIDTextBox.Text,
                    EmployeeID = EmployeeIDTextBox.Text,
                    Bank_acc = BankAccountTextBox.Text
                };
                BusinessLayerServices.PayrollBL().AddPayrollEmployee(newItem);
                DataCorrectorStatic<DomainModelPayroll.Employee>.AddTransaction("A", "Add Employee", null, newItem);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void UpdateUserInfo()
        {
            try
            {
                var oldItem = BusinessLayerServices.TimeStecBL().GetUserInfoByFingerID(FingerIDTextBox.Text);
                var newItem = BusinessLayerServices.TimeStecBL().GetUserInfoByFingerID(FingerIDTextBox.Text);
                newItem.defaultdeptid = BusinessLayerServices.TimeStecBL().GetTimeStecDepartmentID(DepartmentCombobox.SelectedValue.ToString()).DeptID;
                BusinessLayerServices.TimeStecBL().UpdateUserInfo(newItem);
                DataCorrectorStatic<userinfo>.AddTransaction("U", "Add Employee", oldItem, newItem);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DepartmentCombobox_DropDownClosed(object sender, EventArgs e)
        {
            PositionIDTextBox.Text = "";
            PositionTextBox.Text = "";
            PositionTextBox.ToolTip = "";
        }
    }
}
