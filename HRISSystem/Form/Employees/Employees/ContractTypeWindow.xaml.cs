﻿using System;
using System.Windows;
using System.Windows.Input;
using DomainModelHris;
using HRISSystemBL;
using HRISSystem.Shared;

namespace HRISSystem.Form.Employees.Employees
{
    /// <summary>
    /// Interaction logic for ContractTypeWindow.xaml
    /// </summary>
    public partial class ContractTypeWindow : Window
    {
        public ContractTypeWindow()
        {
            InitializeComponent();
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                ClearControl();
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = BusinessLayerServices.ContractBL().GetAllContractTypeList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BindSelectedData();
        }

        private void EditDetailButton_Click(object sender, RoutedEventArgs e)
        {
            BindSelectedData();
        }

        private void BindSelectedData()
        {
            try
            {
                var selected = (ContractType)InformationDataGrid.SelectedItem;
                if (selected != null)
                {
                    ContractTypeIDTextBox.Text = selected.ContractType_ID;
                    ContractTypeNameTHTextBox.Text = selected.ContractTypeNameTH;
                    ContractTypeNameENTextBox.Text = selected.ContractTypeNameEN;
                    FromDatePicker.SelectedDate = selected.ValidFrom;
                    EndDatePicker.SelectedDate = selected.EndDate;
                }
                AddButton.IsEnabled = false;
                UpdateButton.IsEnabled = selected != null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(FromDatePicker.Text)) FromDatePicker.SelectedDate = DateTime.Today;
                    if (string.IsNullOrEmpty(EndDatePicker.Text)) EndDatePicker.SelectedDate = DateTime.MaxValue;
                    if (string.IsNullOrEmpty(ContractTypeIDTextBox.Text)) throw new Exception("กรุณาระบุ ID");
                    if (string.IsNullOrEmpty(ContractTypeNameTHTextBox.Text)) throw new Exception("กรุณาระบุชื่อสัญญา");

                    var oldItem = BusinessLayerServices.ContractBL().GetSingleContractType(ContractTypeIDTextBox.Text);
                    var newItem = new ContractType
                    {
                        ContractType_ID = oldItem.ContractType_ID,
                        ContractTypeNameTH = ContractTypeNameTHTextBox.Text,
                        ContractTypeNameEN = ContractTypeNameENTextBox.Text,
                        ValidFrom = FromDatePicker.SelectedDate,
                        EndDate = EndDatePicker.SelectedDate,
                        CreatedDate = oldItem.CreatedDate,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.ContractBL().UpdateContractType(newItem);
                    DataCorrectorStatic<ContractType>.AddTransaction("U", "Contract Setting", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var oldItem = (ContractType)InformationDataGrid.SelectedItem;
                if (oldItem != null)
                {
                    if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        BusinessLayerServices.ContractBL().DeleteContractType(oldItem);
                        DataCorrectorStatic<ContractType>.AddTransaction("D", "Contract Setting", oldItem, null);
                        MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        ReloadDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearControl()
        {
            try
            {
                ContractTypeIDTextBox.Text = "";
                ContractTypeNameTHTextBox.Text = "";
                ContractTypeNameENTextBox.Text = "";
                FromDatePicker.Text = "";
                EndDatePicker.Text = "";
                AddButton.IsEnabled = true;
                UpdateButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearControl();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(FromDatePicker.Text)) FromDatePicker.SelectedDate = DateTime.Today;
                    if (string.IsNullOrEmpty(EndDatePicker.Text)) EndDatePicker.SelectedDate = DateTime.MaxValue;
                    if (string.IsNullOrEmpty(ContractTypeIDTextBox.Text)) throw new Exception("กรุณาระบุ ID");
                    if (string.IsNullOrEmpty(ContractTypeNameTHTextBox.Text)) throw new Exception("กรุณาระบุชื่อสัญญา");

                    var newItem = new ContractType
                    {
                        ContractType_ID = ContractTypeIDTextBox.Text,
                        ContractTypeNameTH = ContractTypeNameTHTextBox.Text,
                        ContractTypeNameEN = ContractTypeNameENTextBox.Text,
                        ValidFrom = FromDatePicker.SelectedDate,
                        EndDate = EndDatePicker.SelectedDate,
                        CreatedDate = DateTime.Now
                    };
                    BusinessLayerServices.ContractBL().AddContractType(newItem);
                    DataCorrectorStatic<ContractType>.AddTransaction("A", "Contract Setting", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
