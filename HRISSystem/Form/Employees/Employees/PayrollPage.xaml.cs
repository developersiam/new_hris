﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HRISSystemBL;
using HRISSystem.Shared;
using DomainModelPayroll;

namespace HRISSystem.Form.Employees.Employees
{
    /// <summary>
    /// Interaction logic for PayrollPage.xaml
    /// </summary>
    public partial class PayrollPage : Page
    {
        public PayrollPage()
        {
            InitializeComponent();
        }

        string EmployeeID;
        string StaffType;

        public PayrollPage(string employeeID, string staffType)
        {
            InitializeComponent();
            EmployeeID = employeeID;
            StaffType = staffType;
            UpdateButton.IsEnabled = !string.IsNullOrEmpty(EmployeeID);
            if (string.IsNullOrEmpty(EmployeeID)) return;
            ReloadData();
        }

        private void ReloadData()
        {
            try
            {
                SalaryTextBox.IsEnabled = StaffType == "1"; // สำหรับ พนักงานประจำ
                NetSalaryTextBox.IsEnabled = StaffType == "1"; // สำหรับ พนักงานประจำ
                WageTextBox.IsEnabled = StaffType != "1"; // สำหรับ รายวัน/รายวันประจำ
                UpdateButton.IsEnabled = StaffType != "";

                var selected = BusinessLayerServices.PayrollBL().GetEmployeeByHrisID(EmployeeID);
                
                if (selected != null)
                {
                    if (selected.Staff_type == "1" && SingletonConfiguration.getInstance().Username.ToLower() == "m.tanainan")
                    {
                        MessageBox.Show("Permission required for staff type.", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else
                    {
                        BankAccountTextBox.Text = selected.Bank_acc;
                        SalaryTextBox.Text = selected.Salary == null ? "" : Math.Round(selected.Salary.Value, 2).ToString();
                        NetSalaryTextBox.Text = selected.NetSalary == null ? "" : Math.Round(selected.NetSalary.Value, 2).ToString();
                        WageTextBox.Text = selected.Wage == null ? "" : Math.Round(selected.Wage.Value, 2).ToString();
                        OTRateTextBox.Text = selected.Ot_hour_rate == null ? "" : Math.Round(selected.Ot_hour_rate.Value, 2).ToString();
                        OTHolidayTextBox.Text = selected.Ot_holiday_rate == null ? "" : Math.Round(selected.Ot_holiday_rate.Value, 2).ToString();
                        OTHolidayRateTextBox.Text = selected.Ot_hour_holiday_rate == null ? "" : Math.Round(selected.Ot_hour_holiday_rate.Value, 2).ToString();
                        //OTSpecialTextBox.Text = selected.Ot_special_rate == null ? "" : selected.Ot_special_rate.ToString();
                        TaxTextBox.Text = selected.Tax == null ? "" : Math.Round(selected.Tax.Value, 2).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var oldItem = BusinessLayerServices.PayrollBL().GetEmployeeByHrisID(EmployeeID);
                    var newItem = BusinessLayerServices.PayrollBL().GetEmployeeByHrisID(EmployeeID);
                    newItem.EmployeeID = EmployeeID;
                    newItem.Bank_acc = BankAccountTextBox.Text;
                    newItem.Salary = string.IsNullOrEmpty(SalaryTextBox.Text) ? (decimal?)null : decimal.Parse(SalaryTextBox.Text);
                    newItem.NetSalary = string.IsNullOrEmpty(NetSalaryTextBox.Text) ? (decimal?)null : decimal.Parse(NetSalaryTextBox.Text);
                    newItem.Wage = string.IsNullOrEmpty(WageTextBox.Text) ? (decimal?)null : decimal.Parse(WageTextBox.Text);
                    newItem.Ot_hour_rate = string.IsNullOrEmpty(OTRateTextBox.Text) ? (decimal?)null : decimal.Parse(OTRateTextBox.Text);
                    newItem.Ot_holiday_rate = string.IsNullOrEmpty(OTHolidayTextBox.Text) ? (decimal?)null : decimal.Parse(OTHolidayTextBox.Text);
                    newItem.Ot_hour_holiday_rate = string.IsNullOrEmpty(OTHolidayRateTextBox.Text) ? (decimal?)null : decimal.Parse(OTHolidayRateTextBox.Text);
                    newItem.Ot_special_rate = oldItem.Ot_special_rate;
                    newItem.Tax = string.IsNullOrEmpty(TaxTextBox.Text) ? (decimal?)null : decimal.Parse(TaxTextBox.Text);
                    BusinessLayerServices.PayrollBL().UpdatePayrollEmpoyee(newItem);
                    DataCorrectorStatic<DomainModelPayroll.Employee>.AddTransaction("U", "Employee Payroll", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SalaryTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            SalaryChanged();
            if(string.IsNullOrEmpty(NetSalaryTextBox.Text)) NetSalaryTextBox.Text = SalaryTextBox.Text;
        }

        private void SalaryTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.Key == Key.Enter) SalaryChanged();
            SalaryChanged();
        }

        private void SalaryChanged()
        {
            SalaryTextBox.Text = HRISService.DataCorrector().FormatNumber(2, SalaryTextBox.Text, true);
            NetSalaryTextBox.Text = SalaryTextBox.Text;
            OTRateTextBox.Text = HRISService.DataCorrector().OTRateCalculate(SalaryTextBox.Text, StaffType);
            OTHolidayTextBox.Text = HRISService.DataCorrector().OTHolidayCalculate(SalaryTextBox.Text, StaffType);
            OTHolidayRateTextBox.Text = HRISService.DataCorrector().OTHolidayRateCalculate(SalaryTextBox.Text, StaffType);
        }

        private void WageTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            WageChange();
        }

        private void WageTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.Key == Key.Enter) WageChange();
            WageChange();
        }

        private void WageChange()
        {
            WageTextBox.Text = HRISService.DataCorrector().FormatNumber(2, WageTextBox.Text, true);
            OTRateTextBox.Text = HRISService.DataCorrector().OTRateCalculate(WageTextBox.Text, StaffType);
            OTHolidayTextBox.Text = HRISService.DataCorrector().OTHolidayCalculate(WageTextBox.Text, StaffType);
            OTHolidayRateTextBox.Text = HRISService.DataCorrector().OTHolidayRateCalculate(WageTextBox.Text, StaffType);
        }
    }
}
