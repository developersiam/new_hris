﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HRISSystemBL;
using DomainModelHris;
using DomainModelPayroll;
using HRISSystem.Shared;
using HRISSystem.ViewModel;

namespace HRISSystem.Form.Employees.Employees
{
    /// <summary>
    /// Interaction logic for ManualWorkingPage.xaml
    /// </summary>
    public partial class ManualWorkingPage : Page
    {
        public ManualWorkingPage()
        {
            InitializeComponent();
        }

        string EmployeeID;
        public ManualWorkingPage(string employeeID)
        {
            try
            {
                InitializeComponent();
                EmployeeID = employeeID;
                UpdateButton.IsEnabled = !string.IsNullOrEmpty(EmployeeID);
                if (string.IsNullOrEmpty(EmployeeID)) return;
                LotCombobox.ItemsSource = BusinessLayerServices.PayrollBL().GetAllLot();
                //var x = HRISService.DataCorrector().GetCurrentLot();
                //LotCombobox.SelectedIndex = 0;
                //ReloadDatagrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void LotCombobox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                var selected = (Lot_Number)LotCombobox.SelectedItem;
                if (selected != null)
                {
                    ManualFromDatePicker.DisplayDateStart = selected.Start_date;
                    ManualFromDatePicker.DisplayDateEnd = selected.Finish_date;
                    ManualToDatePicker.DisplayDateStart = selected.Start_date;
                    ManualToDatePicker.DisplayDateEnd = selected.Finish_date;
                    LotFromDatePicker.SelectedDate = selected.Start_date;
                    LotToDatePicker.SelectedDate = selected.Finish_date;
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadDatagrid()
        {
            try
            {
                var lot = (Lot_Number)LotCombobox.SelectedItem;

                if (lot != null)
                {
                    var fromDate = LotFromDatePicker.SelectedDate.Value;
                    var toDate = LotToDatePicker.SelectedDate.Value;
                    var vmList = new List<ManualWorkingViewmodel>();
                    var manualList = BusinessLayerServices.ManualWorkingBL().GetEachEmployeeManualWorkDateList(EmployeeID, fromDate, toDate);

                    for (var day = fromDate.Date; day.Date <= toDate.Date; day = day.AddDays(1))
                    {
                        var dt = new DateTime(day.Year, day.Month, day.Day);
                        var timeIn = manualList.FirstOrDefault(f => f.ManualWorkDateDate == day && f.ManualWorkDateType_ID == "A01");
                        var timeOut = manualList.FirstOrDefault(f => f.ManualWorkDateDate == day && f.ManualWorkDateType_ID == "A02");
                        var item = new ManualWorkingViewmodel
                        {
                            DayofWeek = dt.DayOfWeek.ToString(),
                            ManualDate = day,
                            TimeIn = timeIn != null ? timeIn.Times : null,
                            TimeOut = timeOut != null ? timeOut.Times : null,
                        };
                        vmList.Add(item);
                    }

                    informationDataGrid.ItemsSource = null;
                    informationDataGrid.ItemsSource = vmList;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void informationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var selected = (ManualWorkingViewmodel)informationDataGrid.SelectedItem;
                if (selected == null) return;
                ManualFromDatePicker.SelectedDate = selected.ManualDate;
                ManualToDatePicker.SelectedDate = selected.ManualDate;
                TimeInTextBox.Text = selected.TimeIn.ToString();
                TimeOutTextBox.Text = selected.TimeOut.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(EmployeeID)) throw new Exception("กรุณาระบุรหัสพนักงาน");
                    if (string.IsNullOrEmpty(LotCombobox.Text)) throw new Exception("กรุณาระบุงวด");
                    var lot = (Lot_Number)LotCombobox.SelectedItem;
                    if (lot.Lock_Acc == "2" || lot.Lock_Acc_Labor == "2") throw new Exception("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก");
                    if (lot.Lock_Hr == "2" || lot.Lock_Hr_Labor == "2") throw new Exception("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก");

                    var validFrom = ManualFromDatePicker.SelectedDate.Value;
                    var endDate = ManualToDatePicker.SelectedDate.Value;

                    if (validFrom == null || endDate == null) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
                    if (validFrom > endDate) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
                    
                    string _timeIn = TimeInTextBox.Text.Trim();
                    string _timeOut = TimeOutTextBox.Text.Trim();
                    if (_timeIn == "" && _timeOut == "") throw new Exception("กรุณาระบุเวลา");
                    var timeIn = _timeIn == "" ? (TimeSpan?)null : TimeSpan.Parse(_timeIn);
                    var timeOut = _timeOut == "" ? (TimeSpan?)null : TimeSpan.Parse(_timeOut);
                    if (timeIn > timeOut) throw new Exception("กรุณาระบุเวลาให้ถูกต้อง");
                    var newItemList = new List<ManualWorkDate>();

                    for (var day = validFrom.Date; day.Date <= endDate.Date; day = day.AddDays(1))
                    {
                        if (timeIn != null)
                        {
                            var manualIn = new ManualWorkDate
                            {
                                Employee_ID = EmployeeID,
                                ManualWorkDateDate = day,
                                ManualWorkDateType_ID = "A01",
                                Times = timeIn.Value,
                                Status = true,
                                ModifiedDate = DateTime.Now
                            };
                            var _manualIn = BusinessLayerServices.ManualWorkingBL().GetSingleManualWorkDate(manualIn.ManualWorkDateDate, "A01", manualIn.Employee_ID);
                            if(_manualIn != null) BusinessLayerServices.ManualWorkingBL().DeleteManuaWorkingDate(_manualIn);
                            BusinessLayerServices.ManualWorkingBL().AddManuaWorkingDate(manualIn);
                            newItemList.Add(manualIn);
                        }

                        if (timeOut != null)
                        {
                            var manualOut = new ManualWorkDate
                            {
                                Employee_ID = EmployeeID,
                                ManualWorkDateDate = day,
                                ManualWorkDateType_ID = "A02",
                                Times = timeOut,
                                Status = true,
                                ModifiedDate = DateTime.Now
                            };
                            var _manualOut = BusinessLayerServices.ManualWorkingBL().GetSingleManualWorkDate(manualOut.ManualWorkDateDate, "A02", manualOut.Employee_ID);
                            if(_manualOut != null) BusinessLayerServices.ManualWorkingBL().DeleteManuaWorkingDate(_manualOut);
                            BusinessLayerServices.ManualWorkingBL().AddManuaWorkingDate(manualOut);
                            newItemList.Add(manualOut);
                        }
                    }
                    DataCorrectorStatic<ManualWorkDate>.AddTransactionList("A", "Employee Manual Work Date", null, newItemList);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(EmployeeID)) throw new Exception("กรุณาระบุรหัสพนักงาน");
                if (string.IsNullOrEmpty(LotCombobox.Text)) throw new Exception("กรุณาระบุงวด");
                var lot = (Lot_Number)LotCombobox.SelectedItem;
                if (lot.Lock_Acc == "2" || lot.Lock_Acc_Labor == "2") throw new Exception("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก");
                if (lot.Lock_Hr_Labor == "2") throw new Exception("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก");

                var selected = (ManualWorkingViewmodel)informationDataGrid.SelectedItem;
                if (selected != null && (selected.TimeIn != null || selected.TimeOut != null))
                {
                    var deleteList = new List<ManualWorkDate>();
                    if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        var timeIn = BusinessLayerServices.ManualWorkingBL().GetSingleManualWorkDate(selected.ManualDate.Value, "A01", EmployeeID);
                        var timeOut = BusinessLayerServices.ManualWorkingBL().GetSingleManualWorkDate(selected.ManualDate.Value, "A02", EmployeeID);
                        if (timeIn != null)
                        {
                            BusinessLayerServices.ManualWorkingBL().DeleteManuaWorkingDate(timeIn);
                            deleteList.Add(timeIn);
                        }
                        if (timeOut != null)
                        {
                            BusinessLayerServices.ManualWorkingBL().DeleteManuaWorkingDate(timeOut);
                            deleteList.Add(timeOut);
                        }
                        DataCorrectorStatic<ManualWorkDate>.AddTransactionList("D", "Employee Manual Work Date", deleteList, null);
                        MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        ReloadDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void TimeInTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TimeInTextBox.Text = HRISService.DataCorrector().FormatTime(TimeInTextBox.Text);
        }

        private void TimeOutTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TimeOutTextBox.Text = HRISService.DataCorrector().FormatTime(TimeOutTextBox.Text);
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LotCombobox.SelectedIndex = -1;
                LotFromDatePicker.SelectedDate = null;
                LotToDatePicker.SelectedDate = null;
                //ManualTypeCombobox.SelectedIndex = -1;
                ManualFromDatePicker.SelectedDate = null;
                ManualToDatePicker.SelectedDate = null;
                TimeInTextBox.Text = "";
                TimeOutTextBox.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
