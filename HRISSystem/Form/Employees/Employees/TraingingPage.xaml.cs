﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModelHris;
using HRISSystemBL;
using HRISSystem.Shared;

namespace HRISSystem.Form.Employees.Employees
{
    /// <summary>
    /// Interaction logic for TraingingPage.xaml
    /// </summary>
    public partial class TraingingPage : Page
    {
        string EmployeeID;
        public TraingingPage()
        {
            InitializeComponent();
        }

        public TraingingPage(string employeeID)
        {
            InitializeComponent();
            EmployeeID = employeeID;
            UpdateButton.IsEnabled = !string.IsNullOrEmpty(employeeID);
            if (string.IsNullOrEmpty(EmployeeID)) return;
            ReloadDatagrid();
            TraingCourseCombobox.ItemsSource = BusinessLayerServices.TrainingBL().GetAllTrainingCourseList();
        }

        private void ReloadDatagrid()
        {
            try
            {
                informationDataGrid.ItemsSource = null;
                informationDataGrid.ItemsSource = BusinessLayerServices.TrainingBL().GetTrainingListByEmployeeID(EmployeeID);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(FromDatePicker.Text)) FromDatePicker.SelectedDate = DateTime.Now;
                if (string.IsNullOrEmpty(ToDatePicker.Text)) ToDatePicker.SelectedDate = DateTime.Now;
                if (string.IsNullOrEmpty(EmployeeID)) throw new Exception("กรุณาระบุรหัสพนักงาน");

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(TraingCourseCombobox.Text)) throw new Exception("กรุณาระบุ Training Course");
                    if (FromDatePicker.SelectedDate > ToDatePicker.SelectedDate) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");

                    var newItem = new Training
                    {
                        Employee_ID = EmployeeID,
                        TrainingCourse_ID = TraingCourseCombobox.SelectedValue.ToString(),
                        BeginDate = FromDatePicker.SelectedDate.Value,
                        EndDate = ToDatePicker.SelectedDate.Value,
                        CreatedDate = DateTime.Now
                    };
                    BusinessLayerServices.TrainingBL().AddTraining(newItem);
                    DataCorrectorStatic<Training>.AddTransaction("A", "Employee Training", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var oldItem = (Training)informationDataGrid.SelectedItem;
                if (oldItem != null)
                {
                    if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        BusinessLayerServices.TrainingBL().DeleteTraining(oldItem);
                        DataCorrectorStatic<Training>.AddTransaction("D", "Employee Training", oldItem, null);
                        MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        ReloadDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddCourseButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Trainings.CourseWindow c = new Trainings.CourseWindow();
                c.ShowDialog();
                TraingCourseCombobox.ItemsSource = BusinessLayerServices.TrainingBL().GetAllTrainingCourseList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
