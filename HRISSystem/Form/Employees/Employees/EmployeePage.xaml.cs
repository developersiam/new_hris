﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using HRISSystemBL;
using DomainModelHris;
using HRISSystem.Shared;
using System.Linq;
using HRISSystem.Helper;

namespace HRISSystem.Form.Employees.Employees
{
    /// <summary>
    /// Interaction logic for Employee_Page.xaml
    /// </summary>
    public partial class EmployeePage : Page
    {
        string username = SingletonConfiguration.getInstance().Username.ToLower();
        string deptCode = SingletonConfiguration.getInstance().DepartmentCode;
        public EmployeePage()
        {
            InitializeComponent();
            //Navigate to first tab
            if (deptCode == "ACC") EmployeeTabcontrol.SelectedIndex = 5;
            else EmployeeTabcontrol.SelectedIndex = 0;
            //Refresh first page
            var _staffType = StaffTypeCombobox.SelectedValue == null ? "" : StaffTypeCombobox.SelectedValue.ToString();
            if (deptCode == "ACC") EmployeeFrame.Navigate(new PayrollPage(EmployeeIDTextBox.Text, _staffType));
            else EmployeeFrame.Navigate(new ShiftPage(EmployeeIDTextBox.Text));

            if (deptCode == "HRM") PayrollTab.Visibility = Visibility.Collapsed;
            if (deptCode == "ACC") ShiftTab.Visibility = Visibility.Collapsed;
            if (deptCode == "ACC") ContractTab.Visibility = Visibility.Collapsed;
            if (deptCode == "ACC") TrainingTab.Visibility = Visibility.Collapsed;
            if (deptCode == "ACC") ManualDateTab.Visibility = Visibility.Collapsed;
            if (deptCode == "ACC") NotPayrollTab.Visibility = Visibility.Collapsed;
        }

        private void BindCombobox()
        {
            if (DepartmentCombobox.Items.Count == 0)
            {
                DepartmentCombobox.ItemsSource = BusinessLayerServices.DeptCodeBL().GetAllDepartment();
            }
            if (TitleCombobox.Items.Count == 0)
            {
                TitleCombobox.ItemsSource = BusinessLayerServices.PersonBL().GetAllTitlenameList();
            }
            if (StaffTypeCombobox.Items.Count == 0)
            {
                StaffTypeCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
            }
            if (StaffStatusCombobox.Items.Count == 0)
            {
                StaffStatusCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffStatusList();
            }
        }

        private void EmployeeTabcontrol_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                // ... Get TabControl reference.
                var item = sender as TabControl;
                var selected = item.SelectedItem as TabItem;
                int EmployeeTabIndex = EmployeeTabcontrol.SelectedIndex;
                if ((sender as TabControl) != null)
                {
                    if ((sender as TabControl).SelectedIndex == 0) //กะการทำงาน (Shift Control)
                    {
                        EmployeeFrame.Navigate(new ShiftPage(EmployeeIDTextBox.Text));
                    }
                    //else if ((sender as TabControl).SelectedIndex == 1) //การรับตำแหน่ง (Posision)
                    //{
                    //    //PersonFrame.Navigate(new Persons.Education_Page());
                    //}
                    else if ((sender as TabControl).SelectedIndex == 1) //การทำสัญญา (Contract)
                    {
                        EmployeeFrame.Navigate(new ContractPage(EmployeeIDTextBox.Text));
                    }
                    else if ((sender as TabControl).SelectedIndex == 2) //ประวัติการฝึกอบรม (Training)
                    {
                        EmployeeFrame.Navigate(new TraingingPage(EmployeeIDTextBox.Text));
                    }
                    else if ((sender as TabControl).SelectedIndex == 3) //บันทึกการทำงานแทน (Manual Working)
                    {
                        EmployeeFrame.Navigate(new ManualWorkingPage(EmployeeIDTextBox.Text));
                    }
                    else if ((sender as TabControl).SelectedIndex == 4) //บันทึกการไม่นำเข้า Payroll (Payroll Data Setup)
                    {
                        EmployeeFrame.Navigate(new PayrollExceptPage(EmployeeIDTextBox.Text));
                    }
                    else if ((sender as TabControl).SelectedIndex == 5) //ข้อมูลทางบัญชี (Payroll)
                    {
                        string staffType = StaffTypeCombobox.SelectedIndex < 0 ? "" : StaffTypeCombobox.SelectedValue.ToString();
                        if (staffType == "1" && username == "m.tanainan")
                        {
                            MessageBox.Show("Permission required for staff type.", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                        else EmployeeFrame.Navigate(new PayrollPage(EmployeeIDTextBox.Text, staffType));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EmployeeIDTextBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left) Search();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        public void Search()
        {
            try
            {
                SearchEmployee_Window window = new SearchEmployee_Window();
                window.ShowDialog();
                if (window.selectedEmployee != null) BindSelectedEmployee(window.selectedEmployee);
                UpdateButton.IsEnabled = !string.IsNullOrEmpty(EmployeeIDTextBox.Text) || window.selectedEmployee != null;
                SearchPositionButton.IsEnabled = !string.IsNullOrEmpty(EmployeeIDTextBox.Text) || window.selectedEmployee != null;
                if (deptCode == "ACC") UpdateButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindSelectedEmployee(Employee i)
        {
            try
            {
                if (i.Staff_type == 1 && username == "m.tanainan")
                {
                    MessageBox.Show("Permission required for staff type.", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                BindCombobox();
                EmployeeIDTextBox.Text = i.Employee_ID;
                PersonIDTextBox.Text = i.Person_ID;
                FingerIDTextBox.Text = i.FingerScanID;
                DepartmentCombobox.SelectedValue = i.DeptCode;
                TitleCombobox.SelectedValue = i.Person.TitleName_ID;
                NameTHTextBox.Text = i.Person.FirstNameTH;
                LastnameTHTextBox.Text = i.Person.LastNameTH;
                NameENTextBox.Text = i.Person.FirstNameEN;
                LastnameENTextBox.Text = i.Person.LastNameEN;
                StartDatePicker.SelectedDate = BusinessLayerServices.EmployeeBL().GetMinStartDateByPerson(i.Person_ID);
                StartEmployeeDatePicker.SelectedDate = i.StartDate;
                EndDatePicker.SelectedDate = i.EndDate;
                StaffTypeCombobox.SelectedValue = i.Staff_type;
                StaffStatusCombobox.SelectedValue = i.Staff_status;
                ShiftWorkTextBox.Text = i.Shift == null ? "ยังไม่ได้ระบุ default shift" : i.Shift.EndTime + " - " + i.Shift.EndTime;
                EmailTextBox.Text = i.Email;

                if (i.Position != null)
                {
                    PositionIDTextBox.Text = i.Position_ID;
                    PositionTextBox.Text = i.Position.PositionNameTH;
                    PositionTextBox.ToolTip = i.Position.PositionNameTH;
                }

                PositionTextBox.IsEnabled = i.Staff_status.Value == 1;
                SearchPositionButton.IsEnabled = i.Staff_status.Value == 1;
                EndDatePicker.IsEnabled = i.Staff_status.Value == 1;
                StaffStatusCombobox.IsEnabled = i.Staff_status.Value == 1;
                StaffTypeCombobox.IsEnabled = i.Staff_status.Value == 1;
                EmailTextBox.IsEnabled = i.Staff_status.Value == 1;
                UpdateButton.IsEnabled = i.Staff_status.Value == 1; // enable only working employee

                //Navigate to first tab
                if (deptCode == "ACC") EmployeeTabcontrol.SelectedIndex = 5;
                else EmployeeTabcontrol.SelectedIndex = 0;
                //Refresh first page
                var _staffType = StaffTypeCombobox.SelectedValue == null ? "" : StaffTypeCombobox.SelectedValue.ToString();
                if (deptCode == "ACC") EmployeeFrame.Navigate(new PayrollPage(EmployeeIDTextBox.Text, _staffType));
                else EmployeeFrame.Navigate(new ShiftPage(EmployeeIDTextBox.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearControl()
        {
            try
            {
                EmployeeIDTextBox.Text = "";
                PersonIDTextBox.Text = "";
                FingerIDTextBox.Text = "";
                DepartmentCombobox.Text = "";
                TitleCombobox.Text = "";
                NameTHTextBox.Text = "";
                LastnameTHTextBox.Text = "";
                NameENTextBox.Text = "";
                LastnameENTextBox.Text = "";
                StartDatePicker.SelectedDate = null;
                StartEmployeeDatePicker.Text = "";
                EndDatePicker.Text = "";
                StaffTypeCombobox.Text = "";
                StaffStatusCombobox.Text = "";
                EmailTextBox.Text = "";
                PositionIDTextBox.Text = "";
                PositionTextBox.Text = "";

                //Navigate to first tab
                if (deptCode == "ACC") EmployeeTabcontrol.SelectedIndex = 5;
                else EmployeeTabcontrol.SelectedIndex = 0;
                //Refresh first page
                var _staffType = StaffTypeCombobox.SelectedValue == null ? "" : StaffTypeCombobox.SelectedValue.ToString();
                if (deptCode == "ACC") EmployeeFrame.Navigate(new PayrollPage(EmployeeIDTextBox.Text, _staffType));
                else EmployeeFrame.Navigate(new ShiftPage(EmployeeIDTextBox.Text));

                UpdateButton.IsEnabled = false;
                SearchPositionButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearControl();
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBoxHelper.Question("ต้องการบันทึกข้อมูลหรือไม่") == MessageBoxResult.Yes)
                {
                    var staffStatus = (StaffStatu)StaffStatusCombobox.SelectedItem;
                    var staffType = (StaffType)StaffTypeCombobox.SelectedItem;

                    if (staffStatus == null)
                        throw new Exception("กรุณาระบุสถานะพนักงาน");

                    if (staffType == null)
                        throw new Exception("กรุณาระบุประเภทของพนักงาน");

                    if (string.IsNullOrEmpty(EmployeeIDTextBox.Text) ||
                        string.IsNullOrEmpty(PersonIDTextBox.Text) ||
                        string.IsNullOrEmpty(FingerIDTextBox.Text))
                        throw new Exception("กรุณาระบุรหัสพนักงาน");

                    if (string.IsNullOrEmpty(PositionTextBox.Text))
                        throw new Exception("กรุณาระบุ Position");

                    // Update Some field
                    var oldItem = BusinessLayerServices.EmployeeBL().GetEmployeeByEmployeeID(EmployeeIDTextBox.Text);
                    // End staff status
                    if (staffStatus.StaffStatusID == 2)
                    {
                        var endDate = EndDatePicker.SelectedDate;
                        if (endDate == null) throw new Exception("กรุณาระบุวันที่สิ้นสุดการทำงาน");
                        var existingOTList = BusinessLayerServices.OTBL().GetExistingOT(EmployeeIDTextBox.Text, EndDatePicker.SelectedDate.Value);
                        if (existingOTList.Any()) throw new Exception("พบข้อมูลการบันทึกโอทีหลังวันสิ้นสุดการทำงาน กรุณาตรวจสอบการทำโอทีก่อนสิ้นสุดการทำงาน");

                        var result = MessageBoxResult.No;
                        if (oldItem.Staff_type == 1) result = MessageBox.Show("ต้องการให้คงบัญชีถึงสิ้นเดือนใช่หรือไม่ ? (คงบัญชีถึงสิ้นเดือนหมายถึง ระบบเงินเดือนจะคำนวนเงินเดือนตั้งแต่วันที่ 1-สิ้นเดือนที่มาทำงานจริงรวมทั้งคำนวนวันทำโอทีตั้งแต่ 26-25 ให้ หากไม่แน่ใจกรุณาตรวจสอบกับฝ่ายบัญชีอีกครั้ง) \n หากต้องการให้คงบัญชีถึงสิ้นเดือนให้กดปุ่ม Yes , หากไม่ต้องการให้คงบัญชีถึงส้ินเดือนให้กดปุ่ม No , หากไม่แน่ใจและต้องการยกเลิกเพื่อสอบถามบัญชีให้กดปุ่ม Cancel", "Confirmation!", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                        var payroll = BusinessLayerServices.PayrollBL().GetSinglePayrollByID(oldItem.Noted);
                        if (result != MessageBoxResult.Cancel)
                        {
                            if (result == MessageBoxResult.Yes) payroll.Staff_status = "3"; //คงบัญชี
                            else if (result == MessageBoxResult.No) payroll.Staff_status = "2"; //ไม่คงบัญชี
                            BusinessLayerServices.PayrollBL().UpdatePayrollEmpoyee(payroll);
                        }
                    }

                    var newItem = BusinessLayerServices.EmployeeBL().GetEmployeeByEmployeeID(EmployeeIDTextBox.Text);
                    newItem.Position = null;
                    newItem.Position_ID = PositionIDTextBox.Text;
                    newItem.EndDate = EndDatePicker.SelectedDate;
                    newItem.StaffType = null; // Clear Cascade
                    newItem.Staff_type = staffType.StaffTypeID;
                    newItem.Staff_status = staffStatus.StaffStatusID;
                    newItem.Email = EmailTextBox.Text;
                    newItem.ModifiedDate = DateTime.Now;
                    BusinessLayerServices.EmployeeBL().UpdateEmployee(newItem);
                    DataCorrectorStatic<Employee>.AddTransaction("U", "Employee Setting", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchPositionButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var dept = (Department)DepartmentCombobox.SelectedItem;
                if (dept == null) throw new Exception("กรุณาเลือกแผนก");

                var p = new PositionWindow(dept.DeptCode);
                p.ShowDialog();
                if (p.selected == null) return;
                PositionIDTextBox.Text = p.selected.Position_ID;
                PositionTextBox.Text = p.selected.PositionNameTH;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var p = new EmployeeWindow();
                p.ShowDialog();

                if (string.IsNullOrEmpty(p.recentlyAddEmployeeID))
                    return;

                var emp = BusinessLayerServices.EmployeeBL()
                    .GetEmployeeByEmployeeID(p.recentlyAddEmployeeID);

                BindSelectedEmployee(emp);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }

}
