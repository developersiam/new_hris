﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModelHris;
using HRISSystemBL;
using HRISSystem.Shared;

namespace HRISSystem.Form.Employees.Employees
{
    /// <summary>
    /// Interaction logic for ShiftPage.xaml
    /// </summary>
    public partial class ShiftPage : Page
    {
        string EmployeeID;
        public ShiftPage()
        {
            InitializeComponent();
        }
        public ShiftPage(string employeeID)
        {
            InitializeComponent();
            EmployeeID = employeeID;
            UpdateButton.IsEnabled = !string.IsNullOrEmpty(employeeID);
            if (string.IsNullOrEmpty(EmployeeID)) return;
            ReloadDatagrid();
            ShiftCombobox.ItemsSource = BusinessLayerServices.ShiftBL().GetAllShiftList();
        }

        private void ReloadDatagrid()
        {
            try
            {
                informationDataGrid.ItemsSource = null;
                informationDataGrid.ItemsSource = BusinessLayerServices.ShiftBL().GetShiftControlListByEmployeeID(EmployeeID);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(ShiftDatePicker.Text)) ShiftDatePicker.SelectedDate = DateTime.Now;
                if (string.IsNullOrEmpty(ShiftToDatePicker.Text)) ShiftToDatePicker.SelectedDate = DateTime.Now;
                if (string.IsNullOrEmpty(EmployeeID)) throw new Exception("กรุณาระบุรหัสพนักงาน");

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(ShiftCombobox.Text)) throw new Exception("กรุณาเลือกกะที่ต้องการบันทึก");
                    if (ShiftDatePicker.SelectedDate > ShiftToDatePicker.SelectedDate) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");

                    var validFrom = ShiftDatePicker.SelectedDate.Value;
                    var endDate = ShiftToDatePicker.SelectedDate.Value;
                    var newItemList = new List<ShiftControl>();

                    for (var day = validFrom.Date; day.Date <= endDate.Date; day = day.AddDays(1))
                    {
                        var item = new ShiftControl
                        {
                            Employee_ID = EmployeeID,
                            Shift_ID = ((Shift)ShiftCombobox.SelectedItem).Shift_ID,
                            ValidFrom = day,
                            EndDate = day,
                            Noted = RemarkTextBox.Text,
                            CreatedDate = DateTime.Now
                        };
                        BusinessLayerServices.ShiftBL().AddShiftControl(item);
                        newItemList.Add(item);
                    }
                    DataCorrectorStatic<ShiftControl>.AddTransactionList("A", "Employee Shift", null, newItemList);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var oldItem = (ShiftControl)informationDataGrid.SelectedItem;
                if (oldItem != null)
                {
                    if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        BusinessLayerServices.ShiftBL().DeleteShiftControl(oldItem);
                        DataCorrectorStatic<ShiftControl>.AddTransaction("D", "Employee Shift", oldItem, null);
                        MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        ReloadDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddShiftButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                References.Shift_Window w = new References.Shift_Window();
                w.ShowDialog();
                ShiftCombobox.ItemsSource = BusinessLayerServices.ShiftBL().GetAllShiftList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
