﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DomainModelHris;
using HRISSystemBL;
using HRISSystem.Shared;

namespace HRISSystem.Form.Employees.Employees
{
    /// <summary>
    /// Interaction logic for ContractPage.xaml
    /// </summary>
    public partial class ContractPage : Page
    {
        public ContractPage()
        {

        }

        string EmployeeID;
        public ContractPage(string employeeID)
        {
            InitializeComponent();
            EmployeeID = employeeID;
            AddButton.IsEnabled = !string.IsNullOrEmpty(EmployeeID);
            if (string.IsNullOrEmpty(EmployeeID)) return;
            ReloadDatagrid();
            ContractTypeCombobox.ItemsSource = BusinessLayerServices.ContractBL().GetAllContractTypeList();
        }

        private void ReloadDatagrid()
        {
            try
            {
                Clear();
                informationDataGrid.ItemsSource = null;
                informationDataGrid.ItemsSource = BusinessLayerServices.ContractBL().GetContractListByEmployeeID(EmployeeID);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void informationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BindSelectedControl();
        }

        private void EditDetailButton_Click(object sender, RoutedEventArgs e)
        {
            BindSelectedControl();
        }

        private void BindSelectedControl()
        {
            try
            {
                var s = (EmployeeContract)informationDataGrid.SelectedItem;
                if (s != null)
                {
                    ContractTypeCombobox.IsEnabled = false;
                    ContractTypeCombobox.SelectedValue = s.ContractType_ID;
                    FromDatePicker.SelectedDate = s.StartContract;
                    EndDateDatePicker.SelectedDate = s.EndContract;
                }
                UpdateButton.IsEnabled = s != null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(EmployeeID)) throw new Exception("กรุณาระบุรหัสพนักงาน");
                    if (string.IsNullOrEmpty(ContractTypeCombobox.Text)) throw new Exception("กรุณาเลือกประเภทสัญญา");
                    if (string.IsNullOrEmpty(FromDatePicker.Text) || string.IsNullOrEmpty(EndDateDatePicker.Text)) throw new Exception("กรุณาระบุวันที่");

                    var oldItem = BusinessLayerServices.ContractBL().GetSingleEmployeeContract(EmployeeID, ContractTypeCombobox.SelectedValue.ToString());
                    var newItem = new EmployeeContract
                    {
                        Employee_ID = oldItem.Employee_ID,
                        ContractType_ID = oldItem.ContractType_ID,
                        StartContract = FromDatePicker.SelectedDate,
                        EndContract = EndDateDatePicker.SelectedDate,
                        CreatedDate = oldItem.CreatedDate,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.ContractBL().UpdateContract(newItem);
                    DataCorrectorStatic<EmployeeContract>.AddTransaction("U", "Employee Contract", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(EmployeeID)) throw new Exception("กรุณาระบุรหัสพนักงาน");
                    if (string.IsNullOrEmpty(ContractTypeCombobox.Text)) throw new Exception("กรุณาเลือกประเภทสัญญา");
                    if (string.IsNullOrEmpty(FromDatePicker.Text) || string.IsNullOrEmpty(EndDateDatePicker.Text)) throw new Exception("กรุณาระบุวันที่");

                    var newItem = new EmployeeContract
                    {
                        Employee_ID = EmployeeID,
                        ContractType_ID = ((ContractType)ContractTypeCombobox.SelectedItem).ContractType_ID,
                        StartContract = FromDatePicker.SelectedDate,
                        EndContract = EndDateDatePicker.SelectedDate,
                        CreatedDate = DateTime.Now
                    };
                    BusinessLayerServices.ContractBL().AddContract(newItem);
                    DataCorrectorStatic<EmployeeContract>.AddTransaction("A", "Employee Contract", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var oldItem = (EmployeeContract)informationDataGrid.SelectedItem;
                if (oldItem != null)
                {
                    if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        BusinessLayerServices.ContractBL().DeleteContract(oldItem);
                        DataCorrectorStatic<EmployeeContract>.AddTransaction("D", "Employee Contract", oldItem, null);
                        MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        ReloadDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddContractTypeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ContractTypeWindow w = new ContractTypeWindow();
                w.ShowDialog();
                ContractTypeCombobox.ItemsSource = BusinessLayerServices.ContractBL().GetAllContractTypeList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Clear()
        {
            ContractTypeCombobox.IsEnabled = true;
            ContractTypeCombobox.Text = "";
            FromDatePicker.SelectedDate = null;
            EndDateDatePicker.SelectedDate = null;
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }
    }
}
