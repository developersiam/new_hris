﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DomainModelHris;
using DomainModelPayroll;
using HRISSystemBL;
using HRISSystem.Shared;

namespace HRISSystem.Form.Walefare
{
    /// <summary>
    /// Interaction logic for ManualWorkingDateWindow.xaml
    /// </summary>
    public partial class ManualWorkingDateWindow : Window
    {
        public Lot_Number lotNumber;

        public ManualWorkingDateWindow()
        {
            InitializeComponent();
            ManualTypeCombobox.ItemsSource = BusinessLayerServices.ManualWorkingBL().GetManualWorkTypeList();
            ManualDatePicker.IsEnabled = false;
        }

        private void TimeTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TimeTextBox.Text = HRISService.DataCorrector().FormatTime(TimeTextBox.Text);
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var manualType = (ManualWorkDateType)ManualTypeCombobox.SelectedItem;
                    var manualDate = ManualDatePicker.SelectedDate;
                    var employeeID = EmployeeIDTextBox.Text;
                    var times = TimeTextBox.Text;
                    if (string.IsNullOrEmpty(LotTextBox.Text)) throw new Exception("กรุณาเลือกงวด");
                    if (lotNumber.Lock_Acc == "2" || lotNumber.Lock_Acc_Labor == "2") throw new Exception("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก");
                    if (lotNumber.Lock_Hr_Labor == "2") throw new Exception("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก");
                    if (manualType == null) throw new Exception("กรุณาระบุประเภทการบันทึกเวลา");
                    if (manualDate == null) throw new Exception("กรุณาระบุวันที่");
                    if (string.IsNullOrEmpty(times)) throw new Exception("กรุณาระบุเวลา");

                    var oldItem = BusinessLayerServices.ManualWorkingBL().GetSingleManualWorkDate(manualDate.Value, manualType.ManualWorkDateType_ID, employeeID);
                    if (oldItem != null) BusinessLayerServices.ManualWorkingBL().DeleteManuaWorkingDate(oldItem);
                    var newItem = new ManualWorkDate
                    {
                        Employee_ID = EmployeeIDTextBox.Text,
                        ManualWorkDateDate = ManualDatePicker.SelectedDate.Value,
                        ManualWorkDateType_ID = ManualTypeCombobox.SelectedValue.ToString(),
                        Times = TimeSpan.Parse(times),
                        Status = true,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.ManualWorkingBL().AddManuaWorkingDate(newItem);
                    DataCorrectorStatic<ManualWorkDate>.AddTransaction("A", "Manual Workdate", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ManualTypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                var manualType = (ManualWorkDateType)ManualTypeCombobox.SelectedItem;
                if (manualType != null)
                {
                    var manualDate = ManualDatePicker.SelectedDate;
                    var employeeID = EmployeeIDTextBox.Text;
                    if (manualDate == null) throw new Exception("กรุณาเลือกวันที่");
                    //var manualDate = TimeTextBox.selecte)) throw new Exception("กรุณาระบุเวลา");
                    var manual = BusinessLayerServices.ManualWorkingBL().GetSingleManualWorkDate(manualDate.Value, manualType.ManualWorkDateType_ID, employeeID);
                    TimeTextBox.Text = manual == null ? "" : manual.Times.Value.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
