﻿using HRISSystem.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DomainModelHris;
using HRISSystemBL;

namespace HRISSystem.Form.Walefare
{
    /// <summary>
    /// Interaction logic for OTWindow.xaml
    /// </summary>
    public partial class OTWindow : Window
    {
        public OTViewModel selectedOT;
        public OTWindow()
        {
            InitializeComponent();
        }
        public OTWindow(OTViewModel o, string formAction)
        {
            InitializeComponent();
            BindControl(o.Employee_ID);
            ReloadDatagrid(o);
            InformationDataGrid.Columns[6].Visibility = formAction == "Select" ? Visibility.Visible : Visibility.Collapsed;
            InformationDataGrid.Columns[7].Visibility = formAction == "Delete" ? Visibility.Visible : Visibility.Collapsed;
        }

        private void ReloadDatagrid(OTViewModel o)
        {
            var otList = BusinessLayerServices.OTBL().GetHolidayOT(o.Employee_ID, o.OTDATE.Value);
            var otView = new List<OTViewModel>();
            foreach (var item in otList)
            {
                otView.Add(new OTViewModel
                {
                    Employee_ID = item.Employee_ID,
                    FingerScanID = item.Employee.FingerScanID,
                    OTDATE = item.OTDate,
                    Holiday = item.OTType == "H" ? item.NcountOT : 0,
                    OTHoliday = item.OTType == "S" ? item.NcountOT : 0,
                    Remark = item.Remark == null ? "" : item.Remark,
                    PayrollLot = item.PayrollLot,
                    OTType = item.OTType,
                    RequestTypeName = item.OTRequestType.RequestFlagName,
                    RequestType = item.RequestType
                });
            }
            InformationDataGrid.ItemsSource = null;
            InformationDataGrid.ItemsSource = otView;
        }

        private void BindControl(string employee_ID)
        {
            var e = BusinessLayerServices.EmployeeBL().GetEmployeeByEmployeeID(employee_ID);
            EmployeeIDTextBox.Text = e.Employee_ID;
            TitleTextBox.Text = e.Person.TitleName.TitleNameTH;
            NameTHTextBox.Text = e.Person.FirstNameTH;
            LastnameTHTextBox.Text = e.Person.LastNameTH;
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            SelectOT();
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            SelectOT();
        }

        private void SelectOT()
        {
            try
            {
                selectedOT = (OTViewModel)InformationDataGrid.SelectedItem;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var selected = (OTViewModel)InformationDataGrid.SelectedItem;
                if (MessageBox.Show("ยืนยันการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (selected != null)
                    {
                        if (string.IsNullOrEmpty(EmployeeIDTextBox.Text)) throw new Exception("กรุณาระบุพนักงาน");
                        var _lotNumber = BusinessLayerServices.PayrollBL().GetLotByDateRange(selected.OTDATE.Value, selected.OTDATE.Value).FirstOrDefault();
                        if (_lotNumber.Lock_Acc == "2" || _lotNumber.Lock_Acc_Labor == "2") throw new Exception("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก");
                        if (_lotNumber.Lock_Hr_Labor == "2") throw new Exception("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก");

                        var item = BusinessLayerServices.OTBL().GetSingleOT(selected.Employee_ID, selected.OTDATE, selected.OTType, selected.RequestType);
                        BusinessLayerServices.OTBL().DeleteOT(item);
                        MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
