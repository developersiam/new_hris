﻿using DomainModelHris;
using HRISSystem.Form.Employees;
using HRISSystem.Shared;
using HRISSystem.ViewModel;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HRISSystem.Form.Walefare
{
    /// <summary>
    /// Interaction logic for LeaveSetupPage.xaml
    /// </summary>
    public partial class LeaveSetupPage : Page
    {
        public LeaveSetupPage()
        {
            InitializeComponent();
            LeaveTypeCombobox.ItemsSource = BusinessLayerServices.LeaveTypeBL().GetAllLeaveType().Where(w => w.LeaveType_ID == "L001").ToList();
            LeaveTypeCombobox.SelectedIndex = 0;
            RecordTypeCombobox.ItemsSource = RecordList();
            BindStatusList();
            DepartmentCombobox.ItemsSource = DepartmentList();
            BindStaffType();
        }

        private List<StaffType> RecordList()
        {
            var result = new List<StaffType>();
            result.Insert(0, new StaffType { StaffTypeID = 0, Description = "Record by Department" });
            result.Insert(1, new StaffType { StaffTypeID = 1, Description = "Record by Employee" });
            return result;
        }

        private void BindStatusList()
        {
            var result = new List<StaffType>();
            //result.Insert(0, new StaffType { StaffTypeID = 0, Description = "-- All --" });
            result.Insert(0, new StaffType { StaffTypeID = 1, Description = "Available" });
            result.Insert(1, new StaffType { StaffTypeID = 2, Description = "Expired" });
            
            StatusCombobox.ItemsSource = result;
            StatusCombobox.SelectedIndex = 0;
        }

        private List<Department> DepartmentList()
        {
            var result = new List<Department>();
            result.Add(new Department { DeptCode = "", DeptName = "All Department" });
            result.AddRange(BusinessLayerServices.DeptCodeBL().GetAllDepartment());
            return result;
        }

        private void BindStaffType()
        {
            var staffList = new List<StaffType>();
            //staffList.Add(new StaffType { StaffTypeID = 0, Description = "-- All --" });
            //staffList.Add(new StaffType { StaffTypeID = 1, Description = "พนักงานประจำ" });
            //staffList.Add(new StaffType { StaffTypeID = 2, Description = "พนักงานรายวันประจำ" });
            staffList = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
            StafftypeCombobox.ItemsSource = staffList;
        }

        private void RecordTypeCombobox_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                var selected = (StaffType)RecordTypeCombobox.SelectedItem;
                if (selected == null) return;
                DepartmentCombobox.SelectedIndex = -1;
                EmployeeIDTextBox.Text = "";
                NameTHTextBox.Text = "";
                DepartmentCombobox.Visibility = selected.StaffTypeID == 0 ? Visibility.Visible : Visibility.Collapsed;
                StafftypeCombobox.IsEnabled = selected.StaffTypeID == 0; //Enable when record by department
                StafftypeCombobox.Visibility = Visibility.Visible;
                StafftypeCombobox.SelectedIndex = -1;
                RecordByEmployeeStackPanel.Visibility = selected.StaffTypeID == 1 ? Visibility.Visible : Visibility.Collapsed;
                EnableDetail(false);
                InformationDataGrid.ItemsSource = null;
                StatusCombobox.SelectedIndex = 2;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DepartmentCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var dept = (Department)DepartmentCombobox.SelectedItem;
            StafftypeCombobox.SelectedIndex = 0;
            UpdateButton.IsEnabled = dept != null;
            EnableDetail(dept != null);
            ReloadDatagrid();
        }

        private void EmployeeIDTextBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                var selected = SearchEmployee();
                if (selected != null)
                {
                    EmployeeIDTextBox.Text = selected[0];
                    NameTHTextBox.Text = selected[1];
                    DepartmentCombobox.SelectedValue = selected[2];
                    StafftypeCombobox.SelectedValue = selected[3]; // == "1" ? 1 : 2;
                    YearCombobox.SelectedIndex = -1;
                    ValidFromDatePicker.SelectedDate = null;
                    ValidToDatePicker.SelectedDate = null;
                    LeaveAmountTextBox.Text = "";
                    ReloadDatagrid();
                }
                UpdateButton.IsEnabled = !string.IsNullOrEmpty(EmployeeIDTextBox.Text) || selected != null;
                EnableDetail(!string.IsNullOrEmpty(EmployeeIDTextBox.Text) || selected != null);
            }
        }

        public string[] SearchEmployee()
        {
            try
            {
                var window = new SearchEmployee_Window();
                window.ShowDialog();
                if (window.selectedEmployee != null)
                {
                    var s = window.selectedEmployee;
                    string[] result = { s.Employee_ID, s.Person.TitleName.TitleNameTH + " " + s.Person.FirstNameTH + " " + s.Person.LastNameTH, s.DeptCode, s.Staff_type.ToString()};
                    return result;
                }
                else return null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }
        }

        private void EnableDetail(bool isEnable)
        {
            LeaveAmountTextBox.IsEnabled = isEnable;
            ValidFromDatePicker.IsEnabled = false;
            ValidToDatePicker.IsEnabled = false;
            BindYearCombobox();
            YearCombobox.IsEnabled = isEnable;
        }

        private void BindYearCombobox()
        {
            if (YearCombobox.Items.Count <= 0)
            {
                var yearList = new List<string>();
                for (int i = DateTime.Now.Year; i > DateTime.Now.Year - 3; i--)
                {
                    yearList.Add(i.ToString() + "/" + "1");
                    yearList.Add(i.ToString() + "/" + "2");
                }
                YearCombobox.ItemsSource = yearList;
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันการบันทึกหรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;

                var rec = (StaffType)RecordTypeCombobox.SelectedItem;
                var year = YearCombobox.SelectedValue == null ? (int?)null : Convert.ToInt16(YearCombobox.Text.Substring(0, 4));
                var leaveType = (LeaveType)LeaveTypeCombobox.SelectedItem;
                decimal? leaveAmo = LeaveAmountTextBox.Text.Trim() == "" ? (decimal?)null : Convert.ToDecimal(LeaveAmountTextBox.Text);
                var dept = (Department)DepartmentCombobox.SelectedItem;
                var from = ValidFromDatePicker.SelectedDate;
                var to = ValidToDatePicker.SelectedDate;
                var staff = (StaffType)StafftypeCombobox.SelectedItem;
                
                if (leaveType == null || leaveAmo == null || year == null || from == null || to == null) throw new Exception("โปรดระบุข้อมูลให้ครบถ้วน");
                if (from > to) throw new Exception("โปรดระบุวันที่ให้ถูกต้อง");
                if ((leaveAmo > 7) || (leaveAmo < 0)) throw new Exception("โปรดระบุจำนวนวันลาให้ถูกต้อง (ระหว่าง 0-7)");

                // By Employee
                if (rec.StaffTypeID == 1 && EmployeeIDTextBox.Text == "") throw new Exception("โปรดระบุข้อมูลให้ครบถ้วน");
                // By department
                else if (rec.StaffTypeID == 0 && (dept == null || staff == null)) throw new Exception("โปรดระบุข้อมูลให้ครบถ้วน");

                var employeeList = BusinessLayerServices.LeaveTypeBL().GetLeaveSetupUpdateEmployeeList(dept.DeptCode, EmployeeIDTextBox.Text, staff.StaffTypeID);

                foreach (var emp in employeeList)
                {
                    var leave = new LeaveSetup();
                    //n.DeptCode = dept.DeptCode;
                    leave.Employee_ID = emp.Employee_ID;
                    //n.Name = NameTHTextBox.Text;
                    leave.LeaveType_ID = leaveType.LeaveType_ID;
                    //n.LeaveTypeNameTH = leave.LeaveTypeNameTH;
                    leave.Year = year.Value;
                    leave.LeaveAmount = leaveAmo;
                    leave.ValidFrom = from.Value;
                    leave.ValidTo = to.Value;
                    leave.ModifiedDate = DateTime.Now;
                    leave.ModifiedBy = SingletonConfiguration.getInstance().Username;
                    BusinessLayerServices.LeaveTypeBL().UpdateLeaveSetup(leave);
                }

                MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                ReloadDatagrid();
                //Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void StafftypeCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void StatusCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                var searchBy = (StaffType)RecordTypeCombobox.SelectedItem;
                var status = (StaffType)StatusCombobox.SelectedItem;
                var dept = (Department)DepartmentCombobox.SelectedItem;
                var employeeID = EmployeeIDTextBox.Text;
                var staff = (StaffType)StafftypeCombobox.SelectedItem;

                var leaveSetupList = new List<LeaveSetup>();

                if (searchBy == null) return;
                //Filter by Employee
                if (searchBy.StaffTypeID == 1 && status != null && employeeID != "")
                    leaveSetupList = BusinessLayerServices.LeaveTypeBL().GetLeaveSetupList("", employeeID, status.StaffTypeID, 0);
                //Filter by Department
                else if (searchBy.StaffTypeID == 0 && status != null && dept != null)
                    leaveSetupList = BusinessLayerServices.LeaveTypeBL().GetLeaveSetupList(dept.DeptCode, "", status.StaffTypeID, staff.StaffTypeID);
                else return;

                var showList = SetExpired(leaveSetupList);
                showList = showList.OrderBy(o => o.LeaveSetups.Employee_ID).ThenBy(o => o.LeaveSetups.ValidTo).ToList();

                TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", showList.Count);
                
                InformationDataGrid.ItemsSource = showList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private List<LeaveSetupViewModel> SetExpired(List<LeaveSetup> leaveSetupList)
        {
            var result = new List<LeaveSetupViewModel>();
            leaveSetupList.ForEach(f =>
            {
                var leaveSetupVM = new LeaveSetupViewModel();
                leaveSetupVM.LeaveSetups = f;
                leaveSetupVM.IsExpired = f.ValidTo < DateTime.Now; //True if expired
                result.Add(leaveSetupVM);
            });
            result = result.OrderBy(o => o.LeaveSetups.Year).ThenBy(t => t.LeaveSetups.Employee_ID).ThenBy(t => t.LeaveSetups.LeaveType_ID).ThenBy(t => t.LeaveSetups.ValidFrom).ToList();
            return result;
        }


        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var selected = (LeaveSetupViewModel)InformationDataGrid.SelectedItem;
                if (selected != null)
                {
                    var existed = BusinessLayerServices.LeaveTypeBL().GetSingleLeaveSetup(selected.LeaveSetups.Employee_ID, selected.LeaveSetups.LeaveType_ID, selected.LeaveSetups.ValidFrom);
                    RecordTypeCombobox.SelectedIndex = 1; // change to record by employee
                    //RecordTypeCombobox.IsEnabled = false;
                    EmployeeIDTextBox.Text = existed.Employee_ID;
                    RecordByEmployeeStackPanel.Visibility = Visibility.Visible;
                    DepartmentCombobox.SelectedValue = existed.Employee.DeptCode;
                    DepartmentCombobox.Visibility = Visibility.Collapsed;
                    StafftypeCombobox.Visibility = Visibility.Collapsed;
                    NameTHTextBox.Text = string.Format("{0} {1}", existed.Employee.Person.FirstNameTH, existed.Employee.Person.LastNameTH);
                    LeaveTypeCombobox.SelectedValue = existed.LeaveType_ID;
                    LeaveAmountTextBox.Text = existed.LeaveAmount.ToString();
                    LeaveAmountTextBox.IsEnabled = existed.LeaveAmount <= 7; //Disable when more than 7, it's old data.
                    string _month = existed.ValidFrom.Month.ToString() == "1" ? "1" : "2";
                    string year = existed.Year.ToString() + "/" + _month;
                    YearCombobox.SelectedValue = year;
                    //YearCombobox.IsEnabled = false;
                    ValidFromDatePicker.SelectedDate = existed.ValidFrom;
                    //ValidFromDatePicker.IsEnabled = false;
                    ValidToDatePicker.SelectedDate = existed.ValidTo;
                    //ValidToDatePicker.IsEnabled = false;
                    UpdateButton.IsEnabled = true;
                    //EmployeeIDTextBox.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            try
            {
                RecordTypeCombobox.IsEnabled = true;
                EmployeeIDTextBox.Text = "";
                EmployeeIDTextBox.IsEnabled = true;
                NameTHTextBox.Text = "";
                if (EmployeeIDTextBox.Visibility == Visibility.Visible) InformationDataGrid.ItemsSource = null;
                DepartmentCombobox.SelectedIndex = -1;
                LeaveTypeCombobox.SelectedIndex = 0;
                StafftypeCombobox.SelectedIndex = -1;
                LeaveAmountTextBox.Text = "";
                YearCombobox.SelectedIndex = -1;
                ValidFromDatePicker.SelectedDate = null;
                ValidToDatePicker.SelectedDate = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var selected = (LeaveSetupViewModel)InformationDataGrid.SelectedItem;
                    var existed = BusinessLayerServices.LeaveTypeBL().GetLeaveSetupByID(selected.LeaveSetups.LeaveSetup_ID);
                    if (existed != null)
                    {
                        //var oldItem = BusinessLayerServices.OTBL().GetSingleOT(selected.Employee_ID, selected.OTDATE, selected.OTType, selected.RequestType);
                        BusinessLayerServices.LeaveTypeBL().DeleteLeaveSetup(existed);
                        //DataCorrectorStatic<OT>.AddTransaction("D", "OT", oldItem, null);
                        MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        ReloadDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ValidFromDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            var from = ValidFromDatePicker.SelectedDate;
            if (from != null) ValidToDatePicker.SelectedDate = from.Value.AddYears(2);
        }

        private void YearCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var selected = YearCombobox.SelectedItem;
                if (selected != null)
                {
                    string _selected = selected.ToString();
                    string _year = _selected.Substring(0, 4);
                    string _part = _selected.Substring(_selected.Length - 1);
                    int year = Convert.ToInt16(_year);

                    DateTime firstDay = _part == "1" ? new DateTime(year, 1, 1) : new DateTime(year, 7, 1);
                    DateTime lastDay = firstDay.AddYears(2).AddDays(-1); 

                    ValidFromDatePicker.SelectedDate = firstDay;
                    ValidToDatePicker.SelectedDate = lastDay;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void LeaveAmountTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                //string _leaveAmo = HRISService.DataCorrector().FormatNumber(3, LeaveAmountTextBox.Text, true);
                //int leaveAmo = 0;
                //if (_leaveAmo != "") leaveAmo = Convert.ToInt16(_leaveAmo);
                //if (leaveAmo > 7)
                //{
                //    //LeaveAmountTextBox.Text = "";
                //    //throw new Exception("สามารถตั้งค่าวันลาได้สูงสุด 7 วัน กรุณาตรวจสอบ");
                //} 
                //else LeaveAmountTextBox.Text = _leaveAmo;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
