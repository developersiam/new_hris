﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HRISSystem.ViewModel;
using HRISSystemBL;
using DomainModelPayroll;
using HRISSystem.Shared;
using DomainModelHris;
using HRISSystem.Form.Employees;

namespace HRISSystem.Form.Walefare
{
    /// <summary>
    /// Interaction logic for OffSiteWorkPage.xaml
    /// </summary>
    public partial class OffSiteWorkPage : Page
    {
        //List<OffsiteNotedViewModel> notedList;
        List<WorkUpCountry> workUpCountryList;
        public OffSiteWorkPage()
        {
            InitializeComponent();
            BindCombobox();
        }

        private void BindCombobox()
        {
            try
            {
                TitleCombobox.ItemsSource = BusinessLayerServices.PersonBL().GetAllTitlenameList();
                StaffTypeCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
                LotCombobox.ItemsSource = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);
                SearchFromDatePicker.SelectedDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                SearchToDatePicker.SelectedDate = SearchFromDatePicker.SelectedDate.Value.AddMonths(1).AddDays(-1);
                NotedCombobox.ItemsSource = BusinessLayerServices.OffSiteWorkBL().WorkUpCountryNotedList();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private List<OffsiteNotedViewModel> OffSiteNotedList()
        {
            var notedList = new List<OffsiteNotedViewModel>();
            notedList.Add(new OffsiteNotedViewModel { NoteFlag = 1, NoteFlagName = "ปฏิบัติงานต่างจังหวัด (Workup country)" });
            notedList.Add(new OffsiteNotedViewModel { NoteFlag = 2, NoteFlagName = "ปฏิบัติงานนอกสถานที่ (Off-site work)" });
            notedList.Add(new OffsiteNotedViewModel { NoteFlag = 3, NoteFlagName = "ปฏิบัติงานที่บ้าน (Work from home)" });
            return notedList;
        }

        private void EmployeeIDTextBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left) Search();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void Search()
        {
            try
            {
                SearchEmployee_Window window = new SearchEmployee_Window();
                window.ShowDialog();
                if (window.selectedEmployee != null) BindSelectedEmployee(window.selectedEmployee);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindSelectedEmployee(DomainModelHris.Employee employee)
        {
            try
            {
                Clear();
                EmployeeIDTextBox.Text = employee.Employee_ID;
                FingerIDTextBox.Text = employee.FingerScanID;
                TitleCombobox.SelectedValue = employee.Person.TitleName_ID;
                NameTHTextBox.Text = employee.Person.FirstNameTH;
                LastnameTHTextBox.Text = employee.Person.LastNameTH;
                StaffTypeCombobox.SelectedValue = employee.Staff_type;
                AddButton.IsEnabled = true;
                UpdateButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Clear()
        {
            try
            {
                EmployeeIDSearchTextBox.Text = "";
                EmployeeIDTextBox.Text = "";
                FingerIDTextBox.Text = "";
                TitleCombobox.SelectedIndex = -1;
                NameTHTextBox.Text = "";
                LastnameTHTextBox.Text = "";
                StaffTypeCombobox.SelectedIndex = -1;
                WorkFromDatePicker.Text = "";
                WorkToDatePicker.Text = "";
                DateTypeTextBlock.Text = "";
                NotedCombobox.SelectedIndex = -1;
                AddButton.IsEnabled = false;
                UpdateButton.IsEnabled = false;
                WorkFromDatePicker.IsEnabled = true;
                WorkToDatePicker.IsEnabled = true;
                NotedCombobox.IsEnabled = true;

                var currentLot = BusinessLayerServices.PayrollBL().GetCurrentDateLot();
                if (currentLot != null)
                {
                    string stringLot = string.Format("{0}/{1}", currentLot.Lot_Month, currentLot.Lot_Year);
                    LotCombobox.SelectedValue = stringLot;
                    //SearchLotCombobox.SelectedValue = stringLot;
                }
                else MessageBox.Show("ไม่พบ Lot number ที่ตรงกับวันที่ปัจจุบัน");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {

                    ConditionChecking();
                    var validFrom = WorkFromDatePicker.SelectedDate.Value;
                    var endDate = WorkToDatePicker.SelectedDate.Value;
                    var newItemList = new List<WorkUpCountry>();

                    for (var day = validFrom.Date; day.Date <= endDate.Date; day = day.AddDays(1))
                    {
                        var item = new WorkUpCountry
                        {
                            Employee_ID = EmployeeIDTextBox.Text,
                            WorkDate = day,
                            Status = true,
                            Noted = NotedCombobox.Text,
                            ModifiedDate = DateTime.Now
                        };
                        BusinessLayerServices.OffSiteWorkBL().AddOffSiteWork(item);
                        newItemList.Add(item);
                    }
                    DataCorrectorStatic<WorkUpCountry>.AddTransactionList("A", "Offsite Work", null, newItemList);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    EmployeeIDSearchTextBox.Text = EmployeeIDTextBox.Text;
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ConditionChecking()
        {
            if (string.IsNullOrEmpty(EmployeeIDTextBox.Text)) throw new Exception("กรุณาระบุพนักงาน");
            if (string.IsNullOrEmpty(LotCombobox.Text)) throw new Exception("กรุณาระบุงวด");
            if (string.IsNullOrEmpty(NotedCombobox.Text)) throw new Exception("กรุณาระบุประเภทการทำงาน");

            var workFromDate = WorkFromDatePicker.SelectedDate.Value;
            var workToDate = WorkToDatePicker.SelectedDate.Value;
            for (var day = workFromDate.Date; day.Date <= workToDate.Date; day = day.AddDays(1))
            {
                if(day.DayOfWeek == DayOfWeek.Sunday) throw new Exception("ไม่สามารถบันทึกการทำงานในวันระบุได้ เนื่องจากเป็นวันอาทิตย์");
            }
            if (workFromDate > workToDate) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
            if (workFromDate == null || workToDate == null) throw new Exception("กรุณาระบุวันที่");
            var _lotList = BusinessLayerServices.PayrollBL().GetLotByDateRange(workFromDate, workToDate);
            if (_lotList.Count() == 1)
            {
                var _lotNumber = _lotList.FirstOrDefault();
                if (_lotNumber.Lock_Acc == "2" || _lotNumber.Lock_Acc_Labor == "2") throw new Exception("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก");
                if (_lotNumber.Lock_Hr_Labor == "2") throw new Exception("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก");
            }
            else
            {
                throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    ConditionChecking();
                    var validFrom = WorkFromDatePicker.SelectedDate.Value;
                    var endDate = WorkToDatePicker.SelectedDate.Value;
                    var newItemList = new List<WorkUpCountry>();

                    for (var day = validFrom.Date; day.Date <= endDate.Date; day = day.AddDays(1))
                    {
                        var item = new WorkUpCountry
                        {
                            Employee_ID = EmployeeIDTextBox.Text,
                            WorkDate = day,
                            Status = true,
                            Noted = NotedCombobox.SelectedValue.ToString(),
                            ModifiedDate = DateTime.Now
                        };
                        BusinessLayerServices.OffSiteWorkBL().UpdateOffSiteWork(item);

                        if (NotedCombobox.SelectedValue.ToString() == "3")
                        {
                            var manualIn = new ManualWorkDate
                            {
                                Employee_ID = EmployeeIDTextBox.Text,
                                ManualWorkDateDate = day,
                                ManualWorkDateType_ID = "A01",
                                Times = TimeSpan.Parse("08:00"),
                                Status = true,
                                ModifiedDate = DateTime.Now
                            };
                            var manualOut = new ManualWorkDate
                            {
                                Employee_ID = EmployeeIDTextBox.Text,
                                ManualWorkDateDate = day,
                                ManualWorkDateType_ID = "A02",
                                Times = TimeSpan.Parse("17:00"),
                                Status = true,
                                ModifiedDate = DateTime.Now
                            };
                            UpdateManualDate(manualIn, manualOut);
                        }
                    }
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    EmployeeIDSearchTextBox.Text = EmployeeIDTextBox.Text;
                    NameSearchTextBox.Text = "";
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateManualDate(ManualWorkDate manualIn, ManualWorkDate manualOut)
        {
            var timeIn = BusinessLayerServices.ManualWorkingBL().GetSingleManualWorkDate(manualIn.ManualWorkDateDate, "A01", manualIn.Employee_ID);
            var timeOut = BusinessLayerServices.ManualWorkingBL().GetSingleManualWorkDate(manualOut.ManualWorkDateDate, "A02", manualOut.Employee_ID);
            if (timeIn != null) BusinessLayerServices.ManualWorkingBL().DeleteManuaWorkingDate(manualIn);
            BusinessLayerServices.ManualWorkingBL().AddManuaWorkingDate(manualIn);
            if (timeOut != null) BusinessLayerServices.ManualWorkingBL().DeleteManuaWorkingDate(manualOut);
            BusinessLayerServices.ManualWorkingBL().AddManuaWorkingDate(manualOut);
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void NameSearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            EmployeeIDSearchTextBox.Text = "";
            FilterDatagrid();
        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                var name = NameSearchTextBox.Text;
                var empID = EmployeeIDSearchTextBox.Text;
                var from = SearchFromDatePicker.SelectedDate;
                var to = SearchToDatePicker.SelectedDate;

                if (from == null || to == null) return;

                workUpCountryList = BusinessLayerServices.OffSiteWorkBL().GetFilterdOffsiteList("", from.Value, to.Value);
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = workUpCountryList;
                TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", InformationDataGrid.Items.Count);
                if (name != "" || empID != "") FilterDatagrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FilterDatagrid()
        {
            if (workUpCountryList == null) return;

            var filterList = new List<WorkUpCountry>();
            var name = NameSearchTextBox.Text;
            var empID = EmployeeIDSearchTextBox.Text;

            if (empID != "") filterList = workUpCountryList.Where(w => w.Employee_ID == empID).ToList();
            else if (name != "") filterList = workUpCountryList.Where(w => w.Employee.Person.FirstNameTH.Contains(name)).ToList();
            else filterList = workUpCountryList;

            InformationDataGrid.ItemsSource = null;
            InformationDataGrid.ItemsSource = filterList;
            TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", InformationDataGrid.Items.Count);
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var selected = (WorkUpCountry)InformationDataGrid.SelectedItem;
                if (selected != null)
                {
                    var emp = BusinessLayerServices.EmployeeBL().GetEmployeeByEmployeeID(selected.Employee_ID);
                    EmployeeIDTextBox.Text = selected.Employee_ID;
                    FingerIDTextBox.Text = emp.FingerScanID;
                    TitleCombobox.SelectedValue = emp.Person.TitleName.TitleName_ID;
                    NameTHTextBox.Text = emp.Person.FirstNameTH;
                    LastnameTHTextBox.Text = emp.Person.LastNameTH;
                    StaffTypeCombobox.SelectedValue = emp.Staff_type;
                    WorkFromDatePicker.Text = "";
                    WorkToDatePicker.Text = "";
                    WorkFromDatePicker.SelectedDate = selected.WorkDate;
                    WorkToDatePicker.SelectedDate = selected.WorkDate;
                    //var noted = notedList.FirstOrDefault(s => s.NoteFlagName == selected.Noted);
                    NotedCombobox.SelectedValue = selected.WorkUpCountryNoted.NotedID;
                    //NotedCombobox.Text = selected.Noted;

                    var _lot = BusinessLayerServices.PayrollBL().GetLotByDateRange(selected.WorkDate, selected.WorkDate).FirstOrDefault();
                    LotCombobox.SelectedValue = _lot.Lot_Month + "/" + _lot.Lot_Year;

                    WorkFromDatePicker.IsEnabled = false;
                    WorkToDatePicker.IsEnabled = false;
                    AddButton.IsEnabled = false;
                    UpdateButton.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var oldItem = (WorkUpCountry)InformationDataGrid.SelectedItem;
                BindSelected(oldItem);
                if (MessageBox.Show("ยืนยันการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (oldItem != null)
                    {
                        ConditionChecking();
                        BusinessLayerServices.OffSiteWorkBL().DeleteOffSiteWork(oldItem);
                        //DataCorrectorStatic<WorkUpCountry>.AddTransaction("D", "Offsite Work", oldItem, null);
                        MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        EmployeeIDSearchTextBox.Text = oldItem.Employee_ID;
                        ReloadDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindSelected(WorkUpCountry selected)
        {
            //var selected = (WorkUpCountry)InformationDataGrid.SelectedItem;
            if (selected != null)
            {
                var emp = BusinessLayerServices.EmployeeBL().GetEmployeeByEmployeeID(selected.Employee_ID);
                EmployeeIDTextBox.Text = selected.Employee_ID;
                FingerIDTextBox.Text = emp.FingerScanID;
                TitleCombobox.SelectedValue = emp.Person.TitleName.TitleName_ID;
                NameTHTextBox.Text = emp.Person.FirstNameTH;
                LastnameTHTextBox.Text = emp.Person.LastNameTH;
                StaffTypeCombobox.SelectedValue = emp.Staff_type;
                WorkFromDatePicker.Text = "";
                WorkToDatePicker.Text = "";
                WorkFromDatePicker.SelectedDate = selected.WorkDate;
                WorkToDatePicker.SelectedDate = selected.WorkDate;
                //var noted = notedList.FirstOrDefault(s => s.NoteFlagName == selected.Noted);
                NotedCombobox.SelectedValue = selected.WorkUpCountryNoted.NotedID;
                //NotedCombobox.Text = selected.Noted;

                var _lot = BusinessLayerServices.PayrollBL().GetLotByDateRange(selected.WorkDate, selected.WorkDate).FirstOrDefault();
                LotCombobox.SelectedValue = _lot.Lot_Month + "/" + _lot.Lot_Year;

                WorkFromDatePicker.IsEnabled = false;
                WorkToDatePicker.IsEnabled = false;
                AddButton.IsEnabled = false;
                UpdateButton.IsEnabled = true;
            }
        }

        private void LotCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var selected = (Lot_Number)LotCombobox.SelectedItem;
                if (selected != null)
                {
                    LotFromDatePicker.SelectedDate = selected.Start_date;
                    LotToDatePicker.SelectedDate = selected.Finish_date;
                    WorkFromDatePicker.DisplayDateStart = selected.Start_date;
                    WorkFromDatePicker.DisplayDateEnd = selected.Finish_date;
                    WorkToDatePicker.DisplayDateStart = selected.Start_date;
                    WorkToDatePicker.DisplayDateEnd = selected.Finish_date;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
