﻿using HRISSystem.Form.Employees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HRISSystem.ViewModel;
using HRISSystemBL;
using DomainModelHris;
using DomainModelPayroll;
using HRISSystem.Shared;
using System.Runtime.InteropServices.ComTypes;

namespace HRISSystem.Form.Walefare
{
    /// <summary>
    /// Interaction logic for LeavePage.xaml
    /// </summary>
    public partial class LeavePage : Page
    {
        List<LeaveType> staffLeaveType;
        List<LeaveType> labourLeaveType;
        List<LeaveType> laboursLeaveType;
        List<Leave> leavesList;
        public LeavePage()
        {
            try
            {
                leavesList = new List<Leave>();
                InitializeComponent();
                BindLeaveType();
                BindYearCombobox();
                SearchYearCombobox.SelectedIndex = 0;
                ReloadDatagrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindLeaveType()
        {
            staffLeaveType = BusinessLayerServices.LeaveTypeBL().GetAllLeaveType(); //พนักงานประจำ
            staffLeaveType.RemoveAt(4);
            labourLeaveType = BusinessLayerServices.LeaveTypeBL().GetAllLeaveType(); //พนักงานรายวันประจำ
            var _labourLeaveType = labourLeaveType.First(f => f.LeaveType_ID == "L002");
            if (_labourLeaveType != null) _labourLeaveType.LeaveTypeNameTH = "ลากิจพิเศษ";
            laboursLeaveType = BusinessLayerServices.LeaveTypeBL().GetAllLeaveType(); //พนักงานรายวัน & รายวันชั่วคราว
            laboursLeaveType.RemoveAt(0);
            laboursLeaveType.RemoveAt(0);

        }

        private void BindYearCombobox()
        {
            var yearList = new List<string>();
            for (int i = DateTime.Now.Year; i > DateTime.Now.Year - 3; i--)
            {
                yearList.Add(i.ToString());
            }
            SearchYearCombobox.ItemsSource = yearList;
        }

        private void LotCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var selected = (LotNumberViewModel)LotCombobox.SelectedItem;
                if (selected != null)
                {
                    SearchLotCombobox.SelectedValue = selected.LotNumber;
                    LotFromDatePicker.SelectedDate = selected.Start_date;
                    LotToDatePicker.SelectedDate = selected.Finish_date;
                    FromDatePicker.DisplayDateStart = selected.Start_date;
                    FromDatePicker.DisplayDateEnd = selected.Finish_date;
                    ToDatePicker.DisplayDateStart = selected.Start_date;
                    ToDatePicker.DisplayDateEnd = selected.Finish_date;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EmployeeIDTextBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left) Search();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void Search()
        {
            try
            {
                SearchEmployee_Window window = new SearchEmployee_Window();
                window.ShowDialog();
                BindSelectedEmployee(window.selectedEmployee);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindSelectedEmployee(DomainModelHris.Employee item)
        {
            try
            {
                if (item != null)
                {
                    if (item.Staff_status == 2) throw new Exception("ผิดพลาด ไม่สามารถบันทึกข้อมูลพนักงานที่มีสถานะสิ้นสุดเได้");
                    EmployeeIDTextBox.Text = item.Employee_ID;
                    PersonIDTextBox.Text=item.Person_ID;
                    NameTextBox.Text = string.Format("{0} {1} {2}", item.Person.TitleName.TitleNameTH, item.Person.FirstNameTH, item.Person.LastNameTH);
                    NameSearchTextBox.Text = string.Format("{0} {1}", item.Person.FirstNameTH, item.Person.LastNameTH);
                    NameFilter();

                    LeaveTypeCombobox.ItemsSource = item.Staff_type == 1 ? staffLeaveType : item.Staff_type == 2 || item.Staff_type == 3 ? labourLeaveType : laboursLeaveType;

                    NotedTextBox.Text = "";
                    RemainingHolidayTextbox.Text = "";
                    RemainingPersonalTextbox.Text = "";
                    RemainingPersonalLabourTextbox.Text = "";
                    RemainingSickTextbox.Text = "";
                    FromDatePicker.SelectedDate = null;
                    ToDatePicker.SelectedDate = null;

                    RemainingHolidayTextbox.Visibility = Visibility.Visible;
                    RemainingPersonalTextbox.Visibility = item.Staff_type == 1 ? Visibility.Visible : Visibility.Collapsed;
                    RemainingPersonalLabourTextbox.Visibility = item.Staff_type == 2 || item.Staff_type == 3 ? Visibility.Visible : Visibility.Collapsed;
                    RemainingSickTextbox.Visibility = Visibility.Visible;
                }
                UpdateButton.IsEnabled = item != null || !string.IsNullOrEmpty(EmployeeIDTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void GetRemainingLeave()
        {
            try
            {
                var from = FromDatePicker.SelectedDate;
                var to = ToDatePicker.SelectedDate;
                var empID = EmployeeIDTextBox.Text;
                if (empID == "" || from == null || to == null) return;
                if (from > to) return;
                var employee = BusinessLayerServices.EmployeeBL().GetEmployeeByEmployeeID(empID);

                // ลาพักผ่อน
                var remainingHolidayList = BusinessLayerServices.LeaveTypeBL().GetAvialableLeaveSetupListByDate(empID, from.Value, to.Value);
                var holiday = remainingHolidayList.Where(w => w.LeaveType_ID == "L001").Sum(s => s.LeaveAmount);
                RemainingHolidayTextbox.Text = string.Format("{0:n4}", holiday);

                var _leaveList = BusinessLayerServices.LeaveTypeBL().GetYearlyLeaveListByPerson(employee.Person_ID, from.Value.Year).ToList();

                // ลากิจ
                var _personal = GetRemainingPersonal(employee, _leaveList, from.Value);

                // ลาป่วย
                var SickList = _leaveList.Where(w => w.LeaveType_ID == "L003").ToList();
                var Sick = SickList.Sum(s => s.NcountLeave);
                var _sick = 30 - Sick;

                RemainingPersonalTextbox.Text = string.Format("{0:n4}", _personal);
                RemainingPersonalLabourTextbox.Text = string.Format("{0:n4}", _personal);
                RemainingSickTextbox.Text = string.Format("{0:n4}", _sick < 0 ? 0 : _sick);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private decimal GetRemainingPersonal(DomainModelHris.Employee employee, List<Leave> _leaveList, DateTime from)
        {
            var startDate = employee.StartDate.Value;
            //startDate = new DateTime(2023, 02, 20);
            var probation = (startDate.AddMonths(3) - startDate).TotalDays;
            var _workAge = from - startDate;
            var workAge = _workAge.TotalDays;
            var year = from.Year; 
            int quota = 0;

            // ไม่ผ่านโปรไม่มีสิทธิ์ลา
            if (workAge < probation) return 0;

            // วันลาที่ต้องได้รับ
            if (workAge > 365)
            {
                quota = 1; // เริ่ม มค. ได้ 1 วัน
                var today = DateTime.Now.DayOfYear; // Today number of year
                if (employee.Staff_type == 1)
                {
                    if (from >= new DateTime(year, 3, 1)) quota = 2;  //ผ่านเข้า มี.ค.
                    if (from >= new DateTime(year, 5, 1)) quota = 3;  //ผ่านเข้า พ.ค.
                    if (from >= new DateTime(year, 7, 1)) quota = 4;  //ผ่านเข้า ก.ค.
                    if (from >= new DateTime(year, 9, 1)) quota = 5;  //ผ่านเข้า ก.ย.
                    if (from >= new DateTime(year, 11, 1)) quota = 6; //ผ่านเข้า พ.ย.
                }
                else
                {
                    if (from >= new DateTime(year, 5, 1)) quota = 2;  //ผ่านเข้า พ.ค.
                    if (from >= new DateTime(year, 9, 1)) quota = 3;  //ผ่านเข้า ก.ย.
                }
            }
            else
            {
                if (employee.Staff_type == 1)
                {
                    var period_1 = startDate.AddMonths(2);
                    var period_2 = startDate.AddMonths(4);
                    var period_3 = startDate.AddMonths(6);
                    var period_4 = startDate.AddMonths(8);
                    var period_5 = startDate.AddMonths(10);
                    var period_6 = startDate.AddMonths(12);

                    if (from > period_1) quota = 1; //ทำงาน 2 เดือนได้รับ โควต้าลากิจ 1 วัน
                    if (from > period_2) quota = 2; 
                    if (from > period_3) quota = 3; 
                    if (from > period_4) quota = 4; 
                    if (from > period_5) quota = 5; 
                    if (from > period_6) quota = 6; 
                }
                else
                {
                    var period_1 = startDate.AddMonths(4);
                    var period_2 = startDate.AddMonths(6);
                    var period_3 = startDate.AddMonths(12);

                    if (from > period_1) quota = 1; //ทำงาน 4 เดือนได้รับ โควต้าลากิจ 1 วัน
                    if (from > period_2) quota = 2;
                    if (from > period_3) quota = 3;
                }
            }

            // วันลาที่ใช้ไป
            var PersonalList = _leaveList.Where(w => w.LeaveType_ID == "L002" && w.Status).ToList();
            var used = PersonalList.Sum(s => s.NcountLeave);

            // วันลาสุทธิ
            var remainingPersonal = quota - used;

            return remainingPersonal.GetValueOrDefault();
        }

        private void FromDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            double? leaveAmount = CalculateDate();
            LeaveAmountTextBox.IsEnabled = leaveAmount > 0 && leaveAmount <= 1;
            LeaveAmountTextBox.Text = leaveAmount == null ? "" : leaveAmount.ToString();
            GetRemainingLeave();
        }

        private double? CalculateDate()
        {
            try
            {
                var leave = (LeaveType)LeaveTypeCombobox.SelectedItem;
                var fromDate = FromDatePicker.SelectedDate;
                var toDate = ToDatePicker.SelectedDate;
                if (leave == null || fromDate == null || toDate == null) return null;
                if (fromDate > toDate) return null;
                
                var dateList = new List<DateTime>();
                var holidayList = new List<Holiday>();
                for (DateTime date = fromDate.Value; date <= toDate.Value; date = date.AddDays(1))
                {
                    dateList.Add(date);
                    var holiday = BusinessLayerServices.ReferenceBL().GetHolidayByDate(date);
                    if (holiday != null) holidayList.Add(holiday);
                }
                var sundayList = dateList.Where(d => d.DayOfWeek == DayOfWeek.Sunday).ToList();
                // ลาคลอดเงื่อนไขคือ นับรวม วันอาทิตย์และวันนักขัตฤกษ์ด้วย P'Beau 02/06/2020
                double result = leave.LeaveType_ID == "L004" ? dateList.Count : dateList.Count - sundayList.Count - holidayList.Count;
                LeaveAmountTextBox.IsEnabled = dateList.Count > 0 && dateList.Count <= 1;
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(EmployeeIDTextBox.Text)) throw new Exception("กรุณาระบุรหัสพนักงาน");
                    var leaveType = (LeaveType)LeaveTypeCombobox.SelectedItem;
                    if (leaveType == null) throw new Exception("กรุณาระบุประเภทการลา");
                    var fromDate = FromDatePicker.SelectedDate;
                    var toDate = ToDatePicker.SelectedDate;
                    if (fromDate == null || toDate == null) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
                    if (fromDate > toDate) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
                    if (string.IsNullOrEmpty(LeaveAmountTextBox.Text)) throw new Exception("กรุณาระบุจำนวนวันลา");
                    var leaveAmo = decimal.Parse(LeaveAmountTextBox.Text);
                    var leaveCal = CalculateDate();
                    if (leaveCal <= 0) throw new Exception("กรุณาระบุจำนวนวันลาให้ถูกต้อง");
                    if ((decimal)leaveCal < leaveAmo) throw new Exception("กรุณาระบุจำนวนวันลาให้ถูกต้อง");
                    //if (!IsRemaing()) throw new Exception("กรุณาระบุจำนวนวันลาให้ถูกต้อง");
                    //var lotNumber = (LotNumberViewModel)LotCombobox.SelectedItem;
                    
                    if (!IsRemaing())
                    {
                        if (MessageBox.Show("จำนวนวันลาคงเหลือน้อยกว่าวันลาที่ร้องขอ วันลาที่เกินมาจะกลายเป็นลาเกิน ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;
                    }

                    Update(fromDate.Value, toDate.Value, leaveType.LeaveType_ID, leaveCal.Value, leaveAmo);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private bool IsRemaing()
        {
            var holiday = Convert.ToDecimal(RemainingHolidayTextbox.Text);
            var _personal = RemainingPersonalTextbox.Text == "" ? 0 : Convert.ToDecimal(RemainingPersonalTextbox.Text);
            var _personalLabour = RemainingPersonalLabourTextbox.Text == "" ? 0 : Convert.ToDecimal(RemainingPersonalLabourTextbox.Text);
            var personal = RemainingPersonalTextbox.Visibility == Visibility.Visible ? _personal : _personalLabour;
            var sick = Convert.ToDecimal(RemainingSickTextbox.Text);

            //var pregnant = Convert.ToDecimal(RemainingPregnantTextbox.Text);
            var leaveAmo = decimal.Parse(LeaveAmountTextBox.Text);
            var leaveType = (LeaveType)LeaveTypeCombobox.SelectedItem;
            var leaveID = leaveType.LeaveType_ID;

            decimal sum = 0;

            if (leaveID == "L001") sum = holiday - leaveAmo;
            else if (leaveID == "L002") sum = personal - leaveAmo;
            else if (leaveID == "L003") sum = sick - leaveAmo;
            //else if (leaveID == "L004") sum = pregnant - leaveAmo;

            var result = sum >= 0;
            return result;
        }

        private void Update(DateTime fromDate, DateTime toDate, string leaveTypeID, double leaveCal, decimal leaveAmo)
        {
            var personal = RemainingPersonalTextbox.Text == "" ? 0 : Convert.ToDecimal(RemainingPersonalTextbox.Text);
            var personalLabour = RemainingPersonalLabourTextbox.Text == "" ? 0 : Convert.ToDecimal(RemainingPersonalLabourTextbox.Text);
            var sick = Convert.ToDecimal(RemainingSickTextbox.Text);
            var remaining = leaveTypeID == "L003" ? sick : RemainingPersonalTextbox.Visibility == Visibility.Visible ? personal : personalLabour;
            var isOver = !IsOverCheckbox.IsChecked.Value;

            for (var day = fromDate; day <= toDate; day = day.AddDays(1))
            {
                var holiday = BusinessLayerServices.ReferenceBL().GetHolidayByDate(day);
                // ลาป่วย ไม่รวม วันอาทิตย์ และวันหยุด P'Beau 02/06/2020
                // ลาคลอดเงื่อนไขคือ นับรวม วันอาทิตย์และวันนักขัตฤกษ์ด้วย P'Beau 02/06/2020
                if ((holiday == null && day.DayOfWeek != DayOfWeek.Sunday) || leaveTypeID == "L004")
                {
                    var item = new Leave
                    {
                        Employee_ID = EmployeeIDTextBox.Text,
                        LeaveDate = day,
                        LeaveType_ID = LeaveTypeCombobox.SelectedValue.ToString(),
                        NcountLeave = leaveAmo / (decimal)leaveCal,
                        Status = isOver,
                        Noted = NotedTextBox.Text,
                        ModifiedDate = DateTime.Now
                    };
                    var existed = item.LeaveType_ID == "L001" ? BusinessLayerServices.LeaveTypeBL().AddHolidayLeave(item) : BusinessLayerServices.LeaveTypeBL().AddLeave(item, remaining); //Add leave
                    remaining = existed; // อัพเดทจำนวนโควต้าลากิจ/กิจพิเศษ
                    if (remaining <= 0 && leaveTypeID != "L001") break; // หยุด loop เมื่อเข้าเงื่อนไขลาเกิน
                }
            }
            MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
            ReloadDatagrid();
            GetRemainingLeave();
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var item = (Leave)InformationDataGrid.SelectedItem;
                var lotNumber = BusinessLayerServices.PayrollBL().GetSingleLotByDate(item.LeaveDate);
                if (item.Employee.Staff_status == 2) throw new Exception("สถานะพนักงานนี้สิ้นสุดไปแล้ว ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบ");
                //if (lotNumber.Lock_Acc == "2") throw new Exception("ข้อมูลที่เลือกอยู่ในงวดที่บัญชีทำการล็อคแล้ว ไม่สามารถแก้ไขข้อมูลได้");
                if (item != null)
                {
                    var leaveSetupID = item.LeaveSetup_ID;
                    var leaveType = item.LeaveType.LeaveTypeNameTH;
                    var leaveAmo = item.NcountLeave;
                    var from = DateTime.Now;
                    var to = DateTime.Now; 

                    string alertText = "";
                    if (leaveSetupID != 1 && leaveType == "L001") alertText = string.Format("{0} วันลา {1} วันจะถูกคืนให้ ให้ช่วงวันลา {2}-{3} \nต้องการลบข้อมูลหรือไม่", leaveType, leaveAmo, from, to);
                    else if (leaveType == "L001") alertText = string.Format("{0} วันลา {1} วันจะไม่ถูกคืนให้ระบบ \nต้องการลบข้อมูลหรือไม่", leaveType, leaveAmo);
                    else alertText = "ต้องการลบข้อมูลหรือไม่";

                    if (MessageBox.Show(alertText, "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        var lotList = BusinessLayerServices.PayrollBL().GetLotByDateRange(item.LeaveDate, item.LeaveDate);
                        Lot_Number lockedLot = lotList.SingleOrDefault(s => s.Lock_Acc == "2");
                        if (lockedLot != null)
                        {
                            if (MessageBox.Show("ข้อมูลนี้เป็นการแก้ไขข้อมูลย้อนหลัง ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                            {
                                Delete(item);
                            }
                        }
                        else Delete(item);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Delete(Leave oldItem)
        {
            BusinessLayerServices.LeaveTypeBL().DeleteLeave(oldItem);
            //DataCorrectorStatic<Leave>.AddTransaction("D", "Leave", oldItem, null);
            MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
            ReloadDatagrid();
            GetRemainingLeave();
        }

        private void NameSearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            NameFilter();
        }

        private void NameFilter()
        {
            try
            {
                var searchText = NameSearchTextBox.Text.Trim();
                string[] searchArr = searchText.Split(' ');
                var name = searchArr[0];
                var lname = searchArr.Count() > 1 ? searchArr[1] : "";
                var empID = EmployeeIDTextBox.Text;
                var perID = PersonIDTextBox.Text;
                var fromDate = SearchFromDatePicker.SelectedDate;
                var toDate = SearchToDatePicker.SelectedDate;
                if (fromDate == null || toDate == null) return;
                if (leavesList == null || !leavesList.Any()) return;
                //if (leavesList == null || !leavesList.Any()) ReloadDatagrid();

                var showList = name == "" ? leavesList : leavesList.Where(w => w.Employee.Person.FirstNameTH.Contains(name)).ToList();
                showList = lname == "" ? showList : showList.Where(w => w.Employee.Person.LastNameTH.Contains(lname)).ToList();
                showList = perID == "" ? showList : showList.Where(w => w.Employee.Person_ID == perID).ToList();

                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = showList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                var fromDate = SearchFromDatePicker.SelectedDate;
                var toDate = SearchToDatePicker.SelectedDate;
                if (fromDate == null || toDate == null) return;

                leavesList = BusinessLayerServices.LeaveTypeBL().GetLeaveList(fromDate.Value, toDate.Value);
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = CastLeaveList(leavesList);
                NameFilter();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private List<Leave> CastLeaveList(List<Leave> leaveList)
        {
            leaveList.ForEach(f =>
                {
                    f.LeaveType.LeaveTypeNameTH = f.Employee.Staff_type != 1 && f.LeaveType_ID == "L002" ? "ลากิจพิเศษ" : f.LeaveType.LeaveTypeNameTH;
                });
            return null;
        }

        private void LeaveTypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            double? leaveAmount = CalculateDate();
            LeaveAmountTextBox.Text = leaveAmount == null ? "" : leaveAmount.ToString();
        }

        private void SearchFromDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void SearchLotCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var selected = (Lot_Number)SearchLotCombobox.SelectedItem;
                if (selected != null)
                {
                    SearchFromDatePicker.SelectedDate = selected.Start_date;
                    SearchToDatePicker.SelectedDate = selected.Finish_date;
                    SearchFromDatePicker.DisplayDateStart = selected.Start_date;
                    SearchFromDatePicker.DisplayDateEnd = selected.Finish_date;
                    SearchFromDatePicker.DisplayDateStart = selected.Start_date;
                    SearchFromDatePicker.DisplayDateEnd = selected.Finish_date;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void YearCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var selected = SearchYearCombobox.SelectedItem;
                if (selected != null)
                {
                    string _year = selected.ToString();
                    int year = Convert.ToInt16(_year);

                    DateTime firstDay = new DateTime(year, 1, 1);
                    DateTime lastDay = firstDay.AddYears(1).AddDays(-1);

                    SearchFromDatePicker.SelectedDate = firstDay;
                    SearchFromDatePicker.DisplayDateStart = firstDay;
                    SearchFromDatePicker.DisplayDateEnd = lastDay;
                    SearchToDatePicker.SelectedDate = lastDay;
                    SearchToDatePicker.DisplayDateStart = firstDay;
                    SearchToDatePicker.DisplayDateEnd = lastDay;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void LeaveAmountTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            var _leaveAmo = HRISService.DataCorrector().FormatNumber(4, LeaveAmountTextBox.Text, false);
            var leaveAmo = Convert.ToDouble(_leaveAmo);
            var calculate = CalculateDate();
            if (calculate == 1) LeaveAmountTextBox.Text = leaveAmo <= 0 || leaveAmo > 1 ? "1" : leaveAmo.ToString();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            EmployeeIDTextBox.Text = "";
            PersonIDTextBox.Text = "";
            NameTextBox.Text = "";
            LeaveTypeCombobox.SelectedIndex = -1;
            FromDatePicker.SelectedDate = null;
            ToDatePicker.SelectedDate = null;
            LeaveAmountTextBox.Text = "";
            RemainingHolidayTextbox.Visibility = Visibility.Collapsed;
            RemainingPersonalTextbox.Visibility = Visibility.Collapsed;
            RemainingPersonalLabourTextbox.Visibility = Visibility.Collapsed;
            RemainingSickTextbox.Visibility= Visibility.Collapsed;
            UpdateButton.IsEnabled = false;
            SearchYearCombobox.SelectedIndex = 0;
            NameSearchTextBox.Text = "";
            ReloadDatagrid();
        }
    }
}
