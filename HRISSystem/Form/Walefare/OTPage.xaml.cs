﻿using HRISSystem.Form.Employees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HRISSystem.ViewModel;
using HRISSystemBL;
using DomainModelPayroll;
using HRISSystem.Shared;
using DomainModelHris;

namespace HRISSystem.Form.Walefare
{
    /// <summary>
    /// Interaction logic for OTPage.xaml
    /// </summary>
    public partial class OTPage : Page
    {
        List<OTViewModel> otView;
        public OTPage()
        {
            InitializeComponent();
            BindCombobox();
        }

        private void BindCombobox()
        {
            try
            {
                var deptCode = SingletonConfiguration.getInstance().DepartmentCode.ToUpper();
                TitleCombobox.ItemsSource = BusinessLayerServices.PersonBL().GetAllTitlenameList();
                StaffTypeCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
                LotCombobox.ItemsSource = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);
                SearchLotCombobox.ItemsSource = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);
                OTTypeCombobox.ItemsSource = BusinessLayerServices.OTBL().GetOTType();
                RequestTypeCombobox.ItemsSource = BusinessLayerServices.OTBL().GetOTRequestType();
                OTHourCombobox.ItemsSource = BindOTHour();
                OTMinuteCombobox.ItemsSource = BindOTMinute();
                ButtonStackPanel.Visibility = deptCode == "ACC" ? Visibility.Collapsed : Visibility.Visible;
                Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private List<OTMinutes> BindOTMinute()
        {
            List<OTMinutes> otMinuteList = new List<OTMinutes>();
            otMinuteList.Add(new OTMinutes { Minutes = "0", Multiplier = 0m });
            otMinuteList.Add(new OTMinutes { Minutes = "10", Multiplier = 0.167m });
            otMinuteList.Add(new OTMinutes { Minutes = "20", Multiplier = 0.33m });
            otMinuteList.Add(new OTMinutes { Minutes = "30", Multiplier = 0.50m });
            otMinuteList.Add(new OTMinutes { Minutes = "40", Multiplier = 0.67m });
            otMinuteList.Add(new OTMinutes { Minutes = "50", Multiplier = 0.83m });
            return otMinuteList;
        }

        private List<decimal> BindOTHour()
        {
            List<decimal> otHourList = new List<decimal>();
            for (decimal i = 0; i < 13; i++)
            {
                otHourList.Add(i);
            }
            return otHourList;
        }

        private void EmployeeIDTextBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left) Search();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void Search()
        {
            try
            {
                SearchEmployee_Window window = new SearchEmployee_Window();
                window.ShowDialog();
                if (window.selectedEmployee != null) BindSelectedEmployee(window.selectedEmployee);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindSelectedEmployee(DomainModelHris.Employee employee)
        {
            try
            {
                Clear();
                EmployeeIDTextBox.Text = employee.Employee_ID;
                FingerIDTextBox.Text = employee.FingerScanID;
                TitleCombobox.SelectedValue = employee.Person.TitleName_ID;
                NameTHTextBox.Text = employee.Person.FirstNameTH;
                LastnameTHTextBox.Text = employee.Person.LastNameTH;
                StaffTypeCombobox.SelectedValue = employee.Staff_type;
                AddButton.IsEnabled = true;
                SpecialOTButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Clear()
        {
            try
            {
                EmployeeIDTextBox.Text = "";
                FingerIDTextBox.Text = "";
                TitleCombobox.SelectedIndex = -1;
                NameTHTextBox.Text = "";
                LastnameTHTextBox.Text = "";
                StaffTypeCombobox.SelectedIndex = -1;
                OTDatePicker.Text = "";
                DateTypeTextBlock.Text = "";
                OTTypeCombobox.SelectedIndex = -1;
                RequestTypeCombobox.SelectedIndex = -1;
                OTHourCombobox.SelectedIndex = -1;
                OTMinuteCombobox.SelectedIndex = -1;
                AddButton.IsEnabled = false;
                UpdateButton.IsEnabled = false;
                OTDatePicker.IsEnabled = true;
                OTTypeCombobox.IsEnabled = true;
                RequestTypeCombobox.IsEnabled = true;
                ManuaulWorkdateButton.IsEnabled = false;
                SpecialOTButton.IsEnabled = false;

                LotCombobox.SelectedValue = HRISService.DataCorrector().GetCurrentLot();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void OTDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (OTDatePicker.SelectedDate != null)
                {
                    CheckDateType();
                    ManuaulWorkdateButton.IsEnabled = OTDatePicker.SelectedDate != null;
                    if (string.IsNullOrEmpty(EmployeeIDTextBox.Text) || string.IsNullOrEmpty(FingerIDTextBox.Text)) throw new Exception("กรุณาเลือกพนักงาน.");
                    CheckINOut();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CheckINOut()
        {
            TimeSpan? scanIn, scanOut;

            var otDate = OTDatePicker.SelectedDate.Value;
            var scanedDate = BusinessLayerServices.TimeStecBL().GetCheckInOut(FingerIDTextBox.Text, otDate);
            var manualDate = BusinessLayerServices.ManualWorkingBL().GetManualDateByDateAndID(otDate, EmployeeIDTextBox.Text);

            if (manualDate.Count() <= 0 && scanedDate.Count() <= 0)
            {
                ScaninTextBox.Text = "";
                ScanoutTextBox.Text = "";
                TotalWorkTextBox.Text = "";
                MessageBox.Show("ไม่พบเวลาการทำงานในวันที่ " + otDate, "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                //เช็คเวลาเข้าทำงาน
                if (manualDate.Where(e => e.ManualWorkDateType_ID == "A01").Count() > 0) scanIn = manualDate.Where(e => e.ManualWorkDateType_ID == "A01").FirstOrDefault().Times;
                else if (scanedDate.Count > 0) scanIn = scanedDate.Min(c => c.checktime.TimeOfDay);
                else scanIn = null;
                ScaninTextBox.Text = scanIn.ToString();

                //เช็คเวลาออกงาน
                if (manualDate.Where(e => e.ManualWorkDateType_ID == "A02").Count() > 0) scanOut = manualDate.Where(e => e.ManualWorkDateType_ID == "A02").FirstOrDefault().Times;
                else if (scanedDate.Count > 0) scanOut = scanedDate.Max(c => c.checktime.TimeOfDay);
                else scanOut = null;
                ScanoutTextBox.Text = scanOut.ToString();

                //คำนวนเวลาทำงาน
                if (scanIn != null && scanOut != null)
                {
                    TimeSpan span = scanOut.GetValueOrDefault() - scanIn.GetValueOrDefault();
                    span = (scanOut.GetValueOrDefault() != scanIn.GetValueOrDefault()) ? span.Subtract(new TimeSpan(0, 30, 0)) : span; // If timein <> timeout the Subtract 30 minutes for Break.
                    TotalWorkTextBox.Text = string.Format("{0} hours , {1} minutes", span.Hours, span.Minutes);
                }
                else TotalWorkTextBox.Text = "";
            }
        }

        private void CheckDateType()
        {
            try
            {
                DateTime otDate = OTDatePicker.SelectedDate.Value;
                if (otDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    DateTypeTextBlock.Text = "วันอาทิตย์";
                    DateTypeTextBlock.Foreground = Brushes.OrangeRed;
                    OTTypeCombobox.SelectedValue = "H";
                }
                else
                {
                    //เช็คว่าเป็น holiday ที่กำหนดไว้หรือไม่  
                    var holiday = BusinessLayerServices.ReferenceBL().GetHolidayByDate(otDate);
                    if (holiday == null)
                    {
                        DateTypeTextBlock.Text = "วันธรรมดา";
                        DateTypeTextBlock.Foreground = Brushes.Black;
                        OTTypeCombobox.SelectedValue = "N";
                    }
                    else
                    {
                        if (holiday.HolidayFlag == true)//ตามประกาศบริษัทฯ
                        {
                            DateTypeTextBlock.Text = holiday.HolidayNameTH.ToString() + " (วันหยุดตามประกาศบริษัท)";
                            DateTypeTextBlock.Foreground = Brushes.Blue;
                            OTTypeCombobox.SelectedValue = "H";
                        }
                        else //ถ้าเป็นวันหยุดพิเศษ เช่น งานเลี้ยง คริตส์มาส 
                        {
                            DateTypeTextBlock.Text = holiday.HolidayNameTH.ToString() + " (วันหยุดพิเศษ)";
                            DateTypeTextBlock.Foreground = Brushes.OrangeRed;
                            OTTypeCombobox.SelectedValue = "H";
                        }
                    }
                }
                RequestTypeCombobox.SelectedIndex = -1;
                OTHourCombobox.SelectedIndex = -1;
                OTMinuteCombobox.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ConditionChecking()
        {
            if (string.IsNullOrEmpty(EmployeeIDTextBox.Text)) throw new Exception("กรุณาระบุพนักงาน");
            if (LotCombobox.Text == "" || LotFromDatePicker.Text == "" || LotToDatePicker.Text == "") throw new Exception("กรุณาระบุ Lot");
            if (OTDatePicker.Text == "") throw new Exception("กรุณาระบุวันที่ทำ OT");
            //21 June 2018 ไม่อนุญาตให้คีย์ล่วงหน้าได้
            if (Convert.ToDateTime(OTDatePicker.Text) > Convert.ToDateTime(DateTime.Now)) throw new Exception("ไม่สามารถบันทึกวันที่ทำโอทีล่วงหน้าได้ กรุณาตรวจสอบข้อมูลอีกครั้ง");
            // 29Oct2018 ไม่อนุญาติให้คีย์ข้อมูลย้อนหลัง (OT ไม่ตรง Lot)
            if (Convert.ToDateTime(OTDatePicker.Text) < Convert.ToDateTime(LotFromDatePicker.Text)) throw new Exception("ไม่สามารถบันทึกการทำโอทีได้ เนื่องจากคุณระบุวันที่ทำโอทีย้อนหลังจาก Lot ปัจจุบัน");

            //8 Aug 2018 ให้เช็คการอนุญาตให้คีย์ในช่วง lot ที่เลือกไว้ และ lot นั้นจะต้องไม่ lock จากบัญชีและ HRM ด้วย
            var _lotNumber = (LotNumberViewModel)LotCombobox.SelectedItem;
            if (_lotNumber.Lock_Acc == "2" || _lotNumber.Lock_Acc_Labor == "2") throw new Exception("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก");
            if (_lotNumber.Lock_Hr_Labor == "2") throw new Exception("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก");
            if (ScaninTextBox.Text == "" || ScanoutTextBox.Text == "") throw new Exception("เนื่องจากพนักงานท่านนี้ไม่มีข้อมูลการสแกนนิ้วในวันที่ดังกล่าวจึงไม่สามารถบันทึกโอทีได้ กรุณาตรวจสอบ");

            DateTime otDate = OTDatePicker.SelectedDate.Value;
            TimeSpan span = (Convert.ToDateTime(ScanoutTextBox.Text) - Convert.ToDateTime(ScaninTextBox.Text));
            //วันธรรมดาที่ไม่ใช่วันหยุดประจำปีของบริษัท หากทำงานไม่เกิน 8 ชั่วโมง ไม่อนุญาตให้ทำการบันทึกโอที
            var holiday = BusinessLayerServices.ReferenceBL().GetHolidayByDate(otDate);
            if (holiday == null && otDate.DayOfWeek != DayOfWeek.Sunday && span.Hours < 8) throw new Exception("ไม่สามารถบันทึกการขอทำโอที เนื่องจากมีการมีการทำงานน้อยกว่า 8 ชัวโมง");
        }

        private bool OTAmountChecking()
        {
            DateTime otDate = OTDatePicker.SelectedDate.Value;
            TimeSpan span = (Convert.ToDateTime(ScanoutTextBox.Text) - Convert.ToDateTime(ScaninTextBox.Text));
            var holiday = BusinessLayerServices.ReferenceBL().GetHolidayByDate(otDate);
            if (holiday == null || otDate.DayOfWeek != DayOfWeek.Sunday) 
            {
                //ให้คำนวนชั่วโมงกับนาทีที่เกิน  8 ชั่วโมง โดยให้คิดเป็นหน่วยนาที เพื่อเช็คกับจำนวนโอที user คีย์เข้าในระบบ
                var otHour = Convert.ToDecimal(OTHourCombobox.Text.ToString());
                var otMinute = Convert.ToDecimal(OTMinuteCombobox.Text.ToString()); // Minute unit
                decimal scanedTime = (((span.Hours - 8) * 60) + (span.Minutes)) - 30; // Subtract 30m for Break
                                                                                      //decimal scanedTime = ((span.Hours - 8) * 60) + (span.Minutes);
                                                                                      //decimal requestedTime = (Convert.ToDecimal(txtNcountOT.Text) * 60);
                decimal requestedTime = ((otHour * 60) + otMinute);
                if (requestedTime > scanedTime)
                {
                    var result = (MessageBox.Show("จำนวนโอทีที่ระบุมากกว่าที่ระบบคำนวนได้ ต้องการบันทึกข้อมูลต่อใช่หรือไม่ ?", "Confirmation", MessageBoxButton.YesNo));
                    return result == MessageBoxResult.Yes ? true : false;
                }
            }
            return true;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (OTHourCombobox.Text == "" || OTMinuteCombobox.Text == "") throw new Exception("กรุณาระบุเวลาทำ OT ให้ครบถ้วน");
                if (OTTypeCombobox.Text == "") throw new Exception("กรุณาเลือกประเภท OT");
                if (OTTypeCombobox.SelectedValue.ToString() == "H")
                {
                    if (MessageBox.Show("การบันทึกการทำงาน Holiday มีหน่วยนับเป็นชั่วโมง (กด Yes เพื่อยืนยัน)", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;
                }
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    ConditionChecking();
                    if (OTAmountChecking())
                    {
                        var otHour = Convert.ToDecimal(OTHourCombobox.Text.ToString());
                        var otMinute = Convert.ToDecimal(OTMinuteCombobox.SelectedValue.ToString()); // Multiplier value
                        decimal otAmount = otHour + otMinute;
                        var newItem = new OT
                        {
                            Employee_ID = EmployeeIDTextBox.Text,
                            OTDate = OTDatePicker.SelectedDate.Value,
                            OTType = (string)OTTypeCombobox.SelectedValue,
                            RequestType = (string)RequestTypeCombobox.SelectedValue,
                            NcountOT = otAmount,
                            Status = true,
                            ModifiedDate = DateTime.Now,
                            AdminApproveStatus = "A",
                            ManagerApproveStatus = "A",
                            FinalApproverStatus = "A",
                            HRApproveStatus = "A",
                            Remark = "Create by HRM",
                            PayrollLot = LotCombobox.SelectedValue.ToString()
                        };
                        BusinessLayerServices.OTBL().AddOT(newItem);
                        DataCorrectorStatic<OT>.AddTransaction("A", "OT", null, newItem);
                        MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        SearchLotCombobox.SelectedValue = LotCombobox.SelectedValue.ToString();
                        ReloadDatagrid();
                        
                        NameSearchTextBox.Text = NameTHTextBox.Text;
                        FilterDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (OTHourCombobox.Text == "" || OTMinuteCombobox.Text == "") throw new Exception("กรุณาระบุเวลาทำ OT ให้ครบถ้วน");
                if (OTTypeCombobox.Text == "") throw new Exception("กรุณาเลือกประเภท OT");
                if (OTTypeCombobox.SelectedValue.ToString() == "H")
                {
                    if (MessageBox.Show("การบันทึกการทำงาน Holiday มีหน่วยนับเป็นชั่วโมง (กด Yes เพื่อยืนยัน)", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;
                }
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    ConditionChecking();
                    if (OTAmountChecking())
                    {
                        var otHour = Convert.ToDecimal(OTHourCombobox.Text.ToString());
                        var otMinute = Convert.ToDecimal(OTMinuteCombobox.SelectedValue.ToString()); // Multiplier value
                        decimal otAmount = otHour + otMinute;

                        var oldItem = BusinessLayerServices.OTBL().GetSingleOT(EmployeeIDTextBox.Text, OTDatePicker.SelectedDate.Value, OTTypeCombobox.SelectedValue.ToString(), RequestTypeCombobox.SelectedValue.ToString());
                        var newItem = BusinessLayerServices.OTBL().GetSingleOT(EmployeeIDTextBox.Text, OTDatePicker.SelectedDate.Value, OTTypeCombobox.SelectedValue.ToString(), RequestTypeCombobox.SelectedValue.ToString());
                        newItem.NcountOT = otAmount;
                        newItem.ModifiedDate = DateTime.Now;
                        newItem.ModifiedUser = SingletonConfiguration.getInstance().Username;
                        BusinessLayerServices.OTBL().UpdateOT(newItem);
                        DataCorrectorStatic<OT>.AddTransaction("U", "OT", oldItem, newItem);
                        MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        SearchLotCombobox.SelectedValue = LotCombobox.SelectedValue.ToString();
                        ReloadDatagrid();
                        
                        NameSearchTextBox.Text = NameTHTextBox.Text;
                        FilterDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void NameSearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.Key != Key.Escape && e.Key != Key.Enter) FilterDatagrid();
        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            FilterDatagrid();
        }

        private void FilterDatagrid()
        {
            try
            {
                if (otView != null)
                {
                    var filterList = new List<OTViewModel>();
                    if (NameSearchTextBox.Text == "") filterList = otView;
                    else filterList = otView.Where(w => w.FirstNameTH.StartsWith(NameSearchTextBox.Text)).ToList();

                    InformationDataGrid.ItemsSource = null;
                    InformationDataGrid.ItemsSource = filterList;
                    OTNTextBox.Text = string.Format("{0:0.###}", filterList.Sum(s => s.OTnormal));
                    OTHTextBox.Text = string.Format("{0:0.###}", filterList.Sum(s => s.OTHoliday));
                    OTWTextBox.Text = string.Format("{0:0.###}", filterList.Sum(s => s.OTWeekend));
                    OTHHTextBox.Text = string.Format("{0:0.###}", filterList.Sum(s => s.OTHoliday));
                    OTSTextBox.Text = string.Format("{0:0.###}", filterList.Sum(s => s.OTSpecial));
                    TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", filterList.Count);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadDatagrid()
        {
            try
            {
                if (SearchFromDatePicker.SelectedDate != null && SearchToDatePicker.SelectedDate != null)
                {
                    NameSearchTextBox.Text = "";

                    otView = new List<OTViewModel>();
                    var otList = BusinessLayerServices.OTBL().GetFilterdOTList("", SearchFromDatePicker.SelectedDate.Value, SearchToDatePicker.SelectedDate.Value);
                    
                    var otEmployee = otList.GroupBy(g => new { g.Employee_ID, g.OTDate })
                                           .Select(s => new OTViewModel { Employee_ID = s.Key.Employee_ID, OTDATE = s.Key.OTDate}).ToList();

                    foreach (var item in otEmployee)
                    {
                        var _otList = otList.Where(w => w.Employee_ID == item.Employee_ID && w.OTDate == item.OTDATE);
                        var _otData = _otList.FirstOrDefault();
                        otView.Add(new OTViewModel
                        {
                            Employee_ID = item.Employee_ID,
                            FingerScanID = _otData.Employee.FingerScanID,
                            TitleNameTH = _otData.Employee.Person.TitleName.TitleNameTH,
                            FirstNameTH = _otData.Employee.Person.FirstNameTH,
                            LastNameTH = _otData.Employee.Person.LastNameTH,
                            OTDATE = _otData.OTDate,
                            OTnormal = _otList.Where(e => e.OTType == "N").Sum(e => e.NcountOT),
                            Holiday = _otList.Where(e => e.OTType == "H").Sum(e => e.NcountOT),
                            OTHoliday = _otList.Where(e => e.OTType == "S").Sum(e => e.NcountOT),
                            OTSpecial = _otList.Where(e => e.OTType == "SP").Sum(e => e.NcountOT),
                            Remark = _otData.Remark == null ? "" : _otData.Remark,
                            PayrollLot = _otData.PayrollLot,
                            OTType = _otData.OTType,
                            RequestTypeName = _otData.OTRequestType.RequestFlagName,
                            RequestType = _otData.RequestType,
                            AdminApproveStatus = _otData.AdminApproveStatus,
                            ManagerApproveStatus = _otData.ManagerApproveStatus,
                            FinalApproverStatus = _otData.FinalApproverStatus,
                            HRApproveStatus = _otData.HRApproveStatus
                        });
                    }

                    //var _weekendHoliday = otView.Where(e => e.Employee.Staff_type != 1 && e.OTType == "H" && e.OTDate.DayOfWeek == DayOfWeek.Sunday).Sum(e => e.NcountOT); // Only labour's sunday working

                    InformationDataGrid.ItemsSource = null;
                    InformationDataGrid.ItemsSource = otView;
                    OTNTextBox.Text = string.Format("{0:0.###}", otView.Sum(s => s.OTnormal));
                    //OTNTextBox.Visibility = Visibility.Collapsed;
                    OTHTextBox.Text = string.Format("{0:0.###}", otView.Sum(s => s.Holiday));
                    //OTHTextBox.Visibility = Visibility.Collapsed;
                    OTWTextBox.Text = string.Format("{0:0.###}", otView.Sum(s => s.OTWeekend));
                    //OTWTextBox.Visibility = Visibility.Collapsed;
                    OTHHTextBox.Text = string.Format("{0:0.###}", otView.Sum(s => s.OTHoliday));
                    //OTHHTextBox.Visibility = Visibility.Collapsed;
                    OTSTextBox.Text = string.Format("{0:0.###}", otView.Sum(s => s.OTSpecial));
                    //OTSTextBox.Visibility = Visibility.Collapsed;
                    TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", otView.Count);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var selected = (OTViewModel)InformationDataGrid.SelectedItem;
                if (selected.Holiday > 0 && selected.OTHoliday > 0)
                {
                    OTWindow w = new OTWindow(selected, "Select");
                    w.ShowDialog();
                    selected = w.selectedOT; 
                }
                if (selected != null)
                {
                    var emp = BusinessLayerServices.EmployeeBL().GetEmployeeByEmployeeID(selected.Employee_ID);
                    EmployeeIDTextBox.Text = selected.Employee_ID;
                    FingerIDTextBox.Text = selected.FingerScanID;
                    TitleCombobox.SelectedValue = emp.Person.TitleName.TitleName_ID;
                    NameTHTextBox.Text = emp.Person.FirstNameTH;
                    LastnameTHTextBox.Text = emp.Person.LastNameTH;
                    StaffTypeCombobox.SelectedValue = emp.Staff_type;

                    var ot = BusinessLayerServices.OTBL().GetSingleOT(selected.Employee_ID, selected.OTDATE, selected.OTType, selected.RequestType);
                    LotCombobox.SelectedValue = selected.PayrollLot;
                    OTDatePicker.Text = "";
                    OTDatePicker.SelectedDate = selected.OTDATE;
                    OTTypeCombobox.SelectedValue = selected.OTType;
                    RequestTypeCombobox.SelectedValue = selected.RequestType;

                    string otAmount = ot.NcountOT.ToString();
                    decimal otHour = Math.Truncate(Convert.ToDecimal(otAmount));
                    decimal otMinute = Convert.ToDecimal(otAmount) - otHour;
                    OTHourCombobox.SelectedValue = otHour;
                    OTMinuteCombobox.SelectedValue = otMinute;

                    AddButton.IsEnabled = false;
                    UpdateButton.IsEnabled = true;
                    OTDatePicker.IsEnabled = false;
                    OTTypeCombobox.IsEnabled = false;
                    RequestTypeCombobox.IsEnabled = false;
                    ManuaulWorkdateButton.IsEnabled = false;
                }
                //else Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var selected = (OTViewModel)InformationDataGrid.SelectedItem;
                if (selected.Holiday > 0 && selected.OTHoliday > 0)
                {
                    OTWindow w = new OTWindow(selected, "Delete");
                    w.ShowDialog();
                }
                else if (MessageBox.Show("ยืนยันการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {                    
                    if (selected != null)
                    {
                        ConditionChecking();
                        var oldItem = BusinessLayerServices.OTBL().GetSingleOT(selected.Employee_ID, selected.OTDATE, selected.OTType, selected.RequestType);
                        BusinessLayerServices.OTBL().DeleteOT(oldItem);
                        DataCorrectorStatic<OT>.AddTransaction("D", "OT", oldItem, null);
                        MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        Clear();
                        ReloadDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void LotCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var selected = (Lot_Number)LotCombobox.SelectedItem;
                if (selected != null)
                {
                    LotFromDatePicker.SelectedDate = selected.Start_date;
                    LotToDatePicker.SelectedDate = selected.Finish_date;
                    OTDatePicker.DisplayDateStart = selected.Start_date;
                    OTDatePicker.DisplayDateEnd = selected.Finish_date;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchLotCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var selected = (Lot_Number)SearchLotCombobox.SelectedItem;
                if (selected != null)
                {
                    SearchFromDatePicker.SelectedDate = selected.Start_date;
                    SearchToDatePicker.SelectedDate = selected.Finish_date;
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ManuaulWorkdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManualWorkingDateWindow window = new ManualWorkingDateWindow();
                window.EmployeeIDTextBox.Text = EmployeeIDTextBox.Text;
                window.LotTextBox.Text = LotCombobox.SelectedValue.ToString();
                window.LotFromDatePicker.SelectedDate = LotFromDatePicker.SelectedDate;
                window.LotToDatePicker.SelectedDate = LotToDatePicker.SelectedDate;
                window.ManualDatePicker.DisplayDateStart = LotFromDatePicker.SelectedDate;
                window.ManualDatePicker.DisplayDateEnd = LotToDatePicker.SelectedDate;
                window.ManualDatePicker.SelectedDate = OTDatePicker.SelectedDate;
                window.lotNumber = (Lot_Number)LotCombobox.SelectedItem;
                window.ShowDialog();

                var otDate = OTDatePicker.SelectedDate;
                OTDatePicker.SelectedDate = null;
                OTDatePicker.SelectedDate = otDate;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SpecialOTButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
