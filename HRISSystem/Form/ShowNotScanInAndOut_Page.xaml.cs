﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HRISSystemBL;
using HRISSystem.ViewModel;

namespace HRISSystem.Form
{
    /// <summary>
    /// Interaction logic for ShowNotScanInAndOut_Page.xaml
    /// </summary>
    public partial class ShowNotScanInAndOut_Page : Page
    {
        public ShowNotScanInAndOut_Page()
        {
            //InitializeComponent();
            //FromDatePicker.SelectedDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            //ToDatePicker.SelectedDate = DateTime.Now;
            //AllRadioButton.IsChecked = true;
        }

        //private void SearchButton_Click(object sender, RoutedEventArgs e)
        //{
        //    ReloadDataGrid();
        //}

        //private void ReloadDataGrid()
        //{
        //    try
        //    {
        //        if (FromDatePicker.SelectedDate == null) throw new Exception("กรุณาระบุวันที่");
        //        if (ToDatePicker.SelectedDate == null) throw new Exception("กรุณาระบุวันที่");
        //        if (FromDatePicker.SelectedDate > ToDatePicker.SelectedDate) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");

        //        InformationDataGrid.ItemsSource = null;
        //        InformationDataGrid.ItemsSource = GetNotScanInList();
        //        TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", InformationDataGrid.Items.Count);
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
        //    }
        //}

        //private List<NotScanInAndOutViewModel> GetNotScanInList()
        //{
        //    var result = new List<NotScanInAndOutViewModel>();
        //    var fromDate = FromDatePicker.SelectedDate.Value;
        //    var toDate = ToDatePicker.SelectedDate.Value;
        //    var notLoggedInList = BusinessLayerServices.EmployeeBL().sp_GetEmployeeNOTScanInAndOutByDateRange(fromDate, toDate);

        //    int _count = 0;
        //    foreach (var notLoggedIn in notLoggedInList)
        //    {
        //        //var attendanceList = BusinessLayerServices.EmployeeBL().sp_GetEmployeeTimeAttendanceByDateAndEmployeeID(fromDate, toDate, notLoggedIn.userid);
        //        var attendanceList = BusinessLayerServices.EmployeeBL().sp_GetEmployeeTimeAttendanceByDateAndEmployee1(fromDate, toDate, notLoggedIn.userid);
        //        NotScanInAndOutViewModel x = new NotScanInAndOutViewModel(); ;
        //        foreach (var attendance in attendanceList)
        //        {
        //            if (attendance.CHECKIN == null)
        //            {
        //                _count++;
        //                x = new NotScanInAndOutViewModel
        //                {
        //                    EmployeeID = attendance.Employee_ID,
        //                    FingerScanID = attendance.FINGERSCANCODE.ToString(),
        //                    Titlename = attendance.TITLENAMETH,
        //                    Firstname = attendance.FIRSTNAMETH,
        //                    Lastname = attendance.LASTNAMETH,
        //                    StaffType = attendance.Staff_type == "1" ? "พนักงานประจำ" : attendance.Staff_type == "2" ? "พนักงานรายวันประจำ" : "พนักงานรายวัน",
        //                    NCount = _count
        //                };
        //            }
        //            else _count = 0;
        //        }

        //        if (_count >= 5)
        //        {
        //            result.Add(x);
        //        }
        //    }

        //    //if (result.Any())
        //    //{
        //    //    if (FulltimeRadioButton.IsChecked.Value) result = result.Where(w => w.StaffType == "พนักงานประจำ").ToList();
        //    //    if (RegularRadioButton.IsChecked.Value) result = result.Where(w => w.StaffType == "พนักงานรายวันประจำ").ToList();
        //    //    if (DailyRadioButton.IsChecked.Value) result = result.Where(w => w.StaffType == "พนักงานรายวัน").ToList();
        //    //}

        //    return result;
        //}

        //private void ViewButton_Click(object sender, RoutedEventArgs e)
        //{
        //    ViewData();
        //}

        //private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        //{
        //    ViewData();
        //}

        //private void ViewData()
        //{
        //    try
        //    {
        //        NotScanInAndOutViewModel selected = (NotScanInAndOutViewModel)InformationDataGrid.SelectedItem;
        //        Report.HR015Report report = new Report.HR015Report();
        //        report.FromDatePicker.SelectedDate = FromDatePicker.SelectedDate;
        //        report.ToDatePicker.SelectedDate = ToDatePicker.SelectedDate;
        //        report.EmployeeIDTextBox.Text = selected.EmployeeID;
        //        report.NameTHTextBox.Text = selected.Firstname;
        //        report.LastnameTHTextBox.Text = selected.Lastname;
        //        report.ShowDialog();
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
        //    }
        //}
    }
}
