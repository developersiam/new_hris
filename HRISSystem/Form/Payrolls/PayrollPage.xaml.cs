﻿using DomainModelHris;
using DomainModelPayroll;
using HRISSystem.Shared;
using HRISSystem.ViewModel;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OfficeOpenXml;
using Spire.Xls;
using System.Data;
using System.Windows.Media.Animation;
using System.ComponentModel;
using System.Threading;

namespace HRISSystem.Form.Payrolls
{
    /// <summary>
    /// Interaction logic for PayrollPage.xaml
    /// </summary>
    public partial class PayrollPage : Page
    {
        List<PNDViewModel> studentLoanList;
        List<PNDViewModel> specialList;
        List<PNDViewModel> supportList;
        List<PayrollViewModel> payrollList;
        public PayrollPage()
        {
            InitializeComponent();
            LotCombobox.ItemsSource = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);
            LotCombobox.SelectedIndex = -1;
            DepartmentCombobox.ItemsSource = DepartmentList();
            DepartmentCombobox.SelectedIndex = -1;
            studentLoanList = new List<PNDViewModel>();
            specialList = new List<PNDViewModel>();
            supportList = new List<PNDViewModel>();
            BindStaffType();
            PBar.Value = 0;

        }

        private List<Department> DepartmentList()
        {
            List<Department> result = new List<Department>();
            result.Add(new Department { DeptCode = "", DeptName = "All Department" });
            result.AddRange(BusinessLayerServices.DeptCodeBL().GetAllDepartment());
            return result;
        }

        private void BindStaffType()
        {
            var dept = SingletonConfiguration.getInstance().DepartmentCode.ToUpper();
            var username = SingletonConfiguration.getInstance().Username.ToLower();
            var staffList = new List<StaffType>();
            if(username != "m.tanainan") staffList.Add(new StaffType { StaffTypeID = 1, Description = "พนักงานประจำ" });
            staffList.Add(new StaffType { StaffTypeID = 23, Description = "พนักงานรายวัน/รายวันประจำ" });
            staffList.Add(new StaffType { StaffTypeID = 4, Description = "พนักงานรายวันชั่วคราว" });
            StafftypeCombobox.ItemsSource = staffList;

            StafftypeCombobox.SelectedValue = username == "m.tanainan" ? 23 : 1;
            //StafftypeCombobox.IsEnabled = dept == "IT" ? true : false; // Enable only ITC
            StafftypeCombobox.IsEnabled = true; // Enable ALL 27/05/2022
        }

        private void LotCombobox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                var selected = (Lot_Number)LotCombobox.SelectedItem;
                if (selected == null) return;
                LotFromDatePicker.SelectedDate = selected != null ? selected.Start_date : null;
                LotToDatePicker.SelectedDate = selected != null ? selected.Finish_date : null;
                SalaryPaidDatePicker.SelectedDate = selected != null ? selected.Salary_Paid_date : null;
                OTPaidDatePicker.SelectedDate = selected != null ? selected.Ot_Paid_date : null;
                Clear();

                if (selected.Lock_Hr != "2" && selected.Lock_Hr_Labor != "2")
                {
                    MessageBox.Show("ไม่สามารถบันทึกข้อมูลงวดนี้ได้ ฝ่ายทรัพยากรณ์บุคคลยังไม่ได้ทำการล็อคงวด", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    UpdateButton.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            DepartmentCombobox.SelectedIndex = -1;
            InformationDataGrid.ItemsSource = null;
            TotalIncomeTextBox.Text = "";
            TotalPNDTextBox.Text = "";
            TotalStudentLoanTextBox.Text = "";
            TotalSSOTextBox.Text = "";
            TotalDeductTextBox.Text = "";
            GrandTotalTextBox.Text = "";
            TotalLunchTextBox.Text = "";
            TotalNightTextBox.Text = "";
            TotalNetSalaryTextBox.Text = "";
            TotalSalaryTextBox.Text = "";
            TotalSpecialTextBox.Text = "";
            UpdateButton.IsEnabled = false;
            ReportButton.IsEnabled = false;
            ReCalculateButton.IsEnabled = false;
            StudentLoanFULTextbox.Text = "";
            studentLoanList.Clear();
            SpecialFULTextbox.Text = "";
            specialList.Clear();
            SupportFULTextbox.Text = "";
            supportList.Clear();
            UpdateTextBlock.Text = "Finish";
            PBar.Value = 0;
            PBarTextBlock.Text = "Status.";
        }

        private void DepartmentCombobox_DropDownClosed(object sender, EventArgs e)
        {
            if (DepartmentCombobox.Text != "")
            {
                ReloadDatagrid("Normal");
                UpdateButton.IsEnabled = true;
                ReportButton.IsEnabled = true;
                ReCalculateButton.IsEnabled = true;
            }
        }

        private void StafftypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            ReloadDatagrid("Normal");
        }

        private void ReCalculateButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid("Recalculate");
        }

        private void ReloadDatagrid(string reloadType)
        {
            try
            {
                if (string.IsNullOrEmpty(LotCombobox.Text)) throw new Exception("กรุณาระบุงวด");
                //if (string.IsNullOrEmpty(DepartmentCombobox.Text)) throw new Exception("กรุณาระบุแผนก/ฝ่าย");
                reloadType = studentLoanList.Any() || specialList.Any() ? "Recalculate" : reloadType;
                var dept = (Department)DepartmentCombobox.SelectedItem;
                var lot = (Lot_Number)LotCombobox.SelectedItem;
                var staff = (StaffType)StafftypeCombobox.SelectedItem;
                var from = LotFromDatePicker.SelectedDate.Value;
                var to = LotToDatePicker.SelectedDate.Value;

                if (dept == null || lot == null || staff == null) return;

                payrollList = new List<PayrollViewModel>();
                //LoadingIcon.Visibility = Visibility.Visible;

                if (reloadType == "Normal")
                {
                    var savedList = BusinessLayerServices.PayrollBL().GetPayrollByLotAndDepartment(LotCombobox.Text, dept.DeptCode, staff.StaffTypeID);
                    if (savedList.Any() || savedList.Count > 0) payrollList = HRISService.DataCorrector().CovertToPayrollVM(savedList);
                    else
                    {
                        var newPayrollList = BusinessLayerServices.PayrollBL().sp_Newget(lot.Lot_Month, lot.Lot_Year, dept.DeptCode, staff.StaffTypeID);
                        payrollList = HRISService.DataCorrector().CovertToPayrollVM(newPayrollList, from, to);
                    }
                }
                else if ( reloadType == "Recalculate")
                {
                    var newPayrollList = BusinessLayerServices.PayrollBL().sp_Newget(lot.Lot_Month, lot.Lot_Year, dept.DeptCode, staff.StaffTypeID);
                    payrollList = HRISService.DataCorrector().CovertToPayrollVM(newPayrollList, from, to);
                }

                if (studentLoanList.Any()) studentLoanList.ForEach(f =>
                {
                    var payroll = payrollList.FirstOrDefault(p => p._payroll.Id_card == f.citizenID);
                    if (payroll != null)
                    {
                        payroll._payroll.Student_Loan = f.incomeTax;
                        payroll._payroll.Net_Income -= f.incomeTax;
                    }
                });

                foreach (var f in specialList)
                {
                    var payroll = payrollList.FirstOrDefault(p => p._payroll.Id_card == f.citizenID);
                    if (payroll != null)
                    {
                        payroll._payroll.Special = f.incomeTax;
                        payroll.SumSpecial = f.incomeTax;
                        payroll._payroll.Total_Income += f.incomeTax;
                        payroll._payroll.Net_Income += f.incomeTax;
                    }
                }

                var x = new List<PNDViewModel>();

                if (supportList.Any())
                {
                    payrollList.ForEach(f => 
                    {
                        f._payroll.Lunch_sup = 0;
                        f._payroll.Night_sup = 0;
                    });

                    supportList.ForEach(f =>
                    {
                        var payroll = payrollList.FirstOrDefault(p => p._payroll.Id_card == f.citizenID);
                        if (payroll != null)
                        {
                            payroll._payroll.Lunch_sup = f.incomeTax;
                            payroll._payroll.Night_sup = f.importedIncomeTax;
                            payroll._payroll.Total_Income += (f.incomeTax + f.importedIncomeTax);
                            payroll._payroll.Net_Income += (f.incomeTax + f.importedIncomeTax);
                            x.Add(f);
                        }
                    });
                }

                //x.ForEach(f => supportList.Remove(f));

                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = payrollList;
                Summary();
                UpdateTextBlock.Text = dept.DeptCode == "" ? "Finish All" : "Finish";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Summary()
        {
            try
            {
                TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", payrollList.Count);
                TotalLunchTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s._payroll.Lunch_sup).Value);
                TotalNightTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s._payroll.Night_sup).Value);
                TotalSalaryTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s._payroll.Salary).Value);
                TotalNetSalaryTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s._payroll.NetSalary).Value);
                TotalSpecialTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s.SumSpecial).Value);
                TotalIncomeTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s._payroll.Total_Income).Value);
                TotalPNDTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s.PND).Value); 
                TotalStudentLoanTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s._payroll.Student_Loan).Value);
                TotalSSOTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s._payroll.SSO).Value); 
                TotalDeductTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s._payroll.Cut_wage).Value);
                GrandTotalTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s._payroll.Net_Income)); 
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันการบันทึกหรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;
                var _lot = (Lot_Number)LotCombobox.SelectedItem;
                var lot = BusinessLayerServices.PayrollBL().GetSingleLot(_lot.Lot_Month, _lot.Lot_Year);
                var dept = (Department)DepartmentCombobox.SelectedItem;
                var staffType = (StaffType)StafftypeCombobox.SelectedItem;
                var username = SingletonConfiguration.getInstance().Username.ToLower();
                if (lot == null) throw new Exception("กรุณาระบุงวด");
                if (dept == null) throw new Exception("กรุณาระบุแผนก/ฝ่าย");

                if (username == "m.tanainan" && lot.Lock_Acc_Labor != "2") throw new Exception("ไม่สามารถบันทึกข้อมูลงวดนี้ได้ กรุณาทำการล็อคงวด");
                if ((username == "netchanok" || username == "dachar") && lot.Lock_Acc != "2") throw new Exception("ไม่สามารถบันทึกข้อมูลงวดนี้ได้ กรุณาทำการล็อคงวด");

                var updateList = new List<Payroll>();

                foreach (var p in payrollList)
                {
                    var _payroll = p._payroll;
                    //var totalIncomeLabour = Math.Round(decimal.Divide(_payroll.Total_Income.Value, (decimal)0.97), 2);
                    _payroll.Payroll_date = LotCombobox.Text;
                    _payroll.Salary_paid_date = lot.Salary_Paid_date;
                    _payroll.Ot_paid_date = lot.Ot_Paid_date;
                    //_payroll.Total_Income_Labour = _payroll.Staff_type == "1" ? (decimal?)null : totalIncomeLabour;
                    //_payroll.Tax_Labour = _payroll.Staff_type == "1" ? (decimal?)null : Math.Round(totalIncomeLabour - _payroll.Total_Income.Value, 2);
                    _payroll.Net_Income = Math.Round(_payroll.Net_Income.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero);
                    _payroll.Total_Income = Math.Round(_payroll.Total_Income.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero);
                    _payroll.Username = SingletonConfiguration.getInstance().Username;
                    _payroll.Ddate = DateTime.Now;
                    updateList.Add(_payroll);
                }
                var currentList = BusinessLayerServices.PayrollBL().GetPayrollByLotAndDepartment(LotCombobox.Text, dept.DeptCode, staffType.StaffTypeID);
                BusinessLayerServices.PayrollBL().UpdatePayroll(currentList, updateList);
                //DataCorrectorStatic<Payroll>.AddTransactionList("A", "Payroll", null, updateList);
                MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var payrollDetail = (PayrollViewModel)InformationDataGrid.SelectedItem;
                if (payrollDetail == null) return;

                var p = new PayrollWindow(payrollDetail, (Lot_Number)LotCombobox.SelectedItem);
                p.ShowDialog();
                var _vmDetail = payrollList.FirstOrDefault(f => f._payroll.Employee_id == p.payrollDetail._payroll.Employee_id);
                _vmDetail = p.payrollDetail;

                NameFilter();
                Summary();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReportButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var dept = (Department)DepartmentCombobox.SelectedItem;
                var staff = (StaffType)StafftypeCombobox.SelectedItem;
                if (LotCombobox.SelectedItem == null) throw new Exception("กรุณาระบุงวด");
                if (dept == null) throw new Exception("กรุณาระบุแผนก/ฝ่าย");

                var w = new Report.PR006Report(dept, LotCombobox.Text, staff.StaffTypeID);
                w.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void StudentLoanFULTextbox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ImportExcel("StudentLoan");
        }

        private void StudentLoanImportButton_Click(object sender, RoutedEventArgs e)
        {
            ImportExcel("StudentLoan");
        }

        private void SpecialFULTextbox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ImportExcel("Special");
        }

        private void SpecialImportButton_Click(object sender, RoutedEventArgs e)
        {
            ImportExcel("Special");
        }

        private void ImportExcel(string importType)
        {
            try
            {
                System.Windows.Forms.OpenFileDialog fileDialog = new System.Windows.Forms.OpenFileDialog();
                fileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                fileDialog.FilterIndex = 2;
                fileDialog.RestoreDirectory = true;

                if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (importType == "StudentLoan") StudentLoanFULTextbox.Text = fileDialog.FileName;
                    if (importType == "Special") SpecialFULTextbox.Text = fileDialog.FileName;
                    if (importType == "Support") SupportFULTextbox.Text = fileDialog.FileName;
                    var excelList = ReadExcelFile(fileDialog.FileName);
                    if (excelList == null || excelList.Count <= 0)  throw new Exception("ไม่สามารถเปิดไฟล์ได้ !! กรุณาปิดไฟล์ Excel ที่กำลัง Upload.");
                    if (importType == "StudentLoan") studentLoanList = excelList;
                    if (importType == "Special") specialList = excelList;
                    if (importType == "Support") supportList = excelList;
                }

                ReloadDatagrid("Recalculate");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Excel reading : " + ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public List<PNDViewModel> ReadExcelFile(string FilePath)
        {
            Workbook wb = new Workbook();
            wb.LoadFromFile(FilePath);
            Worksheet sheet = wb.Worksheets[0];
            var collection = DataTableToList(sheet.ExportDataTable());
            return collection;
        }

        public List<PNDViewModel> DataTableToList(DataTable Datable)
        {
            List<PNDViewModel> PndViewmodelList = new List<PNDViewModel>();
            foreach (DataRow row in Datable.Rows)
            {
                if (row[0].ToString() == "") break;
                PNDViewModel model = new PNDViewModel();
                model.citizenID = row[0].ToString();
                try { model.incomeTax = Convert.ToDecimal(row[1].ToString()); } // Use incomeTax Instead Studentloan and Special
                catch (Exception) { model.incomeTax = 0; }
                try { model.importedIncomeTax = Convert.ToDecimal(row[2].ToString()); } // Use importedIncomeTax Instead Night shift support
                catch (Exception) { model.importedIncomeTax = 0; }
                PndViewmodelList.Add(model);
            }
            return PndViewmodelList;
        }

        private void SupportImportButton_Click(object sender, RoutedEventArgs e)
        {
            ImportExcel("Support");
        }

        private void SupportFULTextbox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ImportExcel("Support");
        }

        private void FilterTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            NameFilter();
        }

        private void NameFilter()
        {
            try
            {
                if (payrollList == null || !payrollList.Any()) return;
                var filter = FilterTextBox.Text.Trim();
                var filterList = filter == "" ? payrollList : payrollList.Where(w => w._payroll.Fname.Contains(filter)).ToList();
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = filterList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
