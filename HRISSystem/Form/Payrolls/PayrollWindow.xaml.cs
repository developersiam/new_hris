﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DomainModelPayroll;
using HRISSystemBL;
using HRISSystem.ViewModel;

namespace HRISSystem.Form.Payrolls
{
    /// <summary>
    /// Interaction logic for PayrollWindow.xaml
    /// </summary>
    public partial class PayrollWindow : Window
    {
        public PayrollViewModel payrollDetail;
        public PayrollWindow(PayrollViewModel _payrollDetail, Lot_Number _lot)
        {
            InitializeComponent();
            StaffTypeCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
            BindData(_payrollDetail);
            payrollDetail = _payrollDetail;
            PayrollWindows.Focus();
        }

        private void BindData(PayrollViewModel _vmDetail)
        {
            try
            {
                if (_vmDetail == null) return;
                var _payrollDetail = _vmDetail._payroll;
                //TitleCombobox.Text = _payrollDetail.
                EmployeeTextBox.Text = _payrollDetail.Employee_id;
                NameTHTextBox.Text = _payrollDetail.Fname;
                LastnameTHTextBox.Text = _payrollDetail.Lname;
                StaffTypeCombobox.SelectedValue = _payrollDetail.Staff_type;
                SalaryTextBox.Text = _payrollDetail.Staff_type == "1" ? (_payrollDetail.Salary == null ? "" : _payrollDetail.Salary.Value.ToString("0.##")) : "";
                NetSalaryTextBox.Text = _payrollDetail.Staff_type == "1" ? (_payrollDetail.NetSalary == null ? "" : _payrollDetail.NetSalary.Value.ToString("0.##")) : "";
                WageTextBox.Text = _payrollDetail.Wage == null ? "" : _payrollDetail.Wage.Value.ToString("0.##");
                WorkCountTextBox.Text = _payrollDetail.Working_date == null ? "" : _payrollDetail.Working_date.Value.ToString("0.##");
                OTHourRateTextBox.Text = _payrollDetail.Ot_hour_rate == null ? "0" : _payrollDetail.Ot_hour_rate.Value.ToString("0.##");
                OTHourTextBox.Text = _payrollDetail.Cot_hour == null ? "0" : _payrollDetail.Cot_hour.Value.ToString("0.##");
                SumOTHourTextBox.Text = _vmDetail.SumOT == null ? "0" : _vmDetail.SumOT.Value.ToString("0.##");
                OTHolidayRateTextBox.Text = _payrollDetail.Ot_holiday_rate == null ? "0" : _payrollDetail.Ot_holiday_rate.Value.ToString("0.##");
                OTHolidayTextBox.Text = _payrollDetail.Cot_holiday == null ? "0" : _payrollDetail.Cot_holiday.Value.ToString("0.##");
                SumOTHolidayTextBox.Text = _vmDetail.SumOTH == null ? "0" : _vmDetail.SumOTH.Value.ToString("0.##");
                OTHolidayHourRateTextBox.Text = _payrollDetail.Ot_hour_holiday_rate == null ? "0" : _payrollDetail.Ot_hour_holiday_rate.Value.ToString("0.##");
                OTHolidayHourTextBox.Text = _payrollDetail.Cot_hour_holiday == null ? "0" : _payrollDetail.Cot_hour_holiday.Value.ToString("0.##");
                SumOTHolidayHourTextBox.Text = _vmDetail.SumOTHH == null ? "0" : _vmDetail.SumOTHH.Value.ToString("0.##");
                SpecialOTTextBox.Text = _payrollDetail.Ot_special_rate == null ? "0" : _payrollDetail.Ot_special_rate.Value.ToString("0.##");
                SpecialTextBox.Text = _payrollDetail.Special == null ? "0" : _payrollDetail.Special.Value.ToString("0.##");
                BonusTextBox.Text = _payrollDetail.Bonus == null ? "0" : _payrollDetail.Bonus.Value.ToString("0.##");
                LunchSupTextBox.Text = _payrollDetail.Lunch_sup == null ? "0" : _payrollDetail.Lunch_sup.Value.ToString("0.##");
                NightSupTextBox.Text = _payrollDetail.Night_sup == null ? "0" : _payrollDetail.Night_sup.Value.ToString("0.##");
                TotalIncomeTextBox.Text = _payrollDetail.Total_Income == null ? "0" : _payrollDetail.Total_Income.Value.ToString("0.##");
                PNDTextBox.Text = _vmDetail.PND == null ? "0" : _vmDetail.PND.Value.ToString("0.##");
                SSOTextBox.Text = _payrollDetail.SSO == null ? "0" : _payrollDetail.SSO.Value.ToString("0.##");
                CutWageTextBox.Text = _payrollDetail.Cut_wage == null ? "0" : _payrollDetail.Cut_wage.Value.ToString("0.##");
                StudentLoanTextBox.Text = _payrollDetail.Student_Loan == null ? "0" : _payrollDetail.Student_Loan.Value.ToString("0.##");
                //NetInComeTextBox.Text = _payrollDetail.NetInCome == null ? "" : _payrollDetail.NetInCome.Value.ToString("0.##");
                NetInComeTextBox.Text = _payrollDetail.Net_Income.GetValueOrDefault().ToString("0.##");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            ReCalculate();
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) UpdateButton.Focus();
        }

        private void ReCalculate()
        {
            try
            {
                var netSalary = NetSalaryTextBox.Text == "" ? 0 : Convert.ToDecimal(NetSalaryTextBox.Text);
                var wage = WageTextBox.Text == "" ? 0 : Convert.ToDecimal(WageTextBox.Text);
                var cWork = WorkCountTextBox.Text == "" ? 0 : Convert.ToDecimal(WorkCountTextBox.Text);
                var sumOT = SumOTHourTextBox.Text == "" ? 0 : Convert.ToDecimal(SumOTHourTextBox.Text);
                var sumOTH = SumOTHolidayTextBox.Text == "" ? 0 : Convert.ToDecimal(SumOTHolidayTextBox.Text);
                var sumOTHH = SumOTHolidayHourTextBox.Text == "" ? 0 : Convert.ToDecimal(SumOTHolidayHourTextBox.Text);
                var specialOT = SpecialOTTextBox.Text == "" ? 0 : Convert.ToDecimal(SpecialOTTextBox.Text);
                var special = SpecialTextBox.Text == "" ? 0 : Convert.ToDecimal(SpecialTextBox.Text);
                var bonus = BonusTextBox.Text == "" ? 0 : Convert.ToDecimal(BonusTextBox.Text);
                var lunch = LunchSupTextBox.Text == "" ? 0 : Convert.ToDecimal(LunchSupTextBox.Text);
                var night = NightSupTextBox.Text == "" ? 0 : Convert.ToDecimal(NightSupTextBox.Text);

                var totalIncome = netSalary + (cWork * wage) + sumOT + sumOTH + sumOTHH + specialOT + special + bonus + lunch + night;
                TotalIncomeTextBox.Text = totalIncome.ToString();

                var pnd = PNDTextBox.Text == "" ? 0 : Convert.ToDecimal(PNDTextBox.Text);
                var sso = SSOTextBox.Text == "" ? 0 : Convert.ToDecimal(SSOTextBox.Text);
                var cutWage = CutWageTextBox.Text == "" ? 0 : Convert.ToDecimal(CutWageTextBox.Text);
                var stuLoan = StudentLoanTextBox.Text == "" ? 0 : Convert.ToDecimal(StudentLoanTextBox.Text);

                var netIncome = totalIncome - (pnd + sso + cutWage + stuLoan);
                NetInComeTextBox.Text = netIncome.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Update()
        {
            try
            {
                ReCalculate();
                if (MessageBox.Show("ยืนยันการบันทึกหรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;
                payrollDetail._payroll.Special = SpecialTextBox.Text == "" || SpecialTextBox.Text == "0" ? (decimal?)null : Convert.ToDecimal(SpecialTextBox.Text);
                payrollDetail.SumSpecial = SpecialTextBox.Text == "" || SpecialTextBox.Text == "0" ? (decimal?)null : Convert.ToDecimal(SpecialTextBox.Text);
                payrollDetail._payroll.Bonus = BonusTextBox.Text == "" || BonusTextBox.Text == "0" ? (decimal?)null : Convert.ToDecimal(BonusTextBox.Text);
                payrollDetail._payroll.Total_Income = TotalIncomeTextBox.Text == "" ? (decimal?)null : Convert.ToDecimal(TotalIncomeTextBox.Text);
                payrollDetail._payroll.Cut_wage = CutWageTextBox.Text == "" || CutWageTextBox.Text == "0" ? (decimal?)null : Convert.ToDecimal(CutWageTextBox.Text);
                payrollDetail._payroll.Student_Loan = StudentLoanTextBox.Text == "" || StudentLoanTextBox.Text == "0" ? (decimal?)null : Convert.ToDecimal(StudentLoanTextBox.Text);
                payrollDetail._payroll.Tax = PNDTextBox.Text == "" || PNDTextBox.Text == "0" ? (decimal?)null : Convert.ToDecimal(PNDTextBox.Text);
                payrollDetail.PND = PNDTextBox.Text == "" || PNDTextBox.Text == "0" ? (decimal?)null : Convert.ToDecimal(PNDTextBox.Text);
                //payrollDetail.NetInCome = NetInComeTextBox.Text == "" ? (decimal?)null : Convert.ToDecimal(NetInComeTextBox.Text);
                payrollDetail._payroll.Net_Income = Convert.ToDecimal(NetInComeTextBox.Text);

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            Update();
        }
        private void PayrollWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && PayrollWindows.IsFocused) Update();
            else if (e.Key == Key.Escape) Close();
        }
    }
}
