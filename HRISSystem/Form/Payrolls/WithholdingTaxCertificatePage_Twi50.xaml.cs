﻿using HRISSystem.Shared;
using HRISSystem.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Spire.Xls;
using DomainModelHris;
using HRISSystemBL;
using DomainModelPayroll;

namespace HRISSystem.Form.Payrolls
{
    /// <summary>
    /// Interaction logic for WithholdingTaxCertificatePage_Twi50.xaml
    /// </summary>
    public partial class WithholdingTaxCertificatePage_Twi50 : Page
    {
        List<Payroll> bonusList;
        List<Payroll> retireList;
        List<Payroll> payrollList;
        List<Payroll> filterList;

        public WithholdingTaxCertificatePage_Twi50()
        {
            InitializeComponent();
            bonusList= new List<Payroll>();
            retireList= new List<Payroll>();
            YearCombobox.ItemsSource = HRISService.DataCorrector().GetLotYearList();
            DepartmentCombobox.ItemsSource = DepartmentList();
            DepartmentCombobox.SelectedIndex= 0;
            StafftypeCombobox.ItemsSource = StaffTypeList();
            StafftypeCombobox.SelectedIndex= 0;
        }

        private List<Department> DepartmentList()
        {
            List<Department> result = new List<Department>();
            result.Add(new Department { DeptCode = "", DeptName = "All Department" });
            result.AddRange(BusinessLayerServices.DeptCodeBL().GetAllDepartment());
            return result;
        }

        private List<StaffType> StaffTypeList()
        {
            var staffList = new List<StaffType>();
            staffList.Add(new StaffType { StaffTypeID = 0, Description = "พนักงานทุกประเภท" });
            staffList.Add(new StaffType { StaffTypeID = 1, Description = "พนักงานประจำ" });
            staffList.Add(new StaffType { StaffTypeID = 23, Description = "พนักงานรายวัน/รายวันประจำ" });
            staffList.Add(new StaffType { StaffTypeID = 4, Description = "พนักงานรายวันชั่วคราว" });
            StafftypeCombobox.ItemsSource = staffList;
            return staffList;
        }

        private void YearCombobox_DropDownClosed(object sender, EventArgs e)
        {
            var selected = YearCombobox.SelectedItem;
            if (selected != null)
            {
                var year = Convert.ToInt16(YearCombobox.SelectedValue.ToString());
                DateTime firstDay = new DateTime(year, 1, 1);
                DateTime lastDay = firstDay.AddYears(1).AddDays(-1);
                LotFromDatePicker.SelectedDate = firstDay;
                LotToDatePicker.SelectedDate= lastDay;
                StafftypeCombobox.SelectedIndex = 0;
                DepartmentCombobox.SelectedIndex= 0;
                FilterTextBox.Text = "";
                ReloadDatagrid("Normal");
            }
        }

        private void StafftypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            FilterDatagrid();
        }

        private void DepartmentCombobox_DropDownClosed(object sender, EventArgs e)
        {
            FilterDatagrid();
        }

        private void ReloadDatagrid(string mode)
        {
            try
            {
                if (string.IsNullOrEmpty(YearCombobox.SelectedValue.ToString())) throw new Exception("กรุณาระบุปีภาษี");
                mode = bonusList.Any() || retireList.Any() ? "Recalculate" : mode;

                var dept = (Department)DepartmentCombobox.SelectedItem;
                var year = YearCombobox.Text;
                var staff = (StaffType)StafftypeCombobox.SelectedItem;

                if (dept == null || year == "" || staff == null) return;

                payrollList = new List<Payroll>();

                if (mode == "Normal")
                {

                    var savedList = BusinessLayerServices.SummaryPayrollBL().GetPayrollSummaryList(Convert.ToInt16(year));
                    if (savedList.Any() || savedList.Count > 0) payrollList = HRISService.DataCorrector().ConvertSummaryPRToPayroll(savedList);
                    else
                    {
                        payrollList = BusinessLayerServices.SummaryPayrollBL().GetCalculatePayrollList(Convert.ToInt16(year));
                    }
                }
                else if (mode == "Recalculate")
                {
                    payrollList = BusinessLayerServices.SummaryPayrollBL().GetCalculatePayrollList(Convert.ToInt16(year));
                }

                if (bonusList.Any()) bonusList.ForEach(bonus =>
                {
                    var payroll = payrollList.FirstOrDefault(p => p.Id_card == bonus.Id_card);
                    if (payroll != null)
                    {
                        payroll.Bonus = bonus.Wage;
                    }
                });

                if (retireList.Any()) retireList.ForEach(retire =>
                {
                    var payroll = payrollList.FirstOrDefault(p => p.Id_card == retire.Id_card);
                    if (payroll != null)
                    {
                        payroll.Lunch_sup = retire.Wage; 
                    }
                });

                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = payrollList;
                Summary(payrollList);
                UpdateTextBlock.Text = dept.DeptCode == "" ? "Finish All" : "Finish";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Summary(List<Payroll> showList)
        {
            try
            {
                TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", showList.Count);
                TotalIncomeTextBox.Text = string.Format("{0:n}", showList.Sum(s => s.Total_Income).Value);
                BonusTextBox.Text = string.Format("{0:n}", showList.Sum(s => s.Bonus).Value);
                Net_IncomeTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s.Net_Income).Value);
                RetireTextBox.Text = string.Format("{0:n}", showList.Sum(s => s.Lunch_sup).Value);
                EarlyTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s.Night_sup).Value);
                TaxTextBox.Text = string.Format("{0:n}", showList.Sum(s => s.Tax).Value);
                SSOTextBox.Text = string.Format("{0:n}", showList.Sum(s => s.SSO).Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FilterDatagrid()
        {
            try
            {
                var dept = (Department)DepartmentCombobox.SelectedItem;
                //var year = (Lot_Number)YearCombobox.SelectedItem;
                var year = YearCombobox.Text;
                var staff = (StaffType)StafftypeCombobox.SelectedItem;
                var text = FilterTextBox.Text.Trim();

                if (dept == null || year == null || staff == null) return;
                if (payrollList == null) return;

                if (dept.DeptCode == "" && staff.StaffTypeID == 0) filterList = payrollList; // Both All
                else
                {
                    if (dept.DeptCode != "" && staff.StaffTypeID == 0) filterList = payrollList.Where(w => w.Dept_code == dept.DeptCode).ToList(); //Only dept
                    else if (dept.DeptCode == "" && staff.StaffTypeID != 0) //Only staff
                    {
                        if (staff.StaffTypeID == 23) filterList = payrollList.Where(w => w.Staff_type == "2" || w.Staff_type == "3").ToList();
                        else filterList = payrollList.Where(w => w.Staff_type == staff.StaffTypeID.ToString()).ToList();
                    }
                    else // Both
                    {
                        if (staff.StaffTypeID == 23) filterList = payrollList.Where(w => w.Staff_type == "2" || w.Staff_type == "3").ToList();
                        else filterList = payrollList.Where(w => w.Staff_type == staff.StaffTypeID.ToString()).ToList();
                        filterList = payrollList.Where(w => w.Dept_code == dept.DeptCode).ToList();
                    }
                }

                if (text != "") filterList = filterList.Where(w => w.Fname.StartsWith(text)).ToList();

                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = filterList;
                Summary(filterList);
                UpdateTextBlock.Text = dept.DeptCode == "" ? "Finish All" : "Finish";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BonusFULTextbox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ImportExcel("Bonus");
        }

        private void BonusImportButton_Click(object sender, RoutedEventArgs e)
        {
            ImportExcel("Bonus");
        }

        private void RetireFULTextbox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ImportExcel("Retire");
        }

        private void RetireImportButton_Click(object sender, RoutedEventArgs e)
        {
            ImportExcel("Retire");
        }

        private void ImportExcel(string importType)
        {
            try
            {
                System.Windows.Forms.OpenFileDialog fileDialog = new System.Windows.Forms.OpenFileDialog();
                fileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                fileDialog.FilterIndex = 2;
                fileDialog.RestoreDirectory = true;

                if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (importType == "Bonus") BonusFULTextbox.Text = fileDialog.FileName;
                    if (importType == "Retire") RetireFULTextbox.Text = fileDialog.FileName;

                    var excelList = ReadExcelFile(fileDialog.FileName);
                    if (excelList == null || excelList.Count <= 0) throw new Exception("ไม่สามารถเปิดไฟล์ได้ !! กรุณาปิดไฟล์ Excel ที่กำลัง Upload.");
                    if (importType == "Bonus") bonusList = excelList;
                    if (importType == "Retire") retireList = excelList;
                }

                ReloadDatagrid("Recalculate");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Excel reading : " + ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public List<Payroll> ReadExcelFile(string FilePath)
        {
            Workbook wb = new Workbook();
            wb.LoadFromFile(FilePath);
            Worksheet sheet = wb.Worksheets[0];
            var collection = DataTableToList(sheet.ExportDataTable());
            return collection;
        }

        public List<Payroll> DataTableToList(DataTable Datable)
        {
            List<Payroll> PndViewmodelList = new List<Payroll>();
            foreach (DataRow row in Datable.Rows)
            {
                if (row[0].ToString() == "") break;
                var model = new Payroll();
                model.Id_card = row[0].ToString();
                try { model.Wage = Convert.ToDecimal(row[1].ToString()); } // Use incomeTax Instead Studentloan and Special
                catch (Exception) { model.Wage = 0; }
                //try { model.importedIncomeTax = Convert.ToDecimal(row[2].ToString()); } // Use importedIncomeTax Instead Night shift support
                //catch (Exception) { model.importedIncomeTax = 0; }
                PndViewmodelList.Add(model);
            }
            return PndViewmodelList;
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ReCalculateButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid("Recalculate");
        }

        private void ReportButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void FilterTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                var dept = (Department)DepartmentCombobox.SelectedItem;
                //var year = (Lot_Number)YearCombobox.SelectedItem;
                var year = YearCombobox.Text;
                var staff = (StaffType)StafftypeCombobox.SelectedItem;
                var text = FilterTextBox.Text.Trim();

                if (dept == null || year == null || staff == null) return;
                if (payrollList == null) return;
                if (filterList == null) filterList = payrollList;

                var filterText = new List<Payroll>();

                if (text != "")
                {
                    filterText = filterList.Where(w => w.Fname.StartsWith(text)).ToList();
                }
                else filterText = filterList;

                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = filterText;
                Summary(filterText);
                UpdateTextBlock.Text = dept.DeptCode == "" ? "Finish All" : "Finish";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
