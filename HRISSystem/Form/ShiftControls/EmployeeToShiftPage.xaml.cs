﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using HRISSystemBL;
using HRISSystem.Shared;
using DomainModelHris;
using System.Windows.Input;
using HRISSystem.Form.Employees;
using HRISSystem.ViewModel;
using HRISSystem.Helper;
using Microsoft.Office.Interop.Excel;
using HRISSystemBL.BL.HRISBL;
using HRISSystem.MVVM.View.Report;
using System.Runtime.InteropServices;
using HRISSystem.MVVM.ViewModel.Report;
using HRISSystemBL.BL.PayrollBL;

namespace HRISSystem.Form.ShiftControls
{
    /// <summary>
    /// Interaction logic for EmployeeToShiftPage.xaml
    /// </summary>
    public partial class EmployeeToShiftPage : System.Windows.Controls.Page
    {
        List<Employee> _employeeList;
        List<ShiftControl> _shiftControlList;

        public EmployeeToShiftPage()
        {
            try
            {
                InitializeComponent();
                ShiftComboBox.ItemsSource = HRISBLServices
                    .ShiftBL()
                    .GetAll()
                    .OrderBy(x => x.StartTime)
                    .ThenBy(x => x.EndTime);
                DepartmentCombobox.ItemsSource = BusinessLayerServices
                    .DeptCodeBL()
                    .GetAllDepartment()
                    .OrderBy(x => x.DeptCode);
                StaffTypeCombobox.ItemsSource = BusinessLayerServices
                    .EmployeeBL()
                    .GetAllStaffTypeList()
                    .OrderBy(x => x.Description);
                StaffTypeCombobox.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #region Function
        private void EmployeeDataGridBinding()
        {
            try
            {
                var dept = (Department)DepartmentCombobox.SelectedItem;
                var stype = (StaffType)StaffTypeCombobox.SelectedItem;

                if (dept == null)
                    return;

                _employeeList = BusinessLayerServices.EmployeeBL()
                    .GetEmployeeListByDepartment(dept.DeptCode)
                    .Where(x => x.Staff_status == 1)
                    .ToList();

                if (stype != null)
                    _employeeList = _employeeList
                        .Where(x => x.Staff_type == stype.StaffTypeID)
                        .ToList();

                EmployeeDataGrid.ItemsSource = null;
                EmployeeDataGrid.ItemsSource = _employeeList;

                if (!string.IsNullOrEmpty(NameTHTextBox.Text))
                    NameFilter();

                TotalEmployeeLable.Content = _employeeList.Count;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void NameFilter()
        {
            try
            {
                var searchText = NameTHTextBox.Text.Trim();
                if (string.IsNullOrEmpty(searchText))
                {
                    Clear();
                }
                else
                {
                    EmployeeDataGrid.ItemsSource = null;
                    EmployeeDataGrid.ItemsSource = _employeeList
                        .Where(w => w.Person.FirstNameTH.Contains(searchText))
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Clear()
        {
            NameTHTextBox.Text = string.Empty;
            EmployeeDataGrid.ItemsSource = null;
            EmployeeDataGrid.ItemsSource = _employeeList;
            SelectedEmployeeLabel.Content = 0;
            AssignButton.IsEnabled = EmployeeDataGrid.SelectedItems.Count > 0 ? true : false;
        }

        private void ShiftControlDataGridBinding()
        {
            try
            {
                var shift = (Shift)ShiftComboBox.SelectedItem;
                var from = FromDatePicker.SelectedDate;
                var to = ToDatePicker.SelectedDate;

                if (shift == null)
                    return;

                if (from == null)
                    return;

                if (to == null)
                    return;

                if (from > to)
                    return;

                _shiftControlList = new List<ShiftControl>();
                _shiftControlList = HRISBLServices.ShiftControlBL()
                    .GetByDateRange(shift.Shift_ID,
                    from.Value,
                    to.Value);
                var list = _shiftControlList
                    .GroupBy(x => new
                    {
                        x.Employee_ID,
                        x.Employee.Person.FirstNameTH,
                        x.Employee.Person.LastNameTH,
                        x.Employee.DeptCode,
                        x.Employee.StaffType.Description,
                        x.Shift_ID,
                        x.Shift.ShiftName,
                        x.Shift.StartTime,
                        x.Shift.EndTime,
                    })
                    .Select(x => new vm_ShiftControl
                    {
                        Employee_ID = x.Key.Employee_ID,
                        FirstName = x.Key.FirstNameTH,
                        LastName = x.Key.LastNameTH,
                        Department = x.Key.DeptCode,
                        StaffTypeName = x.Key.Description,
                        Shift_ID = x.Key.Shift_ID,
                        ShiftName = x.Key.ShiftName,
                        StartTime = (TimeSpan)x.Key.StartTime,
                        EndTime = (TimeSpan)x.Key.EndTime,
                        FromDate = x.Min(y => y.ValidFrom),
                        ToDate = x.Max(y => y.ValidFrom),
                        TotalDate = x.Count()
                    })
                    .OrderBy(x => x.Department)
                    .ThenBy(x => x.FirstName)
                    .ToList();
                ShiftControlDataGrid.ItemsSource = null;
                ShiftControlDataGrid.ItemsSource = list;
                TotalAssignedLable.Content = list.Count;

                int selectedItems = ShiftControlDataGrid.SelectedItems.Count;
                DeleteFromSelectedButton.IsEnabled = selectedItems > 0 ? true : false;
                SelectedDeleteLabel.Content = selectedItems;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void SelectedEmployee()
        {
            int selectedItems = EmployeeDataGrid.SelectedItems.Count;
            AssignButton.IsEnabled = selectedItems > 0 ? true : false;
            SelectedEmployeeLabel.Content = selectedItems;
        }

        private void DeleteRange(vm_ShiftControl model)
        {
            try
            {
                if (PayrollServices.LotNumberBL()
                    .IsLotNumberLockedByDateRange(model.FromDate, model.ToDate) == true)
                    throw new Exception("มีงวดเงินเดือน (Lot Number) ที่ถูกล็อคแล้วจึงไม่สามารถลบข้อมูลได้ โปรดระบุ from date และ to date ใหม่อีกครั้ง");

                //Get shift control from the list of employee shift control.
                var list = new List<ShiftControl>();
                    list.AddRange(
                        _shiftControlList
                        .Where(x => x.Employee_ID == model.Employee_ID &&
                        x.ValidFrom >= model.FromDate &&
                        x.ValidFrom <= model.ToDate));

                var deleteCount = 0;
                foreach (var item in list)
                {
                    HRISBLServices.ShiftControlBL().
                        Delete(item.Shift_ID, item.Employee_ID, item.ValidFrom);
                    deleteCount++;
                }

                ShiftControlDataGridBinding();
                MessageBoxHelper.Info("ลบข้อมูลสำเร็จจำนวน " + deleteCount + " รายการ");

                //while (model.FromDate < model.ToDate)
                //{
                //    BusinessLayerServices.ShiftBL()
                //        .DeleteShiftControl(new ShiftControl
                //        {
                //            Shift_ID = model.Shift_ID,
                //            Employee_ID = model.Employee_ID,
                //            ValidFrom = model.FromDate,
                //            EndDate = model.ToDate,
                //            Noted = "",
                //            CreatedDate = DateTime.Now,
                //            ModifiedDate = DateTime.Now
                //        });
                //}
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion


        private void DepartmentCombobox_DropDownClosed(object sender, EventArgs e)
        {
            EmployeeDataGridBinding();
        }

        private void StaffStatusCombobox_DropDownClosed(object sender, EventArgs e)
        {
            EmployeeDataGridBinding();
        }

        private void StaffTypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            EmployeeDataGridBinding();
        }

        private void NameTHTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            NameFilter();
        }

        private void EmployeeDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            SelectedEmployee();
        }

        private void EmployeeDataGrid_KeyUp(object sender, KeyEventArgs e)
        {
            SelectedEmployee();
        }

        private void ShiftComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ShiftControlDataGridBinding();
        }

        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ShiftControlDataGridBinding();
        }

        private void AssignButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var shift = (Shift)ShiftComboBox.SelectedItem;
                var from = FromDatePicker.SelectedDate;
                var to = ToDatePicker.SelectedDate;
                var dept = (Department)DepartmentCombobox.SelectedItem;

                if (shift == null)
                    throw new ArgumentException("กรุณาระบุกะการทำงาน");

                if (from == null)
                    throw new ArgumentException("กรุณาระบุ From Date");

                if (to == null)
                    throw new ArgumentException("กรุณาระบุ To Date");

                if (from > to)
                    throw new ArgumentException("กรุณาระบุวันที่ให้ถูกต้อง");

                if (EmployeeDataGrid.SelectedItems.Count <= 0)
                    throw new ArgumentException("โปรดเลือกพนักงานที่จะกำหนดกะได้จากตารางรายชื่อพนักงานด้านล่างอย่างน้อย 1 ราย");

                if (LotCheck())
                    throw new ArgumentException("Lot เงินเดือนของช่วงเวลาที่ท่านระบุถูกล็อคแล้ว ไม่สามารถแก้ไขข้อมูลได้");

                if (MessageBoxHelper.Question("ท่านต้องการ Assign กะการทำงานให้พนักงานเหล่านี้ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                DateTime startDate = from.Value;
                var employeeDeptShiftList = HRISBLServices.ShiftControlBL()
                    .GetByDepartmentAndDateRange(from.Value, to.Value, dept.DeptCode);

                var assignedEmployees = 0;
                var dupplicated = 0;
                var totalTransaction = 0;
                while (startDate <= to.Value)
                {
                    foreach (Employee item in EmployeeDataGrid.SelectedItems)
                    {
                        //A duplicate info. the system will not be adding.
                        //An employee should have a single shift per day.
                        totalTransaction++;
                        if (employeeDeptShiftList
                            .Where(x => x.Employee_ID == item.Employee_ID &&
                            x.ValidFrom == startDate)
                            .Count() > 0)
                        {
                            dupplicated++;
                            continue;
                        }

                        //BusinessLayerServices.ShiftBL()
                        //    .AddShiftControl(new ShiftControl
                        //    {
                        //        Shift_ID = shift.Shift_ID,
                        //        Employee_ID = item.Employee_ID,
                        //        ValidFrom = startDate,
                        //        EndDate = startDate,
                        //        Noted = "",
                        //        CreatedDate = DateTime.Now,
                        //        ModifiedDate = DateTime.Now
                        //    });

                        HRISBLServices.ShiftControlBL()
                            .Add(shift.Shift_ID, item.Employee_ID, startDate, "");
                        assignedEmployees++;
                    }
                    startDate = startDate.AddDays(1);
                }
                MessageBoxHelper.Info("บันทึกข้อมูลใหม่จำนวน " + assignedEmployees + " รายการ" + Environment.NewLine +
                    "ข้อมูลซ้ำจำนวน " + dupplicated + " รายการ" + Environment.NewLine +
                    "จากข้อมูลทั้งหมด " + totalTransaction + " รายการ");
                ShiftControlDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private bool LotCheck()
        {
            bool isLock = false;
            var from = FromDatePicker.SelectedDate;
            var to = ToDatePicker.SelectedDate;
            var lotList = BusinessLayerServices.PayrollBL()
                .GetLotByDateRange(from.Value, to.Value);

            foreach (var lot in lotList)
            {
                bool lockHR = lot.Lock_Hr == "2";
                bool lockHrLabor = lot.Lock_Hr_Labor == "2";
                bool lockAcc = lot.Lock_Acc == "2";
                bool lockAccLabor = lot.Lock_Acc_Labor == "2";

                isLock = lockHR || lockHrLabor || lockAcc || lockAccLabor;

                if (isLock)
                    return isLock;
            }
            return isLock;
        }

        private void DeleteRangeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var deletedItem = (vm_ShiftControl)ShiftControlDataGrid.SelectedItem;
                if (deletedItem == null)
                    return;

                if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลกะการทำงานของพนักงานรายนี้" + Environment.NewLine +
                    "ตั้งแต่วันที่ " + deletedItem.FromDate.ToString("dd/MM/yyyy") + Environment.NewLine +
                    "ถึงวันที่ " + deletedItem.ToDate.ToString("dd/MM/yyyy") + Environment.NewLine +
                    " ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                DeleteRange(deletedItem);
                ShiftControlDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DetailsButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ShiftControlDataGrid.SelectedItem == null)
                    return;

                var item = (vm_ShiftControl)ShiftControlDataGrid.SelectedItem;
                var window = new MVVM.View.ShiftWork.EmployeeShiftAssigned();
                var vm = new MVVM.ViewModel.ShiftWork.vm_EmployeeShiftAssigned();
                vm.EmployeeID = item.Employee_ID;
                vm.FromDate = item.FromDate;
                vm.ToDate = item.ToDate;
                window.DataContext = vm;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void EmployeeShiftDetailsButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (EmployeeDataGrid.SelectedItem == null)
                    return;

                var item = (Employee)EmployeeDataGrid.SelectedItem;
                var window = new MVVM.View.ShiftWork.EmployeeShiftAssigned();
                var vm = new MVVM.ViewModel.ShiftWork.vm_EmployeeShiftAssigned();
                vm.EmployeeID = item.Employee_ID;
                vm.FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                vm.ToDate = ((DateTime)vm.FromDate).AddMonths(1).AddDays(-1);
                window.DataContext = vm;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void SummaryByShiftReportButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FromDatePicker.SelectedDate == null)
                    throw new ArgumentException("โปรดระบุวันที่ From Date");

                if (ToDatePicker.SelectedDate == null)
                    throw new ArgumentException("โปรดระบุวันที่ To Date");

                var window = new RPTSW01();
                var vm = new vm_RPTSW01();
                vm.FromDate = (DateTime)FromDatePicker.SelectedDate;
                vm.ToDate = (DateTime)ToDatePicker.SelectedDate;
                vm.Window = window;
                vm.ReportViewer = window.ReportViewer;
                vm.ReportPath = "HRISSystem.Form.Report.Rdlc.RPTSW01.rdlc";
                vm.ReportName = "SW01: รายงานข้อมูลสรุปกะการทำงานพนักงาน";
                window.DataContext = vm;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DetailReportButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FromDatePicker.SelectedDate == null)
                    throw new ArgumentException("โปรดระบุวันที่ From Date");

                if (ToDatePicker.SelectedDate == null)
                    throw new ArgumentException("โปรดระบุวันที่ To Date");

                var window = new RPTSW01();
                var vm = new vm_RPTSW01();
                vm.FromDate = (DateTime)FromDatePicker.SelectedDate;
                vm.ToDate = (DateTime)ToDatePicker.SelectedDate;
                vm.Window = window;
                vm.ReportViewer = window.ReportViewer;
                vm.ReportPath = "HRISSystem.Form.Report.Rdlc.RPTSW02.rdlc";
                vm.ReportName = "SW02: รายงานข้อมูลรายละเอียดกะการทำงานพนักงาน";
                window.DataContext = vm;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeleteFromSelectedButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ShiftControlDataGrid.SelectedItems.Count <= 0)
                    throw new Exception("โปรดเลือกรายชื่อพนักงานที่จะลบกะการทำงานจากตารางทางด้านขวามือ");

                if (ShiftComboBox.SelectedIndex < 0)
                    throw new Exception("โปรดระบุ Shift");

                if (FromDatePicker.SelectedDate == null)
                    throw new Exception("โปรดรุบุ From Date");

                if (FromDatePicker.SelectedDate == null)
                    throw new Exception("โปรดรุบุ To Date");

                if (MessageBoxHelper.Question("ท่านต้องการลบกะการทำงานเหล่านี้ (จำนวนพนักงาน " +
                    ShiftControlDataGrid.SelectedItems.Count + " ราย) ใช่หรือไม่?") ==
                    MessageBoxResult.No)
                    return;

                var shift = (Shift)ShiftComboBox.SelectedItem;
                var from = (DateTime)FromDatePicker.SelectedDate;
                var to = (DateTime)ToDatePicker.SelectedDate;

                if (PayrollServices.LotNumberBL().IsLotNumberLockedByDateRange(from, to) == true)
                    throw new Exception("มีงวดเงินเดือน (Lot Number) ที่ถูกล็อคแล้วจึงไม่สามารถลบข้อมูลได้ โปรดระบุ from date และ to date ใหม่อีกครั้ง");

                //Get shift control from the list of employee shift control.
                var list = new List<ShiftControl>();
                foreach (vm_ShiftControl item in ShiftControlDataGrid.SelectedItems)
                {
                    list.AddRange(
                        _shiftControlList
                        .Where(x => x.Employee_ID == item.Employee_ID &&
                        x.ValidFrom >= item.FromDate &&
                        x.ValidFrom <= item.ToDate));
                }

                var deleteCount = 0;
                foreach (var item in list)
                {
                    HRISBLServices.ShiftControlBL().
                        Delete(item.Shift_ID, item.Employee_ID, item.ValidFrom);
                    deleteCount++;
                }

                ShiftControlDataGridBinding();
                MessageBoxHelper.Info("ลบข้อมูลสำเร็จจำนวน " + deleteCount + " รายการ");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ShiftControlDataGrid_KeyUp(object sender, KeyEventArgs e)
        {
            int selectedItems = ShiftControlDataGrid.SelectedItems.Count;
            DeleteFromSelectedButton.IsEnabled = selectedItems > 0 ? true : false;
            SelectedDeleteLabel.Content = selectedItems;
        }

        private void ShiftControlDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            int selectedItems = ShiftControlDataGrid.SelectedItems.Count;
            DeleteFromSelectedButton.IsEnabled = selectedItems > 0 ? true : false;
            SelectedDeleteLabel.Content = selectedItems;
        }
    }
}
