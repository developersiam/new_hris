﻿using DomainModelHris;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace HRISSystem.Form.ShiftControls
{
    /// <summary>
    /// Interaction logic for ShiftControlWindow.xaml
    /// </summary>
    public partial class ShiftControlWindow : Window
    {
        public List<ShiftControl> employeeList = new List<ShiftControl>();
        public ShiftControlWindow()
        {
            InitializeComponent();
            ShiftCombobox.ItemsSource = BusinessLayerServices.ShiftBL().GetAllShiftList();
        }

        private void ShiftDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void ShiftCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                //string _Department = DepartmentCombobox.Text == "" ? "" : DepartmentCombobox.SelectedValue.ToString();
                //string _Status = StatusCombobox.Text;
                //string _StaffType = StaffTypeCombobox.Text;

                //if (string.IsNullOrEmpty(_Department) && string.IsNullOrEmpty(_Status) && string.IsNullOrEmpty(_StaffType)) return;

                //EmployeeInformationDataGrid.ItemsSource = null;
                //EmployeeInformationDataGrid.ItemsSource = BusinessLayerServices.EmployeeBL().GetFilteredEmployeeList(_Department, _Status, _StaffType);
                var shift = (Shift)ShiftCombobox.SelectedItem;
                var shiftDate = ShiftDatePicker.SelectedDate;

                if (shift == null || shiftDate == null) return;

                EmployeeInformationDataGrid.ItemsSource = null;
                EmployeeInformationDataGrid.ItemsSource = BusinessLayerServices.ShiftBL().GetShiftControl(shift.Shift_ID, shiftDate.Value, shiftDate.Value); 

                SelectedDatagrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in EmployeeInformationDataGrid.SelectedItems)
            {
                employeeList.Add((ShiftControl)item);
            }

            Close();
        }

        private void EmployeeInformationDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            SelectedDatagrid();
        }

        private void EmployeeInformationDataGrid_KeyUp(object sender, KeyEventArgs e)
        {
            SelectedDatagrid();
        }
        private void SelectedDatagrid()
        {
            try
            {
                int? selectedCount = EmployeeInformationDataGrid.SelectedItems.Count;
                SelectButton.Content = string.Format("เลือก ({0})", selectedCount);
                SelectButton.IsEnabled = selectedCount > 0 ? true : false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
