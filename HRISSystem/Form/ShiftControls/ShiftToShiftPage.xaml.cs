﻿using DomainModelHris;
using DomainModelPayroll;
using HRISSystem.Shared;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HRISSystem.Form.ShiftControls
{
    /// <summary>
    /// Interaction logic for ShiftToShiftPage.xaml
    /// </summary>
    public partial class ShiftToShiftPage : Page
    {
        List<ShiftControl> shiftControlList = new List<ShiftControl>();
        public ShiftToShiftPage()
        {
            InitializeComponent(); 
            BindCombobox();
        }

        private void BindCombobox()
        {
            //DepartmentCombobox.ItemsSource = BusinessLayerServices.DeptCodeBL().GetAllDepartment();
            LotCombobox.ItemsSource = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);
            ShiftCombobox.ItemsSource = BusinessLayerServices.ShiftBL().GetAllShiftList();
            //StatusCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffStatusList();
            //StaffTypeCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
        }

        private void LotCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var selected = (Lot_Number)LotCombobox.SelectedItem;
                if (selected != null)
                {
                    LotFromDatePicker.SelectedDate = selected.Start_date;
                    LotToDatePicker.SelectedDate = selected.Finish_date;
                    ShiftCombobox.SelectedIndex = -1;
                    ShiftFromDatePicker.DisplayDateStart = selected.Start_date;
                    ShiftFromDatePicker.DisplayDateEnd = selected.Finish_date;
                    ShiftToDatePicker.DisplayDateStart = selected.Start_date;
                    ShiftToDatePicker.DisplayDateEnd = selected.Finish_date;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ShiftCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ShiftFromDatePicker.SelectedDate = null;
                ShiftToDatePicker.SelectedDate = null;
                NotedTextBox.Text = "";
                InformationDataGrid.ItemsSource = null;

                var selected = (Shift)ShiftCombobox.SelectedItem;
                AddEmployeeButton.IsEnabled = selected != null;
                UpdateButton.IsEnabled = selected != null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ShiftDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                var shift = (Shift)ShiftCombobox.SelectedItem;
                var lotNumber = (Lot_Number)LotCombobox.SelectedItem;
                var shiftFrom = ShiftFromDatePicker.SelectedDate;
                var shiftTo = ShiftToDatePicker.SelectedDate;

                if (shift == null || lotNumber == null || shiftFrom == null || shiftTo == null) return;
                if (shiftFrom > shiftTo) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");

                shiftControlList.Clear();
                shiftControlList = BusinessLayerServices.ShiftBL().GetShiftControl(shift.Shift_ID, shiftFrom.Value, shiftTo.Value);
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = shiftControlList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddEmployeeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var shift = (Shift)ShiftCombobox.SelectedItem;
                if (shift == null) throw new Exception("กรุณาเลือก Shift");

                Employees.SearchMultipleEmployeeWindow windows = new Employees.SearchMultipleEmployeeWindow();
                windows.ShowDialog();

                if (!windows.employeeList.Any()) return;

                foreach (var emp in windows.employeeList)
                {
                    for (var day = ShiftFromDatePicker.SelectedDate.Value; day <= ShiftToDatePicker.SelectedDate.Value; day = day.AddDays(1))
                    {
                        ShiftControl _shiftControl = new ShiftControl
                        {
                            Employee_ID = emp.Employee_ID,
                            Shift_ID = ShiftCombobox.SelectedValue.ToString(),
                            ValidFrom = day,
                            EndDate = day,
                            //crop = null,
                            Noted = NotedTextBox.Text,
                            CreatedDate = DateTime.Now,
                            ModifiedDate = null,
                            Employee = BusinessLayerServices.EmployeeBL().GetEmployeeByEmployeeID(emp.Employee_ID)
                        };
                        // Add Only not duplicate item.
                        if (BusinessLayerServices.ShiftBL().GetSingleShiftControl(_shiftControl) == null) shiftControlList.Add(_shiftControl);
                    }
                }

                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = shiftControlList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddShiftButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                References.Shift_Window w = new References.Shift_Window();
                w.ShowDialog();
                ShiftCombobox.ItemsSource = BusinessLayerServices.ShiftBL().GetAllShiftList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ShiftFromDatePicker.SelectedDate == null || ShiftToDatePicker.SelectedDate == null) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
                if (ShiftFromDatePicker.SelectedDate > ShiftToDatePicker.SelectedDate) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
                if (string.IsNullOrEmpty(ShiftCombobox.Text)) throw new Exception("กรุณาระบุรหัสพนักงาน");

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var newShiftControlList = new List<ShiftControl>();
                    foreach (var _shiftControl in shiftControlList)
                    {
                        if (BusinessLayerServices.ShiftBL().GetSingleShiftControl(_shiftControl) == null)
                        {
                            BusinessLayerServices.ShiftBL().AddShiftControl(_shiftControl);
                            newShiftControlList.Add(_shiftControl);
                        }
                    }
                    if (newShiftControlList.Any()) DataCorrectorStatic<ShiftControl>.AddTransactionList("A", "Shift Control", null, newShiftControlList);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailBtton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var oldItem = (ShiftControl)InformationDataGrid.SelectedItem;
                if (oldItem != null)
                {
                    if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        BusinessLayerServices.ShiftBL().DeleteShiftControl(oldItem);
                        DataCorrectorStatic<ShiftControl>.AddTransaction("D", "Shift Control", oldItem, null);
                        MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        ReloadDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
