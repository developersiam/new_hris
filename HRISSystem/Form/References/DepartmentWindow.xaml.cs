﻿using DomainModelHris;
using HRISSystem.Shared;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Form.References
{
    /// <summary>
    /// Interaction logic for DepartmentWindow.xaml
    /// </summary>
    public partial class DepartmentWindow : Window
    {
        public DepartmentWindow()
        {
            InitializeComponent();
            Clear();
        }

        private void ReloadDatagrid()
        {
            try
            {
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = BusinessLayerServices.DeptCodeBL().GetAllDepartment();
                DatagridItemCount();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Clear()
        {
            DepartmentCodeTextBox.Text = "";
            DepartmentNameTHTextBox.Text = "";
            DepartmentNameENTextBox.Text = "";
            AddButton.IsEnabled = true;
            UpdateButton.IsEnabled = false;
            ReloadDatagrid();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void DatagridItemCount()
        {
            int? selectedCount = InformationDataGrid.SelectedItems.Count;
            TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", selectedCount);
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var s = (Department)InformationDataGrid.SelectedItem;
                if (s != null)
                {
                    DepartmentCodeTextBox.Text = s.DeptCode;
                    DepartmentNameTHTextBox.Text = s.DeptNameTH;
                    DepartmentNameENTextBox.Text = s.DeptName;
                    AddButton.IsEnabled = false;
                }
                UpdateButton.IsEnabled = s != null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var oldItem = (Department)InformationDataGrid.SelectedItem;
                    BusinessLayerServices.DeptCodeBL().Delete(oldItem);
                    DataCorrectorStatic<Department>.AddTransaction("D", "Department Setting", oldItem, null);
                    MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(DepartmentCodeTextBox.Text)) throw new Exception("กรุณาระบุ Department Code");
                if (string.IsNullOrEmpty(DepartmentNameTHTextBox.Text) || string.IsNullOrEmpty(DepartmentNameENTextBox.Text)) throw new Exception("กรุณาระบุชื่อ Department");

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var newItem = new Department
                    {
                        DeptCode = DepartmentCodeTextBox.Text,
                        DeptName = DepartmentNameENTextBox.Text,
                        DeptNameTH = DepartmentNameTHTextBox.Text
                    };
                    BusinessLayerServices.DeptCodeBL().Add(newItem);
                    DataCorrectorStatic<Department>.AddTransaction("A", "Department Setting", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(DepartmentCodeTextBox.Text)) throw new Exception("กรุณาระบุ Department Code");
                if (string.IsNullOrEmpty(DepartmentNameTHTextBox.Text) || string.IsNullOrEmpty(DepartmentNameENTextBox.Text)) throw new Exception("กรุณาระบุชื่อ Department");

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var oldItem = BusinessLayerServices.DeptCodeBL().GetSingleDepartment(DepartmentCodeTextBox.Text);
                    var newItem = new Department
                    {
                        DeptCode = DepartmentCodeTextBox.Text,
                        DeptName = DepartmentNameENTextBox.Text,
                        DeptNameTH = DepartmentNameTHTextBox.Text
                    };
                    BusinessLayerServices.DeptCodeBL().Update(newItem);
                    DataCorrectorStatic<Department>.AddTransaction("U", "Department Setting", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
