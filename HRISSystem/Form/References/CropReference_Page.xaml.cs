﻿using System.Windows;
using System.Windows.Controls;
using DomainModelHris;
using HRISSystemBL;
using System;
using System.Linq;
using HRISSystem.Shared;

namespace HRISSystem.Form.References
{
    /// <summary>
    /// Interaction logic for CropReference_Page.xaml
    /// </summary>
    public partial class CropReference_Page : Page
    {
        public CropReference_Page()
        {
            InitializeComponent();
            ReloadCropCombobox();
        }

        private void ReloadCropCombobox()
        {
            try
            {
                CropComboBox.ItemsSource = null;
                CropComboBox.ItemsSource = BusinessLayerServices.ReferenceBL().GetAllCropSetup();
                CropComboBox.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddNewCropButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CropSetup_Window window = new CropSetup_Window();
                window.ShowDialog();
                ClearControl();
                ReloadCropCombobox();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearControl()
        {
            FromDatePicker.Text = "";
            ToDatePicker.Text = "";
            HolidayAmountTextBox.Text = "";
            SpecialHolidayTextBox.Text = "";
            HolidayDataGrid.ItemsSource = null;
            UpdateCropButton.IsEnabled = false;
            AddHolidayButton.IsEnabled = false;
        }

        private void UpdateCropButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var crop = (CropSetup)CropComboBox.SelectedItem;
                if (crop == null) throw new Exception("ไม่สามารถบันทึกข้อมูลได้");
                if (string.IsNullOrEmpty(FromDatePicker.Text) || string.IsNullOrEmpty(ToDatePicker.Text)) throw new Exception("กรุณาระบุวันที่");

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var oldItem = BusinessLayerServices.ReferenceBL().GetCropSetup(crop.Crop);
                    var newItem = new CropSetup
                    {
                        Crop = oldItem.Crop,
                        ValidFrom = FromDatePicker.SelectedDate,
                        EndDate = ToDatePicker.SelectedDate,
                        CreatedDate = oldItem.CreatedDate,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.ReferenceBL().UpdateCropSetup(newItem);
                    DataCorrectorStatic<CropSetup>.AddTransaction("U", "Crop Setting", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadDatagrid()
        {
            try
            {
                var selected = (CropSetup)CropComboBox.SelectedItem;
                if (selected != null)
                {
                    var holidayList = BusinessLayerServices.ReferenceBL().GetHolidayListByCrop(selected.Crop);
                    HolidayDataGrid.ItemsSource = null;
                    HolidayDataGrid.ItemsSource = holidayList;
                    HolidayAmountTextBox.Text = holidayList.Where(w => w.HolidayFlag.Value).ToList().Count.ToString();
                    SpecialHolidayTextBox.Text = holidayList.Where(w => !w.HolidayFlag.Value).ToList().Count.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddHolidayButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var crop = (CropSetup)CropComboBox.SelectedItem;
                if (crop != null)
                {
                    Holiday_Window window = new Holiday_Window(crop, null);
                    window.ShowDialog();
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditDetailButton_Click(object sender, RoutedEventArgs e)
        {
            EditHoliday();
        }

        private void HolidayDataGrid_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            EditHoliday();
        }

        private void EditHoliday()
        {
            try
            {
                var crop = (CropSetup)CropComboBox.SelectedItem;
                var holiday = (Holiday)HolidayDataGrid.SelectedItem;
                if (holiday != null && crop != null)
                {
                    Holiday_Window window = new Holiday_Window(crop, holiday);
                    window.ShowDialog();
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var oldItem = (Holiday)HolidayDataGrid.SelectedItem;
                    BusinessLayerServices.ReferenceBL().DeleteHoliday(oldItem);
                    DataCorrectorStatic<Holiday>.AddTransaction("D", "Holiday Setting", oldItem, null);
                    MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CropComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var selected = (CropSetup)CropComboBox.SelectedItem;
                if (selected != null)
                {
                    FromDatePicker.SelectedDate = selected.ValidFrom;
                    ToDatePicker.SelectedDate = selected.EndDate;

                    // Get holiday
                    ReloadDatagrid();
                }

                // Disable buttons if selected crop is null
                UpdateCropButton.IsEnabled = selected != null;
                AddHolidayButton.IsEnabled = selected != null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
