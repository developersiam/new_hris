﻿using System;
using System.Windows;
using HRISSystemBL;
using DomainModelHris;

namespace HRISSystem.Form.References
{
    /// <summary>
    /// Interaction logic for LeaveType_Window.xaml
    /// </summary>
    public partial class LeaveType_Window : Window
    {
        public LeaveType_Window(int itemCount)
        {
            InitializeComponent();
            ClearControl();
            LeaveIDTextBox.Text = BusinessLayerServices.LeaveTypeBL().GetNewLeaveTypeID();
            throw new NotImplementedException();
            //LeaveIDTextBox.Text = "L" + (itemCount + 1).ToString().PadLeft(3, '0');
        }

        private void ClearControl()
        {
            LeaveIDTextBox.Text = BusinessLayerServices.LeaveTypeBL().GetNewLeaveTypeID();
            NameTHTextBox.Text = "";
            NameENTextBox.Text = "";
            FromDatePicker.Text = "";
            ToDatePicker.Text = "";
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(ToDatePicker.Text)) ToDatePicker.SelectedDate = DateTime.MaxValue;
                if (string.IsNullOrEmpty(LeaveIDTextBox.Text)) throw new NullReferenceException();
                if (BusinessLayerServices.LeaveTypeBL().GetLeaveTypeByID(LeaveIDTextBox.Text) != null) throw new Exception("Duplicate Data.");
                if (string.IsNullOrEmpty(NameTHTextBox.Text)) throw new NullReferenceException();
                if (string.IsNullOrEmpty(FromDatePicker.Text)) throw new NullReferenceException();

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!",MessageBoxButton.YesNo,MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var item = new LeaveType
                    {
                        LeaveType_ID = LeaveIDTextBox.Text,
                        LeaveTypeNameTH = NameTHTextBox.Text,
                        LeaveTypeNameEN = NameENTextBox.Text,
                        CreatedDate = DateTime.Now,
                        ValidFrom = FromDatePicker.SelectedDate,
                        EndDate = ToDatePicker.SelectedDate
                    };
                    BusinessLayerServices.LeaveTypeBL().AddLeaveType(item);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
