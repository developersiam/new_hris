﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DomainModelHris;
using HRISSystemBL;
using HRISSystem.Shared;

namespace HRISSystem.Form.References
{
    /// <summary>
    /// Interaction logic for Holiday_Window.xaml
    /// </summary>
    public partial class Holiday_Window : Window
    {
        string formState;
        public Holiday_Window()
        {
            InitializeComponent();
        }

        public Holiday_Window(CropSetup crop, Holiday holiday)
        {
            InitializeComponent();
            formState = holiday == null ? "Insert" : "Update"; //If Holiday not null is Update form
            AddButton.IsEnabled = holiday == null;
            UpdateButton.IsEnabled = holiday != null;
            HolidayDatePicker.IsEnabled = holiday == null; // HolidayDate is Key
            CropTextBox.Text = crop.Crop.ToString();
            FromDatePicker.SelectedDate = crop.ValidFrom;
            ToDatePicker.SelectedDate = crop.EndDate;
            // Holiday
            if (holiday != null)
            {
                HolidayDatePicker.SelectedDate = holiday.Date;
                NameTHTextBox.Text = holiday.HolidayNameTH;
                NameENTextBox.Text = holiday.HolidayNameEN;
                RemarkTextBox.Text = holiday.Noted;
                SpecialHolidayRadioButton.IsChecked = !holiday.HolidayFlag.GetValueOrDefault();
            }
            HolidayRadioButton.IsChecked = holiday == null ? true : holiday.HolidayFlag.GetValueOrDefault();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(CropTextBox.Text)) throw new Exception("กรุณาระบุ Crop");
                if (string.IsNullOrEmpty(ToDatePicker.Text) || string.IsNullOrEmpty(FromDatePicker.Text) || string.IsNullOrEmpty(HolidayDatePicker.Text)) throw new Exception("กรุณาระบุวันที่");
                if (string.IsNullOrEmpty(NameENTextBox.Text)) throw new Exception("กรุณาระบุชื่อ Holiday");
                if (!HolidayRadioButton.IsChecked.Value && !SpecialHolidayRadioButton.IsChecked.Value) throw new Exception("กรุณาระบุประเภท Holiday");

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var newItem = new Holiday
                    {
                        Crop = Convert.ToInt32(CropTextBox.Text),
                        Date = HolidayDatePicker.SelectedDate.Value,
                        HolidayNameTH = NameTHTextBox.Text,
                        HolidayNameEN = NameENTextBox.Text,
                        ValidFrom = FromDatePicker.SelectedDate,
                        EndDate = ToDatePicker.SelectedDate,
                        Noted = RemarkTextBox.Text,
                        HolidayFlag = HolidayRadioButton.IsChecked,
                        CreatedDate = DateTime.Now
                    };
                    BusinessLayerServices.ReferenceBL().AddHoliday(newItem);
                    DataCorrectorStatic<Holiday>.AddTransaction("A", "Holiday Setting", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(CropTextBox.Text)) throw new Exception("กรุณาระบุ Crop");
                if (string.IsNullOrEmpty(ToDatePicker.Text) || string.IsNullOrEmpty(FromDatePicker.Text) || string.IsNullOrEmpty(HolidayDatePicker.Text)) throw new Exception("กรุณาระบุวันที่");                
                if (string.IsNullOrEmpty(NameENTextBox.Text)) throw new Exception("กรุณาระบุชื่อ Holiday");
                if (!HolidayRadioButton.IsChecked.Value && !SpecialHolidayRadioButton.IsChecked.Value) throw new Exception("กรุณาระบุประเภท Holiday");

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var holidayDate = HolidayDatePicker.SelectedDate.Value;
                    var oldItem = BusinessLayerServices.ReferenceBL().GetHolidayByDate(holidayDate);
                    var newItem = new Holiday
                    {
                        Crop = oldItem.Crop,
                        Date = oldItem.Date,
                        HolidayNameTH = NameTHTextBox.Text,
                        HolidayNameEN = NameENTextBox.Text,
                        ValidFrom = oldItem.ValidFrom,
                        EndDate = oldItem.EndDate,
                        Noted = RemarkTextBox.Text,
                        HolidayFlag = HolidayRadioButton.IsChecked,
                        CreatedDate = oldItem.CreatedDate
                    };
                    BusinessLayerServices.ReferenceBL().UpdateHoliday(newItem);
                    DataCorrectorStatic<Holiday>.AddTransaction("U", "Holiday Setting", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
