﻿using System;
using System.Windows;
using System.Windows.Controls;
using HRISSystemBL;
using DomainModelHris;
using HRISSystem.Shared;

namespace HRISSystem.Form.References
{
    /// <summary>
    /// Interaction logic for BranchRef_Page.xaml
    /// </summary>
    public partial class BranchRef_Page : Page
    {
        public BranchRef_Page()
        {
            InitializeComponent();
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                ClearControl();
                BranchDataGrid.ItemsSource = null;
                BranchDataGrid.ItemsSource = BusinessLayerServices.ReferenceBL().GetAllBranch();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void ClearControl()
        {
            BranchIDTextBox.Text = "";
            BranchIDTextBox.IsEnabled = true;
            NameTHTextBox.Text = "";
            NameENTextBox.Text = "";
            AddressTextBox.Text = "";
            Phone1TextBox.Text = "";
            Phone2TextBox.Text = "";
            FaxTextBox.Text = "";
            FromDatePicker.Text = "";
            ToDatePicker.Text = "";
            UpdateButton.IsEnabled = false;
            AddButton.IsEnabled = true;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(ToDatePicker.Text)) ToDatePicker.SelectedDate = DateTime.MaxValue;
                if (string.IsNullOrEmpty(BranchIDTextBox.Text)) throw new Exception("กรุณาระบุ Branch ID");
                if (BusinessLayerServices.ReferenceBL().GetBranchByID(BranchIDTextBox.Text) != null) throw new Exception("Branch ID ซ้ำกรุณาตรวจสอบ");
                if (string.IsNullOrEmpty(NameTHTextBox.Text)) throw new Exception("กรุณาระบุ Branch name");
                if (string.IsNullOrEmpty(FromDatePicker.Text) || string.IsNullOrEmpty(ToDatePicker.Text)) throw new Exception("กรุณาระบุวันที่");

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var newItem = new Branch
                    {
                        Branch_ID = BranchIDTextBox.Text,
                        BranchTH = NameTHTextBox.Text,
                        BranchEN = NameENTextBox.Text,
                        Address = AddressTextBox.Text,
                        PhoneNumber1 = Phone1TextBox.Text,
                        PhoneNumber2 = Phone2TextBox.Text,
                        FaxNumber = FaxTextBox.Text,
                        CreatedDate = DateTime.Now,
                        ValidFrom = FromDatePicker.SelectedDate,
                        EndDate = ToDatePicker.SelectedDate
                    };
                    BusinessLayerServices.ReferenceBL().AddBranch(newItem);
                    DataCorrectorStatic<Branch>.AddTransaction("A", "Branch Setting", null, newItem);
                    ReloadDatagrid();
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(ToDatePicker.Text)) ToDatePicker.SelectedDate = DateTime.MaxValue;
                if (string.IsNullOrEmpty(BranchIDTextBox.Text)) throw new Exception("กรุณาระบุ Branch ID");
                if (string.IsNullOrEmpty(NameTHTextBox.Text)) throw new Exception("กรุณาระบุ Branch name");
                if (string.IsNullOrEmpty(FromDatePicker.Text) || string.IsNullOrEmpty(ToDatePicker.Text)) throw new Exception("กรุณาระบุวันที่");

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var oldItem = BusinessLayerServices.ReferenceBL().GetBranchByID(BranchIDTextBox.Text);
                    var newItem = new Branch
                    {
                        Branch_ID = oldItem.Branch_ID,
                        BranchTH = NameTHTextBox.Text,
                        BranchEN = NameENTextBox.Text,
                        Address = AddressTextBox.Text,
                        PhoneNumber1 = Phone1TextBox.Text,
                        PhoneNumber2 = Phone2TextBox.Text,
                        FaxNumber = FaxTextBox.Text,
                        ValidFrom = FromDatePicker.SelectedDate,
                        EndDate = ToDatePicker.SelectedDate,
                        CreatedDate = oldItem.CreatedDate,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.ReferenceBL().UpdateBranch(newItem);
                    DataCorrectorStatic<Branch>.AddTransaction("U", "Branch Setting", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditDetailButton_Click(object sender, RoutedEventArgs e)
        {
            BindSelectedControl();
        }

        private void BranchDataGrid_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            BindSelectedControl();
        }

        private void BindSelectedControl()
        {
            try
            {
                ClearControl();
                var item = (Branch)BranchDataGrid.SelectedItem;
                if (item != null)
                {
                    BranchIDTextBox.Text = item.Branch_ID;
                    BranchIDTextBox.IsEnabled = false;
                    NameTHTextBox.Text = item.BranchTH;
                    NameENTextBox.Text = item.BranchEN;
                    AddressTextBox.Text = item.Address;
                    Phone1TextBox.Text = item.PhoneNumber1;
                    Phone2TextBox.Text = item.PhoneNumber2;
                    FaxTextBox.Text = item.FaxNumber;
                    FromDatePicker.SelectedDate = item.ValidFrom.GetValueOrDefault();
                    ToDatePicker.SelectedDate = item.EndDate.GetValueOrDefault();
                    UpdateButton.IsEnabled = true;
                    AddButton.IsEnabled = false;
                }
                else UpdateButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var oldItem = (Branch)BranchDataGrid.SelectedItem;
                    BusinessLayerServices.ReferenceBL().DeleteBranch(oldItem);
                    DataCorrectorStatic<Branch>.AddTransaction("D", "Branch Setting", oldItem, null);
                    MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearControl();
        }
    }
}
