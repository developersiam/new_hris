﻿using System;
using System.Windows;
using System.Windows.Controls;
using HRISSystemBL;
using DomainModelHris;
using HRISSystem.Shared;

namespace HRISSystem.Form.References
{
    /// <summary>
    /// Interaction logic for LeaveType_Page.xaml
    /// </summary>
    public partial class LeaveType_Page : Page
    {
        public LeaveType_Page()
        {
            InitializeComponent();
            ReloadData();
        }

        private void ReloadData()
        {
            try
            {
                ClearControl();
                LeaveDataGrid.ItemsSource = null;
                LeaveDataGrid.ItemsSource = BusinessLayerServices.LeaveTypeBL().GetAllLeaveType();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearControl()
        {
            LeaveIDTextBox.Text = BusinessLayerServices.LeaveTypeBL().GetNewLeaveTypeID();
            NameTHTextBox.Text = "";
            NameENTextBox.Text = "";
            FromDatePicker.Text = "";
            ToDatePicker.Text = "";
            UpdateButton.IsEnabled = false;
            AddButton.IsEnabled = true;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            //LeaveType_Window window = new LeaveType_Window(LeaveDataGrid.Items.Count);
            //window.ShowDialog();
            //ReloadData();
            try
            {
                if (string.IsNullOrEmpty(ToDatePicker.Text)) ToDatePicker.SelectedDate = DateTime.MaxValue;
                if (string.IsNullOrEmpty(LeaveIDTextBox.Text)) throw new Exception("กรุณาระบุ ID");
                if (string.IsNullOrEmpty(NameTHTextBox.Text)) throw new Exception("กรุณาประเภทการลา");
                if (string.IsNullOrEmpty(FromDatePicker.Text) || string.IsNullOrEmpty(ToDatePicker.Text)) throw new Exception("กรุณาระบุวันที่");

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var newItem = new LeaveType
                    {
                        LeaveType_ID = LeaveIDTextBox.Text,
                        LeaveTypeNameTH = NameTHTextBox.Text,
                        LeaveTypeNameEN = NameENTextBox.Text,
                        CreatedDate = DateTime.Now,
                        ValidFrom = FromDatePicker.SelectedDate,
                        EndDate = ToDatePicker.SelectedDate
                    };
                    BusinessLayerServices.LeaveTypeBL().AddLeaveType(newItem);
                    DataCorrectorStatic<LeaveType>.AddTransaction("A", "LeaveType Setting", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(ToDatePicker.Text)) ToDatePicker.SelectedDate = DateTime.MaxValue;
                if (string.IsNullOrEmpty(LeaveIDTextBox.Text)) throw new Exception("กรุณาระบุ ID");
                if (string.IsNullOrEmpty(NameTHTextBox.Text)) throw new Exception("กรุณาประเภทการลา");
                if (string.IsNullOrEmpty(FromDatePicker.Text) || string.IsNullOrEmpty(ToDatePicker.Text)) throw new Exception("กรุณาระบุวันที่");

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var oldItem = BusinessLayerServices.LeaveTypeBL().GetLeaveTypeByID(LeaveIDTextBox.Text);
                    var newItem = BusinessLayerServices.LeaveTypeBL().GetLeaveTypeByID(LeaveIDTextBox.Text);
                    newItem.LeaveTypeNameTH = NameTHTextBox.Text;
                    newItem.LeaveTypeNameEN = NameENTextBox.Text;
                    newItem.ValidFrom = FromDatePicker.SelectedDate;
                    newItem.EndDate = ToDatePicker.SelectedDate;
                    newItem.ModifiedDate = DateTime.Now;
                    BusinessLayerServices.LeaveTypeBL().UpdateLeaveType(newItem);
                    DataCorrectorStatic<LeaveType>.AddTransaction("U", "LeaveType Setting", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditDetailButton_Click(object sender, RoutedEventArgs e)
        {
            BindSelectedControl();
        }

        private void LeaveDataGrid_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            BindSelectedControl();
        }

        private void BindSelectedControl()
        {
            try
            {
                ClearControl();
                var item = (LeaveType)LeaveDataGrid.SelectedItem;
                if (item != null)
                {
                    LeaveIDTextBox.Text = item.LeaveType_ID;
                    NameTHTextBox.Text = item.LeaveTypeNameTH;
                    NameENTextBox.Text = item.LeaveTypeNameEN;
                    FromDatePicker.SelectedDate = item.ValidFrom.GetValueOrDefault();
                    ToDatePicker.SelectedDate = item.EndDate.GetValueOrDefault();
                    UpdateButton.IsEnabled = true;
                    AddButton.IsEnabled = false;
                }
                else UpdateButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var oldItem = (LeaveType)LeaveDataGrid.SelectedItem;
                    BusinessLayerServices.LeaveTypeBL().DeleteLeaveType(oldItem);
                    DataCorrectorStatic<LeaveType>.AddTransaction("D", "LeaveType Setting", oldItem, null);
                    MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearControl();
        }
    }
}
