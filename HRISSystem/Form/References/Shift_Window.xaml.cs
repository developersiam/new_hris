﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DomainModelHris;
using HRISSystemBL;
using System.Globalization;
using HRISSystem.Shared;


namespace HRISSystem.Form.References
{
    /// <summary>
    /// Interaction logic for Shift_Window.xaml
    /// </summary>
    public partial class Shift_Window : Window
    {
        public Shift_Window()
        {
            InitializeComponent();
            ShiftIDTextBox.Text = BusinessLayerServices.ShiftBL().GetMaxShiftID();
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(ToDatePicker.Text)) ToDatePicker.SelectedDate = DateTime.MaxValue;
                if (string.IsNullOrEmpty(ShiftIDTextBox.Text) || string.IsNullOrEmpty(StartTimeTextBox.Text)) throw new Exception("กรุณาระบุเวลา");
                if (string.IsNullOrEmpty(EndTimeTextBox.Text) || string.IsNullOrEmpty(FromDatePicker.Text)) throw new Exception("กรุณาระบุวันที่");

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var newItem = new Shift
                    {
                        Shift_ID = ShiftIDTextBox.Text,
                        StartTime = TimeSpan.Parse(StartTimeTextBox.Text),
                        EndTime = TimeSpan.Parse(EndTimeTextBox.Text),
                        ValidFrom = FromDatePicker.SelectedDate,
                        IsOverDay = false,
                        Lunch_Day = LunchDay_CheckBox.IsChecked,
                        Lunch_Night = LunchNight_Checkbox.IsChecked,
                        Night_Support = NightSupport_Checkbox.IsChecked,
                        EndDate = ToDatePicker.SelectedDate,
                        CreatedDate = DateTime.Now
                    };
                    BusinessLayerServices.ShiftBL().AddShift(newItem);
                    //DataCorrectorStatic<Shift>.AddTransaction("A", "Shift Setting", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void StartTimeTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            StartTimeTextBox.Text = HRISService.DataCorrector().FormatTime(StartTimeTextBox.Text);
        }

        private void EndTimeTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            EndTimeTextBox.Text = HRISService.DataCorrector().FormatTime(EndTimeTextBox.Text);
        }
    }
}
