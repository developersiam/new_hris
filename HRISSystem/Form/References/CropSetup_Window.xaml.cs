﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DomainModelHris;
using HRISSystemBL;
using HRISSystem.Shared;

namespace HRISSystem.Form.References
{
    /// <summary>
    /// Interaction logic for CropSetup_Window.xaml
    /// </summary>
    public partial class CropSetup_Window : Window
    {
        public CropSetup_Window()
        {
            InitializeComponent();
        }

        private void ClearControl()
        {
            CropTextBox.Text = "";
            FromDatePicker.Text = "";
            ToDatePicker.Text = "";
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(CropTextBox.Text)) throw new Exception("กรุณาระบุเลข Crop");
                if (BusinessLayerServices.ReferenceBL().GetCropSetup(int.Parse(CropTextBox.Text)) != null) throw new Exception("ไม่สามารถบันทึกข้อมูลได้เลข Crop ซ้ำ, กรุณาตรวจสอบ.");
                if (string.IsNullOrEmpty(FromDatePicker.Text) || string.IsNullOrEmpty(ToDatePicker.Text)) throw new Exception("กรุณาระบุวันที่.");

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var newItem = new CropSetup
                    {
                        Crop = int.Parse(CropTextBox.Text),
                        CreatedDate = DateTime.Now,
                        ValidFrom = FromDatePicker.SelectedDate,
                        EndDate = ToDatePicker.SelectedDate
                    };
                    BusinessLayerServices.ReferenceBL().AddCropSetup(newItem);
                    DataCorrectorStatic<CropSetup>.AddTransaction("A", "Crop Setting", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CropTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(CropTextBox.Text)) CropTextBox.Text = HRISService.DataCorrector().FormatNumber(0, CropTextBox.Text, true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
