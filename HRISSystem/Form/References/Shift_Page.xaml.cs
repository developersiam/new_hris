﻿using HRISSystemBL;
using DomainModelHris;
using HRISSystem.Shared;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using HRISSystem.Helper;
using HRISSystem.MVVM.ViewModel.ShiftWork;
using System.Windows.Media;
using System.ComponentModel;

namespace HRISSystem.Form.References
{
    /// <summary>
    /// Interaction logic for Shift_Page.xaml
    /// </summary>
    public partial class Shift_Page : Page
    {
        public Shift_Page()
        {
            InitializeComponent();
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                ClearControl();
                ShiftDataGrid.ItemsSource = null;
                ShiftDataGrid.ItemsSource = BusinessLayerServices.ShiftBL().GetAllShiftList();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Shift_Window window = new Shift_Window();
                window.ShowDialog();
                ReloadDatagrid();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearControl()
        {
            ShiftIDTextBox.Text = "";
            ShiftNameTextBox.Text = "";
            ShiftNameTextBox.Background = null;
            StartTimeTextBox.Text = "";
            EndTimeTextBox.Text = "";
            FromDatePicker.Text = "";
            ToDatePicker.Text = "";
            UpdateButton.IsEnabled = false;
            AddButton.IsEnabled = true;
            LunchDay_CheckBox.IsChecked = false;
            LunchNight_Checkbox.IsChecked = false;
            NightSupport_Checkbox.IsChecked = false;
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(ToDatePicker.Text)) ToDatePicker.SelectedDate = DateTime.MaxValue;
                if (string.IsNullOrEmpty(ShiftIDTextBox.Text) || string.IsNullOrEmpty(StartTimeTextBox.Text)) throw new Exception("กรุณาระบุเวลา");
                if (string.IsNullOrEmpty(EndTimeTextBox.Text) || string.IsNullOrEmpty(FromDatePicker.Text)) throw new Exception("กรุณาระบุวันที่");

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var datagridlist = ShiftDataGrid.Items;
                    var createDate = new DateTime();
                    foreach (var datagridItem in datagridlist)
                    {
                        var _shift = (Shift)datagridItem;
                        if(_shift.ShiftName == ShiftNameTextBox.Text) throw new Exception("ไม่สามารถบันทึกได้เนื่องจากชื้อถูกใช้ไปแล้ว");
                        if (_shift.Shift_ID == ShiftIDTextBox.Text) createDate = _shift.CreatedDate.Value;
                    }
                    //var oldItem = BusinessLayerServices.ShiftBL().GetShiftByID(ShiftIDTextBox.Text);
                    var newItem = new Shift
                    {
                        Shift_ID = ShiftIDTextBox.Text,
                        ShiftName = ShiftNameTextBox.Text,
                        StartTime = TimeSpan.Parse(StartTimeTextBox.Text),
                        EndTime = TimeSpan.Parse(EndTimeTextBox.Text),
                        CreatedDate = createDate,
                        ValidFrom = FromDatePicker.SelectedDate,
                        Lunch_Day = LunchDay_CheckBox.IsChecked,
                        Lunch_Night = LunchNight_Checkbox.IsChecked,
                        Night_Support = NightSupport_Checkbox.IsChecked,
                        EndDate = ToDatePicker.SelectedDate,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.ShiftBL().UpdateShift(newItem);
                    //DataCorrectorStatic<Shift>.AddTransaction("U", "Shift Setting", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void EditDetailButton_Click(object sender, RoutedEventArgs e)
        {
            BindSelectedControl();
        }

        private void ShiftDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BindSelectedControl();
        }

        private void BindSelectedControl()
        {
            try
            {
                var shift = (Shift)ShiftDataGrid.SelectedItem;
                if (shift != null)
                {
                    ShiftIDTextBox.Text = shift.Shift_ID;
                    ShiftNameTextBox.Text = shift.ShiftName;
                    SolidColorBrush brush = (SolidColorBrush)new BrushConverter().ConvertFromString(shift.ShiftName);
                    ShiftNameTextBox.Background = brush;
                    StartTimeTextBox.Text = shift.StartTime.GetValueOrDefault().ToString();
                    EndTimeTextBox.Text = shift.EndTime.GetValueOrDefault().ToString();
                    FromDatePicker.SelectedDate = shift.ValidFrom;
                    ToDatePicker.SelectedDate = shift.EndDate;
                    LunchDay_CheckBox.IsChecked = shift.Lunch_Day;
                    LunchNight_Checkbox.IsChecked = shift.Lunch_Night;
                    NightSupport_Checkbox.IsChecked = shift.Night_Support;
                    UpdateButton.IsEnabled = true;
                    AddButton.IsEnabled = false;
                }
                else UpdateButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var oldItem = (Shift)ShiftDataGrid.SelectedItem;
                    BusinessLayerServices.ShiftBL().DeleteShift(oldItem);
                    DataCorrectorStatic<Shift>.AddTransaction("D", "Shift Setting", oldItem, null);
                    MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void StartTimeTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
           StartTimeTextBox.Text = HRISService.DataCorrector().FormatTime(StartTimeTextBox.Text);
        }

        private void EndTimeTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            EndTimeTextBox.Text = HRISService.DataCorrector().FormatTime(EndTimeTextBox.Text);
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearControl();
        }

        private void ChangeNameButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var window = new MVVM.View.ShiftWork.ShiftColor();
                var vm = new vm_ShiftColor();
                window.DataContext = vm;
                vm.Window = window;
                window.ShowDialog();

                SolidColorBrush brush = (SolidColorBrush)new BrushConverter().ConvertFromString(vm.SelectedColor);
                ShiftNameTextBox.Text = vm.SelectedColor;
                ShiftNameTextBox.Background = brush;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}

