﻿using DomainModelHris;
using HRISSystem.Shared;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Form.References
{
    /// <summary>
    /// Interaction logic for PositionWindow.xaml
    /// </summary>
    public partial class PositionWindow : Window
    {
        public PositionWindow()
        {
            InitializeComponent();
            var departmentList = BusinessLayerServices.DeptCodeBL().GetAllDepartment();
            departmentList.Insert(0, new Department { DeptCode = "", DeptName = "All" });
            DepartmentCombobox.ItemsSource = departmentList;
            Clear();
        }

        private void ReloadDatagrid()
        {
            try
            {
                var dept = (Department)DepartmentCombobox.SelectedItem;
                var positionList = BusinessLayerServices.PositionBL().GetAllList().OrderBy(o => o.PositionNameTH).ToList();
                var filterList = new List<Position>();

                if (dept == null || dept.DeptCode == "") filterList = positionList;
                else filterList = filterList = positionList.Where(o => o.DeptCode == dept.DeptCode).ToList();

                UpdateButton.IsEnabled = (dept != null && dept.DeptCode != "");
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = filterList;
                DatagridItemCount();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Clear()
        {
            PositionIDTextBox.Text = BusinessLayerServices.PositionBL().GetNewPositionID();
            DepartmentCombobox.SelectedIndex = -1;
            DepartmentCombobox.IsEnabled = true;
            PositionNameTHTextBox.Text = "";
            PositionNameENTextBox.Text = "";
            DescriptionTextBox.Text = "";
            //FromDatePicker.SelectedDate = DateTime.Now;
            //EndDatePicker.SelectedDate = DateTime.MaxValue;
            //AddButton.IsEnabled = true;
            //UpdateButton.IsEnabled = false;
            FromDatePicker.SelectedDate = null; 
            EndDatePicker.SelectedDate = null;
            ReloadDatagrid();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void DatagridItemCount()
        {
            int? selectedCount = InformationDataGrid.SelectedItems.Count;
            TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", selectedCount);
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var s = (Position)InformationDataGrid.SelectedItem;
                if (s != null)
                {
                    DepartmentCombobox.IsEnabled = s.DeptCode == "";
                    DepartmentCombobox.SelectedValue = s.DeptCode;
                    PositionIDTextBox.Text = s.Position_ID;
                    PositionNameTHTextBox.Text = s.PositionNameTH;
                    PositionNameENTextBox.Text = s.PositionNameEN;
                    DescriptionTextBox.Text = s.JobSpecification;
                    FromDatePicker.SelectedDate = s.ValidFrom;
                    EndDatePicker.SelectedDate = s.EndDate;
                }
                //UpdateButton.IsEnabled = s != null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var oldItem = (Position)InformationDataGrid.SelectedItem;
                    BusinessLayerServices.PositionBL().DeletePosition(oldItem);
                    DataCorrectorStatic<Position>.AddTransaction("D", "Position Setting", oldItem, null);
                    MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var dept = (Department)DepartmentCombobox.SelectedValue;
                if (FromDatePicker.SelectedDate == null) FromDatePicker.SelectedDate = DateTime.Now;
                if (EndDatePicker.SelectedDate == null) EndDatePicker.SelectedDate = DateTime.MaxValue;
                if (string.IsNullOrEmpty(PositionIDTextBox.Text)) throw new Exception("กรุณาระบุไอดี");
                if (string.IsNullOrEmpty(PositionNameTHTextBox.Text)) throw new Exception("กรุณาระบุชื่อตำแหน่ง");
                if (dept == null || dept.DeptCode == "") throw new Exception("กรุณาระบุแผนกที่ต้องการบันทึก");

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var newItem = new Position
                    {
                        Position_ID = PositionIDTextBox.Text,
                        PositionNameTH = PositionNameTHTextBox.Text,
                        PositionNameEN = PositionNameENTextBox.Text,
                        JobSpecification = DescriptionTextBox.Text,
                        DeptCode = dept.DeptCode,
                        CreatedDate = DateTime.Now,
                        ValidFrom = FromDatePicker.SelectedDate,
                        EndDate = EndDatePicker.SelectedDate,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.PositionBL().AddPosition(newItem);
                    DataCorrectorStatic<Position>.AddTransaction("A", "Position Setting", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var dept = (Department)DepartmentCombobox.SelectedItem;
                var fromDate = FromDatePicker.SelectedDate;
                var toDate = EndDatePicker.SelectedDate;
                if (dept == null) throw new Exception("กรุณาระบุแผนก");
                if (fromDate == null || toDate == null) throw new Exception("กรุณาระบุวันที่ให้ถูดต้อง");
                if (fromDate > toDate) throw new Exception("กรุณาระบุวันที่ให้ถูดต้อง");
                if (string.IsNullOrEmpty(PositionIDTextBox.Text)) throw new Exception("กรุณาระบุไอดี");
                if (string.IsNullOrEmpty(PositionNameTHTextBox.Text)) throw new Exception("กรุณาระบุชื่อตำแหน่ง");

                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var oldItem = BusinessLayerServices.PositionBL().GetPositionByID(PositionIDTextBox.Text);
                    var newItem = new Position
                    {
                        Position_ID = PositionIDTextBox.Text,
                        PositionNameTH = PositionNameTHTextBox.Text,
                        PositionNameEN = PositionNameENTextBox.Text,
                        JobSpecification = DescriptionTextBox.Text,
                        DeptCode = dept.DeptCode,
                        CreatedDate = oldItem == null ? DateTime.Now : oldItem.CreatedDate,
                        ValidFrom = FromDatePicker.SelectedDate,
                        EndDate = EndDatePicker.SelectedDate,
                        ModifiedDate = DateTime.Now
                    };
                    if (oldItem == null)
                    {
                        BusinessLayerServices.PositionBL().AddPosition(newItem);
                        DataCorrectorStatic<Position>.AddTransaction("A", "Position Setting", null, newItem);
                    }
                    else
                    {
                        BusinessLayerServices.PositionBL().UpdatePosition(newItem);
                        DataCorrectorStatic<Position>.AddTransaction("U", "Position Setting", oldItem, newItem);
                    }
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DepartmentCombobox_DropDownClosed(object sender, EventArgs e)
        {
            ReloadDatagrid();
        }

        private void FromDatePicker_CalendarClosed(object sender, RoutedEventArgs e)
        {
            if (EndDatePicker.SelectedDate == null) EndDatePicker.SelectedDate = DateTime.MaxValue;
        }
    }
}
