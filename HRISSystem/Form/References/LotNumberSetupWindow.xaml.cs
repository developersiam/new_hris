﻿using DomainModelPayroll;
using HRISSystem.Shared;
using HRISSystem.ViewModel;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Form.References
{
    /// <summary>
    /// Interaction logic for LotNumberSetupWindow.xaml
    /// </summary>
    public partial class LotNumberSetupWindow : Window
    {
        public LotNumberSetupWindow()
        {
            try
            {
                InitializeComponent();
                BindCombobox();
                ReloadDatagrid();
                PermissionSet();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindCombobox()
        {
            try
            {
                //LotMonthCombobox.ItemsSource = HRISService.DataCorrector().GetLotMonthList();
                LotYearCombobox.ItemsSource = HRISService.DataCorrector().GetLotYearList();

                List<LockStatusViewModel> lockStatusList = new List<LockStatusViewModel>();
                lockStatusList.Add(new LockStatusViewModel { StatusName = "Lock", StatusValue = "2" });
                lockStatusList.Add(new LockStatusViewModel { StatusName = "Unlock", StatusValue = "1" });
                PRStaffLockStatusCombobox.ItemsSource = lockStatusList;
                PRLabourLockStatusCombobox.ItemsSource = lockStatusList;
                HRStaffLockStatusCombobox.ItemsSource = lockStatusList;
                HRLabourLockStatusCombobox.ItemsSource = lockStatusList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadDatagrid()
        {
            try
            {
                var lotList = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot());
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = lotList;
                TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", lotList.Count.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PermissionSet()
        {
            try
            {
                //SingletonConfiguration.getInstance().DepartmentCode = "ACC";
                var deptCode = SingletonConfiguration.getInstance().DepartmentCode;
                var username = SingletonConfiguration.getInstance().Username.ToLower();

                if (deptCode == "HRM")
                {
                    LotFromDatePicker.IsEnabled = false;
                    LotToDatePicker.IsEnabled = false;
                    SalaryPaidDatePicker.IsEnabled = false;
                    OTPdaidDatePicker.IsEnabled = false;
                    PRLabourLockStatusCombobox.IsEnabled = false;
                    PRStaffLockStatusCombobox.IsEnabled = false;
                    //LotMonthCombobox.IsEnabled = false;
                    LotMonthTextBox.IsEnabled = false;
                    LotYearCombobox.IsEnabled = false;
                    //HRLabourLockStatusCombobox.IsEnabled = PRLabourLockStatusCombobox.SelectedValue.ToString() == "1";
                    //HRStaffLockStatusCombobox.IsEnabled = PRStaffLockStatusCombobox.SelectedValue.ToString() == "1";
                }
                else if (deptCode == "ACC")
                {
                    LotFromDatePicker.IsEnabled = true;
                    LotToDatePicker.IsEnabled = true;
                    SalaryPaidDatePicker.IsEnabled = true;
                    OTPdaidDatePicker.IsEnabled = true;
                    //PRLabourLockStatusCombobox.IsEnabled = username == "m.tanainan";
                    PRLabourLockStatusCombobox.IsEnabled = true; //26/10/2021 P'Cream asked for permission
                    PRStaffLockStatusCombobox.IsEnabled = (username == "netchanok" || username == "dachar");
                    //LotMonthCombobox.IsEnabled = true;
                    LotMonthTextBox.IsEnabled = true;
                    LotYearCombobox.IsEnabled = true;
                    HRLabourLockStatusCombobox.IsEnabled = false;
                    HRStaffLockStatusCombobox.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                try
                {
                    LotNumberViewModel selected = (LotNumberViewModel)InformationDataGrid.SelectedItem;
                    if (selected != null)
                    {
                        //LotMonthCombobox.SelectedValue = selected.Lot_Month;
                        LotMonthTextBox.Text = selected.Lot_Month;
                        LotYearCombobox.SelectedValue = selected.Lot_Year;
                        LotFromDatePicker.SelectedDate = selected.Start_date;
                        LotToDatePicker.SelectedDate = selected.Finish_date;
                        SalaryPaidDatePicker.SelectedDate = selected.Salary_Paid_date;
                        OTPdaidDatePicker.SelectedDate = selected.Ot_Paid_date;
                        PRStaffLockStatusCombobox.SelectedValue = selected.Lock_Acc;
                        PRLabourLockStatusCombobox.SelectedValue = selected.Lock_Acc_Labor;
                        HRStaffLockStatusCombobox.SelectedValue = selected.Lock_Hr;
                        HRLabourLockStatusCombobox.SelectedValue = selected.Lock_Hr_Labor;

                        var deptCode = SingletonConfiguration.getInstance().DepartmentCode;
                        if (deptCode == "HRM")
                        {
                            bool payrollLocked = selected.Lock_Acc == "2" || selected.Lock_Acc_Labor == "2";
                            HRStaffLockStatusCombobox.IsEnabled = selected.Lock_Acc != "2";
                            HRLabourLockStatusCombobox.IsEnabled = selected.Lock_Acc_Labor != "2";
                            if (payrollLocked) MessageBox.Show("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้ว ไม่สามารถแก้ไขงวดนี้ได้", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            try
            {
                //LotMonthCombobox.SelectedValue = null;
                LotMonthTextBox.Text = "";
                LotYearCombobox.SelectedValue = null;
                LotFromDatePicker.SelectedDate = null;
                LotToDatePicker.SelectedDate = null;
                SalaryPaidDatePicker.SelectedDate = null;
                OTPdaidDatePicker.SelectedDate = null;
                PRStaffLockStatusCombobox.SelectedValue = null;
                PRLabourLockStatusCombobox.SelectedValue = null;
                HRStaffLockStatusCombobox.SelectedValue = null;
                HRLabourLockStatusCombobox.SelectedValue = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    //if (LotMonthCombobox.SelectedValue == null || LotMonthCombobox.SelectedValue == null) throw new Exception("กรุณาระบุงวด");
                    if (LotMonthTextBox.Text.Trim() == "" || LotYearCombobox.SelectedValue == null) throw new Exception("กรุณาระบุงวด");
                    if (LotFromDatePicker.SelectedDate == null || LotToDatePicker.SelectedDate == null) throw new Exception("กรุณาระบุช่วงวันที่ของงวด");
                    if (SalaryPaidDatePicker.SelectedDate == null) throw new Exception("กรุณาระบุวันที่จ่ายเงินเดือน");
                    if (OTPdaidDatePicker.SelectedDate == null) throw new Exception("กรุณาระบุวันที่จ่ายโอที");
                    var fromDate = LotFromDatePicker.SelectedDate.Value;
                    var toDate = LotToDatePicker.SelectedDate.Value;

                    //var recordedLot = BusinessLayerServices.PayrollBL().GetSingleLot(LotMonthCombobox.Text, LotYearCombobox.Text);
                    var recordedLot = BusinessLayerServices.PayrollBL().GetSingleLot(LotMonthTextBox.Text, LotYearCombobox.Text);

                    //var recordedLot = BusinessLayerServices.PayrollBL().GetLotByDateRange(LotFromDatePicker.SelectedDate.Value, LotToDatePicker.SelectedDate.Value);

                    //if (!recordedLot.Any()) AddLotnumber();
                    //else if (recordedLot.Count() == 1) UpdateLotnumber(recordedLot.FirstOrDefault());
                    //else throw new Exception("ไม่สามารถบันทึกได้ กรุณาตรวจสอบวันที่");

                    if (recordedLot == null) AddLotnumber();
                    else UpdateLotnumber(recordedLot);

                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                    Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddLotnumber()
        {
            var newItem = new Lot_Number
            {
                //Lot_Month = LotMonthCombobox.Text,
                Lot_Month = LotMonthTextBox.Text,
                Lot_Year = LotYearCombobox.Text,
                Start_date = LotFromDatePicker.SelectedDate,
                Finish_date = LotToDatePicker.SelectedDate,
                Lock_Hr = HRStaffLockStatusCombobox.SelectedValue == null ? null : HRStaffLockStatusCombobox.SelectedValue.ToString(),
                Lock_Acc = PRStaffLockStatusCombobox.SelectedValue == null ? null : PRStaffLockStatusCombobox.SelectedValue.ToString(),
                Salary_Paid_date = SalaryPaidDatePicker.SelectedDate,
                Ot_Paid_date = OTPdaidDatePicker.SelectedDate,
                Lock_Acc_Labor = PRLabourLockStatusCombobox.SelectedValue == null ? null : PRLabourLockStatusCombobox.SelectedValue.ToString(),
                Lock_Hr_Labor = HRLabourLockStatusCombobox.SelectedValue == null ? null : HRLabourLockStatusCombobox.SelectedValue.ToString(),
                LockedAll = false
            };
            BusinessLayerServices.PayrollBL().AddLotnumber(newItem);
            //DataCorrectorStatic<Lot_Number>.AddTransaction("A", "Lot Number Setting", null, newItem);
        }

        private void UpdateLotnumber(Lot_Number oldItem)
        {
            var newItem = BusinessLayerServices.PayrollBL().GetSingleLot(oldItem.Lot_Month, oldItem.Lot_Year);
            //newItem.Lot_Month = LotMonthTextBox.Text;
            //newItem.Lot_Year = LotYearCombobox.Text;
            newItem.Lock_Hr = HRStaffLockStatusCombobox.SelectedValue == null ? null : HRStaffLockStatusCombobox.SelectedValue.ToString();
            newItem.Lock_Hr_Labor = HRLabourLockStatusCombobox.SelectedValue == null ? null : HRLabourLockStatusCombobox.SelectedValue.ToString();
            newItem.Lock_Acc = PRStaffLockStatusCombobox.SelectedValue == null ? null : PRStaffLockStatusCombobox.SelectedValue.ToString();
            newItem.Lock_Acc_Labor = PRLabourLockStatusCombobox.SelectedValue == null ? null : PRLabourLockStatusCombobox.SelectedValue.ToString();
            newItem.Ot_Paid_date = OTPdaidDatePicker.SelectedDate;
            newItem.Salary_Paid_date = SalaryPaidDatePicker.SelectedDate;
            newItem.Start_date = LotFromDatePicker.SelectedDate;
            newItem.Finish_date = LotToDatePicker.SelectedDate;
            BusinessLayerServices.PayrollBL().UpdateLotnumber(newItem);
            //DataCorrectorStatic<Lot_Number>.AddTransaction("U", "Lot Number Setting", oldItem, newItem);
        }
    }
}
