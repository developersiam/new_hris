﻿using System;
using System.Windows;
using System.Windows.Input;
using DomainModelHris;
using HRISSystemBL;
using HRISSystem.Shared;

namespace HRISSystem.Form.Trainings
{
    /// <summary>
    /// Interaction logic for CourseWindow.xaml
    /// </summary>
    public partial class CourseWindow : Window
    {
        public CourseWindow()
        {
            InitializeComponent();
            ReloadDatagrid();
            //TrainingCourseIDTextBox.Text = BusinessLayerServices.TrainingBL().GetNewTrainingCourseID();
        }

        private void ReloadDatagrid()
        {
            try
            {
                ClearControl();
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = BusinessLayerServices.TrainingBL().GetAllTrainingCourseList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            BindSelectedData();
        }

        private void EditDetailButton_Click(object sender, RoutedEventArgs e)
        {
            BindSelectedData();
        }

        private void BindSelectedData()
        {
            try
            {
                var selected = (TrainingCourse)InformationDataGrid.SelectedItem;
                if (selected != null)
                {
                    TrainingCourseIDTextBox.IsEnabled = false;
                    TrainingCourseIDTextBox.Text = selected.TrainingCourse_ID;
                    TrainingCourseNameTHTextBox.Text = selected.TrainingCourseNameTH;
                    TrainingCourseNameENTextBox.Text = selected.TrainingCourseNameEN;
                    FromDatePicker.SelectedDate = selected.ValidFrom;
                    EndDatePicker.SelectedDate = selected.EndDate;
                }

                AddButton.IsEnabled = selected == null;
                UpdateButton.IsEnabled = selected != null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(FromDatePicker.Text)) FromDatePicker.SelectedDate = DateTime.Today;
                if (string.IsNullOrEmpty(EndDatePicker.Text)) EndDatePicker.SelectedDate = DateTime.MaxValue;
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(TrainingCourseIDTextBox.Text)) throw new Exception("กรุณาระบุ Course ID");
                    if (string.IsNullOrEmpty(TrainingCourseNameTHTextBox.Text)) throw new Exception("กรุณาระบุชื่อ Course");

                    var oldItem = BusinessLayerServices.TrainingBL().GetTrainingCourseByID(TrainingCourseIDTextBox.Text);
                    var newItem = new TrainingCourse
                    {
                        TrainingCourse_ID = oldItem.TrainingCourse_ID,
                        TrainingCourseNameTH = TrainingCourseNameTHTextBox.Text,
                        TrainingCourseNameEN = TrainingCourseNameENTextBox.Text,
                        ValidFrom = FromDatePicker.SelectedDate,
                        EndDate = EndDatePicker.SelectedDate,
                        CreatedDate = oldItem.CreatedDate,
                        ModifiedDate = DateTime.Now
                    };

                    BusinessLayerServices.TrainingBL().UpdateTrainingCourse(newItem);
                    DataCorrectorStatic<TrainingCourse>.AddTransaction("U", "Training Course Setting", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var oldItem = (TrainingCourse)InformationDataGrid.SelectedItem;
                if (oldItem != null)
                {
                    if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        BusinessLayerServices.TrainingBL().DeleteTrainingCourse(oldItem);
                        DataCorrectorStatic<TrainingCourse>.AddTransaction("D", "Training Course Setting", oldItem, null);
                        MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        ReloadDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearControl()
        {
            try
            {
                TrainingCourseIDTextBox.Text = "";
                TrainingCourseIDTextBox.IsEnabled = true;
                TrainingCourseNameTHTextBox.Text = "";
                TrainingCourseNameENTextBox.Text = "";
                FromDatePicker.Text = "";
                EndDatePicker.Text = "";
                AddButton.IsEnabled = true;
                UpdateButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearControl();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(FromDatePicker.Text)) FromDatePicker.SelectedDate = DateTime.Today;
                if (string.IsNullOrEmpty(EndDatePicker.Text)) EndDatePicker.SelectedDate = DateTime.MaxValue;
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(TrainingCourseIDTextBox.Text)) throw new Exception("กรุณาระบุ Course ID");
                    if (string.IsNullOrEmpty(TrainingCourseNameTHTextBox.Text)) throw new Exception("กรุณาระบุชื่อ Course");

                    var newItem = new TrainingCourse
                    {
                        TrainingCourse_ID = TrainingCourseIDTextBox.Text,
                        TrainingCourseNameTH = TrainingCourseNameTHTextBox.Text,
                        TrainingCourseNameEN = TrainingCourseNameENTextBox.Text,
                        ValidFrom = FromDatePicker.SelectedDate,
                        EndDate = EndDatePicker.SelectedDate,
                        ModifiedDate = DateTime.Now
                    };
                    BusinessLayerServices.TrainingBL().AddTrainingCourse(newItem);
                    DataCorrectorStatic<TrainingCourse>.AddTransaction("A", "Training Course Setting", null, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
