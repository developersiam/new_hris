﻿using DomainModelHris;
using HRISSystem.Shared;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HRISSystem.Form.Trainings
{
    /// <summary>
    /// Interaction logic for TraingPage.xaml
    /// </summary>
    public partial class TraingPage : Page
    {
        List<Training> trainingList = new List<Training>();
        public TraingPage()
        {
            InitializeComponent();
            TraingCourseCombobox.ItemsSource = BusinessLayerServices.TrainingBL().GetAllTrainingCourseList();
        }

        private void TraingCourseCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var course = (TrainingCourse)TraingCourseCombobox.SelectedItem;
            if (course != null)
            {
                FromDatePicker.SelectedDate = course.ValidFrom;
                ToDatePicker.SelectedDate = course.EndDate;
                ReloadDatagrid();
            }
            AddEmployeeButton.IsEnabled = course != null;
            UpdateButton.IsEnabled = course != null;
        }

        private void ReloadDatagrid()
        {
            try
            {
                var course = (TrainingCourse)TraingCourseCombobox.SelectedItem;

                trainingList.Clear();
                trainingList = BusinessLayerServices.TrainingBL().GetTraingListByCourseID(course.TrainingCourse_ID);
                trainingList = trainingList.OrderBy(o => o.Employee_ID).ThenBy(t => t.BeginDate).ToList();
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = trainingList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddCourseButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CourseWindow c = new CourseWindow();
                c.ShowDialog();
                TraingCourseCombobox.ItemsSource = null;
                TraingCourseCombobox.ItemsSource = BusinessLayerServices.TrainingBL().GetAllTrainingCourseList();
                TraingCourseCombobox.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (!trainingList.Any()) throw new Exception("ไม่สามารถบันทึกได้ ไม่พบข้อมูลที่ต้องการบันทึก");

                    foreach (var training in trainingList)
                    {
                        var newItem = training;
                        newItem.Employee = null;
                        if (BusinessLayerServices.TrainingBL().GetSingleTraining(training) == null)
                        {
                            BusinessLayerServices.TrainingBL().AddTraining(newItem);
                            DataCorrectorStatic<Training>.AddTransaction("A", "Training", null, newItem);
                        }
                    }
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var oldItem = (Training)InformationDataGrid.SelectedItem;
                    if (oldItem == null) throw new Exception("กรุณาเลือกข้อมูลที่ต้องการ");

                    if (BusinessLayerServices.TrainingBL().GetSingleTraining(oldItem) != null)
                    {
                        BusinessLayerServices.TrainingBL().DeleteTraining(oldItem);
                        DataCorrectorStatic<Training>.AddTransaction("D", "Training", oldItem, null);
                    }
                    trainingList.Remove(oldItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddEmployeeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var course = (TrainingCourse)TraingCourseCombobox.SelectedItem;
                if (course == null) throw new Exception("กรุณาเลือก Training Course");

                TrainingWindow w = new TrainingWindow();
                w.ShowDialog();
                
                foreach (var m in w.employeeList)
                {
                    Training _training = new Training
                    {
                        BeginDate = w.TrainingFromDatePicker.SelectedDate.Value,
                        EndDate = w.TrainingToDatePicker.SelectedDate.Value,
                        TrainingCourse_ID = course.TrainingCourse_ID,
                        Employee_ID = m.Employee_ID,
                        Status = null,
                        CreatedDate = DateTime.Now,
                        ModifiedDate = null,
                        Employee = BusinessLayerServices.EmployeeBL().GetEmployeeByEmployeeID(m.Employee_ID)
                    };
                    // Add Only not duplicate item.
                    if (BusinessLayerServices.TrainingBL().GetSingleTraining(_training) == null) trainingList.Add(_training);
                }

                trainingList = trainingList.OrderBy(o => o.Employee_ID).ThenBy(t => t.BeginDate).ToList();
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = trainingList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
