﻿using DomainModelHris;
using HRISSystem.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Form.Report
{
    /// <summary>
    /// Interaction logic for PR009Report.xaml
    /// </summary>
    public partial class PR009Report : Window
    {
        public PR009Report()
        {
            InitializeComponent();
            YearCombobox.ItemsSource = HRISService.DataCorrector().GetLotYearList();
            //var staffList = new List<StaffType>();
            //staffList.Add(new StaffType { StaffTypeID = 1, Description = "พนักงานประจำ" });
            //staffList.Add(new StaffType { StaffTypeID = 23, Description = "พนักงานรายวัน/รายวันประจำ" });
            //StafftypeCombobox.ItemsSource = staffList;
            Clear();
        }

        private void YearCombobox_DropDownClosed(object sender, EventArgs e)
        {
            ReloadDatagrid();
        }

        private void StafftypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {

        }

        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            YearCombobox.SelectedIndex = -1;
            StafftypeCombobox.SelectedIndex = -1;
            InformationDataGrid.ItemsSource = null;
            StafType_0_Checkbox.IsChecked = false;
            StafType_1_Checkbox.IsChecked = false;
            StafType_2_Checkbox.IsChecked = false;
            StafType_3_Checkbox.IsChecked = false;
            StafType_4_Checkbox.IsChecked = false;
        }

        private void StafType_0_Checkbox_Checked(object sender, RoutedEventArgs e)
        {
            var checkAll = StafType_0_Checkbox.IsChecked;
            StafType_1_Checkbox.IsChecked = checkAll;
            StafType_2_Checkbox.IsChecked = checkAll;
            StafType_3_Checkbox.IsChecked = checkAll;
            StafType_4_Checkbox.IsChecked = checkAll;
        }

        private void StafType_0_Checkbox_Unchecked(object sender, RoutedEventArgs e)
        {
            //var checkAll = StafType_0_Checkbox.IsChecked;
            //StafType_1_Checkbox.IsChecked = checkAll;
            //StafType_2_Checkbox.IsChecked = checkAll;
            //StafType_3_Checkbox.IsChecked = checkAll;
            //StafType_4_Checkbox.IsChecked = checkAll;
        }

        private void StafType_Checkbox_Click(object sender, RoutedEventArgs e)
        {
            var s1 = StafType_1_Checkbox.IsChecked.GetValueOrDefault();
            var s2 = StafType_2_Checkbox.IsChecked.GetValueOrDefault();
            var s3 = StafType_3_Checkbox.IsChecked.GetValueOrDefault();
            var s4 = StafType_4_Checkbox.IsChecked.GetValueOrDefault();

            StafType_0_Checkbox.IsChecked = s1 && s2 && s3 && s4;
        }
    }
}
