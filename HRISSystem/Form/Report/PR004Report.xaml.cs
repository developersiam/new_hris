﻿using DomainModelHris;
using DomainModelPayroll;
using HRISSystem.Shared;
using HRISSystem.ViewModel;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Form.Report
{
    /// <summary>
    /// Interaction logic for PR004Report.xaml
    /// </summary>
    public partial class PR004Report : Window
    {
        List<RptPR04ViewModel> payrollList;
        public PR004Report()
        {
            InitializeComponent();
            BindCombobox();
        }

        private void BindCombobox()
        {
            LotCombobox.ItemsSource = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);
            LotCombobox.SelectedIndex = -1;

            var username = SingletonConfiguration.getInstance().Username.ToLower();
            var staffList = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
            staffList.Insert(0, new StaffType { StaffTypeID = 0, Description = "ทั้งหมด" });
            if (username == "m.tanainan") staffList = staffList.Where(w => w.StaffTypeID != 1).ToList();
            StafftypeCombobox.ItemsSource = staffList;
        }

        private void LotCombobox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                var selected = (Lot_Number)LotCombobox.SelectedItem;
                if (selected == null) return;
                LotFromDatePicker.SelectedDate = selected != null ? selected.Start_date : null;
                LotToDatePicker.SelectedDate = selected != null ? selected.Finish_date : null;
                SalaryPaidDatePicker.SelectedDate = selected != null ? selected.Salary_Paid_date : null;
                OTPaidDatePicker.SelectedDate = selected != null ? selected.Ot_Paid_date : null;
                Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Clear()
        {
            StafftypeCombobox.SelectedIndex = -1;
            InformationDataGrid.ItemsSource = null;
            SumBasicSalaryTextBox.Text = "";
            SumActualSalaryTextBox.Text = "";
            SumTotalIncomeTextBox.Text = "";
            SumNetIncomeTextBox.Text = "";
        }

        private void StafftypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            if(StafftypeCombobox.SelectedValue != null) ReloadDatagrid();
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                if (string.IsNullOrEmpty(LotCombobox.Text)) throw new Exception("กรุณาระบุงวด");
                if (string.IsNullOrEmpty(StafftypeCombobox.Text)) throw new Exception("กรุณาระบุประเภทพนักงาน");
                var staff = (StaffType)StafftypeCombobox.SelectedItem;

                payrollList = new List<RptPR04ViewModel>();
                var payroll = BusinessLayerServices.PayrollBL().GetPayrollByLot(LotCombobox.Text);
                payroll = payroll.Where(w => w.Net_Income > 0 || w.Working_date > 0 || w.SSO > 0).ToList();
                if (staff.StaffTypeID != 0) payroll = payroll.Where(w => w.Staff_type == staff.StaffTypeID.ToString()).ToList();
                payrollList = HRISService.DataCorrector().ConvertToRPTPR04(payroll, LotCombobox.Text);

                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = payrollList;
                Summary();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void Summary()
        {
            try
            {
                TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", payrollList.Count);
                SumBasicSalaryTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s.Salary).Value);
                SumActualSalaryTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s.NetSalary).Value);
                SumOTTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s.OTSum).Value);
                SumOTHTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s.OTHSum).Value);
                SumOTHHTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s.OTHHSum).Value);
                SumSpecialTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s.Special));
                SumTotalIncomeTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s.TotalIncome));
                SumTaxTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s.Tax));
                SumSSOTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s.SSO));
                SumCutwageTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s.Cutwage));
                SumStuLoanTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s.StudentLoan));
                SumNetIncomeTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s.NetIncome));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(LotCombobox.Text)) throw new Exception("กรุณาระบุงวด");
                if (string.IsNullOrEmpty(StafftypeCombobox.Text)) throw new Exception("กรุณาระบุประเภทพนักงาน");

                System.Windows.Forms.FolderBrowserDialog folderDlg = new System.Windows.Forms.FolderBrowserDialog();
                folderDlg.ShowNewFolderButton = true;

                // Show the FolderBrowserDialog.
                System.Windows.Forms.DialogResult result = folderDlg.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    string folderPath = HRISService.DataCorrector().ExportExcelPayrollStaffType(folderDlg.SelectedPath, LotCombobox.Text, payrollList);
                    Environment.SpecialFolder root = folderDlg.RootFolder;
                    MessageBox.Show("Export complete.", "Complete", MessageBoxButton.OK, MessageBoxImage.Information);
                    System.Diagnostics.Process.Start(folderPath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
