﻿using HRISSystem.Shared;
using HRISSystemBL;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DomainModelPayroll;
using DomainModelHris;

namespace HRISSystem.Form.Report
{
    /// <summary>
    /// Interaction logic for PR005Report.xaml
    /// </summary>
    public partial class PR005Report : Window
    {
        public PR005Report()
        {
            InitializeComponent();
            var username = SingletonConfiguration.getInstance().Username.ToLower();
            LotCombobox.ItemsSource = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);
            LotCombobox.SelectedIndex = -1;

            var staffList = new List<StaffType>();
            staffList.Add(new StaffType { StaffTypeID = 1, Description = "พนักงานประจำ" });
            staffList.Add(new StaffType { StaffTypeID = 23, Description = "พนักงานรายวัน/รายวันประจำ" });
            staffList.Add(new StaffType { StaffTypeID = 4, Description = "พนักงานรายวันชั่วคราว" });

            StafftypeCombobox.ItemsSource = staffList;
        }

        private void LotCombobox_DropDownClosed(object sender, EventArgs e)
        {
            if (LotCombobox.SelectedValue != null) ReloadData();
        }

        private void ReloadData()
        {
            try
            {
                var staff = (StaffType)StafftypeCombobox.SelectedItem;
                var lot = (Lot_Number)LotCombobox.SelectedItem;
                if (lot == null || staff == null) return;
                if (lot == null && staff == null) throw new Exception("กรุณาระบุงวด และ ประเภทพนักงาน");

                var result = new List<Payroll>();
                var payrollList = new List<Payroll>();
                var deptList = new List<Department>();

                payrollList = BusinessLayerServices.PayrollBL()
                    .GetPayrollByLot(LotCombobox.Text);
                //var zz = payrollList.Where(w => w.Fname == "เนตรชนก");
                payrollList = payrollList
                    .Where(w => w.Net_Income > 0 
                    || w.Working_date > 0 
                    || w.SSO > 0)
                    .ToList(); // 2022-07-29 Remove no salary or no SSO or no workdate from list
                //payrollList = payrollList.Where(w => w.Net_Income > 0 || w.Working_date > 0 || w.SSO > 0 || w.Staff_type == "1").ToList();

                if (staff.StaffTypeID == 1)
                    payrollList = payrollList.
                        Where(w => w.Staff_type == "1")
                        .ToList();
                else if (staff.StaffTypeID == 23)
                    payrollList = payrollList
                        .Where(w => w.Staff_type == "2" || w.Staff_type == "3")
                        .ToList();
                else 
                    payrollList = payrollList
                        .Where(w => w.Staff_type == "4")
                        .ToList();

                deptList = payrollList
                    .GroupBy(g => g.Dept_code)
                    .Select(s => new Department { DeptCode = s.Key })
                    .ToList();

                foreach (var dept in deptList)
                {
                    var p = payrollList.Where(w => w.Dept_code == dept.DeptCode).ToList();
                    var d = BusinessLayerServices.DeptCodeBL().GetSingleDepartment(dept.DeptCode);

                    foreach (var i in p)
                    {
                        decimal sumOT = (decimal)i.Cot_hour.GetValueOrDefault() * i.Ot_hour_rate.GetValueOrDefault();
                        i.Cot_hour = Math.Round((double)sumOT, 2, MidpointRounding.AwayFromZero);
                        decimal sumOTH = 0;
                        //if (i.Staff_type != "1")
                        //{
                        //    var countOTH = (decimal)i.Cot_holiday.GetValueOrDefault();
                        //    if (countOTH > 0)
                        //    {
                        //        var amoOTH = (int)countOTH / 8;
                        //        var modOTH = countOTH % 8;
                        //        sumOTH = (decimal.Multiply((decimal)amoOTH, i.Wage.GetValueOrDefault())) + (decimal.Multiply(modOTH, i.Ot_holiday_rate.GetValueOrDefault()));
                        //    }
                        //}
                        //else sumOTH = (decimal)i.Cot_holiday.GetValueOrDefault() * i.Ot_holiday_rate.GetValueOrDefault();
                        var Ot_holiday_rate = i.Staff_type == "1" ? (decimal)i.Ot_holiday_rate.GetValueOrDefault() : i.Wage.GetValueOrDefault() / 8;
                        sumOTH = (decimal)i.Cot_holiday.GetValueOrDefault() * Ot_holiday_rate;
                        i.Cot_holiday = Math.Round((double)sumOTH, 2, MidpointRounding.AwayFromZero);
                        decimal sumOTHH = (decimal)i.Cot_hour_holiday.GetValueOrDefault() * i.Ot_hour_holiday_rate.GetValueOrDefault();
                        i.Cot_hour_holiday = Math.Round((double)sumOTHH, 2, MidpointRounding.AwayFromZero);
                    }

                    var payroll = new Payroll()
                    {
                        Payroll_date = LotCombobox.Text,
                        Salary_paid_date = lot.Salary_Paid_date,
                        Ot_paid_date = lot.Ot_Paid_date,
                        Dept_code = string.Format("[{0}] {1}", d.DeptCode, d.DeptName),
                        Employee_id = p.Count().ToString(),
                        Wage = p.Sum(s => s.Wage.GetValueOrDefault() * (decimal)s.Working_date.GetValueOrDefault()),
                        Salary = p.Sum(s => s.Salary.GetValueOrDefault()),
                        NetSalary = p.Sum(s => s.NetSalary.GetValueOrDefault()),
                        //Cot_hour = p.Sum(s => Math.Round((s.Cot_hour.GetValueOrDefault() * (double)s.Ot_hour_rate.GetValueOrDefault()), 2, MidpointRounding.AwayFromZero)),
                        //Cot_holiday = p.Sum(s => Math.Round((s.Cot_holiday.GetValueOrDefault() * (double)s.Ot_holiday_rate.GetValueOrDefault()), 2, MidpointRounding.AwayFromZero)),
                        //Cot_hour_holiday = p.Sum(s => Math.Round((s.Cot_hour_holiday.GetValueOrDefault() * (double)s.Ot_hour_holiday_rate.GetValueOrDefault()), 2, MidpointRounding.AwayFromZero)),
                        Cot_hour = p.Sum(s => s.Cot_hour),
                        Cot_holiday = p.Sum(s => s.Cot_holiday),
                        Cot_hour_holiday = p.Sum(s => s.Cot_hour_holiday),
                        Special = p.Sum(s => s.Special.GetValueOrDefault() + s.Ot_special_rate.GetValueOrDefault()),
                        Lunch_sup = p.Sum(s => s.Lunch_sup.GetValueOrDefault()),
                        Night_sup = p.Sum(s => s.Night_sup.GetValueOrDefault()),
                        Total_Income = p.Sum(s => s.Total_Income.GetValueOrDefault()),
                        Total_Income_Labour = p.Sum(s => s.Total_Income_Labour.GetValueOrDefault()),
                        //Tax = pnd,
                        //Tax = p.Sum(s => s.Tax.GetValueOrDefault()),
                        Tax = p.Sum(s => Math.Round(s.Tax.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero)),
                        Tax_Labour = p.Sum(s => s.Tax_Labour.GetValueOrDefault()),
                        SSO = p.Sum(s => s.SSO.GetValueOrDefault()),
                        Student_Loan = p.Sum(s => s.Student_Loan.GetValueOrDefault()),
                        Cut_wage = p.Sum(s => s.Cut_wage.GetValueOrDefault()),
                        //Net_Income = p.Sum(s => Math.Round(s.Net_Income.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero))
                        Net_Income = p.Sum(s => s.Net_Income.GetValueOrDefault())
                    };
                    result.Add(payroll);
                }

                result = result.OrderBy(o => o.Dept_code).ToList();

                _ReportViewer.Reset();
                var reportDataSource = new ReportDataSource();
                reportDataSource.Value = result;
                reportDataSource.Name = "DataSet1";
                _ReportViewer.LocalReport.DataSources.Add(reportDataSource);
                if (staff.StaffTypeID == 1) _ReportViewer.LocalReport.ReportEmbeddedResource = "HRISSystem.Form.Report.Rdlc.RPTPR01.rdlc";
                else if (staff.StaffTypeID == 23) _ReportViewer.LocalReport.ReportEmbeddedResource = "HRISSystem.Form.Report.Rdlc.RPTPR04.rdlc";
                else _ReportViewer.LocalReport.ReportEmbeddedResource = "HRISSystem.Form.Report.Rdlc.RPTPR07.rdlc";
                _ReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadData();
        }

        private void StafftypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            if (StafftypeCombobox.SelectedValue != null) ReloadData();
        }
    }
}
