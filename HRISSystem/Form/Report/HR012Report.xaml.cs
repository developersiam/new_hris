﻿using DomainModelHris;
using DomainModelPayroll;
using HRISSystemBL;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Form.Report
{
    /// <summary>
    /// Interaction logic for HR012Report.xaml
    /// </summary>
    public partial class HR012Report : Window
    {
        public HR012Report()
        {
            InitializeComponent();

            var deptList = BusinessLayerServices.DeptCodeBL().GetAllDepartment();
            deptList.Insert(0, new Department { DeptCode = "ALL", DeptNameTH = "ทั้งหมด", DeptName = "ทั้งหมด" });
            DepartmentCombobox.ItemsSource = deptList;

            var staffList = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
            staffList.Insert(0, new StaffType { StaffTypeID = 0, Description = "ทั้งหมด" });
            StafftypeCombobox.ItemsSource = staffList;

            var lot = BusinessLayerServices.PayrollBL().GetCurrentDateLot();
            if (lot != null)
            {
                FromDatePicker.SelectedDate = lot.Start_date;
                ToDatePicker.SelectedDate = lot.Finish_date;
            }
        }

        private void ReloadDatagrid()
        {
            try
            {
                var fromDate = FromDatePicker.SelectedDate;
                var toDate = ToDatePicker.SelectedDate;
                var dept = (Department)DepartmentCombobox.SelectedItem;
                var staff = (StaffType)StafftypeCombobox.SelectedItem;

                if (dept == null || staff == null || fromDate == null || toDate == null) return;

                //if (dept == null) throw new Exception("กรุณาเลือกแผนก/ฝ่าย");
                //if (staff == null) throw new Exception("กรุณาเลือกประเภทพนักงาน");
                //if (fromDate == null || toDate == null) throw new Exception("กรุณาระบุช่วงเวลาให้ถูกต้อง");
                if (fromDate > toDate) throw new Exception("กรุณาระบุช่วงเวลาให้ถูกต้อง");

                _ReportViewer.Reset();
                var reportDataSource = new ReportDataSource();
                if (dept.DeptCode == "ALL")
                {
                    var reportList = BusinessLayerServices.EmployeeBL().sp_GetTimeAttendance(fromDate.Value, toDate.Value);
                    if (staff.StaffTypeID != 0) reportList = reportList.Where(w => w.Staff_type == staff.StaffTypeID).ToList();
                    reportDataSource.Value = reportList;
                }
                else
                {
                    var reportList = BusinessLayerServices.EmployeeBL().sp_GetTimeAttendanceByDept(fromDate.Value, toDate.Value, dept.DeptCode);
                    if (staff.StaffTypeID != 0) reportList = reportList.Where(w => w.Staff_type == staff.StaffTypeID).ToList();
                    reportDataSource.Value = reportList;
                }
                reportDataSource.Name = "GetTimeAttendanceByDeptDataSet";
                _ReportViewer.LocalReport.DataSources.Add(reportDataSource);

                _ReportViewer.LocalReport.ReportEmbeddedResource = "HRISSystem.Form.Report.Rdlc.RPTHR014.rdlc";
                _ReportViewer.RefreshReport();

                //TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", filterList.Count);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void Combobox_DropDownClosed(object sender, EventArgs e)
        {
            ReloadDatagrid();
        }
    }
}
