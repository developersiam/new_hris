﻿using HRISSystem.Helper;
using HRISSystemBL;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Form.Report
{
    /// <summary>
    /// Interaction logic for RPTSW01.xaml
    /// </summary>
    public partial class RPTSW01 : Window
    {
        public RPTSW01()
        {
            InitializeComponent();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                if (FromDatePicker.SelectedDate == null || ToDatePicker.SelectedDate == null)
                    throw new ArgumentException("โปรดระบุวันที่ให้ครบถ้วย");

                var fromDate = FromDatePicker.SelectedDate.Value;
                var toDate = ToDatePicker.SelectedDate.Value;

                if (fromDate > toDate)
                    throw new ArgumentException("โปรดระบุวันที่ให้ถูกต้อง");

                HRISReportDataset.ShiftWorkCalendarDataTable dataTable = new HRISReportDataset.ShiftWorkCalendarDataTable();
                HRISReportDatasetTableAdapters.ShiftWorkCalendarTableAdapter tableAdapter = new HRISReportDatasetTableAdapters.ShiftWorkCalendarTableAdapter();
                tableAdapter.Fill(dataTable, fromDate, toDate);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "ShiftWorkCalendarDataSet";
                reportDataSource.Value = dataTable;

                _ReportViewer.Reset();
                _ReportViewer.LocalReport.DataSources.Add(reportDataSource);
                _ReportViewer.LocalReport.ReportEmbeddedResource = "HRISSystem.Form.Report.Rdlc.RPTSW02.rdlc";
                _ReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
