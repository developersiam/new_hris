﻿using DomainModelHris;
using DomainModelPayroll;
using HRISSystem.Shared;
using HRISSystemBL;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Form.Report
{
    /// <summary>
    /// Interaction logic for PR006Report.xaml
    /// </summary>
    public partial class PR006Report : Window
    {
        int staffTypeID;
        public PR006Report()
        {
            InitializeComponent();
            DepartmentCombobox.ItemsSource = DepartmentList();
            DepartmentCombobox.SelectedIndex = -1;
            LotCombobox.ItemsSource = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);
            LotCombobox.SelectedIndex = -1;

            BindStaffType();
        }

        public PR006Report(Department dept, string lot, int _staffTypeID)
        {
            InitializeComponent();
            staffTypeID = _staffTypeID;
            DepartmentCombobox.ItemsSource = DepartmentList();
            LotCombobox.ItemsSource = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);
            DepartmentCombobox.SelectedValue = dept.DeptCode;
            LotCombobox.SelectedValue = lot;

            BindStaffType();
            ReloadData();
        }

        private List<Department> DepartmentList()
        {
            List<Department> result = new List<Department>();
            result.Add(new Department { DeptCode = "", DeptName = "All Department" });
            result.AddRange(BusinessLayerServices.DeptCodeBL().GetAllDepartment());
            return result;
        }

        private void BindStaffType()
        {
            var dept = SingletonConfiguration.getInstance().DepartmentCode.ToUpper();
            var username = SingletonConfiguration.getInstance().Username.ToLower();
            var staffList = new List<StaffType>();

            staffList.Add(new StaffType { StaffTypeID = 1, Description = "พนักงานประจำ" });
            staffList.Add(new StaffType { StaffTypeID = 23, Description = "พนักงานรายวัน/รายวันประจำ" });
            staffList.Add(new StaffType { StaffTypeID = 4, Description = "พนักงานรายวันชั่วคราว" });

            if (username == "m.tanainan")
                staffList.Remove(staffList.Single(x => x.StaffTypeID == 1));

            StafftypeCombobox.ItemsSource = staffList;
            StafftypeCombobox.SelectedValue = staffTypeID;
            //StafftypeCombobox.SelectedValue = username == "m.tanainan" ? 23 : (username == "netchanok" || username == "dachar") ? 1 : -1;
            //StafftypeCombobox.IsEnabled = dept == "IT" ? true : false; // Enable only ITC
        }

        private void LotCombobox_DropDownClosed(object sender, EventArgs e)
        {
            ReloadData();
        }

        private void DepartmentCombobox_DropDownClosed(object sender, EventArgs e)
        {
            ReloadData();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadData();
        }

        private void StafftypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            ReloadData();
        }

        private void ReloadData()
        {
            try
            {
                var dept = (Department)DepartmentCombobox.SelectedItem;
                var lot = (Lot_Number)LotCombobox.SelectedItem;
                var staff = (StaffType)StafftypeCombobox.SelectedItem;
                //if (lot == null && dept == null) throw new Exception("กรุณาระบุงวด และ แผนก");
                if (lot == null || dept == null || staff == null) return;

                var result = new List<Payroll>();
                var payrollList = BusinessLayerServices.PayrollBL().GetPayrollByLotAndDepartment(LotCombobox.Text, dept.DeptCode, staff.StaffTypeID);
                payrollList = payrollList.Where(w => w.Net_Income > 0 || w.Working_date > 0 || w.SSO > 0).ToList(); // 2022-07-29 Remove no salary or no SSO or no workdate from list
                payrollList = payrollList.Where(w => BusinessLayerServices.EmployeeBL().GetEmployeeByAccCode(w.Employee_id).EndDate >= lot.Start_date).ToList(); //2021-06-28 Check by stafftype instead
                if (!payrollList.Any()) throw new Exception("ไม่พบข้อมูล ที่บันทึกในงวดนี้กรุณาตรวจสอบ");
                foreach (var payroll in payrollList)
                {
                    //payroll.Dept_code = string.Format("[{0}] {1}", dept.DeptCode, dept.DeptName);
                    payroll.Dept_code = payroll.Dept_code;
                    payroll.Tax = Math.Round(payroll.Tax.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero);
                    payroll.Special = payroll.Special.GetValueOrDefault() + payroll.Ot_special_rate.GetValueOrDefault();
                    decimal sumOT = (decimal)payroll.Cot_hour.GetValueOrDefault() * payroll.Ot_hour_rate.GetValueOrDefault();
                    payroll.Cot_hour = Math.Round((double)sumOT, 2, MidpointRounding.AwayFromZero);
                    //decimal sumOTH = 0;
                    //var countOTH = (decimal)payroll.Cot_holiday.GetValueOrDefault();
                    //if (countOTH > 0)
                    //{
                    //    var amoOTH = (int)countOTH / 8;
                    //    var modOTH = countOTH % 8;
                    //    sumOTH = (decimal.Multiply((decimal)amoOTH, payroll.Wage.GetValueOrDefault())) + (decimal.Multiply(modOTH, payroll.Ot_holiday_rate.GetValueOrDefault()));
                    //}
                    //payroll.Cot_holiday = (double)sumOTH;
                    var Ot_holiday_rate = payroll.Staff_type == "1" ? (decimal)payroll.Ot_holiday_rate.GetValueOrDefault() : payroll.Wage.GetValueOrDefault() / 8;
                    decimal sumOTH = (decimal)payroll.Cot_holiday.GetValueOrDefault() * Ot_holiday_rate;
                    payroll.Cot_holiday = Math.Round((double)sumOTH, 2, MidpointRounding.AwayFromZero);
                    decimal sumOTHH = (decimal)payroll.Cot_hour_holiday.GetValueOrDefault() * payroll.Ot_hour_holiday_rate.GetValueOrDefault();
                    payroll.Cot_hour_holiday = Math.Round((double)sumOTHH, 2, MidpointRounding.AwayFromZero);
                }

                payrollList = payrollList.OrderBy(o => o.Dept_code).ThenBy(t => t.Employee_id).ToList();

                _ReportViewer.Reset();
                var reportDataSource = new ReportDataSource();
                reportDataSource.Value = payrollList;
                reportDataSource.Name = "DataSet1";
                _ReportViewer.LocalReport.DataSources.Add(reportDataSource);

                string headDepartment = dept.DeptCode == "" ? "All Department" : string.Format("[{0}] {1}", dept.DeptCode, dept.DeptName);

                if (staff.StaffTypeID == 1) _ReportViewer.LocalReport.ReportEmbeddedResource = "HRISSystem.Form.Report.Rdlc.RPTPR03.rdlc";
                else if (staff.StaffTypeID == 23) _ReportViewer.LocalReport.ReportEmbeddedResource = "HRISSystem.Form.Report.Rdlc.RPTPR02.rdlc";
                else _ReportViewer.LocalReport.ReportEmbeddedResource = "HRISSystem.Form.Report.Rdlc.RPTPR06.rdlc";
                ReportParameter[] parameters = new ReportParameter[1];
                parameters[0] = new ReportParameter("HeadDepartment", headDepartment, true);
                _ReportViewer.LocalReport.SetParameters(parameters);
                _ReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
