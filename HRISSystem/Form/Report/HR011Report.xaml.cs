﻿using HRISSystemBL;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Form.Report
{
    /// <summary>
    /// Interaction logic for HR011Report.xaml
    /// </summary>
    public partial class HR011Report : Window
    {
        public HR011Report()
        {
            InitializeComponent();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                if (FromDatePicker.SelectedDate == null || ToDatePicker.SelectedDate == null) return;
                var fromDate = FromDatePicker.SelectedDate.Value;
                var toDate = ToDatePicker.SelectedDate.Value;
                if (fromDate > toDate) return;

                _ReportViewer.Reset();

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1"; //Name of the report dataset in .RDLC file
                reportDataSource.Value = BusinessLayerServices.EmployeeBL().GetEmployeeListByDate(fromDate, toDate);
                _ReportViewer.LocalReport.DataSources.Add(reportDataSource);
                _ReportViewer.LocalReport.ReportEmbeddedResource = "HRISSystem.Form.Report.Rdlc.RPTHR011.rdlc";
                _ReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
