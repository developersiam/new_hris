﻿using DomainModelHris;
using HRISSystem.Shared;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Form.Report
{
    /// <summary>
    /// Interaction logic for PR008Report.xaml
    /// </summary>
    public partial class PR008Report : Window
    {
        public PR008Report()
        {
            InitializeComponent();
            BindCombobox();
        }

        private void BindCombobox()
        {
            var reportTypeList = new List<string>();
            reportTypeList.Add("Yearly Report");
            reportTypeList.Add("Monthly Report");
            ReportTypeCombobox.ItemsSource = reportTypeList;
            ReportTypeCombobox.SelectedIndex = -1;
            YearCombobox.ItemsSource = HRISService.DataCorrector().GetLotYearList();
            YearCombobox.SelectedIndex = -1;
            MonthCombobox.ItemsSource = HRISService.DataCorrector().GetLotMonthList();
            MonthCombobox.SelectedIndex = -1;
            var staffList = new List<StaffType>();
            staffList.Add(new StaffType { StaffTypeID = 1, Description = "พนักงานประจำ" });
            staffList.Add(new StaffType { StaffTypeID = 23, Description = "พนักงานรายวัน/รายวันประจำ" });
            StafftypeCombobox.ItemsSource = staffList;
        }

        private void ReportTypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            var reportType = ReportTypeCombobox.SelectedValue.ToString();
            MonthCombobox.IsEnabled = reportType == "Monthly Report";
            MonthCombobox.SelectedIndex = -1;
            YearCombobox.SelectedIndex = -1;
        }

        private void YearCombobox_DropDownClosed(object sender, EventArgs e)
        {
            ReloadDatagrid();
        }

        private void MonthCombobox_DropDownClosed(object sender, EventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            var reportType = ReportTypeCombobox.SelectedValue.ToString();
            if (reportType == "Monthly Report") MonthlyReport();
            else YearlyReport();
        }

        private void MonthlyReport()
        {
            var selectedYear = YearCombobox.SelectedValue.ToString();
            var selectedMonth = MonthCombobox.SelectedValue.ToString();
            var staff = (StaffType)StafftypeCombobox.SelectedItem;
            if (string.IsNullOrEmpty(selectedYear) || string.IsNullOrEmpty(selectedMonth) || staff == null) return;


        }

        private void YearlyReport()
        {
            var selectedYear = YearCombobox.SelectedValue.ToString();
            var staff = (StaffType)StafftypeCombobox.SelectedItem;
            if (string.IsNullOrEmpty(selectedYear) || staff == null) return;


        }

        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void StafftypeCombobox_DropDownClosed(object sender, EventArgs e)
        {

        }
    }
}
