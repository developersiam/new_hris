﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HRISSystemBL;

namespace HRISSystem.Form.Report
{
    /// <summary>
    /// Interaction logic for HR013Report.xaml
    /// </summary>
    public partial class HR013Report : Window
    {
        string _lotMonth;
        string _lotYear;
        bool _isReportViewerLoaded;
        public HR013Report(string lotMonth, string lotYear)
        {
            InitializeComponent();
            _lotMonth = lotMonth;
            _lotYear = lotYear;
            _ReportViewer.Load += _ReportViewer_Load;
        }

        private void _ReportViewer_Load(object sender, EventArgs e)
        {
            try
            {
                if (!_isReportViewerLoaded)
                {
                    Microsoft.Reporting.WinForms.ReportDataSource reportDataSource = new Microsoft.Reporting.WinForms.ReportDataSource();
                    reportDataSource.Name = "LabourWorkAndOTDataSet"; //Name of the report dataset in .RDLC file
                    reportDataSource.Value = BusinessLayerServices.PayrollBL().GetLabourWorkAndOTFromPayroll(_lotMonth, _lotYear);
                    _ReportViewer.LocalReport.DataSources.Add(reportDataSource);
                    _ReportViewer.LocalReport.ReportEmbeddedResource = "HRISSystem.Form.Rdlc.RPTHR013.rdlc";
                    _ReportViewer.RefreshReport();
                    _isReportViewerLoaded = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Report error!" + ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
