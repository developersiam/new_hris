﻿using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Form.Report
{
    /// <summary>
    /// Interaction logic for HR017Report.xaml
    /// </summary>
    public partial class HR017Report : Window
    {
        public HR017Report()
        {
            InitializeComponent();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FromDatePicker.SelectedDate == null) throw new Exception("กรุณาระบุวันที่");
                var fromDate = FromDatePicker.SelectedDate.Value;

                _ReportViewer.Reset();

                Microsoft.Reporting.WinForms.ReportDataSource reportDataSource = new Microsoft.Reporting.WinForms.ReportDataSource();
                reportDataSource.Name = "EmployeeTimeAttendanceDataSet"; //Name of the report dataset in .RDLC file
                // >>>>>>>>>>>>>>> sp_GetEmployeeTimeAttendanceByDate1
                reportDataSource.Value = BusinessLayerServices.EmployeeBL().GetEmployeeListByDate(fromDate, fromDate);
                _ReportViewer.LocalReport.DataSources.Add(reportDataSource);
                _ReportViewer.LocalReport.ReportEmbeddedResource = "HRISSystem.Form.Rdlc.RPTHR017.rdlc";
                _ReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
