﻿using DomainModelPayroll;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Form.Report
{
    /// <summary>
    /// Interaction logic for PR007Report_Print.xaml
    /// </summary>
    public partial class PR007Report_Print : Window
    {
        public PR007Report_Print()
        {
            InitializeComponent();
        }
        public PR007Report_Print(List<sp_GetSSOReport_Result> ssoReportList, string lotNumber)
        {
            try
            {
                InitializeComponent();

                _ReportViewer.Reset();
                var reportDataSource = new ReportDataSource();
                reportDataSource.Value = ssoReportList;
                reportDataSource.Name = "DataSet1";
                _ReportViewer.LocalReport.DataSources.Add(reportDataSource);
                _ReportViewer.LocalReport.ReportEmbeddedResource = "HRISSystem.Form.Report.Rdlc.RPTPR05.rdlc";
                ReportParameter[] parameters = new ReportParameter[1];
                parameters[0] = new ReportParameter("lotNumber", lotNumber, true);
                _ReportViewer.LocalReport.SetParameters(parameters);
                _ReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
