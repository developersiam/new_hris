﻿using DomainModelHris;
using DomainModelPayroll;
using HRISSystem.Shared;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Form.Report
{
    /// <summary>
    /// Interaction logic for ToPayroll_WoringDate.xaml
    /// </summary>
    public partial class ToPayroll_WoringDate : Window
    {
        public ToPayroll_WoringDate()
        {
            InitializeComponent();
        }

        public ToPayroll_WoringDate(Lot_Number lot, List<sp_GetWorkingDateToPayroll_Result> workList)
        {
            InitializeComponent();
            BindCombobox();
            SearchLotCombobox.SelectedItem = lot;
            //SearchLotCombobox.SelectedValue = string.Format("{0}/{1}", lot.Lot_Month, lot.Lot_Year);

            ReloadReport();
        }

        private void BindCombobox()
        {
            try
            {
                SearchLotCombobox.ItemsSource = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadReport()
        {
            try
            {
                if (SearchFromDatePicker.SelectedDate == null || SearchToDatePicker.SelectedDate == null) throw new Exception("กรุณาระบุวันที่");
                var fromDate = SearchFromDatePicker.SelectedDate.Value;
                var toDate = SearchToDatePicker.SelectedDate.Value;
                if (fromDate > toDate) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");

                _ReportViewer.Reset();

                Microsoft.Reporting.WinForms.ReportDataSource reportDataSource = new Microsoft.Reporting.WinForms.ReportDataSource();
                reportDataSource.Name = "GetEmployeeTimeAttendanceByDateAndEmployee1DataSet"; //Name of the report dataset in .RDLC file
                //sp_GetEmployeeTimeAttendanceByDateAndEmployee1
                //reportDataSource.Value = BusinessLayerServices.EmployeeBL().sp_GetEmployeeTimeAttendanceByDateAndEmployee1(fromDate, toDate, fingerID);

                Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
                //sp_GetEmployeeTimeAttendanceByDateAndEmployee2
                //reportDataSource2.Name = "GetEmployeeTimeAttendanceByDateAndEmployee2DataSet"; //Name of the report dataset in .RDLC file
                //reportDataSource2.Value = BusinessLayerServices.EmployeeBL().sp_GetEmployeeTimeAttendanceByDateAndEmployee2(fromDate, toDate, employeeID);

                _ReportViewer.LocalReport.DataSources.Add(reportDataSource);
                _ReportViewer.LocalReport.DataSources.Add(reportDataSource2);
                _ReportViewer.LocalReport.ReportEmbeddedResource = "HRISSystem.Form.Report.Rdlc.RPTHR015.rdlc";
                _ReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Report error!" + ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
