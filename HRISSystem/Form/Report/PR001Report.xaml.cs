﻿using DomainModelHris;
using DomainModelPayroll;
using HRISSystem.Shared;
using HRISSystem.ViewModel;
using HRISSystemBL;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace HRISSystem.Form.Report
{
    /// <summary>
    /// Interaction logic for PR001Report.xaml
    /// </summary>
    public partial class PR001Report : Window
    {
        public Microsoft.Office.Interop.Excel.Application excelApplication = new Microsoft.Office.Interop.Excel.Application();
        public Microsoft.Office.Interop.Excel.Workbook excelWorkBook;
        public Microsoft.Office.Interop.Excel.Worksheet excelWorkSheet;
        public Microsoft.Office.Interop.Excel.Range excelRange;
        List<PrintSlipViewModel> slipList;

        public PR001Report()
        {
            InitializeComponent();
            LotCombobox.ItemsSource = HRISService.DataCorrector()
                .LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot())
                .Take(48);
            DepartmentCombobox.ItemsSource = BindDepartmentList();
            EmployeeTypeCombobox.ItemsSource = BindEmployeeTypeList();
            EmployeeTypeCombobox.SelectedIndex = 0;

            slipList = new List<PrintSlipViewModel>();
        }

        public List<Department> BindDepartmentList()
        {
            List<Department> result = new List<Department>();
            result.Add(new Department { DeptCode = "", DeptName = "All Department" });
            result.AddRange(BusinessLayerServices.DeptCodeBL().GetAllDepartment());
            return result;
        }

        public List<StaffType> BindEmployeeTypeList()
        {
            var username = SingletonConfiguration.getInstance().Username.ToLower();
            var result = new List<StaffType>();
            //result.Add(new StaffType { StaffTypeID = 1, Description = "พนักงานประจำ" });

            result.Add(new StaffType { StaffTypeID = 2, Description = "พนักงานรายวัน/รายวันประจำ" });
            result.Add(new StaffType { StaffTypeID = 4, Description = "พนักงานรายวันชั่วคราว" });

            if (username == "netchanok" || username == "dachar")
                result.Add(new StaffType { StaffTypeID = 1, Description = "พนักงานประจำ" });

            return result;
        }

        public List<string> BindEmployeeFiltereList()
        {
            List<string> result = new List<string>();
            result.Add("ทั้งหมด");
            result.Add("เฉพาะพนักงาน");
            return result;
        }

        private void LotCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var selected = (Lot_Number)LotCombobox.SelectedItem;
                LotFromDatePicker.SelectedDate = selected != null ? selected.Start_date : null;
                LotToDatePicker.SelectedDate = selected != null? selected.Finish_date : null;
                InformationDataGrid.ItemsSource = null;
                PrintItemCount();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DepartmentCombobox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                InformationDataGrid.ItemsSource = null;
                PrintItemCount();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EmployeeTypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                InformationDataGrid.ItemsSource = null;
                PrintItemCount();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void GetDataButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (LotCombobox.Text == "") throw new Exception("กรุณาระบุงวด");
                if (DepartmentCombobox.Text == "") throw new Exception("กรุณาระบุแผนก");
                if (EmployeeTypeCombobox.Text == "") throw new Exception("กรุณาระบุประเภท");

                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = GetPrintSlipList();
                PrintItemCount();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        List<PrintSlipViewModel> GetPrintSlipList()
        {
            var dept = (Department)DepartmentCombobox.SelectedItem;
            var staffType = EmployeeTypeCombobox.SelectedValue.ToString();
            var payrollList = BusinessLayerServices.PayrollBL().GetPrintSlip(LotCombobox.Text, dept.DeptCode, staffType);

            slipList = new List<PrintSlipViewModel>();
            foreach (var payroll in payrollList)
            {
                var deptName = BusinessLayerServices.DeptCodeBL().GetSingleDepartment(payroll.Dept_code);
                var staffName = BusinessLayerServices.EmployeeBL().GetStaffTypeByID(int.Parse(staffType));
                slipList.Add(new PrintSlipViewModel
                {
                    PayrollDate = payroll.Payroll_date,
                    DepartmentName = deptName.DeptNameTH,
                    DepartmentCode = payroll.Dept_code,
                    TitleNameTH = payroll.Tname,
                    FirstNameTH = payroll.Fname,
                    LastNameTH = payroll.Lname,
                    EmployeeIdHRIS = payroll.EmployeeID,
                    EmployeeIdPayroll = payroll.Employee_id,
                    StaffType = payroll.Staff_type,
                    StaffTypeDescription = staffName.Description
                });
            }
            slipList = slipList.OrderBy(o => o.DepartmentCode).ThenBy(t => t.EmployeeIdHRIS).ToList();
            return slipList;
        }

        private void FilterTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            FilterEmployee();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            FilterEmployee();
        }

        private void FilterEmployee()
        {
            try
            {
                if (!slipList.Any()) return;
                var searchText = FilterTextBox.Text.Trim();
                var filterList = new List<PrintSlipViewModel>();

                if (searchText != "") filterList = slipList.Where(w => w.FirstNameTH.Contains(searchText)).ToList();
                else filterList = slipList;

                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = filterList;
                PrintItemCount();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            LotCombobox.SelectedIndex = -1;
            DepartmentCombobox.SelectedIndex = -1;
            EmployeeTypeCombobox.SelectedIndex = 0;
            FilterTextBox.Text = "";
            slipList.Clear();
            InformationDataGrid.ItemsSource = null;
            PrintItemCount();
        }

        private void PrintItemCount()
        {
            int? selectedCount = InformationDataGrid.SelectedItems.Count;
            TotalTextBlock.Text = string.Format("รายการปริ้นท์ทั้งหมด {0} รายการ", selectedCount);
            PrintButton.IsEnabled = selectedCount > 0 ? true : false;
        }

        private void InformationDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            PrintItemCount();
        }

        private void InformationDataGrid_KeyUp(object sender, KeyEventArgs e)
        {
            PrintItemCount();
        }

        private void PrintButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (LotCombobox.Text == "") throw new Exception("กรุณาระบุงวด");
                if (DepartmentCombobox.Text == "") throw new Exception("กรุณาระบุแผนก");
                if (EmployeeTypeCombobox.Text == "") throw new Exception("กรุณาระบุประเภท");

                var selectedSlip = new List<sp_GetPayRollForPrintSlip_Result>();
                var listPaySlip = BusinessLayerServices.PayrollBL().GetPrintSlipResult(LotCombobox.Text, DepartmentCombobox.SelectedValue.ToString());
                var selectedItem = InformationDataGrid.SelectedItems;
                foreach (var item in selectedItem)
                {
                    selectedSlip.AddRange(listPaySlip.Where(w => w.EmployeeIdPayroll == ((PrintSlipViewModel)item).EmployeeIdPayroll));
                }

                if (selectedSlip == null || !selectedSlip.Any()) { MessageBox.Show("ไม่พบข้อมูล"); return; }

                Print(selectedSlip);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Print(List<sp_GetPayRollForPrintSlip_Result> selectedSlip)
        {
            foreach (var i in selectedSlip)
            {
                //28 Aug 2018 ให้ใช้ slip format ใหม่ เริ่มต้นในงวดการจ่ายเดือนสิงหาคม 2018 เป็นต้นไป
                excelWorkBook = excelApplication.Workbooks.Open(@"C:\HRIS\Template_Payslip_July2020.xlsx");
                //string _filePath = System.IO.Path.Combine(Environment.CurrentDirectory, @"Template_Payslip_July2018.xlsx");
                //excelWorkBook = excelApplication.Workbooks.Open(_filePath);

                excelWorkSheet = excelWorkBook.ActiveSheet;
                excelWorkSheet.PageSetup.Zoom = false;
                excelWorkSheet.PageSetup.FitToPagesWide = 1;
                excelApplication.Visible = false;

                PutDataintoCell("E2", i.Salary_paid_date.GetValueOrDefault().ToString("dd/MM/yyyy")); //วันที่จ่าย
                PutDataintoCell("H2", i.DepartmentName.ToString());//ฝ่าย
                PutDataintoCell("L2", i.DepartmentName.ToString()); //แผนก

                PutDataintoCell("D3", i.EmployeeIdPayroll.ToString()); //รหัสพนักงาน
                PutDataintoCell("F3", i.EmployeeName.ToString());//ชื่อ-สกุลพนักงาน
                PutDataintoCell("L3", i.POSITIONTH.ToString());//ตำแหน่ง

                //รายได้
                PutDataintoCell("D8", i.Working_date.ToString()); //จำนวนวันทำงาน
                PutDataintoCell("D9", i.Cot_holiday.ToString()); //จำนวนวันทำงานวันหยุด
                PutDataintoCell("D10", i.Cot_hour.ToString()); //จำนวค่าล่วงเวลา 1.5 เท่า
                PutDataintoCell("D11", i.Cot_hour_holiday.ToString());  //จำนวนค่าล่วงเวลา 3 เท่า

                //PutDataintoCell("E8", i.Salary.ToString()); // เงินเดือน/ค่าจ้าง
                PutDataintoCell("E8", i.NetSalary.ToString()); // เงินเดือน/ค่าจ้าง (ที่ได้ในเดือนนั้นโดยยังไม่รวมโอที)
                PutDataintoCell("E9", i.SUM_HOLIDAY.ToString()); //เงินค่าทำงานวันหยุด
                PutDataintoCell("E10", i.SUMOT_HOUR.ToString()); //เงินค่าล่วงเวลา 1.5 เท่า
                PutDataintoCell("E11", i.SUM_HOUR_HOLIDAY.ToString()); //เงินค่าล่วงเวลา 3เท่า
                //PutDataintoCell("E12", i.BONUSBAHT.ToString()); // เงินโบนัส
                PutDataintoCell("E12", i.Bonus.ToString()); // เงินโบนัส
                PutDataintoCell("E13", i.Lunch_sup.ToString()); // ค่าอาหาร
                PutDataintoCell("E14", i.Night_sup.ToString()); // กะดึก
                PutDataintoCell("E15", i.Special.ToString()); //เงินได้อื่นๆ
                PutDataintoCell("E17", i.Total_Income.ToString()); //รวมเงินได้

                //รายจ่าย
                PutDataintoCell("I8", i.SSO.ToString());
                PutDataintoCell("I9", i.Tax.ToString());
                PutDataintoCell("I10", i.Student_Loan.ToString());
                PutDataintoCell("I11", i.Cut_wage.ToString());
                PutDataintoCell("I17", i.SUMDEDUCTS.ToString()); //รวมเงินหัก

                PutDataintoCell("M5", i.Id_card.ToString()); //เลขผู้เสียภาษี
                PutDataintoCell("M7", i.Id_card.ToString()); //เลขบัตรประชาชน
                PutDataintoCell("M8", i.BankAcount.ToString()); //เลขบัญชีธนาคาร

                PutDataintoCell("M11", i.SUMNET_INCOME.ToString()); //ยอดสะสมรายได้
                PutDataintoCell("M12", i.SUMTAX.ToString()); //ยอดสะสมภาษี
                PutDataintoCell("M13", i.SUMSSO.ToString()); //ยอดสะสมประกันสังคม
                PutDataintoCell("M14", i.SUMSTUDENT_LOAN.ToString()); //ยอดสะสมgเงินกยศ

                PutDataintoCell("M16", i.Net_Income.ToString()); //รายได้สุทธิ

                excelWorkSheet.PrintOutEx();
                excelWorkBook.Close(0);
                excelApplication.Quit();
            }
        }

        private void PutDataintoCell(string strCell, string value)
        {
            excelRange = excelWorkSheet.Range[strCell];
            excelWorkSheet.Range[strCell].Value = value;
        }
    }
}
