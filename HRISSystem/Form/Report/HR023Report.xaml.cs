﻿using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Form.Report
{
    /// <summary>
    /// Interaction logic for HR023Report.xaml
    /// </summary>
    public partial class HR023Report : Window
    {
        public HR023Report()
        {
            InitializeComponent();
            DepartmentCombobox.ItemsSource = BusinessLayerServices.DeptCodeBL().GetAllDepartment();
            AllDepartmentRadioButton.IsChecked = true;
            AllStaffRadioButton.IsChecked = true;
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FromDatePicker.SelectedDate == null || ToDatePicker.SelectedDate == null) throw new Exception("กรุณาระบุวันที่");
                var fromDate = FromDatePicker.SelectedDate.Value;
                var toDate = ToDatePicker.SelectedDate.Value;
                if (fromDate > toDate) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
                if (DepartmentRadioButton.IsChecked == true && DepartmentCombobox.Text == "") throw new Exception("กรัณาเลือกแผนก");

                _ReportViewer.Reset();

                Microsoft.Reporting.WinForms.ReportDataSource reportDataSource = new Microsoft.Reporting.WinForms.ReportDataSource();
                reportDataSource.Name = "GetEmployeeTimeAttendanceByDate1DataSet";
                // if(AllDepartment) >>>>>>>>>>>>>>> sp_GetEmployeeTimeAttendanceByDate1
                reportDataSource.Value = BusinessLayerServices.EmployeeBL().GetEmployeeListByDate(fromDate, toDate);
                _ReportViewer.LocalReport.DataSources.Add(reportDataSource);
                _ReportViewer.LocalReport.ReportEmbeddedResource = "HRISSystem.Form.Rdlc.RPTHR023_2.rdlc";
                _ReportViewer.RefreshReport();



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DepartmentRadioButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DepartmentCombobox.IsEnabled = DepartmentRadioButton.IsChecked.Value;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
