﻿using HRISSystem.Helper;
using HRISSystemBL;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Form.Report
{
    /// <summary>
    /// Interaction logic for HR015Report.xaml
    /// </summary>
    public partial class HR015Report : Window
    {
        public HR015Report()
        {
            InitializeComponent();
        }

        private void TextBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left) Search();
        }

        private void SearchEmployeeButton_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        public void Search()
        {
            try
            {
                var window = new Employees.SearchEmployee_Window();
                window.ShowDialog();
                var selected = window.selectedEmployee;
                if (selected != null)
                {
                    EmployeeIDTextBox.Text = selected.Employee_ID;
                    NameTHTextBox.Text = selected.Person.FirstNameTH;
                    LastnameTHTextBox.Text = selected.Person.LastNameTH;
                    FingerIDTextBox.Text = selected.FingerScanID;

                    var fromDate = FromDatePicker.SelectedDate;
                    var toDate = ToDatePicker.SelectedDate;
                    if (fromDate == null && toDate == null)
                    {
                        var lot = BusinessLayerServices.PayrollBL().GetCurrentDateLot();
                        if (lot != null)
                        {
                            FromDatePicker.SelectedDate = lot.Start_date;
                            ToDatePicker.SelectedDate = lot.Finish_date;
                        }
                    }

                    if (EmployeeIDTextBox.Text.TrimEnd() != "" &&
                        fromDate != null &&
                        toDate != null &&
                        (fromDate <= toDate))
                        ReloadReport();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadReport();
        }

        private void ReloadReport()
        {
            try
            {
                if (string.IsNullOrEmpty(EmployeeIDTextBox.Text) || string.IsNullOrEmpty(FingerIDTextBox.Text))
                    throw new Exception("กรุณาเลือกพนักงาน");

                if (FromDatePicker.SelectedDate == null || ToDatePicker.SelectedDate == null)
                    throw new Exception("กรุณาระบุวันที่");

                var fromDate = FromDatePicker.SelectedDate.Value;
                var toDate = ToDatePicker.SelectedDate.Value;
                if (fromDate > toDate)
                    throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");

                var employeeID = EmployeeIDTextBox.Text;
                var fingerID = Convert.ToInt32(FingerIDTextBox.Text);
                var reportDataSource1 = new ReportDataSource();
                var reportDataSource2 = new ReportDataSource();
                var dataTable1 = new HRISReportDataset.sp_GetEmployeeTimeAttendanceByDateAndEmployee1DataTable();
                var dataTable2 = new HRISReportDataset.sp_GetEmployeeTimeAttendanceByDateAndEmployee2DataTable();
                var tableAdapter1 = new HRISReportDatasetTableAdapters.sp_GetEmployeeTimeAttendanceByDateAndEmployee1TableAdapter();
                var tableAdapter2 = new HRISReportDatasetTableAdapters.sp_GetEmployeeTimeAttendanceByDateAndEmployee2TableAdapter();

                tableAdapter1.Fill(dataTable1, fromDate, toDate, fingerID);
                tableAdapter2.Fill(dataTable2, fromDate, toDate, employeeID);
                reportDataSource1.Name = "GetEmployeeTimeAttendanceByDateAndEmployee1DataSet";
                reportDataSource2.Name = "GetEmployeeTimeAttendanceByDateAndEmployee2DataSet";
                reportDataSource1.Value = dataTable1;
                reportDataSource2.Value = dataTable2;

                _ReportViewer.Reset();
                _ReportViewer.LocalReport.DataSources.Add(reportDataSource1);
                _ReportViewer.LocalReport.DataSources.Add(reportDataSource2);
                _ReportViewer.LocalReport.ReportEmbeddedResource = "HRISSystem.Form.Report.Rdlc.RPTHR015.rdlc";
                _ReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DatePicker_CalendarClosed(object sender, RoutedEventArgs e)
        {
            var fromDate = FromDatePicker.SelectedDate;
            var toDate = ToDatePicker.SelectedDate;

            if (fromDate != null && toDate != null && (fromDate <= toDate))
                ReloadReport();
        }
    }
}
