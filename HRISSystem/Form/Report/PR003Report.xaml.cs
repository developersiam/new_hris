﻿using HRISSystem.Shared;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DomainModelHris;
using DomainModelPayroll;

namespace HRISSystem.Form.Report
{
    /// <summary>
    /// Interaction logic for PR003Report.xaml
    /// </summary>
    public partial class PR003Report : Window
    {
        List<sp_GetPayrollEmployee_Result> payrollList;
        public PR003Report()
        {
            InitializeComponent();
            var username = SingletonConfiguration.getInstance().Username.ToLower();
            //var staffList = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList().Where(w => w.StaffTypeID != 4).ToList();
            var staffList = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
            staffList.Insert(0, new StaffType { StaffTypeID = 0, Description = "ทั้งหมด" });
            if (username == "m.tanainan") staffList = staffList.Where(w => w.StaffTypeID == 2 || w.StaffTypeID == 3 || w.StaffTypeID == 4).ToList();
            StafftypeCombobox.ItemsSource = staffList;

            var staffStatusList = BusinessLayerServices.EmployeeBL().GetAllStaffStatusList();
            staffStatusList.Insert(0, new StaffStatu { StaffStatusID = 0, Description = "ทั้งหมด" });
            StaffStatusCombobox.ItemsSource = staffStatusList;
        }

        private void StafftypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            ReloadDatagrid();
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                var staff = (StaffType)StafftypeCombobox.SelectedItem;
                var status = (StaffStatu)StaffStatusCombobox.SelectedItem;
                if (staff == null || status == null) return;

                payrollList = BusinessLayerServices.PayrollBL().sp_GetPayrollEmployee(staff.StaffTypeID, status.StaffStatusID);

                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = payrollList;
                Summary();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Summary()
        {
            try
            {
                TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", payrollList.Count);
                SumBasicSalaryTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s.WageSalary).Value);
                SumActualSalaryTextBox.Text = string.Format("{0:n}", payrollList.Sum(s => s.ActWageSalary).Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(StafftypeCombobox.Text)) throw new Exception("กรุณาระบุประเภทพนักงาน");

                System.Windows.Forms.FolderBrowserDialog folderDlg = new System.Windows.Forms.FolderBrowserDialog();
                folderDlg.ShowNewFolderButton = true;

                // Show the FolderBrowserDialog.
                System.Windows.Forms.DialogResult result = folderDlg.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    var deptCode = SingletonConfiguration.getInstance().DepartmentCode.ToUpper();
                    string folderPath = HRISService.DataCorrector().ExportExcelPayrollEmployee(folderDlg.SelectedPath, payrollList, deptCode != "HRM");
                    Environment.SpecialFolder root = folderDlg.RootFolder;
                    MessageBox.Show("Export complete.", "Complete", MessageBoxButton.OK, MessageBoxImage.Information);
                    System.Diagnostics.Process.Start(folderPath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
