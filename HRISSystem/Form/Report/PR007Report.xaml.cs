﻿using DomainModelPayroll;
using HRISSystem.Shared;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Form.Report
{
    /// <summary>
    /// Interaction logic for RPTPR007xaml.xaml
    /// </summary>
    public partial class PR007Report : Window
    {
        List<sp_GetSSOReport_Result> ssoReportList;
        public PR007Report()
        {
            InitializeComponent();
            LotCombobox.ItemsSource = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);
            LotCombobox.SelectedIndex = -1;
        }

        private void LotCombobox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                var selected = (Lot_Number)LotCombobox.SelectedItem;
                if (selected == null) return;
                LotFromDatePicker.SelectedDate = selected != null ? selected.Start_date : null;
                LotToDatePicker.SelectedDate = selected != null ? selected.Finish_date : null;
                SalaryPaidDatePicker.SelectedDate = selected != null ? selected.Salary_Paid_date : null;
                OTPaidDatePicker.SelectedDate = selected != null ? selected.Ot_Paid_date : null;
                ReloadDatagrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                if (string.IsNullOrEmpty(LotCombobox.Text)) throw new Exception("กรุณาระบุงวด");

                ssoReportList = BusinessLayerServices.PayrollBL().sp_GetSSOReport(LotCombobox.Text);

                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = ssoReportList;
                Summary();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Summary()
        {
            try
            {
                TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", ssoReportList.Count);
                SumBasicSalaryTextBox.Text = string.Format("{0:n}", ssoReportList.Sum(s => s.Salary).Value);
                SumSSOTextBox.Text = string.Format("{0:n}", ssoReportList.Sum(s => s.SSO));
                SumNetSalaryTextBox.Text = string.Format("{0:n}", ssoReportList.Sum(s => s.NetSalary));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(LotCombobox.Text)) throw new Exception("กรุณาระบุงวด");

                System.Windows.Forms.FolderBrowserDialog folderDlg = new System.Windows.Forms.FolderBrowserDialog();
                folderDlg.ShowNewFolderButton = true;

                // Show the FolderBrowserDialog.
                System.Windows.Forms.DialogResult result = folderDlg.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    string folderPath = HRISService.DataCorrector().ExportExcelPR007(folderDlg.SelectedPath, LotCombobox.Text, ssoReportList);
                    Environment.SpecialFolder root = folderDlg.RootFolder;
                    MessageBox.Show("Export complete.", "Complete", MessageBoxButton.OK, MessageBoxImage.Information);
                    System.Diagnostics.Process.Start(folderPath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PrintButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(LotCombobox.Text)) throw new Exception("กรุณาระบุงวด");
                if (ssoReportList.Any())
                {
                    //var x = LotCombobox.Text;
                    //x = x.Remove(2);
                    //x = "0." + x;
                    //var y = Convert.ToDecimal(x);
                    //foreach (var item in ssoReportList)
                    //{
                    //    item.Salary = y;
                    //}
                    var w = new PR007Report_Print(ssoReportList, LotCombobox.Text);
                    w.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
