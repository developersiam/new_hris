﻿using DomainModelHris;
using DomainModelPayroll;
using HRISSystem.Shared;
using HRISSystemBL;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Form.Report
{
    /// <summary>
    /// Interaction logic for HR018Report.xaml
    /// </summary>
    public partial class HR018Report : Window
    {
        List<Payroll> payrollList;
        public HR018Report()
        {
            InitializeComponent();
            LotCombobox.ItemsSource = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);

            var deptList = BusinessLayerServices.DeptCodeBL().GetAllDepartment();
            deptList.Insert(0, new Department { DeptCode = "ALL", DeptNameTH = "ทั้งหมด", DeptName = "ทั้งหมด" });
            DepartmentCombobox.ItemsSource = deptList;
            
            var staffList = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
            staffList.RemoveAt(0);
            staffList.Insert(0, new StaffType { StaffTypeID = 0, Description = "ทั้งหมด" });
            StafftypeCombobox.ItemsSource = staffList;

            payrollList = new List<Payroll>();
        }

        private void LotCombobox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                var selected = (Lot_Number)LotCombobox.SelectedItem;
                if (selected == null) return;
                LotFromDatePicker.SelectedDate = selected != null ? selected.Start_date : null;
                LotToDatePicker.SelectedDate = selected != null ? selected.Finish_date : null;
                //SalaryPaidDatePicker.SelectedDate = selected != null ? selected.Salary_Paid_date : null;
                //OTPaidDatePicker.SelectedDate = selected != null ? selected.Ot_Paid_date : null;
                Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Clear()
        {
            DepartmentCombobox.SelectedIndex = -1;
            StafftypeCombobox.SelectedIndex = -1;
            _ReportViewer.Reset();
            payrollList.Clear();
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var lot = (Lot_Number)LotCombobox.SelectedItem;
                var dept = (Department)DepartmentCombobox.SelectedItem;
                var staff = (StaffType)StafftypeCombobox.SelectedItem;

                if (lot == null) throw new Exception("กรุณาเลือกงวดที่ต้องการ");
                if (dept == null) throw new Exception("กรุณาเลือกแผนก/ฝ่าย");
                if (staff == null) throw new Exception("กรุณาเลือกประเภทพนักงาน");
                var filterList = new List<Payroll>();

                payrollList = BusinessLayerServices.PayrollBL().GetPayrollByLot(LotCombobox.Text);

                filterList = payrollList.Where(w => w.Staff_type != "1").ToList();
                if (dept.DeptCode != "ALL") filterList = filterList.Where(w => w.Dept_code == dept.DeptCode).ToList();
                if (staff.StaffTypeID != 0) filterList = filterList.Where(w => w.Staff_type == staff.StaffTypeID.ToString() || w.Staff_type == staff.Description).ToList();

                foreach (var i in filterList)
                {
                    i.Staff_type = i.Staff_type == "2" ? "รายวันประจำ" : "รายวัน";
                }

                filterList = filterList.OrderBy(o => o.Dept_code).ThenBy(t => t.Employee_id).ToList();

                var workLabourList = BusinessLayerServices.PayrollBL().GetWorkDateByLot(lot.Lot_Month, lot.Lot_Year);
                //var payrollEmplist = new List<DomainModelHris.Employee>();
                var payrollEmplist = BusinessLayerServices.EmployeeBL().GetEmployeeListByDepartment(dept.DeptCode);
                int importedLabour = 0; 
                foreach (var i in payrollEmplist)
                {
                    var _importedLabour = workLabourList.SingleOrDefault(s => s.Employee_id == i.Noted);
                    if(_importedLabour != null) importedLabour++;
                }

                _ReportViewer.Reset();
                var reportDataSource = new ReportDataSource();
                reportDataSource.Value = filterList;
                reportDataSource.Name = "DataSet1";
                _ReportViewer.LocalReport.DataSources.Add(reportDataSource);
                _ReportViewer.LocalReport.ReportEmbeddedResource = "HRISSystem.Form.Report.Rdlc.RPTHR018.rdlc";
                ReportParameter[] parameters = new ReportParameter[3];
                parameters[0] = new ReportParameter("lotNumber", LotCombobox.Text, true);
                parameters[1] = new ReportParameter("importedLabour", payrollEmplist.Any() ? importedLabour.ToString() : filterList.Count.ToString(), true);
                parameters[2] = new ReportParameter("paidLabour", filterList.Count.ToString(), true);
                _ReportViewer.LocalReport.SetParameters(parameters);
                _ReportViewer.RefreshReport();

                //TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", filterList.Count);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}