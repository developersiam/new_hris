﻿using DomainModelPayroll;
using HRISSystem.Shared;
using HRISSystem.ViewModel;
using HRISSystemBL;
using Spire.Xls;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OfficeOpenXml;
//using Microsoft.Office.Interop.Excel;

namespace HRISSystem.Form.Report
{
    /// <summary>
    /// Interaction logic for PR002Report.xaml
    /// </summary>
    public partial class PR002Report : Window
    {
        public Microsoft.Office.Interop.Excel.Worksheet excelWorkSheet;
        public Microsoft.Office.Interop.Excel.Range excelRange;
        List<PNDViewModel> pndList = new List<PNDViewModel>();
        List<PNDViewModel> mismatchedList = new List<PNDViewModel>();

        public PR002Report()
        {
            InitializeComponent();
            LotCombobox.ItemsSource = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);
        }

        private void LotCombobox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                var selected = (Lot_Number)LotCombobox.SelectedItem;
                if (selected == null) return;
                LotFromDatePicker.SelectedDate = selected != null ? selected.Start_date : null;
                LotToDatePicker.SelectedDate = selected != null ? selected.Finish_date : null;
                ReloadDataGrid();
                //PrintItemCount();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadDataGrid()
        {
            try
            {
                FileUploadTextbox.Text = "";
                if (LotCombobox.Text == "") throw new Exception("กรุณาเลือกงวด");
                
                pndList.Clear();
                pndList = ConvertToPNDViewModel(BusinessLayerServices.PayrollBL().GetPayrollByLot(LotCombobox.Text));
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = pndList;

                MatchedDataButton.Content = string.Format("สำเร็จ ({0})", pndList == null ? "0" : pndList.Count.ToString());
                MisMatchedDataButton.Content = "ข้แมูลไม่ตรงกัน (0)";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private List<PNDViewModel> ConvertToPNDViewModel(List<Payroll> payrollList)
        {
            if (payrollList == null) return null;
            payrollList = payrollList.Where(w => (w.Tax != null || w.Tax_Labour != null)).ToList();

            int _pndIndex = 1;
            List<PNDViewModel> result = new List<PNDViewModel>();
            foreach (var item in payrollList)
            {
                var _actualSalary = item.Staff_type == "1" ? item.Total_Income : item.Total_Income_Labour;
                var _incomeTax = item.Staff_type == "1" ? item.Tax : item.Tax_Labour;

                var pnd = new PNDViewModel
                {
                    //taxType = item.Staff_type == "1" ? "1" : "4",
                    taxType = "1",
                    citizenID = item.Id_card,
                    titleName = item.Tname,
                    employeeName = item.Fname + " " + item.Lname,
                    paidDate = item.Salary_paid_date,
                    actualSalary = _actualSalary,
                    incomeTax = _incomeTax,
                    //revenueCondition = item.Staff_type == "1" ? 1 : 2
                    revenueCondition = 1,
                    totalActualSalary = _actualSalary,
                    totalIncomeTax = _incomeTax
                };

                if (pnd.incomeTax > 0)
                {
                    pnd.pndIndex = _pndIndex;
                    result.Add(pnd);
                    _pndIndex++;
                }
            }
            return result;
        }

        private void FileUploadTextbox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ImportExcelFile();
        }

        private void ImportButton_Click(object sender, RoutedEventArgs e)
        {
            ImportExcelFile();
        }

        private void ImportExcelFile()
        {
            try
            {
                if (pndList == null || !pndList.Any()) throw new Exception("กรุณาเลือกข้อมูลจากงวด");

                System.Windows.Forms.OpenFileDialog fileDialog = new System.Windows.Forms.OpenFileDialog();
                fileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                fileDialog.FilterIndex = 2;
                fileDialog.RestoreDirectory = true;

                if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    FileUploadTextbox.Text = fileDialog.FileName;
                    var excelList = ReadExcelFile(fileDialog.FileName);
                    mismatchedList.Clear();

                    if (excelList != null || excelList.Count > 0)
                    {
                        foreach (var i in excelList)
                        {
                            var pnd = pndList.FirstOrDefault(f => f.citizenID == i.citizenID);
                            if (pnd != null)
                            {
                                var _totalActualSalary = pnd.actualSalary + i.importedActualSalary;
                                var _totalIncomeTax = pnd.incomeTax + i.importedIncomeTax;

                                pnd.importedActualSalary = i.importedActualSalary;
                                pnd.importedIncomeTax = i.importedIncomeTax;
                                pnd.totalActualSalary = _totalActualSalary;
                                pnd.totalIncomeTax = _totalIncomeTax;
                                pnd.importStatus = true;
                            }
                            else
                            {
                                //Not Matched list
                                mismatchedList.Add(i);
                            }
                        }
                        InformationDataGrid.ItemsSource = null;
                        InformationDataGrid.ItemsSource = pndList;
                        MatchedDataButton.Content = string.Format("สำเร็จ ({0})", pndList == null ? "0" : pndList.Count.ToString());
                        MisMatchedDataButton.Content = string.Format("ข้แมูลไม่ตรงกัน ({0})", mismatchedList == null ? "0" : mismatchedList.Count.ToString());
                    }
                    else throw new Exception("ไม่สามารถเปิดไฟล์ได้ !! กรุณาปิดไฟล์ Excel ที่กำลัง Upload.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Mapping excel : " + ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public List<PNDViewModel> ReadExcelFile(string FilePath)
        {
            Workbook wb = new Workbook();
            wb.LoadFromFile(FilePath);
            Worksheet sheet = wb.Worksheets[0];
            var collection = DataTableToList(sheet.ExportDataTable());
            return collection;
        }

        public List<PNDViewModel> DataTableToList(DataTable Datable)
        {
            List<PNDViewModel> PndViewmodelList = new List<PNDViewModel>();
            foreach (DataRow row in Datable.Rows)
            {
                if (row[0].ToString() == "") break;
                PNDViewModel model = new PNDViewModel();
                model.citizenID = row[0].ToString();
                try { model.importedIncomeTax = Convert.ToDecimal(row[1].ToString()); }
                catch (Exception) { model.importedIncomeTax = 0; }
                try { model.importedActualSalary = Convert.ToDecimal(row[2].ToString()); }
                catch (Exception) { model.importedActualSalary = 0; }
                PndViewmodelList.Add(model);
            }
            return PndViewmodelList;
        }

        private void MatchedDataButton_Click(object sender, RoutedEventArgs e)
        {
            InformationDataGrid.ItemsSource = null;
            InformationDataGrid.ItemsSource = pndList;
        }

        private void MisMatchedDataButton_Click(object sender, RoutedEventArgs e)
        {
            InformationDataGrid.ItemsSource = null;
            InformationDataGrid.ItemsSource = mismatchedList;
        }

        private void ExportTextButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (pndList == null || !pndList.Any()) return;

                System.Windows.Forms.SaveFileDialog fileDialog = new System.Windows.Forms.SaveFileDialog();
                fileDialog.Filter = "Data Files (*.txt)|*.txt";
                fileDialog.DefaultExt = "txt";
                fileDialog.AddExtension = true;

                if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    using (var writer = new StreamWriter(fileDialog.FileName))
                    {
                        foreach (var p in pndList)
                        {
                            string pndText = p.pndIndex + "|" + p.taxType + "|" + p.citizenID + "||" +
                                            p.titleName + "|" + p.employeeName + "|" + p.paidDate.Value.ToString("dd-MM-yyyy") + "|" +
                                            string.Format("{0:n}", p.totalActualSalary.Value) + "|" + string.Format("{0:n}", p.totalIncomeTax.Value) + "|" +
                                            p.revenueCondition;
                            writer.WriteLine(pndText);
                        }
                    }
                    MessageBox.Show("Export ข้อมูลสำเร็จ", "Complete", MessageBoxButton.OK, MessageBoxImage.Information);
                    System.Diagnostics.Process.Start(System.IO.Path.GetDirectoryName(fileDialog.FileName));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            pndList.Clear();
            mismatchedList.Clear();
            InformationDataGrid.ItemsSource = null;
            MatchedDataButton.Content = "สำเร็จ (0)";
            MisMatchedDataButton.Content = "ข้แมูลไม่ตรงกัน (0)";
            LotCombobox.SelectedIndex = -1;
            LotFromDatePicker.SelectedDate = null;
            LotToDatePicker.SelectedDate = null;
            FileUploadTextbox.Text = "";
        }

        private void ExportDetailButton_Click(object sender, RoutedEventArgs e)
        {
            ExcelExport();
        }

        private void ExcelExport()
        {
            try
            {
                if (pndList == null || !pndList.Any()) return;

                System.Windows.Forms.FolderBrowserDialog folderDlg = new System.Windows.Forms.FolderBrowserDialog();
                folderDlg.ShowNewFolderButton = true;
                System.Windows.Forms.DialogResult result = folderDlg.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    string folderPath = ExportDetail(folderDlg.SelectedPath);
                    Environment.SpecialFolder root = folderDlg.RootFolder;
                    MessageBox.Show("Export ข้อมูลสำเร็จ.", "Complete", MessageBoxButton.OK, MessageBoxImage.Information);
                    System.Diagnostics.Process.Start(folderPath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public string ExportDetail(string folderName)
        {
            string name = @"" + folderName + "\\Pnd1-" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".xlsx";

            FileInfo info = new FileInfo(name);
            if (info.Exists) File.Delete(name);

            // If you use EPPlus in a noncommercial context
            // according to the Polyform Noncommercial license:
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage pck = new ExcelPackage(info))
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("ภงด.1");
                ws.Cells["A1"].Value = "Payroll Lot : " + LotCombobox.SelectedValue;
                ws.Cells["A2"].Value = "Export Date : " + DateTime.Now;
                ws.Cells["A3"].LoadFromCollection(pndList, true);
                ws.DeleteColumn(14); // Remove column เงื่อนไขภาษี
                ws.DeleteColumn(14); // Remove column สถานะ import
                ws.Protection.IsProtected = false;
                ws.Cells.AutoFitColumns();
                using (ExcelRange col = ws.Cells[4, 7, 3 + pndList.Count, 7]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                pck.Save();
            }
            return @"" + folderName;
        }

        private void ExportFormatButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (pndList == null || !pndList.Any()) return;
                ExportFormat();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ExportFormat()
        {
            var pages = pndList.Count / 8;
            if (pndList.Count % 8 > 0) pages++;
            if (MessageBox.Show("ใช้กระดาษ A4 ทั้งหมด " + pages + " แผ่น กดปุ่ม Yes เพื่อปรินส์ใบแนบนี้", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                int rowCount = 0;
                int _page = 1;
                int index = 1;
                decimal total = 0;
                decimal totalTax = 0;
                var paidDate = new DateTime();
                var excelApplication = new Microsoft.Office.Interop.Excel.Application();
                var excelWorkBook = excelApplication.Workbooks.Open(@"C:\HRIS\Template_Pnd1_Format.xls");
                excelWorkSheet = excelWorkBook.ActiveSheet;

                foreach (var i in pndList)
                {
                    if (rowCount == 0)
                    {
                        //Clean(excelWorkSheet);
                        PutDataintoCell("AY7", _page.ToString());
                        PutDataintoCell("BD7", pages.ToString());
                    }

                    var _rowCount = (rowCount * 2) + 11;
                    var _rowName = _rowCount + 1;
                    PutDataintoCell("A" + _rowCount, index.ToString());
                    index++;
                    var cardnumber = i.citizenID.ToCharArray();
                    PutDataintoCell("B" + _rowCount, cardnumber[0].ToString());
                    PutDataintoCell("D" + _rowCount, cardnumber[1].ToString());
                    PutDataintoCell("E" + _rowCount, cardnumber[2].ToString());
                    PutDataintoCell("F" + _rowCount, cardnumber[3].ToString());
                    PutDataintoCell("G" + _rowCount, cardnumber[4].ToString());
                    PutDataintoCell("I" + _rowCount, cardnumber[5].ToString());
                    PutDataintoCell("J" + _rowCount, cardnumber[6].ToString());
                    PutDataintoCell("K" + _rowCount, cardnumber[7].ToString());
                    PutDataintoCell("L" + _rowCount, cardnumber[8].ToString());
                    PutDataintoCell("M" + _rowCount, cardnumber[9].ToString());
                    PutDataintoCell("O" + _rowCount, cardnumber[10].ToString());
                    PutDataintoCell("P" + _rowCount, cardnumber[11].ToString());
                    PutDataintoCell("R" + _rowCount, cardnumber[12].ToString());
                    var name = i.employeeName.Split(' ');
                    PutDataintoCell("E" + _rowName, i.titleName + " " + name[0]);
                    PutDataintoCell("Q" + _rowName, name[1]);
                    paidDate = i.paidDate.Value;
                    PutDataintoCell("AB" + _rowCount, i.paidDate.Value.ToString("dd/MM/yyyy"));
                    var actual = i.actualSalary;
                    total = total + actual.Value;
                    var tax = i.incomeTax;
                    totalTax = totalTax + tax.Value;
                    var actualText = i.actualSalary.Value.ToString("N2").Split('.');
                    PutDataintoCell("AE" + _rowCount, actualText[0]);
                    PutDataintoCell("AF" + _rowCount, string.IsNullOrEmpty(actualText[1]) ? "00" : actualText[1]);
                    var taxText = i.incomeTax.Value.ToString("N2").Split('.');
                    PutDataintoCell("AR" + _rowCount, taxText[0]);
                    PutDataintoCell("BC" + _rowCount, string.IsNullOrEmpty(taxText[1]) ? "00" : taxText[1]);

                    if (rowCount == 7 || (index - 1) == pndList.Count)
                    {
                        var totalText = total.ToString("N2").Split('.');
                        PutDataintoCell("AE27", totalText[0]);
                        PutDataintoCell("AP27", string.IsNullOrEmpty(totalText[1]) ? "00" : totalText[1]);
                        var totalTaxText = totalTax.ToString("N2").Split('.');
                        PutDataintoCell("AR27", totalTaxText[0]);
                        PutDataintoCell("BC27", string.IsNullOrEmpty(totalTaxText[1]) ? "00" : totalTaxText[1]);
                        var month = paidDate.Month;
                        var year = month == 12 ? paidDate.Year + 1 : paidDate.Year;
                        PutDataintoCell("AN32", MonthThai(month));
                        PutDataintoCell("AY32", year.ToString());

                        var Filename = "C:\\PayrollExcel\\Pnd50\\Pnd11_" + LotCombobox.Text.Replace('/', '_') + "-" + _page + ".xls";
                        excelWorkBook.SaveAs(Filename);
                        //excelWorkBook.Close("Pnd11_" + LotCombobox.Text.Replace('/', '_') + "-" + _page + ".xls", 0);

                        _page++;
                        rowCount = 0;
                    }
                    else rowCount++;
                }
                excelWorkBook.Close(0);
                excelApplication.Quit();
                MessageBox.Show("Export ข้อมูลสำเร็จ.", "Complete", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        private void Clean(Microsoft.Office.Interop.Excel.Worksheet excelWorkSheet)
        {
            for (int rowCount = 0; rowCount < 8; rowCount++)
            {
                var _rowCount = (rowCount * 2) + 11;
                PutDataintoCell("A" + _rowCount, "");
                PutDataintoCell("B" + _rowCount, "");
                PutDataintoCell("D" + _rowCount, "");
                PutDataintoCell("E" + _rowCount, "");
                PutDataintoCell("F" + _rowCount, "");
                PutDataintoCell("G" + _rowCount, "");
                PutDataintoCell("I" + _rowCount, "");
                PutDataintoCell("J" + _rowCount, "");
                PutDataintoCell("K" + _rowCount, "");
                PutDataintoCell("L" + _rowCount, "");
                PutDataintoCell("M" + _rowCount, "");
                PutDataintoCell("O" + _rowCount, "");
                PutDataintoCell("P" + _rowCount, "");
                PutDataintoCell("R" + _rowCount, "");
                PutDataintoCell("E" + _rowCount + 1, "");
                PutDataintoCell("Q" + _rowCount + 1, "");
                PutDataintoCell("AB" + _rowCount, "");
                PutDataintoCell("AE" + _rowCount, "");
                PutDataintoCell("AF" + _rowCount, "");
                PutDataintoCell("AR" + _rowCount, "");
                PutDataintoCell("BC" + _rowCount, "");

            }
        }

        private void PutDataintoCell(string strCell, string value)
        {
            excelRange = excelWorkSheet.Range[strCell];
            excelWorkSheet.Range[strCell].Value = value;
        }

        private string MonthThai(int month)
        {
            var _month = month == 12 ? 1 : month + 1;
            switch (_month)
            {

                case 1:
                    return "มกราคม";
                case 2:
                    return "กุมภาพันธ์";
                case 3:
                    return "มีนาคม";
                case 4:
                    return "มีนาคม";
                case 5:
                    return "พฤษภาคม";
                case 6:
                    return "มิถุนายน";
                case 7:
                    return "กรกฎาคม";
                case 8:
                    return "สิงหาคม";
                case 9:
                    return "กันยายน";
                case 10:
                    return "ตุลาคม";
                case 11:
                    return "พฤศจิกายน";
                case 12:
                    return "ธันวาคม";
                default:
                    return "Nothing";
            }
        }
    }
}
