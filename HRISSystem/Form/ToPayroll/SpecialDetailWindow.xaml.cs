﻿using DomainModelHris;
using DomainModelPayroll;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Form.ToPayroll
{
    /// <summary>
    /// Interaction logic for SpecialDetailWindow.xaml
    /// </summary>
    public partial class SpecialDetailWindow : Window
    {
        List<sp_GetSummarySupportByDateAndEmployee_Result> summaryList;
        Lot_Number lot;
        public SpecialDetailWindow()
        {
            InitializeComponent();
        }
        public SpecialDetailWindow(Lot_Number _lot, List<sp_GetSummarySupportByDateAndEmployee_Result> _summaryList)
        {
            InitializeComponent();
            summaryList = new List<sp_GetSummarySupportByDateAndEmployee_Result>();
            summaryList = _summaryList;
            lot = new Lot_Number();
            lot = _lot;
            LotTextbox.Text = string.Format("{0}/{1}", lot.Lot_Month, lot.Lot_Year);
            SearchFromDatePicker.SelectedDate = lot.Start_date;
            SearchToDatePicker.SelectedDate = lot.Finish_date;
            var staffList = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
            staffList.Insert(0, new StaffType { StaffTypeID = 0, Description = "ทั้งหมด" });
            StaffTypeCombobox.ItemsSource = staffList;
            StaffTypeCombobox.SelectedIndex = 0;
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                var staff = (StaffType)StaffTypeCombobox.SelectedItem;
                var staffID = staff.StaffTypeID.ToString();
                var fromDate = SearchFromDatePicker.SelectedDate;
                var toDate = SearchToDatePicker.SelectedDate;
                if (LotTextbox.Text == "") throw new Exception("กรุณาระบุงวด");
                if (fromDate == null || toDate == null) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
                if (fromDate > toDate) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");

                summaryList = summaryList.Count <= 0 ? BusinessLayerServices.PayrollBL().sp_GetSummarySupportByDate(fromDate.Value, toDate.Value) : summaryList;

                var _itemSource = new List<sp_GetSummarySupportByDateAndEmployee_Result>();

                if (staffID == "0") _itemSource = summaryList;
                else _itemSource = summaryList.Where(w => w.Staff_type == staffID).ToList();

                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = _itemSource;

                Summary();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Summary()
        {
            try
            {
                var dataGridItems = InformationDataGrid.Items;
                var _itemSource = new List<sp_GetSummarySupportByDateAndEmployee_Result>();
                foreach (var item in dataGridItems)
                {
                    _itemSource.Add((sp_GetSummarySupportByDateAndEmployee_Result)item);
                }

                var countSupport1 = _itemSource.Sum(s => s.Lunch_1);
                var countSupport2 = _itemSource.Sum(s => s.Lunch_2);
                short supportLunch = 25;
                var sumSupportLunch1 = (countSupport1 * supportLunch);
                var sumSupportLunch2 = (countSupport2 * supportLunch);
                var countNight = summaryList.Sum(s => s.Night);
                short supportNight = 20;
                var sumSupportNight = countNight * supportNight;
                TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", _itemSource.Count);
                SumNightTextBox.Text = string.Format("{0:n2}", sumSupportNight);
                SumLunch1TextBox.Text = string.Format("{0:n2}", sumSupportLunch1);
                SumLunch2TextBox.Text = string.Format("{0:n2}", sumSupportLunch2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void StaffTypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            Filter();
        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var item = (sp_GetSummarySupportByDateAndEmployee_Result)InformationDataGrid.SelectedItem;
                if (item != null)
                {
                    var w = new SpecialDetailPersonWindow(lot, item);
                    w.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FilterTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            Filter();
        }

        private void Filter()
        {
            try
            {
                var staff = (StaffType)StaffTypeCombobox.SelectedItem;
                var staffID = staff.StaffTypeID.ToString();
                var filter = FilterTextBox.Text.Trim();
                if (summaryList == null || summaryList.Count == 0) return;

                var _itemSource = filter == "" ? summaryList : summaryList.Where(w => w.FirstNameTH.Contains(filter)).ToList();
                if (staffID != "0") _itemSource = _itemSource.Where(w => w.Staff_type == staffID).ToList();

                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = _itemSource;

                Summary();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
