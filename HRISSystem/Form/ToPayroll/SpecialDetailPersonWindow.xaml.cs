﻿using DomainModelHris;
using DomainModelPayroll;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISSystem.Form.ToPayroll
{
    /// <summary>
    /// Interaction logic for SpecialDetailPersonWindow.xaml
    /// </summary>
    public partial class SpecialDetailPersonWindow : Window
    {
        Lot_Number lot;
        sp_GetSummarySupportByDateAndEmployee_Result item;
        public SpecialDetailPersonWindow()
        {
            InitializeComponent();
        }

        public SpecialDetailPersonWindow(Lot_Number _lot, sp_GetSummarySupportByDateAndEmployee_Result _item)
        {
            InitializeComponent();
            lot = new Lot_Number();
            lot = _lot;
            LotTextbox.Text = string.Format("{0}/{1}", lot.Lot_Month, lot.Lot_Year);
            SearchFromDatePicker.SelectedDate = lot.Start_date;
            SearchToDatePicker.SelectedDate = lot.Finish_date;
            item = new sp_GetSummarySupportByDateAndEmployee_Result();
            item = _item;
            EmployeeidTextBox.Text = _item.Employee_ID;
            NameTextBox.Text = string.Format("{0} {1} {2}", _item.TitleNameTH, _item.FirstNameTH, _item.LastNameTH);
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                var fromDate = SearchFromDatePicker.SelectedDate;
                var toDate = SearchToDatePicker.SelectedDate;
                if (LotTextbox.Text == "") throw new Exception("กรุณาระบุงวด");
                if (fromDate == null || toDate == null) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
                if (fromDate > toDate) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");

                var employee = BusinessLayerServices.EmployeeBL().GetEmployeeByEmployeeID(EmployeeidTextBox.Text);
                var fingerID = Convert.ToInt32(employee.FingerScanID);

                var supportList = BusinessLayerServices.PayrollBL().sp_GetSupportByDateAndEmployee(fromDate.Value, toDate.Value, fingerID);
                var countSupport1 = supportList.Sum(s => s.LunchSup_1);
                var countSupport2 = supportList.Sum(s => s.LunchSup_2);
                short supportLunch = 25;
                var sumSupportLunch1 = (countSupport1 * supportLunch);
                var sumSupportLunch2 = (countSupport2 * supportLunch);
                var countNight = supportList.Sum(s => s.Night);
                short supportNight = 20;
                var sumSupportNight = countNight * supportNight;

                InformationDataGrid.ItemsSource = supportList;

                TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", supportList.Count);
                SumNightTextBox.Text = string.Format("{0:n2}", sumSupportNight);
                SumLunch1TextBox.Text = string.Format("{0:n2}", sumSupportLunch1);
                SumLunch2TextBox.Text = string.Format("{0:n2}", sumSupportLunch2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }
    }
}
