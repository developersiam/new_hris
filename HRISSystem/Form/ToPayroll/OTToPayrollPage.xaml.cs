﻿using DomainModelPayroll;
using HRISSystem.Shared;
using HRISSystem.ViewModel;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace HRISSystem.Form.ToPayroll
{
    /// <summary>
    /// Interaction logic for OTToPayrollPage.xaml
    /// </summary>
    public partial class OTToPayrollPage : Page
    {
        List<OTViewModel> otView = new List<OTViewModel>();
        public OTToPayrollPage()
        {
            InitializeComponent();
            BindCombobox();
        }

        private void BindCombobox()
        {
            try
            {
                SearchLotCombobox.ItemsSource = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchLotCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var selected = (Lot_Number)SearchLotCombobox.SelectedItem;
                if (selected != null)
                {
                    SearchFromDatePicker.SelectedDate = selected.Start_date;
                    SearchFromDatePicker.DisplayDateStart = selected.Start_date;
                    SearchFromDatePicker.DisplayDateEnd = selected.Finish_date;
                    SearchToDatePicker.SelectedDate = selected.Finish_date;
                    SearchToDatePicker.DisplayDateStart = selected.Start_date;
                    SearchToDatePicker.DisplayDateEnd = selected.Finish_date;
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                var selectedLot = (Lot_Number)SearchLotCombobox.SelectedItem;
                if (selectedLot == null) { SendButton.IsEnabled = false; ReportButton.IsEnabled = false; throw new Exception("กรุณาระบุงวด"); }
                if (SearchFromDatePicker.SelectedDate == null && SearchToDatePicker.SelectedDate == null) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
                if (SearchFromDatePicker.SelectedDate > SearchToDatePicker.SelectedDate) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");

                var otList = BusinessLayerServices.OTBL().GetFilterdOTList("", SearchFromDatePicker.SelectedDate.Value, SearchToDatePicker.SelectedDate.Value);
                var otEmployee = otList.GroupBy(g => new { g.Employee_ID, g.Employee.Noted }).Select(s => new OTViewModel { Employee_ID = s.Key.Employee_ID, AccCode = s.Key.Noted}).ToList();

                otView.Clear();

                foreach (var item in otEmployee)
                {
                    var _otList = otList.Where(w => w.Employee_ID == item.Employee_ID);
                    var _otData = _otList.FirstOrDefault();
                    otView.Add(new OTViewModel
                    {
                        Employee_ID = item.AccCode,
                        FingerScanID = _otData.Employee.FingerScanID,
                        TitleNameTH = _otData.Employee.Person.TitleName.TitleNameTH,
                        FirstNameTH = _otData.Employee.Person.FirstNameTH,
                        LastNameTH = _otData.Employee.Person.LastNameTH,
                        OTDATE = _otData.OTDate,
                        OTnormal = _otList.Where(e => e.OTType == "N").Sum(e => e.NcountOT),
                        Holiday = _otList.Where(e => e.OTType == "H").Sum(e => e.NcountOT),
                        OTHoliday = _otList.Where(e => e.OTType == "S").Sum(e => e.NcountOT),
                        OTSpecial = _otList.Where(e => e.OTType == "SP").Sum(e => e.NcountOT),
                        Remark = _otData.Remark == null ? "" : _otData.Remark,
                        PayrollLot = _otData.PayrollLot,
                        OTType = _otData.OTType,
                        RequestTypeName = _otData.OTRequestType.RequestFlagName,
                        RequestType = _otData.RequestType,
                        AdminApproveStatus = _otData.AdminApproveStatus,
                        ManagerApproveStatus = _otData.ManagerApproveStatus,
                        FinalApproverStatus = _otData.FinalApproverStatus,
                        HRApproveStatus = _otData.HRApproveStatus
                    });
                }

                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = otView;
                SendButton.Content = string.Format("Send Data ({0})", otView.Count);

                ReportButton.IsEnabled = true;
                if (selectedLot.Lock_Acc == "2" || selectedLot.Lock_Acc_Labor == "2")
                {
                    SendButton.IsEnabled = false; throw new Exception("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้ว ไม่สามารถแก้ไขงวดนี้ได้");
                }
                else SendButton.IsEnabled = true;
                //SendButton.IsEnabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var selectedLot = (Lot_Number)SearchLotCombobox.SelectedItem;
                    if (selectedLot == null) throw new Exception("กรุณาระบุงวด");
                    if (SearchFromDatePicker.SelectedDate == null && SearchToDatePicker.SelectedDate == null) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
                    if (SearchFromDatePicker.SelectedDate > SearchToDatePicker.SelectedDate) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");

                    // Remove old data
                    var otDetailListByLot = BusinessLayerServices.PayrollBL().GetOTDetailByLot(selectedLot.Lot_Month, selectedLot.Lot_Year);
                    var oldItemList = new List<OT_Detail>();
                    if (otDetailListByLot.Any())
                    {
                        foreach (var oldItem in otDetailListByLot)
                        {
                            BusinessLayerServices.PayrollBL().DeleteOTDetail(oldItem);
                            oldItemList.Add(oldItem);
                        }
                        DataCorrectorStatic<OT_Detail>.AddTransactionList("D", "To Payroll OT", oldItemList, null);
                    }

                    // Add new data
                    var newItemList = new List<OT_Detail>();
                    foreach (var item in otView)
                    {
                        var newItem = new OT_Detail
                        {
                            Lot_Month = selectedLot.Lot_Month,
                            Lot_Year = selectedLot.Lot_Year,
                            Employee_id = item.Employee_ID,
                            Ot_hour = Convert.ToDouble(item.OTnormal),
                            Ot_holiday = Convert.ToDouble(item.Holiday),
                            Ot_hour_holiday = Convert.ToDouble(item.OTHoliday),
                            Username = SingletonConfiguration.getInstance().Username,
                            Ddate = DateTime.Now,
                            Ot_Special = Convert.ToDouble(item.OTSpecial)
                        };
                        BusinessLayerServices.PayrollBL().AddOTDetail(newItem);
                        newItemList.Add(newItem);
                    }
                    DataCorrectorStatic<OT_Detail>.AddTransactionList("A", "To Payroll OT", null, newItemList);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReportButton_Click(object sender, RoutedEventArgs e)
        {
            var lotNumber = (Lot_Number)SearchLotCombobox.SelectedItem;
            if (lotNumber != null)
            {
                Report.HR013Report report = new Report.HR013Report(lotNumber.Lot_Month, lotNumber.Lot_Year);
                report.ShowDialog();
            }
        }
    }
}
