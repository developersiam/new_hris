﻿using DomainModelPayroll;
using HRISSystem.Shared;
using HRISSystem.ViewModel;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace HRISSystem.Form.ToPayroll
{
    /// <summary>
    /// Interaction logic for HRLotSetupPage.xaml
    /// </summary>
    public partial class HRLotSetupPage : Page
    {
        public HRLotSetupPage()
        {
            try
            {
                InitializeComponent();
                BindCombobox();
                ReloadDatagrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindCombobox()
        {
            try
            {
                List<LockStatusViewModel> lockStatusList = new List<LockStatusViewModel>();
                lockStatusList.Add(new LockStatusViewModel { StatusName = "Lock", StatusValue = "2" });
                lockStatusList.Add(new LockStatusViewModel { StatusName = "Unlock", StatusValue = "1" });
                StaffLockStatusCombobox.ItemsSource = lockStatusList;
                LabourLockStatusCombobox.ItemsSource = lockStatusList;
                LotCombobox.ItemsSource = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadDatagrid()
        {
            try
            {
                var lotList = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot());
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = lotList;
                TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", lotList.Count.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (e.ChangedButton == MouseButton.Left)
                {
                    LotNumberViewModel selected = (LotNumberViewModel)InformationDataGrid.SelectedItem;
                    if (selected != null)
                    {
                        bool payrollLocked = selected.Lock_Acc == "2" || selected.Lock_Acc_Labor == "2";
                        LotCombobox.SelectedValue = selected.LotNumber;
                        StaffLockStatusCombobox.SelectedValue = selected.Lock_Hr == "2" ? "2" : "1";
                        LabourLockStatusCombobox.SelectedValue = selected.Lock_Hr_Labor == "2" ? "2" : "1";
                        UpdateButton.IsEnabled = !payrollLocked;
                        if (payrollLocked) MessageBox.Show("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้ว ไม่สามารถแก้ไขงวดนี้ได้", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    LotNumberViewModel _lotNumber = (LotNumberViewModel)LotCombobox.SelectedItem;
                    if (_lotNumber == null) throw new Exception("กรุณาระบุงวด");
                    if (_lotNumber.Lock_Acc == "2" || _lotNumber.Lock_Acc_Labor == "2") throw new Exception("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถแก้ไขงวดนี้ได้");
                    if (StaffLockStatusCombobox.Text == "" || LabourLockStatusCombobox.Text == "") throw new Exception("กรุณาระบุสถานะข้อมูล");

                    var oldItem = BusinessLayerServices.PayrollBL().GetLotByDateRange(_lotNumber.Start_date.Value, _lotNumber.Finish_date.Value).FirstOrDefault();
                    var newItem = BusinessLayerServices.PayrollBL().GetLotByDateRange(_lotNumber.Start_date.Value, _lotNumber.Finish_date.Value).FirstOrDefault();
                    newItem.Lock_Hr = StaffLockStatusCombobox.SelectedValue.ToString();
                    newItem.Lock_Hr_Labor = LabourLockStatusCombobox.SelectedValue.ToString();
                    BusinessLayerServices.PayrollBL().UpdateLotnumber(newItem);
                    DataCorrectorStatic<Lot_Number>.AddTransaction("U", "HR Lot Number", oldItem, newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LotCombobox.SelectedIndex = -1;
                StaffLockStatusCombobox.SelectedIndex = -1;
                LabourLockStatusCombobox.SelectedIndex = -1;
                UpdateButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
