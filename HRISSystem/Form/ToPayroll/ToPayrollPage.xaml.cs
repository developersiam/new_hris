﻿using DomainModelHris;
using DomainModelPayroll;
using HRISSystem.Helper;
using HRISSystem.Shared;
using HRISSystem.ViewModel;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HRISSystem.Form.ToPayroll
{
    /// <summary>
    /// Interaction logic for ToPayrollPage.xaml
    /// </summary>
    public partial class ToPayrollPage : Page
    {
        List<sp_GetWorkingDateToPayroll_Result> workList = new List<sp_GetWorkingDateToPayroll_Result>();
        List<OTViewModel> otView = new List<OTViewModel>();
        List<OT_Detail> existingList = new List<OT_Detail>();
        List<sp_GetSummarySupportByDateAndEmployee_Result> summaryList;

        public ToPayrollPage()
        {
            InitializeComponent();
            BindCombobox();

            summaryList = new List<sp_GetSummarySupportByDateAndEmployee_Result>();
            var deptCode = SingletonConfiguration.getInstance().DepartmentCode.ToUpper();
            SendButton.Visibility = (deptCode == "IT" || deptCode == "HRM") ? Visibility.Visible : Visibility.Collapsed;
            ReCalculateButton.Visibility = (deptCode == "IT" || deptCode == "HRM") ? Visibility.Visible : Visibility.Collapsed;
        }

        private void BindCombobox()
        {
            try
            {
                SearchLotCombobox.ItemsSource = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);

                var staffList = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
                staffList.Insert(0, new StaffType { StaffTypeID = 0, Description = "ทั้งหมด" });
                StaffTypeCombobox.ItemsSource = staffList;
                StaffTypeCombobox.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchLotCombobox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                var selected = (Lot_Number)SearchLotCombobox.SelectedItem;
                if (selected != null)
                {
                    StaffTypeCombobox.SelectedIndex = 0;
                    SearchFromDatePicker.SelectedDate = selected.Start_date;
                    SearchFromDatePicker.DisplayDateStart = selected.Start_date;
                    SearchFromDatePicker.DisplayDateEnd = selected.Finish_date;
                    SearchFromDatePicker.IsEnabled = false;
                    SearchToDatePicker.SelectedDate = selected.Finish_date;
                    SearchToDatePicker.DisplayDateStart = selected.Start_date;
                    SearchToDatePicker.DisplayDateEnd = selected.Finish_date;
                    SearchToDatePicker.IsEnabled = false;
                    ReloadDatagrid("");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid("");
        }

        private void ReCalculateButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid("Recalculate");
        }

        private void ReloadDatagrid(string mode)
        {
            try
            {
                var username = SingletonConfiguration.getInstance().Username.ToLower();
                var deptCode = SingletonConfiguration.getInstance().DepartmentCode.ToUpper();
                var fromDate = SearchFromDatePicker.SelectedDate;
                var toDate = SearchToDatePicker.SelectedDate;
                var lot = (Lot_Number)SearchLotCombobox.SelectedItem;
                if (lot == null) throw new Exception("กรุณาระบุงวด");
                if (fromDate == null || toDate == null) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
                if (fromDate > toDate) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
                StaffTypeCombobox.SelectedIndex = 0;

                ReloadWorkingDatagrid(fromDate.Value, toDate.Value, lot, mode);
                ReloadOTDatagrid(fromDate.Value, toDate.Value, lot, mode);
                ReloadSpecial(fromDate.Value, toDate.Value, lot, mode);

                bool payrollLocked = lot.Lock_Acc == "2" || lot.Lock_Acc_Labor == "2";
                SendButton.IsEnabled = !payrollLocked && (deptCode == "IT" || username == "pitchayapa");
                if (payrollLocked) throw new Exception("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้ว ไม่สามารถแก้ไขงวดนี้ได้");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadSpecial(DateTime fromDate, DateTime toDate, Lot_Number lot, string mode)
        {
            SumLunchSupportTextBox.Text = "";
            SumNightSupportTextBox.Text = "";
            summaryList.Clear();

            if (mode == "Recalculate") summaryList = BusinessLayerServices.PayrollBL().sp_GetSummarySupportByDate(fromDate, toDate);
            else
            {
                var existingList = BusinessLayerServices.PayrollBL().GetListSupportDetailByLot(lot.Lot_Month, lot.Lot_Year);
                if (existingList.Any()) summaryList = HRISService.DataCorrector().ConvertTo_sp_GetSummarySupport(existingList);
                else summaryList = BusinessLayerServices.PayrollBL().sp_GetSummarySupportByDate(fromDate, toDate);
            }

            //Lot 10.1/2022
            //summaryList = summaryList.Where(w => w.Staff_type != "1").ToList();

            short supportLunch = 25;
            short supportNight = 20;
            var countSupport1 = summaryList.Sum(s => s.Lunch_1).GetValueOrDefault();
            var countSupport2 = summaryList.Sum(s => s.Lunch_2).GetValueOrDefault();
            var countNight = summaryList.Sum(s => s.Night).GetValueOrDefault(); ;
            var sumSupportLunch = (countSupport1 * supportLunch) + (countSupport2 * supportLunch);
            var sumSupportNight = countNight * supportNight;
            SumLunchSupportTextBox.Text = string.Format("{0:n2}", sumSupportLunch);
            SumNightSupportTextBox.Text = string.Format("{0:n2}", sumSupportNight);
        }

        private void ReloadWorkingDatagrid(DateTime fromDate, DateTime toDate, Lot_Number lot, string mode)
        {
            workList.Clear();
            if (mode == "Recalculate")
                workList = BusinessLayerServices.PayrollBL()
                    .GetWorkingDateToPayroll(fromDate, toDate);
            else
            {
                var existingList = BusinessLayerServices.PayrollBL()
                    .GetWorkDateByLot(lot.Lot_Month, lot.Lot_Year);
                if (existingList.Any())
                    workList = HRISService.DataCorrector()
                        .ConvertToPayrollWorkdate(existingList);
                else workList = BusinessLayerServices.PayrollBL()
                        .GetWorkingDateToPayroll(fromDate, toDate);
                //ReCalculateButton.IsEnabled = existingList.Any();
            }

            //Lot 10.1/2022
            //workList = workList.Where(w => w.Staff_type != "1").ToList();

            WorkingInformationDataGrid.ItemsSource = null;
            WorkingInformationDataGrid.ItemsSource = workList;
            SumWorkdateTextBox.Text = workList.Where(w => w.Staff_type != "4").ToList().Count.ToString();
            SumTemporaryWorkerTextBox.Text = workList.Where(w => w.Staff_type == "4").ToList().Count.ToString();
            TotalWorkingTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", workList.Count);
            WorkingReportButton.IsEnabled = workList.Any();
        }

        private void ReloadOTDatagrid(DateTime fromDate, DateTime toDate, Lot_Number lot, string mode)
        {
            otView.Clear();

            if (mode == "Recalculate")
            {
                otView = GetNewOTList(fromDate, toDate);
            }
            else
            {
                var existingList = BusinessLayerServices.PayrollBL().GetOTDetailByLot(lot.Lot_Month, lot.Lot_Year);
                var test1 = existingList.Where(x => x.Employee_id == "230233").ToList();
                var test4 = existingList.Where(x => x.Employee_id == "230234").ToList();
                var test2 = existingList.Where(x => x.Employee_id == "220031").ToList();
                var test3 = existingList.Where(x => x.Employee_id == "220036").ToList();
                if (existingList.Any())
                    otView = HRISService.DataCorrector()
                        .ConvertToPayrollOT(existingList);
                else
                    otView = GetNewOTList(fromDate, toDate);
                //ReCalculateButton.IsEnabled = existingList.Any();
            }

            //Lot 10.1/2022
            //otView = otView.Where(w => w.Stafftype != 1).ToList();

            otView = otView.OrderBy(o => o.DEPT_CODE).ToList();

            OTInformationDataGrid.ItemsSource = null;
            OTInformationDataGrid.ItemsSource = otView;
            SumOTAmountTextBox.Text = otView.Count.ToString();
            TotalOTTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", otView.Count);
            SumOTTextBox.Text = string.Format("{0:0.###}", otView.Sum(s => s.OTnormal));
            SumHolidayTextBox.Text = string.Format("{0:0.###}", otView.Sum(s => s.Holiday));
            SumOTHHTextBox.Text = string.Format("{0:0.###}", otView.Sum(s => s.OTHoliday));
            SumWeekendTextBox.Text = string.Format("{0:0.###}", otView.Sum(s => s.OTWeekend));
            SumOTSpecialTextBox.Text = string.Format("{0:0.###}", otView.Sum(s => s.OTSpecial));
            OTReportButton.IsEnabled = otView.Any();
        }

        private List<OTViewModel> GetNewOTList(DateTime fromDate, DateTime toDate)
        {
            var result = new List<OTViewModel>();
            //var otList = BusinessLayerServices.OTBL().GetFilterdOTList("", fromDate, toDate).Where(w => w.Employee.Staff_type != 4).ToList();
            var otList = BusinessLayerServices.OTBL().GetFilterdOTList("", fromDate, toDate).ToList();
            //var notPayrollList = BusinessLayerServices.PayrollFlagBL().GetPayrollFlagListByDate(fromDate, toDate);
            //foreach (var notPayroll in notPayrollList)
            //{
            //    otList.RemoveAll(s => s.Employee_ID == notPayroll.Employee_ID && s.OTDate == notPayroll.ValidFrom);
            //}
            var otEmployee = otList.GroupBy(g => new { g.Employee_ID, g.Employee.Noted, g.Employee.Staff_type }).Select(s => new OTViewModel { Employee_ID = s.Key.Employee_ID, AccCode = s.Key.Noted, Stafftype = s.Key.Staff_type }).ToList();

            foreach (var item in otEmployee)
            {
                var _otList = otList.Where(w => w.Employee_ID == item.Employee_ID);
                var _otData = _otList.FirstOrDefault();

                var _holiday = _otList.Where(e => e.OTType == "H").Sum(e => e.NcountOT); // All holiday type
                var _weekendHoliday = _otList.Where(e => e.Employee.Staff_type != 1 && e.OTType == "H" && e.OTDate.DayOfWeek == DayOfWeek.Sunday).Sum(e => e.NcountOT); // Only labour's sunday working

                if (_holiday > 0)
                {

                }

                _holiday = _holiday - _weekendHoliday;

                result.Add(new OTViewModel
                {
                    AccCode = item.AccCode, //HR Employee ID
                    DEPT_CODE = _otData.Employee.DeptCode,
                    Employee_ID = item.Employee_ID, //AccCode
                    FingerScanID = _otData.Employee.FingerScanID,
                    TitleNameTH = _otData.Employee.Person.TitleName.TitleNameTH,
                    FirstNameTH = _otData.Employee.Person.FirstNameTH,
                    LastNameTH = _otData.Employee.Person.LastNameTH,
                    OTDATE = _otData.OTDate,
                    OTnormal = _otList.Where(e => e.OTType == "N").Sum(e => e.NcountOT),
                    Holiday = _holiday,
                    OTHoliday = _otList.Where(e => e.OTType == "S").Sum(e => e.NcountOT),
                    OTWeekend = _weekendHoliday,
                    OTSpecial = _otList.Where(e => e.OTType == "SP").Sum(e => e.NcountOT),
                    Remark = _otData.Remark == null ? "" : _otData.Remark,
                    PayrollLot = _otData.PayrollLot,
                    OTType = _otData.OTType,
                    RequestTypeName = _otData.OTRequestType.RequestFlagName,
                    RequestType = _otData.RequestType,
                    AdminApproveStatus = _otData.AdminApproveStatus,
                    ManagerApproveStatus = _otData.ManagerApproveStatus,
                    FinalApproverStatus = _otData.FinalApproverStatus,
                    HRApproveStatus = _otData.HRApproveStatus,
                    //Stafftype = BusinessLayerServices.EmployeeBL().GetEmployeeByEmployeeID(item.Employee_ID).Staff_type.Value
                    Stafftype = item.Stafftype
                });
            }
            var sumH = result.Sum(s => s.Holiday);
            var sumW = result.Sum(s => s.OTWeekend);
            return result;
        }

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                WorkingInformationDataGrid.ItemsSource = null;
                WorkingInformationDataGrid.ItemsSource = workList;
                OTInformationDataGrid.ItemsSource = null;
                OTInformationDataGrid.ItemsSource = otView;
                StaffTypeCombobox.SelectedIndex = 0;

                if (MessageBoxHelper.Question("ต้องการบันทึกข้อมูลหรือไม่") == MessageBoxResult.No)
                    return;

                var selectedLot = (Lot_Number)SearchLotCombobox.SelectedItem;
                if (selectedLot == null)
                    throw new Exception("กรุณาระบุงวด");
                if (SearchFromDatePicker.SelectedDate == null &&
                    SearchToDatePicker.SelectedDate == null)
                    throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
                if (SearchFromDatePicker.SelectedDate > SearchToDatePicker.SelectedDate)
                    throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");

                if (MessageBoxHelper.Question("ยืนยันการตรวจสอบความถูกต้องของ จำนวนและประเภท OT พร้อมที่จะบันทึกข้อมูลแล้วหรือไม่")
                    == MessageBoxResult.Yes)
                    SendOT(selectedLot);
                if (MessageBoxHelper.Question("ยืนยันการตรวจสอบความถูกต้องของ จำนวนวันทำงานของพนักงานรายวันทุกประเภท พร้อมที่จะบันทึกข้อมูลแล้วหรือไม่")
                    == MessageBoxResult.Yes)
                    SendWorking(selectedLot);
                if (MessageBoxHelper.Question("ยืนยันการตรวจสอบความถูกต้องของ เงินจูงใจและเงินสมทบค่าอาหาร พร้อมที่จะบันทึกข้อมูลแล้วหรือไม่")
                    == MessageBoxResult.Yes)
                    SendSpecial(selectedLot);

                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                ReloadDatagrid("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SendOT(Lot_Number selectedLot)
        {
            // Remove old data
            var otDetailListByLot = BusinessLayerServices.PayrollBL().GetOTDetailByLot(selectedLot.Lot_Month, selectedLot.Lot_Year);
            var oldItemList = new List<OT_Detail>();
            if (otDetailListByLot.Any())
            {
                foreach (var oldItem in otDetailListByLot)
                {
                    BusinessLayerServices.PayrollBL().DeleteOTDetail(oldItem);
                    oldItemList.Add(oldItem);
                }
                DataCorrectorStatic<OT_Detail>.AddTransactionList("D", "To Payroll OT", oldItemList, null);
            }

            // Add new data
            var newItemList = new List<OT_Detail>();
            foreach (var item in otView)
            {
                //var hol = SeparateHoliday(item);
                var newItem = new OT_Detail
                {
                    Lot_Month = selectedLot.Lot_Month,
                    Lot_Year = selectedLot.Lot_Year,
                    Employee_id = item.AccCode,
                    Ot_hour = Convert.ToDouble(item.OTnormal),
                    Ot_holiday = Convert.ToDouble(item.Holiday),
                    Ot_Weekend = Convert.ToDouble(item.OTWeekend),
                    Ot_hour_holiday = Convert.ToDouble(item.OTHoliday),
                    Ot_Special = Convert.ToDouble(item.OTSpecial),
                    Username = SingletonConfiguration.getInstance().Username,
                    Ddate = DateTime.Now
                };
                BusinessLayerServices.PayrollBL().AddOTDetail(newItem);
                newItemList.Add(newItem);
            }
            DataCorrectorStatic<OT_Detail>.AddTransactionList("A", "To Payroll OT", null, newItemList);
        }

        private OT_Detail SeparateHoliday(OTViewModel ot)
        {
            var otDetail = new OT_Detail();
            var fromDate = SearchFromDatePicker.SelectedDate.GetValueOrDefault();
            var toDate = SearchToDatePicker.SelectedDate.GetValueOrDefault();
            decimal _holiday = 0;
            decimal _weekend = 0;

            var holidayList = BusinessLayerServices.OTBL().GetPersonHolidayList(ot.Employee_ID, fromDate, toDate);

            foreach (var item in holidayList)
            {
                //2021-06-24 Separate Sunday and normal Holiday
                var dow = item.OTDate.DayOfWeek;
                _weekend += dow == DayOfWeek.Sunday ? item.NcountOT.Value : 0;
                _holiday += dow != DayOfWeek.Sunday ? item.NcountOT.Value : 0;
            }

            otDetail.Ot_holiday = Convert.ToDouble(_holiday);
            otDetail.Ot_Weekend = Convert.ToDouble(_weekend);

            return otDetail;
        }

        private void SendWorking(Lot_Number selectedLot)
        {
            // Remove old data
            var labourWorkListByLot = BusinessLayerServices.PayrollBL().GetWorkDateByLot(selectedLot.Lot_Month, selectedLot.Lot_Year);
            if (labourWorkListByLot.Any())
            {
                var oldItemList = new List<Labour_work>();
                foreach (var oldItem in labourWorkListByLot)
                {
                    BusinessLayerServices.PayrollBL().DeleteWorkDate(oldItem);
                    oldItemList.Add(oldItem);
                }
                DataCorrectorStatic<Labour_work>.AddTransactionList("D", "To Payroll Workdate", oldItemList, null);
            }

            // Add new data
            var newItemList = new List<Labour_work>();
            foreach (var item in workList)
            {
                var newItem = new Labour_work
                {
                    Lot_Month = selectedLot.Lot_Month,
                    Lot_Year = selectedLot.Lot_Year,
                    //Employee_id = item.AccCode,
                    Employee_id = item.PAYROLL_ID,
                    CWork = Convert.ToDouble(item.NWORKING_DATE),
                    Username = SingletonConfiguration.getInstance().Username,
                    Ddate = DateTime.Now
                };
                BusinessLayerServices.PayrollBL().AddWorkDate(newItem);
                newItemList.Add(newItem);
            }
            DataCorrectorStatic<Labour_work>.AddTransactionList("A", "To Payroll Workdate", null, newItemList);
        }

        private void SendSpecial(Lot_Number selectedLot)
        {
            // Remove old data
            var supportListByLot = BusinessLayerServices.PayrollBL().GetListSupportDetailByLot(selectedLot.Lot_Month, selectedLot.Lot_Year);
            if (supportListByLot.Any())
            {
                foreach (var oldItem in supportListByLot)
                {
                    BusinessLayerServices.PayrollBL().DeleteSupportDetail(oldItem);
                }
            }

            // Add new data
            foreach (var item in summaryList)
            {
                var newItem = new Support_Detail
                {
                    Lot_Month = selectedLot.Lot_Month,
                    Lot_Year = selectedLot.Lot_Year,
                    //Employee_id = item.AccCode,
                    Employee_id = item.Employee_ID,
                    Night = Convert.ToInt16(item.Night.Value),
                    Lunch_1 = Convert.ToInt16(item.Lunch_1.Value),
                    Lunch_2 = Convert.ToInt16(item.Lunch_2.Value),
                    Username = SingletonConfiguration.getInstance().Username,
                    Ddate = DateTime.Now
                };
                BusinessLayerServices.PayrollBL().AddSupportDetail(newItem);
            }
        }

        private void OTReportButton_Click(object sender, RoutedEventArgs e)
        {
            ViewOTReport();
        }

        private void OTTextBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ViewOTReport();
        }

        private void ViewOTReport()
        {
            try
            {
                if (!otView.Any()) throw new Exception("ไม่พบข้อมูล, กรุณาตรวจสอบ");
                System.Windows.Forms.FolderBrowserDialog folderDlg = new System.Windows.Forms.FolderBrowserDialog();
                folderDlg.ShowNewFolderButton = true;

                // Show the FolderBrowserDialog.
                System.Windows.Forms.DialogResult result = folderDlg.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    string folderPath = HRISService.DataCorrector().ExportExcelToPayrollOT(folderDlg.SelectedPath, otView);
                    Environment.SpecialFolder root = folderDlg.RootFolder;
                    MessageBox.Show("Export complete.", "Complete", MessageBoxButton.OK, MessageBoxImage.Information);
                    System.Diagnostics.Process.Start(folderPath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void WorkingReportButton_Click(object sender, RoutedEventArgs e)
        {
            ViewWorkingReport();
        }

        private void WorkingTextBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ViewWorkingReport();
        }

        private void ViewWorkingReport()
        {
            try
            {
                var lot = (Lot_Number)SearchLotCombobox.SelectedItem;
                if (lot == null) throw new Exception("กรุณาระบุงวด");
                if (!workList.Any()) throw new Exception("ไม่พบข้อมูล, กรุณาตรวจสอบ");
                Report.ToPayroll_WoringDate w = new Report.ToPayroll_WoringDate(lot, workList);
                w.ShowDialog();

                //if (!workList.Any()) throw new Exception("ไม่พบข้อมูล, กรุณาตรวจสอบ");
                //System.Windows.Forms.FolderBrowserDialog folderDlg = new System.Windows.Forms.FolderBrowserDialog();
                //folderDlg.ShowNewFolderButton = true;

                //// Show the FolderBrowserDialog.
                //System.Windows.Forms.DialogResult result = folderDlg.ShowDialog();

                //if (result == System.Windows.Forms.DialogResult.OK)
                //{
                //    string folderPath = HRISService.DataCorrector().ExportExcelToPayrollWorkdate(folderDlg.SelectedPath, workList);
                //    Environment.SpecialFolder root = folderDlg.RootFolder;
                //    MessageBox.Show("Export complete.", "Complete", MessageBoxButton.OK, MessageBoxImage.Information);
                //    System.Diagnostics.Process.Start(folderPath);
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SpecialIncomTextBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ViewSpecialIncome();
        }

        private void ViewSpecialIncome()
        {
            try
            {
                var fromDate = SearchFromDatePicker.SelectedDate;
                var toDate = SearchToDatePicker.SelectedDate;
                var lot = (Lot_Number)SearchLotCombobox.SelectedItem;
                if (lot == null) throw new Exception("กรุณาระบุงวด");
                if (fromDate == null || toDate == null) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
                if (fromDate > toDate) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");

                var w = new SpecialDetailWindow(lot, summaryList);
                w.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void StaffTypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            var staffID = ((StaffType)StaffTypeCombobox.SelectedItem).StaffTypeID;

            var filterOTList = staffID == 0 ? otView : otView.Where(w => w.Stafftype == staffID);
            OTInformationDataGrid.ItemsSource = null;
            OTInformationDataGrid.ItemsSource = filterOTList;
            TotalOTTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", filterOTList.Count());

            var filterWorkingList = staffID == 0 ? workList : workList.Where(w => w.Staff_type == staffID.ToString());
            WorkingInformationDataGrid.ItemsSource = null;
            WorkingInformationDataGrid.ItemsSource = filterWorkingList;
            TotalWorkingTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", filterWorkingList.Count());
        }

        private void NWorkdateFilterTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (workList != null)
                {
                    var filterList = new List<sp_GetWorkingDateToPayroll_Result>();
                    if (NWorkingSearchTextBox.Text == "") filterList = workList;
                    else filterList = workList.Where(w => w.FNAME.StartsWith(NWorkingSearchTextBox.Text)).ToList();

                    WorkingInformationDataGrid.ItemsSource = null;
                    WorkingInformationDataGrid.ItemsSource = filterList;
                    TotalWorkingTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", filterList.Count);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void OTFilterTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (otView != null)
                {
                    var filterList = new List<OTViewModel>();
                    if (OTSearchTextBox.Text == "") filterList = otView;
                    else filterList = otView.Where(w => w.FirstNameTH.StartsWith(OTSearchTextBox.Text)).ToList();

                    OTInformationDataGrid.ItemsSource = null;
                    OTInformationDataGrid.ItemsSource = filterList;
                    TotalOTTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", filterList.Count);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
