﻿using DomainModelPayroll;
using HRISSystem.Shared;
using HRISSystem.ViewModel;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace HRISSystem.Form.ToPayroll
{
    /// <summary>
    /// Interaction logic for WorkingDateToParollPage.xaml
    /// </summary>
    public partial class WorkingDateToParollPage : Page
    {
        List<sp_GetWorkingDateToPayroll_Result> workList = new List<sp_GetWorkingDateToPayroll_Result>();
        public WorkingDateToParollPage()
        {
            InitializeComponent();
            BindCombobox();
        }

        private void BindCombobox()
        {
            try
            {
                SearchLotCombobox.ItemsSource = HRISService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchLotCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var selected = (Lot_Number)SearchLotCombobox.SelectedItem;
                if (selected != null)
                {
                    SearchFromDatePicker.SelectedDate = selected.Start_date;
                    SearchFromDatePicker.DisplayDateStart = selected.Start_date;
                    SearchFromDatePicker.DisplayDateEnd = selected.Finish_date;
                    SearchToDatePicker.SelectedDate = selected.Finish_date;
                    SearchToDatePicker.DisplayDateStart = selected.Start_date;
                    SearchToDatePicker.DisplayDateEnd = selected.Finish_date;
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                var selectedLot = (Lot_Number)SearchLotCombobox.SelectedItem;
                if (selectedLot == null) throw new Exception("กรุณาระบุงวด");
                if (SearchFromDatePicker.SelectedDate == null && SearchToDatePicker.SelectedDate == null) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
                if (SearchFromDatePicker.SelectedDate > SearchToDatePicker.SelectedDate) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");

                workList.Clear();
                workList = BusinessLayerServices.PayrollBL().GetWorkingDateToPayroll(SearchFromDatePicker.SelectedDate.Value, SearchToDatePicker.SelectedDate.Value);

                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = workList;
                SendButton.Content = string.Format("ทั้งหมด {0} รายการ", workList.Count);

                bool payrollLocked = selectedLot.Lock_Acc == "2" || selectedLot.Lock_Acc_Labor == "2";
                SendButton.IsEnabled = !payrollLocked;
                if (payrollLocked) throw new Exception("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้ว ไม่สามารถแก้ไขงวดนี้ได้");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var selectedLot = (Lot_Number)SearchLotCombobox.SelectedItem;
                    if (selectedLot == null) throw new Exception("กรุณาระบุงวด");
                    if (SearchFromDatePicker.SelectedDate == null && SearchToDatePicker.SelectedDate == null) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");
                    if (SearchFromDatePicker.SelectedDate > SearchToDatePicker.SelectedDate) throw new Exception("กรุณาระบุวันที่ให้ถูกต้อง");

                    // Remove old data
                    var labourWorkListByLot = BusinessLayerServices.PayrollBL().GetWorkDateByLot(selectedLot.Lot_Month, selectedLot.Lot_Year);
                    if (labourWorkListByLot.Any())
                    {
                        var oldItemList = new List<Labour_work>();
                        foreach (var oldItem in labourWorkListByLot)
                        {
                            BusinessLayerServices.PayrollBL().DeleteWorkDate(oldItem);
                            oldItemList.Add(oldItem);
                        }
                        DataCorrectorStatic<Labour_work>.AddTransactionList("D", "To Payroll Workdate", oldItemList, null);
                    }

                    // Add new data
                    var newItemList = new List<Labour_work>();
                    foreach (var item in workList)
                    {
                        var newItem = new Labour_work
                        {
                            Lot_Month = selectedLot.Lot_Month,
                            Lot_Year = selectedLot.Lot_Year,
                            //Employee_id = item.AccCode,
                            Employee_id = item.PAYROLL_ID,
                            CWork = Convert.ToDouble(item.NWORKING_DATE),
                            Username = SingletonConfiguration.getInstance().Username,
                            Ddate = DateTime.Now
                        };
                        BusinessLayerServices.PayrollBL().AddWorkDate(newItem);
                        newItemList.Add(newItem);
                    }
                    DataCorrectorStatic<Labour_work>.AddTransactionList("A", "To Payroll Workdate", null, newItemList);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReportButton_Click(object sender, RoutedEventArgs e)
        {
            var lotNumber = (Lot_Number)SearchLotCombobox.SelectedItem;
            if (lotNumber != null)
            {
                Report.HR013Report report = new Report.HR013Report(lotNumber.Lot_Month, lotNumber.Lot_Year);
                report.ShowDialog();
            }
        }
    }
}
