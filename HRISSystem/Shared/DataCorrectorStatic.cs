﻿using DomainModelHris;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace HRISSystem.Shared
{
    public static class DataCorrectorStatic<T>
    {
        static string XMLGenerate(T item)
        {
            if (item == null) return string.Empty;

            var stringWriter = new System.IO.StringWriter();
            var serializer = new XmlSerializer(typeof(T));
            var namespaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            var settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;
            namespaces.Add("", "");

            using (var writer = XmlWriter.Create(stringWriter, settings))
            {
                serializer.Serialize(writer, item, namespaces);
                return stringWriter.ToString();
            }
        }

        public static void AddTransaction(string TransactionType, string Noted, T oldItem, T newItem)
        {
            var transaction = new TransactionLog
            {
                TransactionLog_ID = BusinessLayerServices.TransactionBL().GetNewTransactionID(),
                TransactionType_ID = TransactionType,
                Table_ID = "T000",
                TransactionDate = DateTime.Now.Date,
                Noted = Noted,
                ModifiedDate = DateTime.Now,
                OldDataXML = TransactionType == "A" ? null : XMLGenerate(oldItem),
                NewDataXML = TransactionType == "D" ? null : XMLGenerate(newItem),
                ModifiedUser = SingletonConfiguration.getInstance().Username,
            };
            //var t = new TransactionLog();
            //t.TransactionLog_ID = BusinessLayerServices.TransactionBL().GetNewTransactionID();
            //t.TransactionType_ID = TransactionType;
            //t.Table_ID = "T000";
            //t.TransactionDate = DateTime.Now.Date;
            //t.Noted = Noted;
            //t.ModifiedDate = DateTime.Now;
            //t.OldDataXML = TransactionType == "A" ? null : XMLGenerate(oldItem);
            //t.NewDataXML = TransactionType == "D" ? null : XMLGenerate(newItem);
            //t.ModifiedUser = SingletonConfiguration.getInstance().Username;

            BusinessLayerServices.TransactionBL().AddTransaction(transaction);
        }

        static string XMLListGenerate(List<T> itemList)
        {
            if (!itemList.Any()) return string.Empty;
            string xmlresult = "";
            foreach (var item in itemList)
            {
                xmlresult += XMLGenerate(item) + Environment.NewLine;
            }

            return xmlresult;
        }

        public static void AddTransactionList(string TransactionType, string Noted, List<T> oldItemList, List<T> newItemList)
        {
            var transaction = new TransactionLog
            {
                TransactionLog_ID = BusinessLayerServices.TransactionBL().GetNewTransactionID(),
                TransactionType_ID = TransactionType,
                Table_ID = "T000",
                TransactionDate = DateTime.Now.Date,
                Noted = Noted,
                ModifiedDate = DateTime.Now,
                OldDataXML = TransactionType == "A" ? null : XMLListGenerate(oldItemList),
                NewDataXML = TransactionType == "D" ? null : XMLListGenerate(newItemList),
                ModifiedUser = SingletonConfiguration.getInstance().Username,
            };
            BusinessLayerServices.TransactionBL().AddTransaction(transaction);
        }
    }
}
