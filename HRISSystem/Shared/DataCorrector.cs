﻿using HRISSystem.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DomainModelPayroll;
using HRISSystemBL;
using DomainModelHris;
using System.IO;
using OfficeOpenXml;

namespace HRISSystem.Shared
{
    public interface IDataCorrector
    {
        bool NumericCheck(string value, int digit, int? digitRange);
        string FormatNumber(int decimalDigit, string inputString, bool Nullable);
        string FormatTime(string inputString);
        string OTRateCalculate(string income, string staffType);
        string OTHolidayCalculate(string income, string staffType);
        string OTHolidayRateCalculate(string income, string staffType);
        List<LotNumberViewModel> LotNumberList(List<Lot_Number> lotNumber);
        string FirstCapital(string inputString);
        List<sp_GetToPayrollByDepartment_Result> CovertPayrollToModel(List<Payroll> payrollList);
        List<PayrollViewModel> CovertToPayrollVM(List<Payroll> payrollList);
        List<PayrollViewModel> CovertToPayrollVM(List<sp_NEWGet_Result> payrollList, DateTime fromDate, DateTime toDate);
        List<string> GetLotMonthList();
        List<string> GetLotYearList();
        List<RptPR04ViewModel> ConvertToRPTPR04(List<Payroll> payrollList, string lotNumber);
        string ExportExcelPayrollStaffType(string folderName, string lotNumber, List<RptPR04ViewModel> payrollList);
        string ExportExcelPayrollEmployee(string folderName, List<sp_GetPayrollEmployee_Result> payrollList, bool isShowMoney);
        string ExportExcelPR007(string folderName, string lotNumber, List<sp_GetSSOReport_Result> ssoReportList);
        string ExportExcelToPayrollOT(string folderName, List<OTViewModel> otList);
        string ExportExcelToPayrollWorkdate(string folderName, List<sp_GetWorkingDateToPayroll_Result> workList);
        List<sp_GetWorkingDateToPayroll_Result> ConvertToPayrollWorkdate(List<Labour_work> workList);
        List<OTViewModel> ConvertToPayrollOT(List<OT_Detail> otDetail);
        string GetCurrentLot();
        List<sp_GetSummarySupportByDateAndEmployee_Result> ConvertTo_sp_GetSummarySupport(List<Support_Detail> supportList);
        List<Payroll> ConvertSummaryPRToPayroll(List<Payroll_Summary> summaryList);
    }
    public class DataCorrector : IDataCorrector
    {
        public string FormatNumber(int decimalDigit, string inputString, bool Nullable)
        {
            // Digit 0 for Integer type
            Regex regex = new Regex(@"^[0-9]+(\.[0-9]{1,6})?$");
            Match match = regex.Match(inputString);
            if (match.Success)
            {
                decimal x = decimal.Parse(inputString);
                x = Math.Round(x, decimalDigit);
                return x.ToString();
            }
            else if (Nullable) return null; 
            else return "0";
        }

        public string FormatTime(string inputString)
        {
            try
            {
                if (string.IsNullOrEmpty(inputString)) return "00:00:00";

                int hour = 0;
                int minute = 0;
                string[] splited;

                if (inputString.Length <= 2)
                {
                    if (!int.TryParse(inputString, out hour) || hour >= 24) return "00:00:00";
                }
                else
                {
                    inputString = inputString.Replace('.', ':');

                    if (inputString.IndexOf(":") > 0) splited = inputString.Split(':'); //Split with ':'
                    else return "00:00:00";

                    if (splited.Length > 0) int.TryParse(splited[0], out hour);
                    if (splited.Length > 1) int.TryParse(splited[1], out minute);
                }

                TimeSpan result = new TimeSpan(hour, minute, 0);
                return result.ToString();
            }
            catch (Exception)
            {
                return "00:00:00";
            }
        }

        public string OTRateCalculate(string income, string staffType)
        {
            if (string.IsNullOrEmpty(income)) return "";
            decimal _income = decimal.Parse(income);

            decimal result = 0;
            if (staffType == "1") result = decimal.Divide(_income, 30); // พนักงานประจำ
            else result = _income;
            result = decimal.Divide(result, 8);
            result = decimal.Multiply(result, decimal.Parse("1.5"));
            //result = Math.Round(result, 2);
            return Math.Round(result, 2, MidpointRounding.AwayFromZero).ToString();
            //return Math.Round(result, 2).ToString();
        }

        public string OTHolidayCalculate(string income, string staffType)
        {
            //if (string.IsNullOrEmpty(income)) return "";
            //decimal _income = decimal.Parse(income);

            //decimal result = 0;
            //if (staffType == "1") result = decimal.Divide(_income, 30); // พนักงานประจำ
            //else result = _income;
            //return Math.Round(result, 2).ToString();

            // 08/2020 Change Holiday rate from Daily --> Hourly
            //Permanent ((Salary / 30 days) / 8 hours) * 1
            //Temporary (Wage / 8 hours) * 2

            if (string.IsNullOrEmpty(income)) return "";
            decimal _income = decimal.Parse(income);

            decimal _Hourly = 0;
            if (staffType == "1") _Hourly = decimal.Divide(decimal.Divide(_income, 30), 8); // พนักงานประจำ
            else _Hourly = decimal.Divide(_income, 8);

            decimal result = 0;
            result = _Hourly;
            //if (staffType == "1") result = _Hourly; // พนักงานประจำ
            //else result = Math.Round(_Hourly, 2);

            return Math.Round(result, 2, MidpointRounding.AwayFromZero).ToString();
        }

        public string OTHolidayRateCalculate(string income, string staffType)
        {
            if (string.IsNullOrEmpty(income)) return "";
            decimal _income = decimal.Parse(income);

            decimal result = 0;
            if (staffType == "1") result = decimal.Divide(_income, 30); // พนักงานประจำ
            else result = _income;

            result = decimal.Divide(result, 8);
            result = decimal.Multiply(result, 3);

            return Math.Round(result, 2, MidpointRounding.AwayFromZero).ToString();
        }

        public List<LotNumberViewModel> LotNumberList(List<Lot_Number> lotNumber)
        {
            var result = new List<LotNumberViewModel>();
            foreach (var i in lotNumber)
            {
                result.Add(new LotNumberViewModel
                {
                    LotNumber = i.Lot_Month + "/" + i.Lot_Year,
                    Lot_Month = i.Lot_Month,
                    Lot_Year = i.Lot_Year,
                    Start_date = i.Start_date,
                    Finish_date = i.Finish_date,
                    Lock_Hr = i.Lock_Hr,
                    Lock_Hr_Bool = i.Lock_Hr == "2" ? true : false,
                    Lock_Acc = i.Lock_Acc,
                    Lock_Acc_Bool = i.Lock_Acc == "2" ? true : false,
                    Salary_Paid_date = i.Salary_Paid_date,
                    Ot_Paid_date = i.Ot_Paid_date,
                    Lock_Acc_Labor = i.Lock_Acc_Labor,
                    Lock_Acc_Labor_Bool = i.Lock_Acc_Labor == "2" ? true : false,
                    Lock_Hr_Labor = i.Lock_Hr_Labor,
                    Lock_Hr_Labor_Bool = i.Lock_Hr_Labor == "2" ? true : false,
                    LockedAll = i.LockedAll
                });
            }
            return result;
        }

        public bool NumericCheck(string value, int digit, int? digitRange)
        {
            string _digitRange = digitRange == null ? "" : "," + digitRange;
            string expression = @"^\d{"+ digit + _digitRange + "}$";

            //Regex regex = new Regex(@"^\d{0,13}$");
            Regex regex = new Regex(expression);
            Match match = regex.Match(value);
            return match.Success;
        }

        public string FirstCapital(string inputString)
        {
            inputString = inputString.ToLower();
            if (string.IsNullOrEmpty(inputString)) return string.Empty;
            // convert to char array of the string
            char[] letters = inputString.ToCharArray();
            // upper case the first char
            letters[0] = char.ToUpper(letters[0]);
            // return the array made of the new char array
            return new string(letters);
        }

        public List<sp_GetToPayrollByDepartment_Result> CovertPayrollToModel(List<Payroll> payrollList)
        {
            var result = new List<sp_GetToPayrollByDepartment_Result>();
            foreach (var p in payrollList)
            {
                var model = new sp_GetToPayrollByDepartment_Result();
                model.Employee_id = p.Employee_id;
                model.EmployeeID = p.EmployeeID;
                model.Id_card = p.Id_card;
                model.Bank_acc = p.Bank_acc;
                model.Dept_code = p.Dept_code;
                model.Tname = p.Tname;
                model.Fname = p.Fname;
                model.Lname = p.Lname;
                model.Staff_type = p.Staff_type;
                model.StaffType = p.Staff_type == "1" ? "ประจำ" : (p.Staff_type == "2" ? "รายวันประจำ" : "รายวัน");
                //Staff_status = "";
                model.NetSalary = p.Salary;
                model.Wage = p.Wage;
                model.Tax = p.Tax;
                model.CWork = p.Working_date;
                model.Ot_hour_rate = p.Ot_hour_rate;
                model.Ot_holiday_rate = p.Ot_holiday_rate;
                model.Ot_hour_holiday_rate = p.Ot_hour_holiday_rate;
                model.OT_Hour = (decimal?)p.Cot_hour;
                model.OT_Holiday = (decimal?)p.Cot_holiday;
                model.OT_Hour_Holiday = (decimal?)p.Cot_hour_holiday;
                //OT_Special;
                model.OTHour = p.Ot_hour_rate.Value * (decimal?)p.Cot_hour;
                model.OTHoliday = p.Ot_holiday_rate * (decimal?)p.Cot_holiday;
                model.OTHolidayHour = p.Ot_hour_holiday_rate * (decimal?)p.Cot_hour_holiday;
                model.Ot_special_rate = p.Ot_special_rate;
                model.Special = p.Special;
                model.Bonus = p.Bonus;
                model.TotalIncome = p.Total_Income;
                //PND = ;
                model.SSO = (int?)p.SSO;
                model.CutWage = p.Cut_wage;
                model.Student_Loan = p.Student_Loan;
                model.NetInCome = p.Net_Income.Value;
                result.Add(model);
            }
            return result;
        }

        public List<string> GetLotMonthList()
        {
            var result = new List<string>();
            for (int i = 1; i <= 12; i++)
            {
                result.Add(i.ToString("D2"));
            }
            return result;
        }

        public List<string> GetLotYearList()
        {
            var result = new List<string>();
            var currentYear = DateTime.Now.Year;
            for (int i = 0; i <= 24; i++)
            {
                result.Add(currentYear.ToString("D4"));
                currentYear--;
            }
            return result;
        }

        public string ExportExcelPayrollStaffType(string folderName, string lotNumber, List<RptPR04ViewModel> payrollList)
        {
            lotNumber = lotNumber.Replace("/", "-");
            string name = "RPTPR004-" + lotNumber;
            string pathName = @"" + folderName + "\\" + name + ".xlsx";
            FileInfo info = new FileInfo(pathName);

            if (info.Exists) File.Delete(pathName);

            // If you use EPPlus in a noncommercial context
            // according to the Polyform Noncommercial license:
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage pck = new ExcelPackage(info))
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add(name);

                ws.Cells["A1"].LoadFromCollection(payrollList, true);
                ws.Protection.IsProtected = false;
                ws.Cells.AutoFitColumns();

                ws.Cells["A1"].Value = "Department";
                ws.Cells["B1"].Value = "Employee id";
                ws.Cells["C1"].Value = "Title";
                ws.Cells["D1"].Value = "Firstname";
                ws.Cells["E1"].Value = "Lastname";
                ws.Cells["F1"].Value = "Id Card";
                ws.Cells["G1"].Value = "Bank Acc.";
                ws.Cells["H1"].Value = "Basic Salary";
                ws.Cells["I1"].Value = "Actual Salary";
                ws.Cells["J1"].Value = "Wage";
                ws.Cells["K1"].Value = "Working Date";
                ws.Cells["L1"].Value = "OT hour";
                ws.Cells["M1"].Value = "OT holiday";
                ws.Cells["N1"].Value = "OT hour holiday";
                ws.Cells["O1"].Value = "OT holiday weekend";
                ws.Cells["P1"].Value = "Count OT hour";
                ws.Cells["Q1"].Value = "Count OT holiday";
                ws.Cells["R1"].Value = "Count OT hour holiday";
                ws.Cells["S1"].Value = "Count OT holiday weekend";
                ws.Cells["T1"].Value = "Sum OT hour";
                ws.Cells["U1"].Value = "Sum OT holiday ";
                ws.Cells["V1"].Value = "Sum OT hour holiday";
                ws.Cells["W1"].Value = "Sum OT holiday weekend";
                ws.Cells["X1"].Value = "พิเศษ";
                ws.Cells["Y1"].Value = "Bonus";
                ws.Cells["Z1"].Value = "ค่าอาหาร";
                ws.Cells["AA1"].Value = "กะดึก";
                ws.Cells["AB1"].Value = "Total Income Labour";
                ws.Cells["AC1"].Value = "Total Income";
                ws.Cells["AD1"].Value = "Tax Labour 3%";
                ws.Cells["AE1"].Value = "Tax";
                ws.Cells["AF1"].Value = "SSO";
                ws.Cells["AG1"].Value = "หักเงิน";
                ws.Cells["AH1"].Value = "Student loan";
                ws.Cells["AI1"].Value = "Net Income";
                pck.Save();
            }

            return @"" + folderName;
        }

        public string ExportExcelPayrollEmployee(string folderName, List<sp_GetPayrollEmployee_Result> payrollList, bool isShowMoney)
        {
            //foreach (var item in payrollList)
            //{
            //    var staffID = item.Staff_type;
            //    var staffType = staffID == 1 ? "พนักงานประจำ" : staffID == 2 ? "พนักงานรายวันประจำ" : staffID == 3 ? "พนักงานรายวัน" : "พนักงานรายวันชั่วคราว";
            //    item.Staff_type = staffType;
            //}

            string name = "RPTPR003";
            string pathName = @"" + folderName + "\\" + name + ".xlsx";
            FileInfo info = new FileInfo(pathName);
            //isShowMoney = false;

            if (info.Exists) File.Delete(pathName);

            // If you use EPPlus in a noncommercial context
            // according to the Polyform Noncommercial license:
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage pck = new ExcelPackage(info))
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add(name);

                ws.Cells["A1"].LoadFromCollection(payrollList, true);
                ws.Protection.IsProtected = false;
                ws.Cells.AutoFitColumns();

                ws.Cells["A1"].Value = "Employee id"; //Hris Employee id
                ws.Cells["B1"].Value = "Person_ID";
                ws.Cells["C1"].Value = "Employee_id"; //Acc Employee id
                ws.Cells["D1"].Value = "Staff_type_id";
                ws.Cells["E1"].Value = "Staff Type";
                ws.Cells["F1"].Value = "Title";
                ws.Cells["G1"].Value = "Firstname";
                ws.Cells["H1"].Value = "Lastname";
                ws.Cells["I1"].Value = "Id Card";
                ws.Cells["J1"].Value = "Bank Acc.";
                ws.Cells["K1"].Value = "Dept.";
                ws.Cells["L1"].Value = "Joing Date";
                ws.Cells["M1"].Value = "Get Code Date";
                ws.Cells["N1"].Value = "End Date";
                ws.Cells["O1"].Value = "Status";
                ws.Cells["P1"].Value = "Date of birth";
                ws.Cells["Q1"].Value = "Salary / Wage";
                ws.Cells["R1"].Value = "Actual Salary / Wage";
                ws.Cells["S1"].Value = "OT hour";
                ws.Cells["T1"].Value = "OT holiday";
                ws.Cells["U1"].Value = "OT hour holiday";
                ws.Cells["V1"].Value = "Tax";
                ws.Cells["W1"].Value = "Person_ID"; // Education
                ws.Cells["X1"].Value = "Institute Ini";
                ws.Cells["Y1"].Value = "Institute";
                ws.Cells["Z1"].Value = "Education Level";
                ws.Cells["AA1"].Value = "Education";
                ws.Cells["AB1"].Value = "Qualification Ini";
                ws.Cells["AC1"].Value = "Qualification";
                ws.Cells["AD1"].Value = "Major Ini";
                ws.Cells["AE1"].Value = "Major";
                ws.Cells["AF1"].Value = "Admission Date";
                ws.Cells["AG1"].Value = "Graduated Date";
                ws.Cells["AH1"].Value = "GPA";
                ws.Cells["AI1"].Value = "Person_ID"; // Address
                ws.Cells["AJ1"].Value = "Address";
                ws.Cells["AK1"].Value = "Moo";
                ws.Cells["AL1"].Value = "Tambon";
                ws.Cells["AM1"].Value = "Amphur";
                ws.Cells["AN1"].Value = "Province";
                ws.Cells["AO1"].Value = "Zipcode";
                ws.Cells["AP1"].Value = "Phone";
                ws.Cells["AQ1"].Value = "Phone Ext.";
                ws.Cells["AR1"].Value = "Email";

                ws.DeleteColumn(2); //Remove person id
                ws.DeleteColumn(2); //Remove noted
                ws.DeleteColumn(2); //Remove Stafftype id
                if (!isShowMoney) ws.DeleteColumn(13); //Remove Salary
                if (!isShowMoney) ws.DeleteColumn(13); //Remove Actual Salary
                if (!isShowMoney) ws.DeleteColumn(13); //Remove OT
                if (!isShowMoney) ws.DeleteColumn(13); //Remove OTH
                if (!isShowMoney) ws.DeleteColumn(13); //Remove OTHH
                if (!isShowMoney) ws.DeleteColumn(13); //Remove Tax
                ws.DeleteColumn(!isShowMoney ? 13 : 19); //Remove PersonID
                ws.DeleteColumn(!isShowMoney ? 24 : 30); //Remove PersonID
                

                //ws.Cells["K2"].Style.Numberformat.Format = "yyyy-mm-dd";
                using (ExcelRange col = ws.Cells[2, 8, 2 + payrollList.Count, 9]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                using (ExcelRange col = ws.Cells[2, 9, 2 + payrollList.Count, 10]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                using (ExcelRange col = ws.Cells[2, 11, 2 + payrollList.Count, 12]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                using (ExcelRange col = ws.Cells[2, !isShowMoney ? 21 : 27, 2 + payrollList.Count, !isShowMoney ? 22 : 28]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }

                //using (ExcelRange col = ws.Cells[2, 14, 2 + list.Count, 14]) { col.Style.Numberformat.Format = "dd/MM/yyyy HH:mm"; }

                pck.Save();
            }

            return @"" + folderName;
        }

        public List<PayrollViewModel> CovertToPayrollVM(List<Payroll> payrollList)
        {
            var result = new List<PayrollViewModel>();
            foreach (var p in payrollList)
            {
                var vm = new PayrollViewModel();
                vm._payroll = p;
                var staffID = p.Staff_type;
                vm.staffTypeDescription = staffID == "1" ? "ประจำ" : staffID == "2" ? "รายวันประจำ" : "รายวัน";
                var sumOT = decimal.Multiply((decimal)p.Cot_hour.GetValueOrDefault(), p.Ot_hour_rate.GetValueOrDefault());
                vm.SumOT = sumOT > 0 ? sumOT : (decimal?)null;
                var sumOTH = decimal.Multiply((decimal)p.Cot_holiday.GetValueOrDefault(), p.Ot_holiday_rate.GetValueOrDefault());
                vm.SumOTH = sumOTH > 0 ? Math.Round(sumOTH, 2, MidpointRounding.AwayFromZero) : (decimal?)null;
                var sumOTHH = decimal.Multiply((decimal)p.Cot_hour_holiday.GetValueOrDefault(), p.Ot_hour_holiday_rate.GetValueOrDefault());
                vm.SumOTHH = sumOTHH > 0 ? sumOTHH : (decimal?)null;
                var sumSpecial = p.Ot_special_rate.GetValueOrDefault() + p.Special.GetValueOrDefault();
                vm.SumSpecial = sumSpecial == 0 ? (decimal?)null : sumSpecial; 
                var pnd = p.Tax.GetValueOrDefault();
                vm.PND = pnd == 0 ? (decimal?)null : pnd;
                result.Add(vm);
            }
            return result;
        }

        public List<PayrollViewModel> CovertToPayrollVM(List<sp_NEWGet_Result> payrollList, DateTime fromDate, DateTime toDate)
        {
            var result = new List<PayrollViewModel>();
            var actualCutrag = new List<sp_GetActualWorkingDate_Result>(); 
            foreach (var p in payrollList)
            {
                if(!actualCutrag.Any() && p.Staff_type != "1" && p.Dept_code == "CUT") actualCutrag = BusinessLayerServices.PayrollBL().sp_GetActualWorkingDate(fromDate, toDate);
                var vm = new PayrollViewModel();
                vm._payroll = new Payroll();
                vm._payroll.Employee_id = p.Employee_id; //Grid
                vm._payroll.EmployeeID = p.EmployeeID;
                vm._payroll.Dept_code = p.Dept_code; //Grid
                vm._payroll.Tname = p.Tname; //Grid
                vm._payroll.Fname = p.Fname; //Grid
                vm._payroll.Lname = p.Lname; //Grid
                vm._payroll.Id_card = p.Id_card;
                vm._payroll.Bank_acc = p.Bank_acc;
                vm._payroll.Staff_type = p.Staff_type;
                vm.staffTypeDescription = p.StaffType; //Grid
                var staffWage = Math.Round(p.Wage.GetValueOrDefault() * (decimal)p.CWork.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero);
                vm._payroll.Salary = p.Staff_type == "1" ? p.Salary : staffWage;
                vm._payroll.NetSalary = p.Staff_type == "1" ? p.NetSalary : staffWage; //Grid
                vm._payroll.Wage = p.Wage; //Grid
                vm._payroll.Working_date = p.CWork; //Grid
                vm._payroll.Ot_hour_rate = p.Ot_hour_rate;
                var Ot_holiday_rate = p.Staff_type == "1" ? (decimal)p.Ot_holiday_rate.GetValueOrDefault() : p.Wage.GetValueOrDefault() / 8;
                vm._payroll.Ot_holiday_rate = Ot_holiday_rate; // Temporay rate change  29/07/2021
                vm._payroll.Ot_hour_holiday_rate = p.Ot_hour_holiday_rate;
                vm._payroll.Cot_hour = p.Ot_hour == 0 ? null : p.Ot_hour;
                var Cot_holiday_weekend = p.Ot_Weekend.GetValueOrDefault() * 2; // Double hour instead double rate for weekend Holiday (Sunday)
                vm._payroll.Cot_holiday = p.Ot_holiday.GetValueOrDefault() + (Cot_holiday_weekend);
                vm._payroll.Cot_hour_holiday = p.Ot_hour_holiday == 0 ? null : p.Ot_hour_holiday;

                // >>> OT <<<
                var sumOT = decimal.Multiply((decimal)p.Ot_hour.GetValueOrDefault(), p.Ot_hour_rate.GetValueOrDefault());
                vm.SumOT = sumOT > 0 ? Math.Round(sumOT, 2, MidpointRounding.AwayFromZero) : (decimal?)null; //Grid

                //Reverse multiply full day work (8H) must be equal wage
                //decimal sumOTH = 0;
                //var countOTH = (decimal)p.Ot_holiday.GetValueOrDefault();
                //if (countOTH > 0)
                //{
                //    var amoOTH = (int)countOTH / 8;
                //    var modOTH = countOTH % 8;
                //    sumOTH = (decimal.Multiply((decimal)amoOTH, p.Wage.GetValueOrDefault())) + (decimal.Multiply(modOTH, p.Ot_holiday_rate.GetValueOrDefault()));
                //} 
                //var sumOTH = (decimal.Multiply((decimal)(p.Ot_holiday.GetValueOrDefault()), p.Ot_holiday_rate.GetValueOrDefault()));

                var sumOTH = (decimal.Multiply((decimal)(p.Ot_holiday.GetValueOrDefault()), Ot_holiday_rate));
                var sumOTHW = (decimal)Cot_holiday_weekend * Ot_holiday_rate;
                vm.SumOTH = (sumOTH + sumOTHW) > 0 ? Math.Round(sumOTH + sumOTHW, 2, MidpointRounding.AwayFromZero) : (decimal?)null; //Grid
                var sumOTHH = decimal.Multiply((decimal)p.Ot_hour_holiday.GetValueOrDefault(), p.Ot_hour_holiday_rate.GetValueOrDefault());
                vm.SumOTHH = sumOTHH > 0 ? Math.Round(sumOTHH, 2, MidpointRounding.AwayFromZero) : (decimal?)null; //Grid
                var otSpecial = (decimal?)(p.Ot_Special.GetValueOrDefault() * 40);
                vm._payroll.Ot_special_rate = otSpecial == 0 ? null : otSpecial;
                var sumOtSpecial = p.Ot_special_rate.GetValueOrDefault() + (decimal?)(p.Ot_Special.GetValueOrDefault() * 40); // OT แบบเหมา
                var sumNight = Convert.ToDecimal(p.Night * 20);
                vm._payroll.Night_sup = sumNight;
                var sumLunch_1 = Convert.ToDecimal(p.Lunch_1 * 25);
                var sumLunch_2 = Convert.ToDecimal(p.Lunch_2 * 25);
                var sumLunch = sumLunch_1 + sumLunch_2;
                vm._payroll.Lunch_sup = sumLunch;
                //var sumSpecial = sumOtSpecial + sumNight + sumLunch_1 + sumLunch_2;
                var sumSpecial = sumOtSpecial;
                vm._payroll.Special = sumSpecial; // Show in detail window
                vm.SumSpecial = sumSpecial == 0 ? null : sumSpecial; //Grid
                
                var wageSalary = decimal.Multiply(p.Wage.GetValueOrDefault(), (decimal)p.CWork.GetValueOrDefault());
                var netWage = p.NetSalary.GetValueOrDefault() + wageSalary;
                var xxxSumAllOT = sumOT + sumOTH + sumOTHW + sumOTHH;
                var SumAllOT = (vm.SumOT == null ? 0 : vm.SumOT) + (vm.SumOTH == null ? 0 : vm.SumOTH) + (vm.SumOTHH == null ? 0 : vm.SumOTHH);
                var totalIncome = netWage + SumAllOT + sumSpecial + sumLunch; //+ Bonus
                vm._payroll.Total_Income = totalIncome; //Grid 
                var pnd = p.Tax.GetValueOrDefault() > 0 && SumAllOT > 0 ? decimal.Multiply(SumAllOT.Value, (decimal)0.05) + p.Tax.GetValueOrDefault() : p.Tax.GetValueOrDefault();
                vm._payroll.Tax = pnd == 0 ? (decimal?)null : Math.Round(pnd, 2, MidpointRounding.AwayFromZero);
                var totalIncomeLabour = totalIncome.Value + pnd;
                vm._payroll.Total_Income_Labour = p.Staff_type == "1" ? (decimal?)null : Math.Round(totalIncomeLabour, 2, MidpointRounding.AwayFromZero);
                vm._payroll.Tax_Labour = p.Staff_type == "1" ? (decimal?)null : Math.Round(pnd, 2, MidpointRounding.AwayFromZero);
                vm.PND = pnd == 0 ? (decimal?)null : Math.Round(pnd, 2, MidpointRounding.AwayFromZero); //Grid
                decimal? sso = 0;
                if (p.Staff_type != "4")
                {
                    if (netWage > 0)
                    {
                        if (netWage >= p.SSO_Max_Wage) sso = p.SSO_Max_Wage * (p.SSO_pc / 100);
                        else if (netWage < p.SSO_Max_Wage && netWage >= p.SSO_Min_Wage) sso = netWage * (p.SSO_pc / 100);
                        else if (netWage < p.SSO_Min_Wage) sso = p.SSO_Min_Bath;
                    }

                    // SSO Formula 2021-06-26
                    var ssoA = Math.Round(sso.GetValueOrDefault(), 2); // Rounding to 2 digit
                    var ssoB = Math.Round(ssoA, 0, MidpointRounding.AwayFromZero); // then rounding Away from 0
                    sso = ssoB;
                }

                vm._payroll.SSO = p.Nsso == "" ? (decimal?)null : sso; //Grid
                var netIncome = totalIncome - ((vm.PND == null ? 0 : vm.PND) + sso); // - Cut_wage - Student_Loan
                vm._payroll.Net_Income = netIncome;
                result.Add(vm);
            }
            return result;
        }



        public List<RptPR04ViewModel> ConvertToRPTPR04(List<Payroll> payrollList, string lotNumber)
        {
            var result = new List<RptPR04ViewModel>();
            var lotMonth = lotNumber.Substring(0, lotNumber.Length - 5);
            var lotYear = lotNumber.Substring(lotNumber.Length - 4);
            var otdetailList = BusinessLayerServices.PayrollBL().GetOTDetailByLot(lotMonth, lotYear);
            foreach (var p in payrollList)
            {
                var _otDetail = otdetailList.SingleOrDefault(s => s.Employee_id == p.Employee_id);
                var dept = BusinessLayerServices.DeptCodeBL().GetSingleDepartment(p.Dept_code);
                var sumOT = (decimal)p.Cot_hour.GetValueOrDefault() * p.Ot_hour_rate.GetValueOrDefault();
                decimal sumOTH = 0;
                var countOTH = _otDetail == null ? 0 : _otDetail.Ot_holiday;
                if (p.Staff_type != "1")
                {
                    //var countOTH = (decimal)p.Cot_holiday.GetValueOrDefault();
                    if (countOTH > 0)
                    {   // สำหรับ พงน รายวัน จะหาร 8 เพื่อเอาจำนวนวันคูณค่าจ้าง เอาเศษคูฦณกับเรท เพื่อใช้ค่าแรงเต็มวันครบจำนวน
                        var amoOTH = (int)countOTH / 8;
                        var modOTH = countOTH % 8;
                        sumOTH = (decimal.Multiply(amoOTH, p.Wage.GetValueOrDefault())) + (decimal.Multiply((decimal)modOTH, p.Ot_holiday_rate.GetValueOrDefault()));
                    }
                }
                else sumOTH = (decimal)countOTH * p.Ot_holiday_rate.GetValueOrDefault();
                var sumOTHH = (decimal)p.Cot_hour_holiday.GetValueOrDefault() * p.Ot_hour_holiday_rate.GetValueOrDefault();
                var sumOTHW = _otDetail == null ? 0 : (decimal)_otDetail.Ot_Weekend.GetValueOrDefault() * ((p.Wage.GetValueOrDefault() / 8) * 2);
                var sumOTAll = sumOT + sumOTH + sumOTHH + sumOTHW;
                var special = p.Special.GetValueOrDefault() + p.Ot_special_rate.GetValueOrDefault();
                result.Add(new RptPR04ViewModel
                {
                    Department = string.Format("[{0}] {1}", dept.DeptCode, dept.DeptName),
                    Employee_id = p.EmployeeID,
                    Title = p.Tname,
                    Firstname = p.Fname,
                    Lastname = p.Lname,
                    IDCard = p.Id_card,
                    BankAccount = p.Bank_acc,
                    Salary = p.Salary,
                    NetSalary = p.NetSalary,
                    Wage = p.Wage,
                    WorkingDate = p.Working_date,
                    OTRate = p.Ot_hour_rate,
                    OTHRate = p.Ot_holiday_rate,
                    OTHHRate = p.Ot_hour_holiday_rate,
                    OTHWRate = p.Ot_holiday_rate * 2,
                    OTCount = p.Cot_hour,
                    OTHCount = countOTH > 0 ? countOTH : (double?)null,
                    OTHHCount = p.Cot_hour_holiday,
                    OTHWCount = _otDetail == null ? (double?)null : _otDetail.Ot_Weekend > 0 ? _otDetail.Ot_Weekend : (double?)null,
                    OTSum = sumOT > 0 ? Math.Round(sumOT, 2, MidpointRounding.AwayFromZero) : (decimal?)null,
                    OTHSum = sumOTH > 0 ? Math.Round(sumOTH, 2, MidpointRounding.AwayFromZero) : (decimal?)null,
                    OTHHSum = sumOTHH > 0 ? Math.Round(sumOTHH, 2, MidpointRounding.AwayFromZero) : (decimal?)null,
                    OTHWSum = sumOTHW > 0 ? Math.Round(sumOTHW, 2, MidpointRounding.AwayFromZero) : (decimal?)null,
                    Special = special > 0 ? special : (decimal?)null,
                    Bonus = p.Bonus,
                    Lunch_sup = p.Lunch_sup,
                    Night_sup = p.Night_sup,
                    TotalIncomeLabour = p.Total_Income_Labour,
                    TotalIncome = p.Total_Income,
                    TaxLabour = p.Tax_Labour > 0 ? p.Tax_Labour : (decimal?)null,
                    Tax = p.Tax == null ? (decimal?)null : Math.Round(p.Tax.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero),
                    SSO = p.SSO,
                    Cutwage = p.Cut_wage,
                    StudentLoan = p.Student_Loan,
                    NetIncome = p.Net_Income
                });
            }
            return result;
        }

        public string ExportExcelPR007(string folderName, string lotNumber, List<sp_GetSSOReport_Result> ssoReportList)
        {
            lotNumber = lotNumber.Replace("/", "-");
            string name = "Social Security_STEC_" + lotNumber;
            string pathName = @"" + folderName + "\\" + name + ".xlsx";
            FileInfo info = new FileInfo(pathName);

            if (info.Exists) File.Delete(pathName);

            // If you use EPPlus in a noncommercial context
            // according to the Polyform Noncommercial license:
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage pck = new ExcelPackage(info))
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add(name);

                ws.Cells["A1"].LoadFromCollection(ssoReportList, true);
                ws.Protection.IsProtected = false;
                ws.Cells.AutoFitColumns();

                ws.Cells["A1"].Value = "No.";
                ws.Cells["B1"].Value = "SSOID";
                ws.Cells["C1"].Value = "Name";
                ws.Cells["D1"].Value = "Num";
                ws.Cells["E1"].Value = "Salary";
                ws.Cells["F1"].Value = "Wage";
                ws.Cells["G1"].Value = "SSO";
                
                ws.DeleteColumn(5);

                using (ExcelRange col = ws.Cells[2, 5, 2 + ssoReportList.Count, 5]) { col.Style.Numberformat.Format = "#,##0.00"; }
                using (ExcelRange col = ws.Cells[2, 6, 2 + ssoReportList.Count, 6]) { col.Style.Numberformat.Format = "#,##0.00"; }

                pck.Save();
            }

            return @"" + folderName;
        }

        public string GetCurrentLot()
        {
            var currentLot = BusinessLayerServices.PayrollBL().GetCurrentDateLot();
            if (currentLot != null)
            {
                return string.Format("{0}/{1}", currentLot.Lot_Month, currentLot.Lot_Year);
            }
            else throw new Exception("ไม่พบ Lot number ที่ตรงกับวันที่ปัจจุบัน");
        }

        public string ExportExcelToPayrollOT(string folderName, List<OTViewModel> otList)
        {
            string name = "To Payroll OT";
            string pathName = @"" + folderName + "\\" + name + ".xlsx";
            FileInfo info = new FileInfo(pathName);

            if (info.Exists) File.Delete(pathName);

            // If you use EPPlus in a noncommercial context
            // according to the Polyform Noncommercial license:
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage pck = new ExcelPackage(info))
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add(name);

                ws.Cells["A1"].LoadFromCollection(otList, true);
                ws.Protection.IsProtected = false;
                ws.Cells.AutoFitColumns();

                ws.DeleteColumn(3);
                ws.DeleteColumn(3);
                ws.DeleteColumn(6);
                ws.DeleteColumn(14);
                ws.DeleteColumn(14);
                ws.DeleteColumn(14);
                ws.DeleteColumn(14);
                ws.DeleteColumn(14);
                ws.DeleteColumn(14);

                //using (ExcelRange col = ws.Cells[2, 8, 2 + otList.Count, 8]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                pck.Save();
            }

            return @"" + folderName;
        }

        public string ExportExcelToPayrollWorkdate(string folderName, List<sp_GetWorkingDateToPayroll_Result> workList)
        {
            string name = "To Payroll Workdate";
            string pathName = @"" + folderName + "\\" + name + ".xlsx";
            FileInfo info = new FileInfo(pathName);

            if (info.Exists) File.Delete(pathName);

            // If you use EPPlus in a noncommercial context
            // according to the Polyform Noncommercial license:
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage pck = new ExcelPackage(info))
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add(name);

                ws.Cells["A1"].LoadFromCollection(workList, true);
                ws.Protection.IsProtected = false;
                ws.Cells.AutoFitColumns();

                ws.DeleteColumn(1);
                ws.DeleteColumn(1);
                ws.DeleteColumn(5);
                ws.DeleteColumn(6);

                pck.Save();
            }

            return @"" + folderName;
        }

        public List<sp_GetWorkingDateToPayroll_Result> ConvertToPayrollWorkdate(List<Labour_work> workList)
        {
            var result = new List<sp_GetWorkingDateToPayroll_Result>();
            foreach (var item in workList)
            {
                var emp = BusinessLayerServices.EmployeeBL().GetEmployeeByAccCode(item.Employee_id);
                result.Add(new sp_GetWorkingDateToPayroll_Result
                {
                    EMPLOYEE_ID = emp.Employee_ID,
                    DEPT_CODE = emp.DeptCode,
                    FNAME = emp.Person.FirstNameTH,
                    LNAME = emp.Person.LastNameTH,
                    NWORKING_DATE = Convert.ToDecimal(item.CWork),
                    PAYROLL_ID = emp.Noted,
                    Staff_type = emp.StaffType.StaffTypeID.ToString(),
                    Stafftype = emp.StaffType.Description
                });
            }
            return result;
        }

        public List<OTViewModel> ConvertToPayrollOT(List<OT_Detail> otDetail)
        {
            var result = new List<OTViewModel>();
            foreach (var item in otDetail)
            {
                var emp = BusinessLayerServices.EmployeeBL().GetEmployeeByAccCode(item.Employee_id);
                result.Add(new OTViewModel
                {
                    DEPT_CODE = emp.DeptCode,
                    Employee_ID = item.Employee_id,
                    TitleNameTH = emp.Person.TitleName.TitleNameTH,
                    FirstNameTH = emp.Person.FirstNameTH,
                    LastNameTH = emp.Person.LastNameTH,
                    OTnormal = item.Ot_hour == null ? (decimal?)null : Convert.ToDecimal(item.Ot_hour),
                    Holiday = item.Ot_holiday == null ? (decimal?)null : Convert.ToDecimal(item.Ot_holiday),
                    OTHoliday = item.Ot_hour_holiday == null ? (decimal?)null : Convert.ToDecimal(item.Ot_hour_holiday),
                    OTWeekend = item.Ot_Weekend == null ? (decimal?)null : Convert.ToDecimal(item.Ot_Weekend),
                    OTSpecial = item.Ot_Special == null ? (decimal?)null : Convert.ToDecimal(item.Ot_Special),
                    Stafftype = BusinessLayerServices.EmployeeBL().GetEmployeeByAccCode(item.Employee_id).Staff_type.Value
                });
            }
            return result;
        }

        public List<sp_GetSummarySupportByDateAndEmployee_Result> ConvertTo_sp_GetSummarySupport(List<Support_Detail> supportList)
        {
            var result = new List<sp_GetSummarySupportByDateAndEmployee_Result>();
            foreach (var item in supportList)
            {
                var emp = BusinessLayerServices.EmployeeBL().GetEmployeeByEmployeeID(item.Employee_id);
                if (emp != null)
                {
                    result.Add(new sp_GetSummarySupportByDateAndEmployee_Result
                    {
                        code = emp.DeptCode,
                        Employee_ID = item.Employee_id,
                        TitleNameTH = emp.Person.TitleName.TitleNameTH,
                        FirstNameTH = emp.Person.FirstNameTH,
                        LastNameTH = emp.Person.LastNameTH,
                        Staff_type = emp.Staff_type.Value.ToString(),
                        Night = item.Night,
                        Lunch_1 = item.Lunch_1,
                        Lunch_2 = item.Lunch_2
                    });
                }
            }
            return result;
        }

        public List<Payroll> ConvertSummaryPRToPayroll(List<Payroll_Summary> summaryList)
        {
            throw new NotImplementedException();
        }
    }
}
