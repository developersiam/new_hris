﻿using HRISSystem.MVVM.ViewModel.Notice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HRISSystem.MVVM.View.Notice
{
    /// <summary>
    /// Interaction logic for NoticePage.xaml
    /// </summary>
    public partial class NoticePage : Page
    {
        public NoticePage()
        {
            InitializeComponent();
            var vm = new vm_Notice();
            DataContext = vm;
        }
    }
}
