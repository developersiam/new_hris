﻿using DomainModelHris;
using HRISSystem.Form.Report;
using HRISSystem.Helper;
using HRISSystem.ViewModel;
using HRISSystemBL.BL.HRISBL;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using static System.Data.Entity.Infrastructure.Design.Executor;

namespace HRISSystem.MVVM.ViewModel.Report
{
    public class vm_RPTSW01 : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_RPTSW01()
        {
            DepartmentListDataBinding();
        }


        #region Properties
        private Window _window;

        public Window Window
        {
            get { return _window; }
            set { _window = value; }
        }

        private ReportViewer _reportViewer;

        public ReportViewer ReportViewer
        {
            get { return _reportViewer; }
            set { _reportViewer = value; }
        }

        private DateTime _fromDate;

        public DateTime FromDate
        {
            get { return _fromDate; }
            set { _fromDate = value; }
        }

        private DateTime _toDate;

        public DateTime ToDate
        {
            get { return _toDate; }
            set { _toDate = value; }
        }

        private string _reportName;

        public string ReportName
        {
            get { return _reportName; }
            set { _reportName = value; }
        }

        private string _reportPath;

        public string ReportPath
        {
            get { return _reportPath; }
            set { _reportPath = value; }
        }

        #endregion


        #region List
        private List<m_DepartmentDropDownList> _departmentList;

        public List<m_DepartmentDropDownList> DepartmentList
        {
            get { return _departmentList; }
            set { _departmentList = value; }
        }
        #endregion


        #region Command
        private ICommand _onCallDataCommand;

        public ICommand OnCallDataCommand
        {
            get { return _onCallDataCommand ?? (_onCallDataCommand = new RelayCommand(CallData)); }
            set { _onCallDataCommand = value; }
        }

        private void CallData(object obj)
        {
            RenderReport();
        }

        private ICommand _onCheckedCommand;

        public ICommand OnCheckedCommand
        {
            get { return _onCheckedCommand ?? (_onCheckedCommand = new RelayCommand(CheckBoxChecked)); }
            set { _onCheckedCommand = value; }
        }

        private void CheckBoxChecked(object obj)
        {
            try
            {
                var selectedItem = (m_DepartmentDropDownList)obj;
                if (selectedItem == null)
                    return;

                var tempList = new List<m_DepartmentDropDownList>();
                if (selectedItem.DepartmentCode == "All")
                {
                    if (selectedItem.IsSelected == true)
                    {
                        foreach (var item in _departmentList)
                        {
                            item.IsSelected = true;
                            tempList.Add(item);
                        }
                    }
                    else
                    {
                        foreach (var item in _departmentList)
                        {
                            item.IsSelected = false;
                            tempList.Add(item);
                        }
                    }

                    _departmentList.Clear();
                    _departmentList = tempList;
                    RaisePropertyChangedEvent(nameof(DepartmentList));
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #endregion


        #region Function
        private void DepartmentListDataBinding()
        {
            try
            {
                _departmentList = new List<m_DepartmentDropDownList>();
                _departmentList
                    .Add(new m_DepartmentDropDownList
                    {
                        IsSelected = false,
                        DepartmentCode = "All",
                        DepartmentName = "Selected All/Unselected All"
                    });
                foreach (var item in HRISBLServices.DepartmentBL().GetAll())
                {
                    _departmentList.Add(new m_DepartmentDropDownList
                    {
                        IsSelected = false,
                        DepartmentCode = item.DeptCode,
                        DepartmentName = item.DeptCode + "\t" + item.DeptName,
                    });
                }
                RaisePropertyChangedEvent(nameof(DepartmentList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void RenderReport()
        {
            try
            {
                if (_fromDate == null)
                    throw new ArgumentException("โปรดระบุ From Date");

                if (_toDate == null)
                    throw new ArgumentException("โปรดระบุ To Date");

                if (_fromDate > _toDate)
                    throw new ArgumentException("โปรดระบุช่วงวันที่ From Date และ To Date ให้ถูกต้อง");

                var list = _departmentList
                    .Where(x => x.IsSelected == true)
                    .ToList();

                HRISReportDataset.ShiftWorkCalendarDataTable dataTable = new HRISReportDataset.ShiftWorkCalendarDataTable();
                Form.Report.HRISReportDatasetTableAdapters.ShiftWorkCalendarTableAdapter tableAdapter = new Form.Report.HRISReportDatasetTableAdapters.ShiftWorkCalendarTableAdapter();
                tableAdapter.Fill(dataTable, _fromDate, _toDate);

                HRISReportDataset.ShiftWorkCalendarDataTable resultDataTable = new HRISReportDataset.ShiftWorkCalendarDataTable();
                foreach (var item in dataTable)
                {
                    if (list.Where(x => x.DepartmentCode == item.DeptCode).Count() > 0)
                    {
                        resultDataTable.ImportRow(item);
                    }
                }

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "ShiftWorkCalendarDataSet";
                reportDataSource.Value = resultDataTable;

                _reportViewer.Reset();
                _reportViewer.LocalReport.DataSources.Add(reportDataSource);
                _reportViewer.LocalReport.ReportEmbeddedResource = _reportPath;
                _reportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
