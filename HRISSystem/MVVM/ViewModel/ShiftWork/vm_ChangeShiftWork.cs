﻿using DomainModelHris;
using HRISSystem.Helper;
using HRISSystem.Shared;
using HRISSystemBL;
using HRISSystemBL.BL.HRISBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace HRISSystem.MVVM.ViewModel.ShiftWork
{
    public class vm_ChangeShiftWork : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_ChangeShiftWork()
        {
            ShiftListBinding();
        }

        #region Properties
        private Window _window;

        public Window Window
        {
            get { return _window; }
            set { _window = value; }
        }

        private string _employeeID;

        public string EmployeeID
        {
            get { return _employeeID; }
            set { _employeeID = value; }
        }

        private ShiftControl _shiftControl;

        public ShiftControl ShiftControl
        {
            get { return _shiftControl; }
            set { _shiftControl = value; }
        }

        private string _shiftID;

        public string ShiftID
        {
            get { return _shiftID; }
            set { _shiftID = value; }
        }

        private DateTime _shiftDate;

        public DateTime ShiftDate
        {
            get { return _shiftDate; }
            set { _shiftDate = value; }
        }
        #endregion



        #region List
        private List<DomainModelHris.Shift> _shiftList;

        public List<DomainModelHris.Shift> ShiftList
        {
            get { return _shiftList; }
            set { _shiftList = value; }
        }
        #endregion



        #region Command
        private ICommand _onChangeShiftCommand;

        public ICommand OnChangeShiftCommand
        {
            get { return _onChangeShiftCommand ?? (_onChangeShiftCommand = new RelayCommand(ChangeShift)); }
            set { _onChangeShiftCommand = value; }
        }

        private void ChangeShift(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("ท่านต้องการเปลี่ยนกะการทำงานนี้ใช่หรือไม่")
                    == System.Windows.MessageBoxResult.No)
                    return;

                //Save.
                HRISBLServices.ShiftControlBL()
                    .ChangeShiftControl(_employeeID,
                    _shiftControl.ValidFrom,
                    _shiftControl.Shift_ID,
                    _shiftID,
                    SingletonConfiguration.getInstance().Username);

                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onRemoveAssignedShiftCommand;

        public ICommand OnRemoveAssignedShiftCommand
        {
            get { return _onRemoveAssignedShiftCommand ?? (_onRemoveAssignedShiftCommand = new RelayCommand(RemoveAssignedShift)); }
            set { _onRemoveAssignedShiftCommand = value; }
        }

        private void RemoveAssignedShift(object obj)
        {
            try
            {
                if (_shiftControl.Shift_ID == "NA")
                    throw new ArgumentException("ยังไม่มีการบันทึกกะนี้ในระบบ จึงไม่สามารถลบได้");

                if (MessageBoxHelper.Question("ท่านต้องการยกเลิกกะการทำงานนี้ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                HRISBLServices.ShiftControlBL()
                    .Delete(_shiftControl.Shift_ID, _employeeID, _shiftControl.ValidFrom);
                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #endregion



        #region Function
        private void ShiftListBinding()
        {
            _shiftList = BusinessLayerServices.ShiftBL()
                .GetAllShiftList()
                .OrderBy(x => x.StartTime)
                .ThenBy(x => x.EndTime)
                .ToList();
            RaisePropertyChangedEvent(nameof(ShiftList));
        }
        #endregion
    }
}
