﻿using HRISSystem.Helper;
using HRISSystem.MVVM.Model;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Window = System.Windows.Window;

namespace HRISSystem.MVVM.ViewModel.ShiftWork
{
    public class vm_ShiftColor : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_ShiftColor()
        {
        }


        #region Properties

        private Window _window;

        public Window Window
        {
            get { return _window; }
            set { _window = value; }
        }

        private string _selectedColor;

        public string SelectedColor
        {
            get { return _selectedColor; }
            set { _selectedColor = value; }
        }

        private string _shiftID;

        public string ShiftID
        {
            get { return _shiftID; }
            set { _shiftID = value; }
        }
        #endregion



        #region List
        private List<m_ColorPicker> _colorList;

        public List<m_ColorPicker> ColorList
        {
            get
            {
                _colorList = new List<m_ColorPicker>();
                int i = 0;
                foreach (KnownColor color in Enum.GetValues(typeof(KnownColor)))
                {
                    Color col = Color.FromKnownColor(color);
                    if (i >= 27 && i <= 140)
                    {
                        _colorList.Add(new m_ColorPicker
                        {
                            Name = col.Name,
                            R = col.R,
                            G = col.G,
                            B = col.B,
                            Hue = col.GetHue(),
                        });
                    }
                    i++;
                }
                return _colorList
                    .OrderBy(x => x.Hue)
                    .ThenBy(x => x.R * 3 + x.G * 2 + x.B * 1)
                    .ToList();
            }
            set { _colorList = value; }
        }
        #endregion



        #region Command
        private ICommand _onClickCommand;

        public ICommand OnClickCommand
        {
            get { return _onClickCommand ?? (_onClickCommand = new RelayCommand(Click)); }
            set { _onClickCommand = value; }
        }

        private void Click(object obj)
        {
            try
            {
                if (obj == null)
                    return;

                var convert = (m_ColorPicker)obj;
                _selectedColor = convert.Name;
                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #endregion



        #region Function

        #endregion
    }
}
