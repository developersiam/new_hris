﻿using DomainModelHris;
using HRISSystem.Helper;
using HRISSystem.MVVM.View.ShiftWork;
using HRISSystem.ViewModel;
using HRISSystemBL;
using MaterialDesignThemes.Wpf;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace HRISSystem.MVVM.ViewModel.ShiftWork
{
    public class vm_EmployeeShiftAssigned : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_EmployeeShiftAssigned()
        {
            _fromDate = null;
            _toDate = null;
        }


        #region Properties
        private Windows _window;

        public Windows Window
        {
            get { return _window; }
            set { _window = value; }
        }

        private string _employeeID;

        public string EmployeeID
        {
            get { return _employeeID; }
            set
            {
                _employeeID = value;
                if (value != null)
                {
                    EmployeeInfoBinding();
                    ShiftControlListDataBinding();
                }
            }
        }

        private DateTime? _fromDate;

        public DateTime? FromDate
        {
            get { return _fromDate; }
            set
            {
                _fromDate = value;
                if (value != null)
                    ShiftControlListDataBinding();
            }
        }

        private DateTime? _toDate;

        public DateTime? ToDate
        {
            get { return _toDate; }
            set
            {
                _toDate = value;
                if (value != null)
                    ShiftControlListDataBinding();
            }
        }

        private string _employeeName;

        public string EmployeeName
        {
            get { return _employeeName; }
            set { _employeeName = value; }
        }

        private int _assignedDays;

        public int AssignedDays
        {
            get { return _assignedDays; }
            set { _assignedDays = value; }
        }

        private int _unassignedDays;

        public int UnAssignedDays
        {
            get { return _unassignedDays; }
            set { _unassignedDays = value; }
        }
        #endregion



        #region List
        private List<ShiftControl> _shiftControlList;

        public List<ShiftControl> ShiftControlList
        {
            get { return _shiftControlList; }
            set { _shiftControlList = value; }
        }

        private List<ShiftControl> _carlendarShiftList;

        public List<ShiftControl> CalendarShiftList
        {
            get { return _carlendarShiftList; }
            set { _carlendarShiftList = value; }
        }

        #endregion



        #region Command
        private ICommand _onChangeShiftCommand;

        public ICommand OnChangeShiftCommand
        {
            get { return _onChangeShiftCommand ?? (_onChangeShiftCommand = new RelayCommand(ChangeShift)); }
            set { _onChangeShiftCommand = value; }
        }

        private void ChangeShift(object obj)
        {
            try
            {
                if (obj == null)
                    return;

                var shiftControl = (ShiftControl)obj;
                var window = new ChangeShiftWork();
                var vm = new vm_ChangeShiftWork();
                window.DataContext = vm;
                vm.ShiftControl = shiftControl;
                vm.ShiftControl.Noted = shiftControl.Noted;
                vm.EmployeeID = _employeeID;
                vm.Window = window;
                window.ShowDialog();
                ShiftControlListDataBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion



        #region Function
        private void ShiftControlListDataBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_employeeID))
                    return;

                if (_fromDate == null || _toDate == null)
                    return;

                _shiftControlList = BusinessLayerServices.ShiftBL()
                    .GetShiftControlListByEmployeeID(_employeeID)
                    .Where(x => x.ValidFrom >= _fromDate && x.ValidFrom <= _toDate)
                    .ToList();

                //convert null datetime to datetime.
                var from = (DateTime)_fromDate;
                var to = (DateTime)_toDate;

                //Populate a employee shift assigened calendar.
                _carlendarShiftList = new List<ShiftControl>();
                while (from <= to)
                {
                    var item = _shiftControlList.SingleOrDefault(x => x.ValidFrom == from);
                    if (item != null)
                    {
                        item.Noted = "Shift ID : " + item.Shift_ID + 
                            " [" + ((TimeSpan)item.Shift.StartTime).ToString("hh':'mm") + 
                            " - " + ((TimeSpan)item.Shift.EndTime).ToString("hh':'mm") + "]";
                        _carlendarShiftList.Add(item);
                    }

                    else
                        _carlendarShiftList.Add(new ShiftControl
                        {
                            Shift_ID = "NA",
                            Shift = new DomainModelHris.Shift
                            {
                                ShiftName = "LightGray"
                            },
                            Noted = "Shift ID : N/A [-]",
                            ValidFrom = from
                        });

                    from = from.AddDays(1);
                }

                _assignedDays = _shiftControlList.Count;
                _unassignedDays = (int)(to - from).TotalDays - _assignedDays;
                RaisePropertyChangedEvent(nameof(CalendarShiftList));
                RaisePropertyChangedEvent(nameof(ShiftControlList));
                RaisePropertyChangedEvent(nameof(AssignedDays));
                RaisePropertyChangedEvent(nameof(UnAssignedDays));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void EmployeeInfoBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_employeeID))
                    return;

                var employee = BusinessLayerServices.EmployeeBL()
                    .GetEmployeeByEmployeeID(_employeeID);
                _employeeName = employee.Person.FirstNameTH + " " + employee.Person.LastNameTH;
                RaisePropertyChangedEvent(nameof(EmployeeName));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
