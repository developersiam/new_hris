﻿using DomainModelHris;
using DomainModelPayroll;
using HRISSystem.Form.Report;
using HRISSystem.Form.Walefare;
using HRISSystem.Helper;
using HRISSystem.MVVM.View.ShiftWork;
using HRISSystem.MVVM.ViewModel.ShiftWork;
using HRISSystem.ViewModel;
using HRISSystemBL;
using HRISSystemBL.BL.HRISBL;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using static System.Data.Entity.Infrastructure.Design.Executor;

namespace HRISSystem.MVVM.ViewModel.Notice
{

    internal class vm_Notice : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_Notice()
        {
            _fromDate = DateTime.Now.AddDays(-1);
            _toDate = DateTime.Now.AddDays(-1);
            //_fromDate = new DateTime(2023, 06, 09);
            //_toDate = new DateTime(2023, 06, 09);
            DepartmentListDataBinding();
            RenderReport();
        }

        #region Properties

        private DateTime? _fromDate;

        public DateTime? FromDate
        {
            get { return _fromDate; }
            set
            {
                _fromDate = value;
                RenderReport();
            }
        }

        private DateTime? _toDate;

        public DateTime? ToDate
        {
            get { return _toDate; }
            set
            {
                _toDate = value;
                RenderReport();
            }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        #endregion

        #region Command

        private ICommand _onCallDataCommand;

        public ICommand OnCallDataCommand
        {
            get { return _onCallDataCommand ?? (_onCallDataCommand = new RelayCommand(CallData)); }
            set { _onCallDataCommand = value; }
        }

        private void CallData(object obj)
        {
            RenderReport();
        }

        private ICommand _onShiftChangeCommand;

        public ICommand OnShiftChangeCommand
        {
            get { return _onShiftChangeCommand ?? (_onShiftChangeCommand = new RelayCommand(ChangeShift)); }
            set { _onShiftChangeCommand = value; }
        }

        private void ChangeShift(object obj)
        {
            try
            {
                if (obj == null) return;

                var selected = (sp_GetNotify_Result)obj;
                var _shiftControl = new ShiftControl 
                { 
                    Employee_ID = selected.Employee_ID,
                    Shift_ID = selected.Shift_ID,
                    ValidFrom = selected.CalendarDate.Value,
                    EndDate = selected.CalendarDate.Value
                };
                var shiftControl = BusinessLayerServices.ShiftBL().GetSingleShiftControl(_shiftControl);

                var window = new ChangeShiftWork();
                var vm = new vm_ChangeShiftWork();
                window.DataContext = vm;
                vm.ShiftControl = shiftControl;
                vm.EmployeeID = _shiftControl.Employee_ID;
                vm.Window = window;
                window.ShowDialog();
                RenderReport();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onManualWorkdateCommand;

        public ICommand OnManualWorkdateCommand
        {
            get { return _onManualWorkdateCommand ?? (_onManualWorkdateCommand = new RelayCommand(ManualWorkdate)); }
            set { _onManualWorkdateCommand = value; }
        }

        private void ManualWorkdate(object obj)
        {
            try
            {
                if (obj == null) return;

                var selected = (sp_GetNotify_Result)obj;

                var window = new ManualWorkingDateWindow();
                window.EmployeeIDTextBox.Text = selected.Employee_ID;

                var lot = BusinessLayerServices.PayrollBL().GetSingleLotByDate(selected.CalendarDate.Value);
                if (lot == null) throw new NullReferenceException("");

                window.LotTextBox.Text = string.Format("{0}/{1}", lot.Lot_Month, lot.Lot_Year); ;
                window.LotFromDatePicker.SelectedDate = lot.Start_date;
                window.LotToDatePicker.SelectedDate = lot.Finish_date;
                window.ManualDatePicker.DisplayDateStart = lot.Start_date;
                window.ManualDatePicker.DisplayDateEnd = lot.Finish_date;
                window.ManualDatePicker.SelectedDate = selected.CalendarDate;
                window.lotNumber = lot;
                window.ShowDialog();
                RenderReport();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onCheckedCommand;

        public ICommand OnCheckedCommand
        {
            get { return _onCheckedCommand ?? (_onCheckedCommand = new RelayCommand(CheckBoxChecked)); }
            set { _onCheckedCommand = value; }
        }

        private void CheckBoxChecked(object obj)
        {
            try
            {
                var selected = (m_DepartmentDropDownList)obj;
                if (selected == null) return;

                var tempList = new List<m_DepartmentDropDownList>();
                if (selected.DepartmentCode == "All")
                {
                    if (selected.IsSelected == true)
                    {
                        foreach (var item in _departmentList)
                        {
                            item.IsSelected = true;
                            tempList.Add(item);
                        }
                    }
                    else
                    {
                        foreach (var item in _departmentList)
                        {
                            item.IsSelected = false;
                            tempList.Add(item);
                        }
                    }

                    _departmentList.Clear();
                    _departmentList = tempList;
                    RaisePropertyChangedEvent(nameof(DepartmentList));
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }


        #endregion

        #region List


        private List<sp_GetNotify_Result> _notifyList;

        public List<sp_GetNotify_Result> NotifyList
        {
            get { return _notifyList; }
            set { _notifyList = value; }
        }

        private List<m_DepartmentDropDownList> _departmentList;

        public List<m_DepartmentDropDownList> DepartmentList
        {
            get { return _departmentList; }
            set { _departmentList = value; }
        }


        #endregion

        #region Function

        private void DepartmentListDataBinding()
        {
            try
            {
                _departmentList = new List<m_DepartmentDropDownList>();
                _departmentList
                    .Add(new m_DepartmentDropDownList
                    {
                        IsSelected = true,
                        DepartmentCode = "All",
                        DepartmentName = "Selected All/Unselected All"
                    });
                foreach (var item in HRISBLServices.DepartmentBL().GetAll())
                {
                    _departmentList.Add(new m_DepartmentDropDownList
                    {
                        IsSelected = true,
                        DepartmentCode = item.DeptCode,
                        DepartmentName = item.DeptCode + "\t" + item.DeptName,
                    });
                }
                RaisePropertyChangedEvent(nameof(DepartmentList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void RenderReport()
        {
            try
            {
                if (_fromDate == null || _toDate == null) return;
                if (_fromDate > _toDate) throw new Exception("โปรดระบุช่วงวันที่ From Date และ To Date ให้ถูกต้อง");

                var tempNotify = HRISBLServices.NotifyBL().GetNotifyList(_fromDate.Value, _toDate.Value);

                var isSelectedAll = _departmentList.SingleOrDefault(s => s.DepartmentCode == "All").IsSelected;
                if (isSelectedAll)
                {
                    _notifyList = tempNotify.ToList();
                }
                else
                {
                    var selectedDept = new List<string>();

                    foreach (var item in _departmentList.Where(w => w.IsSelected == true))
                    {
                        selectedDept.Add(item.DepartmentCode);
                    }

                    var filteredNotify = tempNotify.Where(w => selectedDept.Contains(w.DEPT)).ToList();
                    _notifyList = filteredNotify;
                }

                _totalRecord = _notifyList.Count;
                RaisePropertyChangedEvent(nameof(NotifyList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #endregion
    }
}
