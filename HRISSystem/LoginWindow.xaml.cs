﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HRISSystem.MVVM.View.ShiftWork;
using HRISSystem.MVVM.ViewModel.ShiftWork;
using HRISSystem.Shared;
namespace HRISSystem
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
            UsernameTextbox.Focus();
            //MainWindow main = new MainWindow(); 
            //main.Title = string.Format("HRIS System - [{0}]", SingletonConfiguration.getInstance().Username);
            //main.ShowDialog();
            //Clear();
        }

        private void LoginTextbox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) Login();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            Login();
        }

        private void Login()
        {
            try
            {
                if (string.IsNullOrEmpty(UsernameTextbox.Text)) throw new Exception("กรุณาระบุชื่อผู้ใช้งาน");
                if (string.IsNullOrEmpty(PasswordTextbox.Password)) throw new Exception("กรุณาระบุรหัสผ่าน");
                if (HRISService.UserAuthentication().GetLogin(UsernameTextbox.Text, PasswordTextbox.Password))
                {
                    MainWindow main = new MainWindow();
                    main.Title = string.Format("HRIS System - [{0}]", SingletonConfiguration.getInstance().Username);
                    main.ShowDialog();
                    Clear();
                }
                else MessageBox.Show("รหัสผ่านไม่ถูกต้อง. หรือผู้ใช้ไม่มีสิทธิ์เข้าใช้งานระบบ กรุณาตรวจสอบ", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            UsernameTextbox.Text = "";
            PasswordTextbox.Password = "";
            UsernameTextbox.Focus();
        }

        private void PasswordTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            PasswordTextbox.SelectAll();
        }

        private void UsernameTextbox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(UsernameTextbox.Text)) UsernameTextbox.Text = HRISService.DataCorrector().FirstCapital(UsernameTextbox.Text);
        }

        private void UsernameTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            UsernameTextbox.SelectAll();
        }
    }
}
