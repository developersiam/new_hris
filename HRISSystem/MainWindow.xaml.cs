﻿using System.Windows;
using HRISSystem.Shared;
using System.Windows.Controls;
using HRISSystem.MVVM.View.ShiftWork;
using HRISSystem.MVVM.ViewModel.ShiftWork;

namespace HRISSystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow(Page page)
        {
            InitializeComponent();
        }

        public MainWindow()
        {
            InitializeComponent();

            //SingletonConfiguration.getInstance().Username = "m.tanainan";
            //SingletonConfiguration.getInstance().DepartmentCode = "ACC";
            var deptCode = SingletonConfiguration.getInstance().DepartmentCode.ToUpper();
            if (deptCode == "ACC")
                payrollMenu();
            if (deptCode == "HRM")
                hrisMenu();

            //MainFrame.Navigate(new MVVM.View.Notice.NoticePage());
        }

        private void payrollMenu()
        {
            var username = SingletonConfiguration.getInstance().Username.ToLower();
            //ProfileMenu.IsEnabled = false;
            MoveEmployeeMenu.IsEnabled = false;
            NotPayrollMenu.IsEnabled = false;

            ManualWorkdateMenu.Visibility = Visibility.Collapsed;
            WalefareMenu.Visibility = username == "netchanok" ? Visibility.Visible : Visibility.Collapsed; //2022-05 Netchanok can access HR's OT Page
            OffSiteMenu.Visibility = Visibility.Collapsed;
            LeaveMenu.Visibility = Visibility.Collapsed;
            LeaveSetupMenu.Visibility = Visibility.Collapsed;
            SocialWalefareMenu.Visibility = Visibility.Collapsed;
            //ToPayrollMenu.Visibility = Visibility.Collapsed;
            TraningMenu.Visibility = Visibility.Collapsed;
            ShiftRefMenu.Visibility = Visibility.Collapsed;

            CropRefMenu.Visibility = Visibility.Collapsed;
            BranchRefMenu.Visibility = Visibility.Collapsed;
            LeaveTypeRefMenu.Visibility = Visibility.Collapsed;
            ShiftRefMenu.Visibility = Visibility.Collapsed;
            OrganizeRefMenu.Visibility = Visibility.Collapsed;
            PositionRefMenu.Visibility = Visibility.Collapsed;

            RPTHR011Menu.Visibility = Visibility.Collapsed;
            RPTHR012Menu.Visibility = Visibility.Collapsed;
            RPTHR015Menu.Visibility = Visibility.Collapsed;
            RPTHR017Menu.Visibility = Visibility.Collapsed;
            RPTHR018Menu.Visibility = Visibility.Collapsed;
            RPTHR020Menu.Visibility = Visibility.Collapsed;
            RPTHR022Menu.Visibility = Visibility.Collapsed;
            RPTHR023Menu.Visibility = Visibility.Collapsed;
            RPTHR024Menu.Visibility = Visibility.Collapsed;
            RPTHR025Menu.Visibility = Visibility.Collapsed;
            if (username == "m.tanainan") RPTPR002Menu.Visibility = Visibility.Collapsed;
            if (username == "m.tanainan") RPTPR007Menu.Visibility = Visibility.Collapsed;
        }

        private void hrisMenu()
        {
            PayrollMenu.Visibility = Visibility.Collapsed;
            RPTPR001Menu.Visibility = Visibility.Collapsed;
            RPTPR002Menu.Visibility = Visibility.Collapsed;
            RPTPR003Menu.Visibility = Visibility.Visible; // 08/2021 P'Pin ask for visible ps. P'Day meeting
            RPTPR004Menu.Visibility = Visibility.Collapsed;
            RPTPR005Menu.Visibility = Visibility.Collapsed;
            RPTPR006Menu.Visibility = Visibility.Collapsed;
            RPTPR007Menu.Visibility = Visibility.Collapsed;

            
        }

        private void MainPageMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.ShiftControls.ShiftToShiftPage());
        }

        private void ProfileMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.Employees.Persons.Person_Page());
        }

        private void EmployeeMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.Employees.Employees.EmployeePage());
        }

        private void OffSiteMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.Walefare.OffSiteWorkPage());
        }

        private void LeaveMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.Walefare.LeavePage());
        }

        private void OTMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.Walefare.OTPage());
        }

        private void SocialWalefareMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.Walefare.SocialWalefarePage());
        }

        private void TraningMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.Trainings.TraingPage());
        }

        private void DeptToShiftMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.ShiftControls.EmployeeToShiftPage());
        }

        private void ShiftToShiftMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.ShiftControls.ShiftToShiftPage());
        }

        private void OTPayrollMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.ToPayroll.OTToPayrollPage());
        }

        private void WorkingDateMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.ToPayroll.WorkingDateToParollPage());
        }

        private void HRLockLotMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.ToPayroll.HRLotSetupPage());
        }

        private void CropRefMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.References.CropReference_Page());
        }

        private void BranchRefMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.References.BranchRef_Page());
        }

        private void LeaveTypeRefMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.References.LeaveType_Page());
        }

        private void ShiftRefMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.References.Shift_Page());
        }

        private void OrganizeRefMenu_Click(object sender, RoutedEventArgs e)
        {
            // throw new NotImplementedException();
        }

        private void ExitMenu_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("ต้องการออกจากโปรแกรมหรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes) Close();
        }

        private void MoveEmployeeMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.Employees.Employees.MoveDepartmentPage());
        }

        private void ShiftControlMenu_Click(object sender, RoutedEventArgs e)
        {
            //MainFrame.Navigate(new Form.ShiftControls.ShiftControlPage());
            MainFrame.Navigate(new Form.ShiftControls.EmployeeToShiftPage());
        }

        private void RPTHR011Menu_Click(object sender, RoutedEventArgs e)
        {
            Form.Report.HR011Report w = new Form.Report.HR011Report();
            w.ShowDialog();
        }

        private void RPTHR015Menu_Click(object sender, RoutedEventArgs e)
        {
            Form.Report.HR015Report w = new Form.Report.HR015Report();
            w.ShowDialog();
        }

        private void RPTHR012Menu_Click(object sender, RoutedEventArgs e)
        {
            Form.Report.HR012Report w = new Form.Report.HR012Report();
            w.ShowDialog();
        }

        private void RPTHR017Menu_Click(object sender, RoutedEventArgs e)
        {
            //Form.Report.HR017Report w = new Form.Report.HR017Report();
            //w.ShowDialog();
        }

        private void RPTHR022Menu_Click(object sender, RoutedEventArgs e)
        {
            //Form.Report.HR022Report w = new Form.Report.HR022Report();
            //w.ShowDialog();
        }

        private void RPTHR023Menu_Click(object sender, RoutedEventArgs e)
        {
            //Form.Report.HR023Report w = new Form.Report.HR023Report();
            //w.ShowDialog();
        }

        private void RPTHR024Menu_Click(object sender, RoutedEventArgs e)
        {
            //Form.Report.HR024Report w = new Form.Report.HR024Report();
            //w.ShowDialog();
        }

        private void RPTHR025Menu_Click(object sender, RoutedEventArgs e)
        {
            //Form.Report.HR025Report w = new Form.Report.HR025Report();
            //w.ShowDialog();
        }

        private void RPTHR018Menu_Click(object sender, RoutedEventArgs e)
        {
            Form.Report.HR018Report w = new Form.Report.HR018Report();
            w.ShowDialog();
        }

        private void RPTHR020Menu_Click(object sender, RoutedEventArgs e)
        {
            //Form.Report.HR020Report w = new Form.Report.HR020Report();
            //w.ShowDialog();
        }

        private void RPTPR001Menu_Click(object sender, RoutedEventArgs e)
        {
            var w = new Form.Report.PR001Report();
            w.ShowDialog();
        }

        private void RPTPR002Menu_Click(object sender, RoutedEventArgs e)
        {
            var w = new Form.Report.PR002Report();
            w.ShowDialog();
        }

        private void PositionRefMenu_Click(object sender, RoutedEventArgs e)
        {
            var w = new Form.References.PositionWindow();
            w.ShowDialog();
        }

        private void PayrollMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.Payrolls.PayrollPage());
        }

        private void LotnumberRefMenu_Click(object sender, RoutedEventArgs e)
        {
            var w = new Form.References.LotNumberSetupWindow();
            w.ShowDialog();
        }

        private void RPTPR003Menu_Click(object sender, RoutedEventArgs e)
        {
            var w = new Form.Report.PR003Report();
            w.ShowDialog();
        }

        private void RPTPR004Menu_Click(object sender, RoutedEventArgs e)
        {
            var w = new Form.Report.PR004Report();
            w.ShowDialog();
        }

        private void RPTPR005Menu_Click(object sender, RoutedEventArgs e)
        {
            var w = new Form.Report.PR005Report();
            w.ShowDialog();
        }

        private void RPTPR006Menu_Click(object sender, RoutedEventArgs e)
        {
            var w = new Form.Report.PR006Report();
            w.ShowDialog();
        }

        private void ManualWorkdateMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.ManualWorkDates.ManualDatePage());
        }

        private void RPTPR007Menu_Click(object sender, RoutedEventArgs e)
        {
            var w = new Form.Report.PR007Report();
            w.ShowDialog();
        }

        private void testMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.ToPayroll.Page1());
        }

        private void ToPayrollMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.ToPayroll.ToPayrollPage());
        }

        private void WageSettingMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.TemporaryWorker.SetupPage());
        }

        private void TemporaryWageMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.TemporaryWorker.TemporaryWorkerPage());
        }

        private void LeaveSetupMenu_Click(object sender, RoutedEventArgs e)
        {
            Navigate_LeaveSetupPage();
        }

        public void Navigate_LeaveSetupPage()
        {
            MainFrame.Navigate(new Form.Walefare.LeaveSetupPage());
        }

        private void NotPayrollMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.Employees.NotPayrollPage());
        }

        private void RPTPR009Menu_Click(object sender, RoutedEventArgs e)
        {
            var w = new Form.Report.PR009Report();
            w.ShowDialog();
        }

        private void TwiMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new Form.Payrolls.WithholdingTaxCertificatePage_Twi50());
        }

        private void NoticeMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new MVVM.View.Notice.NoticePage());
        }
    }
}
