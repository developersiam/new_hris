﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystem.ViewModel
{
    public class PrintSlipViewModel
    {
        public string PayrollDate { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentCode { get; set; }
        public string TitleNameTH { get; set; }
        public string FirstNameTH { get; set; }
        public string LastNameTH { get; set; }
        public string EmployeeIdHRIS { get; set; }
        public string EmployeeIdPayroll { get; set; }
        public string StaffType { get; set; }
        public string StaffTypeDescription { get; set; }
    }
}
