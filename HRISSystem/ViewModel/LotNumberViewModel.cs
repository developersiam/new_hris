﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelPayroll;

namespace HRISSystem.ViewModel
{
    public class LotNumberViewModel : Lot_Number
    {
        public string LotNumber { get; set; }
        public bool Lock_Hr_Bool { get; set; }
        public bool Lock_Acc_Bool { get; set; }
        public bool Lock_Acc_Labor_Bool { get; set; }
        public bool Lock_Hr_Labor_Bool { get; set; }
    }
}
