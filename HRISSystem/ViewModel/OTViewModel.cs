﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystem.ViewModel
{
    public class OTViewModel
    {
        public string DEPT_CODE { get; set; }
        public string Employee_ID { get; set; }
        public string AccCode { get; set; }
        public string FingerScanID { get; set; }
        public string TitleNameTH { get; set; }
        public string FirstNameTH { get; set; }
        public string LastNameTH { get; set; }
        public Nullable<System.DateTime> OTDATE { get; set; }
        public Nullable<decimal> OTnormal { get; set; }
        public Nullable<decimal> Holiday { get; set; }
        public Nullable<decimal> OTHoliday { get; set; }
        public Nullable<decimal> OTWeekend { get; set; }
        public Nullable<decimal> OTSpecial { get; set; }
        public string Remark { get; set; }
        public string PayrollLot { get; set; }
        public string OTType { get; set; }
        public string RequestType { get; set; }
        public string RequestTypeName { get; set; }
        public string AdminApproveStatus { get; set; }
        public string ManagerApproveStatus { get; set; }
        public string FinalApproverStatus { get; set; }
        public string HRApproveStatus { get; set; }
        public int? Stafftype { get; set; }
    }
}
