﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystem.ViewModel
{
    public class NotScanInAndOutViewModel
    {
        public string EmployeeID { get; set; }
        //public string EMPACCCode { get; set; }
        public string FingerScanID { get; set; }
        public string Titlename { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string StaffType { get; set; }
        public int NCount { get; set; }

    }
}
