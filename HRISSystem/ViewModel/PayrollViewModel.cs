﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelPayroll;

namespace HRISSystem.ViewModel
{
    public class PayrollViewModel 
    {
        public string staffTypeDescription { get; set; }
        public decimal? SumOT { get; set; }
        public decimal? SumOTH { get; set; }
        public decimal? SumOTHH { get; set; }
        public decimal? PND { get; set; }
        public decimal? SumSpecial { get; set; }
        public Payroll _payroll { get; set; }
    }
}
