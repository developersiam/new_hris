﻿using DomainModelHris;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystem.ViewModel
{
    public class LeaveSetupViewModel
    {
        public LeaveSetup LeaveSetups { get; set; }
        public bool IsExpired { get; set; }
    }
}
