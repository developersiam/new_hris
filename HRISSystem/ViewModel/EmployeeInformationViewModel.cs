﻿using DomainModelHris;

namespace HRISSystem.ViewModel
{
    public class EmployeeInformationViewModel : sp_OT_GetEmployeeInformation_Result
    {
        //public string EmployeeID { get; set; }
        //public string AccountID { get; set; }
        //public string FingerscanID { get; set; }
        public string FullNameTH { get; set; }
        public string FullNameEN { get; set; }
        //public string StaffType { get; set; }
        //public string Position { get; set; }
    }
}
