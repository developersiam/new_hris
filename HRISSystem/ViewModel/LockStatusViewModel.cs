﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystem.ViewModel
{
    public class LockStatusViewModel
    {
        public string StatusName { get; set; }
        public string StatusValue { get; set; }
    }
}
