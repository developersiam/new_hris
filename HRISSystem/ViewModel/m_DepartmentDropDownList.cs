﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystem.ViewModel
{
    public class m_DepartmentDropDownList
    {
        public bool IsSelected { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }  
    }
}
