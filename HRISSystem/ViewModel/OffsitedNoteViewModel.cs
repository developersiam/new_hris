﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystem.ViewModel
{
    public class OffsiteNotedViewModel
    {
        public int NoteFlag { get; set; }
        public string NoteFlagName { get; set; }
    }
}
