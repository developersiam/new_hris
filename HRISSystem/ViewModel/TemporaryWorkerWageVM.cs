﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelPayroll;

namespace HRISSystem.ViewModel
{
    public class TemporaryWorkerWageVM : sp_GetTemporaryWorkerWage_Result
    {
        public decimal? NetSalary { get; set; }
        public decimal? OTnormal { get; set; }
        public decimal? Holiday { get; set; }
        public decimal? OTHoliday { get; set; }
        public decimal? OTSpecial { get; set; }
        public decimal? SumOT { get; set; }
        public decimal? SumOTH { get; set; }
        public decimal? SumOTHH { get; set; }
        public decimal? SumSpecial { get; set; }
        public decimal? TotalIncome { get; set; }
        public decimal? CutWage { get; set; }
        public decimal? NetIncome { get; set; }
    }
}
