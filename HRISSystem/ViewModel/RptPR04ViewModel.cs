﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystem.ViewModel
{
    public class RptPR04ViewModel
    {
        public string Department { get; set; }
        public string Employee_id { get; set; }
        public string Title { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string IDCard { get; set; }
        public string BankAccount { get; set; }
        public decimal? Salary { get; set; }
        public decimal? NetSalary { get; set; }
        public decimal? Wage { get; set; }
        public double? WorkingDate { get; set; }
        public decimal? OTRate { get; set; }
        public decimal? OTHRate { get; set; }
        public decimal? OTHHRate { get; set; }
        public decimal? OTHWRate { get; set; }
        public double? OTCount { get; set; }
        public double? OTHCount { get; set; }
        public double? OTHHCount { get; set; }
        public double? OTHWCount { get; set; }
        public decimal? OTSum { get; set; }
        public decimal? OTHSum { get; set; }
        public decimal? OTHHSum { get; set; }
        public decimal? OTHWSum { get; set; }
        public decimal? Special { get; set; }
        public decimal? Bonus { get; set; }
        public Nullable<decimal> Lunch_sup { get; set; }
        public Nullable<decimal> Night_sup { get; set; }
        public decimal? TotalIncomeLabour { get; set; }
        public decimal? TotalIncome { get; set; }
        public decimal? TaxLabour { get; set; }
        public decimal? Tax { get; set; }
        public decimal? SSO { get; set; }
        public decimal? Cutwage { get; set; }
        public decimal? StudentLoan { get; set; }
        public decimal? NetIncome { get; set; }
    }
}
