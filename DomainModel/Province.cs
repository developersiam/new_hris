//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModelHris
{
    using System;
    using System.Collections.Generic;
    
    public partial class Province
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Province()
        {
            this.Districts = new HashSet<District>();
            this.IDCardInfoes = new HashSet<IDCardInfo>();
        }
    
        public string Province_ID { get; set; }
        public string Region_ID { get; set; }
        public string ProvinceNameTH { get; set; }
        public string ProvinceNameEN { get; set; }
        public string ProvinceInitialTH { get; set; }
        public string ProvinceInitialEN { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [System.Xml.Serialization.XmlIgnore] public virtual ICollection<District> Districts { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [System.Xml.Serialization.XmlIgnore] public virtual ICollection<IDCardInfo> IDCardInfoes { get; set; }
        public virtual Region Region { get; set; }
    }
}
