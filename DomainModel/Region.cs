//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModelHris
{
    using System;
    using System.Collections.Generic;
    
    public partial class Region
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Region()
        {
            this.Provinces = new HashSet<Province>();
        }
    
        public string Region_ID { get; set; }
        public string RegionNameTH { get; set; }
        public string RegionNameEN { get; set; }
        public string RegionNameInitialTH { get; set; }
        public string RegionNameInitialEN { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [System.Xml.Serialization.XmlIgnore] public virtual ICollection<Province> Provinces { get; set; }
    }
}
