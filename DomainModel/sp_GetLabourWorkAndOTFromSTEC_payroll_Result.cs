//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModelHris
{
    using System;
    
    public partial class sp_GetLabourWorkAndOTFromSTEC_payroll_Result
    {
        public string LOT_YEAR { get; set; }
        public string LOT_MONTH { get; set; }
        public string Employee_id { get; set; }
        public string Fname { get; set; }
        public string Lname { get; set; }
        public string Dept_code { get; set; }
        public string Dept_name { get; set; }
        public Nullable<double> CWork { get; set; }
        public string FingerScanID { get; set; }
        public string HRISEMPLOYEE_ID { get; set; }
        public Nullable<double> Ot_hour { get; set; }
        public Nullable<double> Ot_holiday { get; set; }
        public Nullable<double> Ot_hour_holiday { get; set; }
    }
}
