//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModelHris
{
    using System;
    using System.Collections.Generic;
    
    public partial class Institute
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Institute()
        {
            this.Educations = new HashSet<Education>();
        }
    
        public string Institute_ID { get; set; }
        public string InstituteNameTH { get; set; }
        public string InstituteNameEN { get; set; }
        public string InstituteNameIntitialTH { get; set; }
        public string InstituteNameIntitialEN { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [System.Xml.Serialization.XmlIgnore] public virtual ICollection<Education> Educations { get; set; }
    }
}
