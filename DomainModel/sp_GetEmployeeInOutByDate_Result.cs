//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModelHris
{
    using System;
    
    public partial class sp_GetEmployeeInOutByDate_Result
    {
        public Nullable<int> userid { get; set; }
        public string badgenumber { get; set; }
        public string FingerScanID { get; set; }
        public string Employee_ID { get; set; }
        public string Noted { get; set; }
        public Nullable<int> defaultdeptid { get; set; }
        public string code { get; set; }
        public string TitleNameTH { get; set; }
        public string FirstNameTH { get; set; }
        public string LastNameTH { get; set; }
        public System.DateTime checktime { get; set; }
        public string sn_name { get; set; }
        public Nullable<System.DateTime> check_date { get; set; }
        public Nullable<System.TimeSpan> check_time { get; set; }
    }
}
