//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModelHris
{
    using System;
    using System.Collections.Generic;
    
    public partial class PositionType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PositionType()
        {
            this.LeaveTypeBenefits = new HashSet<LeaveTypeBenefit>();
            this.PositionOrganizations = new HashSet<PositionOrganization>();
        }
    
        public string PositionType_ID { get; set; }
        public string PositionTypeNameTH { get; set; }
        public string PositionTypeNameEN { get; set; }
        public Nullable<bool> SalaryFlag { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ValidFrom { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [System.Xml.Serialization.XmlIgnore] public virtual ICollection<LeaveTypeBenefit> LeaveTypeBenefits { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [System.Xml.Serialization.XmlIgnore] public virtual ICollection<PositionOrganization> PositionOrganizations { get; set; }
    }
}
