//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModelHris
{
    using System;
    using System.Collections.Generic;
    
    public partial class FinalApprover_Type
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FinalApprover_Type()
        {
            this.OTOnlineSetupFinalApproverDetails = new HashSet<OTOnlineSetupFinalApproverDetail>();
        }
    
        public int FinalApprover_Type_ID { get; set; }
        public string Description { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [System.Xml.Serialization.XmlIgnore] public virtual ICollection<OTOnlineSetupFinalApproverDetail> OTOnlineSetupFinalApproverDetails { get; set; }
    }
}
