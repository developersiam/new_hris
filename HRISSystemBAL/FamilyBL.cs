﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelHris;
using HRISSystemDAL;

namespace HRISSystemBL
{
    public interface IFamilyBL
    {
        List<FamilyMember> GetFamilyListByPersonID(string PersonID);
        string GetNewFamilyID();
        void AddFamily(FamilyMember item);
        void UpdateFamily(FamilyMember item);
        void DeleteFamily(FamilyMember item);
        FamilyMember GetSingleFamilyMember(string familyMemberID, string personID);
        List<FamilyMemberType> GetAllRelationshipList();
        string GetNewRelationshipID();
        void AddRelationship(FamilyMemberType item);
        void UpdateRelationship(FamilyMemberType item);
        void DeleteRelationship(FamilyMemberType item);
        FamilyMemberType GetSingleFamilyMemberType(string familyMemberTypeID);
        List<VitalStau> GetAllVitalstatusList();

    }
    public class FamilyBL : IFamilyBL
    {
        public void AddFamily(FamilyMember item)
        {
            DataAccessLayerServices.FamilyMemberRepository().Add(item);
        }

        public void AddRelationship(FamilyMemberType item)
        {
            DataAccessLayerServices.FamilyMemberTypeRepository().Add(item);
        }

        public void DeleteFamily(FamilyMember item)
        {
            DataAccessLayerServices.FamilyMemberRepository().Remove(item);
        }

        public void DeleteRelationship(FamilyMemberType item)
        {
            DataAccessLayerServices.FamilyMemberTypeRepository().Remove(item);
        }

        public List<FamilyMemberType> GetAllRelationshipList()
        {
            return DataAccessLayerServices.FamilyMemberTypeRepository().GetAll().ToList();
        }

        public List<VitalStau> GetAllVitalstatusList()
        {
            return DataAccessLayerServices.VitalStauRepository().GetAll().ToList();
        }

        public List<FamilyMember> GetFamilyListByPersonID(string PersonID)
        {
            return DataAccessLayerServices.FamilyMemberRepository().GetList(g => g.Person_ID == PersonID, g => g.FamilyMemberType, g => g.TitleName, g => g.VitalStau).ToList();
        }

        public string GetNewFamilyID()
        {
            var listString = DataAccessLayerServices.FamilyMemberRepository().GetAll().Select(s => s.FamilyMember_ID).ToList();
            return BusinessLayerServices.DataCorrector().GetMaxIDFromListString(listString);
        }

        public string GetNewRelationshipID()
        {
            var listString = DataAccessLayerServices.FamilyMemberTypeRepository().GetAll().Select(s => s.FamilyMemberType_ID).ToList();
            return BusinessLayerServices.DataCorrector().GetMaxIDFromListString(listString);
        }

        public FamilyMember GetSingleFamilyMember(string familyMemberID, string personID)
        {
            return DataAccessLayerServices.FamilyMemberRepository().GetSingle(g => g.FamilyMember_ID == familyMemberID && g.Person_ID == personID);
        }

        public FamilyMemberType GetSingleFamilyMemberType(string familyMemberTypeID)
        {
            return DataAccessLayerServices.FamilyMemberTypeRepository().GetSingle(g => g.FamilyMemberType_ID == familyMemberTypeID);
        }

        public void UpdateFamily(FamilyMember item)
        {
            DataAccessLayerServices.FamilyMemberRepository().Update(item);
        }

        public void UpdateRelationship(FamilyMemberType item)
        {
            DataAccessLayerServices.FamilyMemberTypeRepository().Update(item);
        }
    }
}
