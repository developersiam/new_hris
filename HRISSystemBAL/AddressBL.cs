﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelHris;
using HRISSystemDAL;

namespace HRISSystemBL
{
    public interface IAddressBL
    {
        List<Address> GetAddressListByPersonID(string PersonID);
        List<AddressType> GetAllAddressType();
        List<Country> GetAllCountry();
        List<Province> GetAllProvince();
        List<District> GetDistrictListByProvinceID(string ProvinceID);
        List<SubDistrict> GetSubDistrictListByDistrictID(string DistrictID);
        void AddAddress(Address item);
        void UpdateAddress(Address item);
        void DeleteAddress(Address item);
        Address GetSingleAddress(string addressTypeID, string personID);
    }
    public class AddressBL : IAddressBL
    {
        public void AddAddress(Address item)
        {
            DataAccessLayerServices.AddressRepository().Add(item);
        }

        public void DeleteAddress(Address item)
        {
            DataAccessLayerServices.AddressRepository().Remove(item);
        }

        public List<Address> GetAddressListByPersonID(string PersonID)
        {
            return DataAccessLayerServices.AddressRepository().GetList(g => g.Person_ID == PersonID, 
                                                                       g => g.AddressType, 
                                                                       g => g.Country, 
                                                                       g => g.SubDistrict, 
                                                                       g => g.SubDistrict.District, 
                                                                       g => g.SubDistrict.District.Province).ToList();
        }

        public List<AddressType> GetAllAddressType()
        {
            return DataAccessLayerServices.AddressTypeRepository().GetAll().ToList();
        }

        public List<Country> GetAllCountry()
        {
            return DataAccessLayerServices.CountryRepository().GetAll().ToList();
        }

        public List<Province> GetAllProvince()
        {
            return DataAccessLayerServices.ProvinceRepository().GetAll().OrderBy(o => o.ProvinceNameTH).ToList();
        }

        public List<District> GetDistrictListByProvinceID(string ProvinceID)
        {
            return DataAccessLayerServices.DistrictRepository().GetList(g => g.Province_ID == ProvinceID).OrderBy(o => o.DistrictNameTH).ToList();
        }

        public Address GetSingleAddress(string addressTypeID, string personID)
        {
            return DataAccessLayerServices.AddressRepository().GetSingle(g => g.AddressType_ID == addressTypeID && g.Person_ID == personID);
        }

        public List<SubDistrict> GetSubDistrictListByDistrictID(string DistrictID)
        {
            return DataAccessLayerServices.SubDistrictRepository().GetList(g => g.District_ID == DistrictID).OrderBy(o => o.SubDistrictNameTH).ToList();
        }

        public void UpdateAddress(Address item)
        {
            DataAccessLayerServices.AddressRepository().Update(item);
        }
    }
}
