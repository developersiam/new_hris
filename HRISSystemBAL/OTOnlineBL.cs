﻿using DomainModelHris;
using HRISSystemDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystemBL
{
    public interface IOTOnlineBL
    {
        List<DomainModelHris.Employee> GetOTOnlineEmployee(string searchType, int? searchStaff, int? searchStatus, string searchText, string requestFrom, string Username);
        OTOnlineSetupAdmin GetSingleOTOnlineAdmin(string Username, string EmployeeID);
        OTOnlineSetupAdminDetail GetSingleOTOnlineAdminDetail(int adminID, string deptCode);
        List<OTOnlineSetupAdminDetail> GetAllOTOnlineAdmin();
        List<OTOnlineSetupAdminDetail> GetListOTOnlineAdmin(string Username);
        void AddOTOnlineAdmin(OTOnlineSetupAdmin o);
        void AddOTOnlineAdminDetail(OTOnlineSetupAdminDetail o);
        void UpdateOTOnlineAdminDetail(OTOnlineSetupAdminDetail o);
        void DeleteOTOnlineAdminDetail(OTOnlineSetupAdminDetail o);
        OTOnlineSetupApprover GetSingleOTOnlineApprover(string Username, string EmployeeID);
        OTOnlineSetupApproverDetail GetSingleOTOnlineApproverDetail(int approverID, string deptCode);
        List<OTOnlineSetupApproverDetail> GetAllOTOnlineApprover();
        List<OTOnlineSetupApproverDetail> GetListOTOnlineApprover(string Username);
        void AddOTOnlineApprover(OTOnlineSetupApprover o);
        void AddOTOnlineApproverDetail(OTOnlineSetupApproverDetail o);
        void UpdateOTOnlineApproverDetail(OTOnlineSetupApproverDetail o);
        void DeleteOTOnlineApproverDetail(OTOnlineSetupApproverDetail o);
        OTOnlineSetupFinalApprover GetSingleOTOnlineFinalApprover(string Username, string EmployeeID);
        List<OTOnlineSetupFinalApproverDetail> GetAllOTOnlineFinalApprover();
        List<OTOnlineSetupFinalApproverDetail> GetListOTOnlineFinalApprover(string Username);
        OTOnlineSetupHRApprover GetSingleOTOnlineHR(string Username);
        void AddOTOnlineFinalApprover(OTOnlineSetupFinalApprover o);
        void AddOTOnlineFinalApproverDetail(OTOnlineSetupFinalApproverDetail o);
        void UpdateOTOnlineFinalApproverDetail(OTOnlineSetupFinalApproverDetail o);
        void DeleteOTOnlineFinalApproverDetail(OTOnlineSetupFinalApproverDetail o);
        //List<OT> GetOTOnlineList(DateTime fromDate, DateTime toDate, int? HRApprover_ID, string requestFrom, string Username);
        List<OT> GetOTOnlineList(DateTime fromDate, DateTime toDate, string requestFrom, string Username);
        List<FinalApprover_Type> GetAllFinalApproverType();
        void UpdateOTOnline(OT model, string actionFrom, string formState, string Username);
        List<OTOnlineSetupHRApprover> GetAllOTOnlineHRApprover();
        //OTOnlineSetupHRApprover GetSingleOTOnlineHRApprover(OTOnlineSetupHRApprover o);
        void AddOTOnlineHRApprover(OTOnlineSetupHRApprover o);
        void DeleteOTOnlineHRApprover(OTOnlineSetupHRApprover o);
        List<string> GetDestinationEmail(List<OT> listOT, string requestFrom, string action);
    }
    public class OTOnlineBL : IOTOnlineBL
    {
        public List<Employee> GetOTOnlineEmployee(string searchType, int? searchStaff, int? searchStatus, string searchText, string requestFrom, string Username)
        {
            var employeeList = DataAccessLayerServices.EmployeeRepository().GetList(w => w.Staff_status == searchStatus,
                                                                                    w => w.Person,
                                                                                    w => w.Person.TitleName,
                                                                                    w => w.StaffStatu,
                                                                                    w => w.StaffType,
                                                                                    w => w.Position).ToList(); //Filter employee by STATUS
            if (searchStaff != null) employeeList = employeeList.Where(w => w.Staff_type == searchStaff).ToList();

            if (!string.IsNullOrEmpty(searchText))
            {
                if (searchType == "ค้นหาจากชื่อ (TH)") employeeList = employeeList.Where(w => w.Person.FirstNameTH.Contains(searchText)).ToList();
                else if (searchType == "ค้นหาจากชื่อ (EN)") employeeList = employeeList.Where(w => w.Person.FirstNameEN.ToLower().Contains(searchText.ToLower())).ToList();
                else if (searchType == "ค้นหาจากนามสกุล (TH)") employeeList = employeeList.Where(w => w.Person.LastNameTH.Contains(searchText)).ToList();
                else if (searchType == "ค้นหาจากนามสกุล (EN)") employeeList = employeeList.Where(w => w.Person.LastNameEN.ToLower().Contains(searchText)).ToList();
                else if (searchType == "ค้นหาจากรหัสพนักงาน") employeeList = employeeList.Where(w => w.Employee_ID == searchText).ToList();
            }

            if (requestFrom == "HR Approver")
            {   // HR Can view all dept
                return employeeList;
            }
            else
            {   // Show only under reponse dept
                var listResult = from emp in employeeList
                                 join prm in GetListOTOnlinePermission(requestFrom, Username) on emp.DeptCode equals prm.DeptCode
                                 select emp;
                return listResult.Distinct().ToList();
            }

            //return employeeList;
        }
        public List<OTOnlineSetupAdminDetail> GetListOTOnlineAdmin(string Username)
        {
            return DataAccessLayerServices.OTOnlineSetupAdminDetailRepository().GetList(g => g.OTOnlineSetupAdmin.AdName.ToLower() == Username.ToLower(), g => g.OTOnlineSetupAdmin).ToList();
        }

        public List<OTOnlineSetupApproverDetail> GetListOTOnlineApprover(string Username)
        {
            return DataAccessLayerServices.OTOnlineSetupApproverDetailRepository().GetList(g => g.OTOnlineSetupApprover.StecUsername == Username, g => g.OTOnlineSetupApprover).ToList();
        }

        public List<OTOnlineSetupFinalApproverDetail> GetListOTOnlineFinalApprover(string Username)
        {
            return DataAccessLayerServices.OTOnlineSetupFinalApproverDetailRepository().GetList(g => g.OTOnlineSetupFinalApprover.StecUsername == Username, g => g.OTOnlineSetupFinalApprover).ToList();
        }

        public OTOnlineSetupHRApprover GetSingleOTOnlineHR(string Username)
        {
            return DataAccessLayerServices.OTOnlineSetupHRApproverRepository().GetSingle(w => w.StecUsername == Username);
        }
        
        public List<OT> GetOTOnlineList(DateTime fromDate, DateTime toDate, string requestFrom, string Username)
        {
            var result = new List<OT>();
            var otList = DataAccessLayerServices.OTRepository().GetList(g => g.Remark == "Create from OT Online" &&
                                                                             g.OTDate >= fromDate &&
                                                                             g.OTDate <= toDate,
                                                                             g => g.Employee, g => g.OTType1,
                                                                                              g => g.OTRequestType,
                                                                                              g => g.Employee.Person,
                                                                                              g => g.Employee.Person.TitleName).ToList();

            if (requestFrom == "Admin") otList = otList.Where(w => w.AdminApproveStatus != "A").ToList();
            if (requestFrom == "Approver") otList = otList.Where(w => w.AdminApproveStatus == "A" &&
                                                                      w.ManagerApproveStatus == null).ToList();
            if (requestFrom == "FinalApprover") otList = otList.Where(w => w.AdminApproveStatus == "A" &&
                                                                           w.ManagerApproveStatus == "A" &&
                                                                           w.FinalApproverStatus == null).ToList();
            if (requestFrom == "HR Approver") otList = otList.Where(w => w.AdminApproveStatus == "A" &&
                                                                         w.ManagerApproveStatus == "A" &&
                                                                         w.FinalApproverStatus == "A" &&
                                                                         w.HRApproveStatus == null).ToList();
            //if (HRApprover_ID != null && (requestFrom == "HR Approver" || requestFrom == "Report"))
            if (requestFrom == "HR Approver" || requestFrom == "Report")
            {   // HR Approver Can view all dept
                result = otList;
            }
            else
            {   // Show only under reponse dept                        
                var listResult = from doc in otList
                                 join prm in GetListOTOnlinePermission(requestFrom, Username) on doc.Employee.DeptCode equals prm.DeptCode
                                 select doc;
                result = listResult.Distinct().ToList();
            }
            return result.OrderBy(o => o.OTDate).ThenBy(t => t.Employee_ID).ToList();
        }

        private List<Department> GetListOTOnlinePermission(string requestFrom, string Username)
        {
            List<Department> listPermission = new List<Department>();
            var adminPermission = DataAccessLayerServices.OTOnlineSetupAdminDetailRepository().GetList(g => g.OTOnlineSetupAdmin.AdName == Username).Select(s => new Department { DeptCode = s.DeptID }).ToList();
            if (requestFrom == "Admin") return adminPermission;
            var approverPermission = DataAccessLayerServices.OTOnlineSetupApproverDetailRepository().GetList(g => g.OTOnlineSetupApprover.StecUsername == Username).Select(s => new Department { DeptCode = s.Dept_code }).ToList();
            if (requestFrom == "Approver") return approverPermission;
            var finalPermission = DataAccessLayerServices.OTOnlineSetupFinalApproverDetailRepository().GetList(g => g.OTOnlineSetupFinalApprover.StecUsername== Username).Select(s => new Department { DeptCode = s.Dept_code }).ToList();
            if (requestFrom == "FinalApprover") return finalPermission;

            else if (requestFrom == "Report")
            {
                if (adminPermission != null && adminPermission.Any()) listPermission.AddRange(adminPermission);
                if (approverPermission != null && approverPermission.Any()) listPermission.AddRange(approverPermission);
                if (finalPermission != null && finalPermission.Any()) listPermission.AddRange(finalPermission);
            }
            return listPermission;
        }

        public List<OTOnlineSetupAdminDetail> GetAllOTOnlineAdmin()
        {
            return DataAccessLayerServices.OTOnlineSetupAdminDetailRepository().GetAll(g => g.OTOnlineSetupAdmin,
                                                                                       g => g.OTOnlineSetupAdmin.Employee,
                                                                                       g => g.OTOnlineSetupAdmin.Employee.Person,
                                                                                       g => g.OTOnlineSetupAdmin.Employee.Person.TitleName)
                                                                                       .OrderBy(o => o.DeptID)
                                                                                       .ThenBy(t => t.OTOnlineSetupAdmin.AdName)
                                                                                       .ToList();
        }

        public OTOnlineSetupAdminDetail GetSingleOTOnlineAdminDetail(int adminID, string deptCode)
        {
            return DataAccessLayerServices.OTOnlineSetupAdminDetailRepository().GetSingle(g => g.Admin_ID == adminID &&
                                                                                               g.DeptID == deptCode,
                                                                                               g => g.OTOnlineSetupAdmin,
                                                                                               g => g.OTOnlineSetupAdmin.Employee,
                                                                                               g => g.OTOnlineSetupAdmin.Employee.Person,
                                                                                               g => g.OTOnlineSetupAdmin.Employee.Person.TitleName);
        }

        public void AddOTOnlineAdmin(OTOnlineSetupAdmin o)
        {
            DataAccessLayerServices.OTOnlineSetupAdminRepository().Add(o);
        }

        public void AddOTOnlineAdminDetail(OTOnlineSetupAdminDetail o)
        {
            DataAccessLayerServices.OTOnlineSetupAdminDetailRepository().Add(o);
        }

        public void UpdateOTOnlineAdminDetail(OTOnlineSetupAdminDetail o)
        {
            DataAccessLayerServices.OTOnlineSetupAdminDetailRepository().Update(o);
        }

        public void DeleteOTOnlineAdminDetail(OTOnlineSetupAdminDetail o)
        {
            DataAccessLayerServices.OTOnlineSetupAdminDetailRepository().Remove(o);
        }

        public OTOnlineSetupApproverDetail GetSingleOTOnlineApproverDetail(int approverID, string deptCode)
        {
            return DataAccessLayerServices.OTOnlineSetupApproverDetailRepository().GetSingle(g => g.Approver_ID == approverID &&
                                                                                                  g.Dept_code == deptCode,
                                                                                                  g => g.OTOnlineSetupApprover,
                                                                                                  g => g.OTOnlineSetupApprover.Employee,
                                                                                                  g => g.OTOnlineSetupApprover.Employee.Person,
                                                                                                  g => g.OTOnlineSetupApprover.Employee.Person.TitleName);
        }

        public List<OTOnlineSetupApproverDetail> GetAllOTOnlineApprover()
        {
            return DataAccessLayerServices.OTOnlineSetupApproverDetailRepository().GetAll(g => g.OTOnlineSetupApprover,
                                                                                          g => g.OTOnlineSetupApprover.Employee,
                                                                                          g => g.OTOnlineSetupApprover.Employee.Person,
                                                                                          g => g.OTOnlineSetupApprover.Employee.Person.TitleName)
                                                                                          .OrderBy(o => o.Dept_code)
                                                                                          .ThenBy(t => t.OTOnlineSetupApprover.StecUsername)
                                                                                          .ToList();
        }

        public void AddOTOnlineApprover(OTOnlineSetupApprover o)
        {
            DataAccessLayerServices.OTOnlineSetupApproverRepository().Add(o);
        }

        public void AddOTOnlineApproverDetail(OTOnlineSetupApproverDetail o)
        {
            DataAccessLayerServices.OTOnlineSetupApproverDetailRepository().Add(o);
        }

        public void UpdateOTOnlineApproverDetail(OTOnlineSetupApproverDetail o)
        {
            DataAccessLayerServices.OTOnlineSetupApproverDetailRepository().Update(o);
        }

        public void DeleteOTOnlineApproverDetail(OTOnlineSetupApproverDetail o)
        {
            DataAccessLayerServices.OTOnlineSetupApproverDetailRepository().Remove(o);
        }

        public OTOnlineSetupAdmin GetSingleOTOnlineAdmin(string Username, string EmployeeID)
        {
            return DataAccessLayerServices.OTOnlineSetupAdminRepository().GetSingle(g => g.AdName.ToLower() == Username.ToLower() || g.Employee_ID == EmployeeID);
        }

        public OTOnlineSetupApprover GetSingleOTOnlineApprover(string Username, string EmployeeID)
        {
            return DataAccessLayerServices.OTOnlineSetupApproverRepository().GetSingle(g => g.StecUsername.ToLower() == Username.ToLower() || g.EmployeeID == EmployeeID);
        }

        public List<OTOnlineSetupFinalApproverDetail> GetAllOTOnlineFinalApprover()
        {
            return DataAccessLayerServices.OTOnlineSetupFinalApproverDetailRepository().GetAll(g => g.OTOnlineSetupFinalApprover,
                                                                                                    g => g.OTOnlineSetupFinalApprover.Employee,
                                                                                                    g => g.OTOnlineSetupFinalApprover.Employee.Person,
                                                                                                    g => g.OTOnlineSetupFinalApprover.Employee.Person.TitleName,
                                                                                                    g => g.FinalApprover_Type)
                                                                                                    .OrderBy(o => o.Dept_code)
                                                                                                    .ThenBy(t => t.OTOnlineSetupFinalApprover.StecUsername)
                                                                                                    .ToList();
        }

        public OTOnlineSetupFinalApprover GetSingleOTOnlineFinalApprover(string Username, string EmployeeID)
        {
            return DataAccessLayerServices.OTOnlineSetupFinalApproverRepository().GetSingle(g => g.StecUsername.ToLower() == Username.ToLower() || g.EmployeeID == EmployeeID);
        }

        public void AddOTOnlineFinalApprover(OTOnlineSetupFinalApprover o)
        {
            DataAccessLayerServices.OTOnlineSetupFinalApproverRepository().Add(o);
        }

        public void AddOTOnlineFinalApproverDetail(OTOnlineSetupFinalApproverDetail o)
        {
            DataAccessLayerServices.OTOnlineSetupFinalApproverDetailRepository().Add(o);
        }

        public void UpdateOTOnlineFinalApproverDetail(OTOnlineSetupFinalApproverDetail o)
        {
            DataAccessLayerServices.OTOnlineSetupFinalApproverDetailRepository().Update(o);
        }

        public void DeleteOTOnlineFinalApproverDetail(OTOnlineSetupFinalApproverDetail o)
        {
            DataAccessLayerServices.OTOnlineSetupFinalApproverDetailRepository().Remove(o);
        }

        public List<FinalApprover_Type> GetAllFinalApproverType()
        {
            return DataAccessLayerServices.FinalApproverTypeRepository().GetAll().ToList();
        }

        public void UpdateOTOnline(OT o, string actionFrom, string formState, string Username)
        {
            var ot = BusinessLayerServices.OTBL().GetSingleOT(o.Employee_ID, o.OTDate, o.OTType, o.RequestType);

            if (actionFrom == "Admin")
            {   // Admin Approve / Update
                if (formState == "Approve")
                {
                    ot.AdminApproveStatus = "A";
                    ot.ManagerApproveStatus = null;
                    ot.HRApproveStatus = null;
                    ot.FinalApproverStatus = null;
                }
                else if (formState == "Update")
                {
                    ot.NcountOT = o.NcountOT;
                    ot.RemarkFromAdmin = o.RemarkFromAdmin;
                }
                ot.ModifiedUser = Username;
            }
            else if (actionFrom == "Approver")
            {   // Manager Approve / Reject (Approver)
                if (formState == "Approve") ot.ManagerApproveStatus = "A";
                else if (formState == "Reject")
                {
                    ot.AdminApproveStatus = null;
                    ot.ManagerApproveStatus = "R";
                    ot.RemarkFromManager = o.RemarkFromManager;
                }
                ot.ManagerApproveUser = Username;
            }
            else if (actionFrom == "FinalApprover")
            {   // Factory Manager Approve (Final Approver)
                if (formState == "Approve") ot.FinalApproverStatus = "A";
                else if (formState == "Reject")
                {
                    ot.AdminApproveStatus = null;
                    ot.ManagerApproveStatus = null;
                    ot.FinalApproverStatus = "R";
                    ot.RemarkFromFinalApprover = o.RemarkFromFinalApprover;
                }
                ot.FinalApproverUser = Username;
            }
            else if (actionFrom == "HR")
            {   // HR Approve / Reject
                if (formState == "Approve") { ot.HRApproveStatus = "A"; ot.Status = true; }
                else if (formState == "Reject")
                {
                    ot.AdminApproveStatus = null;
                    ot.ManagerApproveStatus = null;
                    ot.FinalApproverStatus = null;
                    ot.HRApproveStatus = "R";
                    ot.RemarkFromHR = o.RemarkFromHR;
                }
                ot.HrApproveUser = Username;
            }

            DataAccessLayerServices.OTRepository().Update(ot);
        }

        public List<OTOnlineSetupHRApprover> GetAllOTOnlineHRApprover()
        {
            return DataAccessLayerServices.OTOnlineSetupHRApproverRepository().GetAll(g => g.Employee,
                                                                                      g => g.Employee.Person,
                                                                                      g => g.Employee.Person.TitleName).ToList();
        }

        public void AddOTOnlineHRApprover(OTOnlineSetupHRApprover o)
        {
            DataAccessLayerServices.OTOnlineSetupHRApproverRepository().Add(o);
        }

        public void DeleteOTOnlineHRApprover(OTOnlineSetupHRApprover o)
        {
            DataAccessLayerServices.OTOnlineSetupHRApproverRepository().Remove(o);
        }

        public List<string> GetDestinationEmail(List<OT> listOT, string requestFrom, string action)
        {
            var result = new List<string>();
            var groupDepartment = listOT.GroupBy(g => g.Employee.DeptCode).ToList().Select(s => new { departmentCode = s.Key }).ToList();
            var listAdmin = (from adm in GetAllOTOnlineAdmin()
                             join dep in groupDepartment on adm.DeptID equals dep.departmentCode
                             select adm.OTOnlineSetupAdmin.Employee.Email).ToList();
            var listApprover = (from apv in GetAllOTOnlineApprover()
                                join dep in groupDepartment on apv.Dept_code equals dep.departmentCode
                                select apv.OTOnlineSetupApprover.Employee.Email).ToList();
            var listFapprover = (from fap in GetAllOTOnlineFinalApprover()
                                 join dep in groupDepartment on fap.Dept_code equals dep.departmentCode
                                 select fap.OTOnlineSetupFinalApprover.Employee.Email).ToList();
            var listHR = (from hav in GetAllOTOnlineHRApprover() select hav.Employee.Email).ToList();

            if (requestFrom == "Admin")
            {   // Send to Approver
                if (action == "Approve") result.AddRange(listApprover);
            }
            else if (requestFrom == "Approver")
            {   // Send to Final Approver
                if (action == "Approve") result.AddRange(listFapprover);
                else if (action == "Reject") result.AddRange(listAdmin);
            }
            else if (requestFrom == "FinalApprover")
            {   // Send to HR Approver
                if (action == "Approve") result.AddRange(listHR);
                else if (action == "Reject")
                {
                    result.AddRange(listApprover);
                    result.AddRange(listAdmin);
                }
            }
            else if (requestFrom == "HR")
            {
                if (action == "Reject")
                {
                    result.AddRange(listFapprover);
                    result.AddRange(listApprover);
                    result.AddRange(listAdmin);
                }
            }

            result = result.GroupBy(g => g).Select(s => s.Key).ToList();
            return result;
        }
    }
}
