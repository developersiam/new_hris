﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelHris;
using HRISSystemDAL;

namespace HRISSystemBL
{
    public interface IDeptCodeBL
    {
        List<Department> GetAllDepartment();
        Department GetSingleDepartment(string departmentCode);
        void Add(Department item);
        void Update(Department item);
        void Delete(Department item);
    }
    public class DeptCodeBL : IDeptCodeBL
    {
        public void Add(Department item)
        {
            DataAccessLayerServices.DepartmentRepository().Add(item);
        }

        public void Delete(Department item)
        {
            DataAccessLayerServices.DepartmentRepository().Remove(item);
        }

        public List<Department> GetAllDepartment()
        {
            return DataAccessLayerServices.DepartmentRepository().GetList(g => g.DeptName != null && g.DeptName != "").OrderBy(o => o.DeptName).ToList();
        }

        public Department GetSingleDepartment(string departmentCode)
        {
            return DataAccessLayerServices.DepartmentRepository().GetSingle(g => g.DeptCode == departmentCode);
        }

        public void Update(Department item)
        {
            DataAccessLayerServices.DepartmentRepository().Update(item);
        }
    }
}
