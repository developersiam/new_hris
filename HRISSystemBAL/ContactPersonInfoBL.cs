﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelHris;
using HRISSystemDAL;

namespace HRISSystemBL
{
    public interface IContactPersonInfoBL
    {
        List<ContactPersonInfo> GetContactInfoListByPersonID(string PersonID);
        string GetNewContactInfoID();
        void AddContactInfo(ContactPersonInfo item);
        void UpdateContactInfo(ContactPersonInfo item);
        void DeleteContactInfo(ContactPersonInfo item);
        ContactPersonInfo GetSingleContactPersonInfo(string contactInfoID, string personID);
    }
    public class ContactPersonInfoBL : IContactPersonInfoBL
    {
        public void AddContactInfo(ContactPersonInfo item)
        {
            DataAccessLayerServices.ContactPersonInfoRepository().Add(item);
        }

        public void DeleteContactInfo(ContactPersonInfo item)
        {
            DataAccessLayerServices.ContactPersonInfoRepository().Remove(item);
        }

        public List<ContactPersonInfo> GetContactInfoListByPersonID(string PersonID)
        {
            return DataAccessLayerServices.ContactPersonInfoRepository().GetList(g => g.Person_ID == PersonID).ToList();
        }

        public string GetNewContactInfoID()
        {
            var listString = DataAccessLayerServices.ContactPersonInfoRepository().GetAll().Select(s => s.ContactPersonInfo_ID).ToList();
            return BusinessLayerServices.DataCorrector().GetMaxIDFromListString(listString);
        }

        public ContactPersonInfo GetSingleContactPersonInfo(string contactInfoID, string personID)
        {
            return DataAccessLayerServices.ContactPersonInfoRepository().GetSingle(g => g.ContactPersonInfo_ID == contactInfoID && g.Person_ID == personID);
        }

        public void UpdateContactInfo(ContactPersonInfo item)
        {
            DataAccessLayerServices.ContactPersonInfoRepository().Update(item);
        }
    }
}
