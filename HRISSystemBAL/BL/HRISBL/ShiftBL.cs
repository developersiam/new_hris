﻿using DomainModelHris;
using HRISSystemDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystemBL.BL.HRISBL
{
    public interface IShiftBL
    {
        List<Shift> GetAll();
    }

    public class ShiftBL : IShiftBL
    {
        IHRISUnitOfWork uow;
        public ShiftBL()
        {
            uow = new HRISUnitOfWork();
        }

        public List<Shift> GetAll()
        {
            return uow.ShiftRepo.Get();
        }
    }
}
