﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystemBL.BL.HRISBL
{
    public static class HRISBLServices
    {
        public static IShiftBL ShiftBL()
        {
            IShiftBL obj = new ShiftBL();
            return obj;
        }

        public static IShiftControlBL ShiftControlBL()
        {
            IShiftControlBL obj = new ShiftControlBL();
            return obj;
        }

        public static IDepartmentBL DepartmentBL()
        {
            IDepartmentBL obj = new DepartmentBL();
            return obj;
        }

        public static IOTTypeBL OTTypeBL()
        {
            IOTTypeBL obj = new OTTypeBL();
            return obj;
        }

        public static IOTBL OTBL()
        {
            IOTBL obj = new OTBL();
            return obj;
        }

        public static IOTRequestTypeBL OTRequestTypeBL()
        {
            IOTRequestTypeBL obj = new OTRequestTypeBL();
            return obj;
        }

        public static IEmployeeBL EmployeeBL()
        {
            IEmployeeBL obj = new EmployeeBL();
            return obj;
        }

        public static INotifyBL NotifyBL()
        {
            INotifyBL obj = new NotifyBL();  
            return obj;
        }
    }
}
