﻿using DomainModelHris;
using HRISSystemDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystemBL.BL.HRISBL
{
    public interface IDepartmentBL
    {
        List<Department> GetAll();
    }

    public class DepartmentBL : IDepartmentBL
    {
        IHRISUnitOfWork uow;
        public DepartmentBL()
        {
            uow = new HRISUnitOfWork();
        }

        public List<Department> GetAll()
        {
            return uow.DepartmentRepo.Get();
        }
    }
}
