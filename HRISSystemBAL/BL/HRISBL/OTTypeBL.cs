﻿using DomainModelHris;
using HRISSystemDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystemBL.BL.HRISBL
{
    public interface IOTTypeBL
    {
        List<OTType> GetAl();
    }

    public class OTTypeBL : IOTTypeBL
    {
        IHRISUnitOfWork uow;
        public OTTypeBL()
        {
            uow = new HRISUnitOfWork();
        }

        public List<OTType> GetAl()
        {
            return uow.OTTypeRepo.Get();
        }
    }
}
