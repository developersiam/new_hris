﻿using DomainModelHris;
using HRISSystemBL.BL.PayrollBL;
using HRISSystemDAL;
using HRISSystemDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystemBL.BL.HRISBL
{
    public interface IShiftControlBL
    {
        List<ShiftControl> GetByDateRange(string shiftID, DateTime from, DateTime to);
        List<ShiftControl> GetByDepartmentAndDateRange(DateTime from, DateTime to, string departmentCode);
        ShiftControl GetSingle(string shiftID, string employeeID, DateTime from);
        void ChangeShiftControl(string employeeID, DateTime shiftDate, string oldShiftID, string newShiftID, string user);
        void Add(string shiftID, string employeeID, DateTime shiftDate, string noted);
        void Delete(string shiftID, string employeeID, DateTime shiftDate);
    }

    public class ShiftControlBL : IShiftControlBL
    {
        IHRISUnitOfWork uow;
        public ShiftControlBL()
        {
            uow = new HRISUnitOfWork();
        }

        public void Add(string shiftID, string employeeID, DateTime shiftDate, string noted)
        {
            try
            {
                if (string.IsNullOrEmpty(employeeID))
                    throw new ArgumentException("EmployeeID cannot be empty.");

                if (string.IsNullOrEmpty(shiftID))
                    throw new ArgumentException("ShiftID cannot be empty.");

                var item = GetSingle(shiftID, employeeID, shiftDate);
                if (item != null)
                    throw new ArgumentException("มีการบันทึกกะการทำงานนี้ให้กับพนักงานรายนี้แล้ว");

                uow.ShiftControlRepo.Add(
                    new ShiftControl
                    {
                        Shift_ID = shiftID,
                        Employee_ID = employeeID,
                        ValidFrom = shiftDate,
                        EndDate = shiftDate,
                        Noted = noted,
                        CreatedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ChangeShiftControl(string employeeID, DateTime shiftDate, string oldShiftID, string newShiftID, string user)
        {
            try
            {
                if (string.IsNullOrEmpty(employeeID))
                    throw new ArgumentException("EmployeeID cannot be empty.");

                if (string.IsNullOrEmpty(newShiftID))
                    throw new ArgumentException("ShiftID (new) cannot be empty.");

                if (string.IsNullOrEmpty(employeeID))
                    throw new ArgumentException("User cannot be empty. Please try to login again.");

                if (PayrollServices.LotNumberBL().IsLotNumberLocked(shiftDate) == true)
                    throw new ArgumentException("LotNumber นี้ถูก Lock แล้ว");

                var item = GetSingle(oldShiftID, employeeID, shiftDate);
                if (item == null)
                {
                    Add(newShiftID, employeeID, shiftDate, null);
                }
                else
                {
                    Delete(oldShiftID, employeeID, shiftDate);
                    Add(newShiftID, employeeID, shiftDate, null);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string shiftID, string employeeID, DateTime shiftDate)
        {
            try
            {
                var item = GetSingle(shiftID, employeeID, shiftDate);
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูลกะการทำงานของพนักงานรายนี้ในระบบ");

                if (PayrollServices.LotNumberBL().IsLotNumberLocked(shiftDate) == true)
                    throw new ArgumentException("LotNumber นี้ถูก Lock แล้ว");

                uow.ShiftControlRepo.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ShiftControl> GetByDateRange(string shiftID, DateTime from, DateTime to)
        {
            return uow.ShiftControlRepo
                .Get(x => x.Shift_ID == shiftID &&
                x.ValidFrom >= from &&
                x.ValidFrom <= to
                , null
                , x => x.Employee.StaffType
                , x => x.Employee.StaffStatu
                , x => x.Employee.Person
                , x => x.Shift)
                .ToList();
        }

        public List<ShiftControl> GetByDepartmentAndDateRange(DateTime from, DateTime to, string departmentCode)
        {
            return uow.ShiftControlRepo
                .Get(x => x.ValidFrom >= from &&
                x.ValidFrom <= to &&
                x.Employee.DeptCode == departmentCode,
                null,
                x => x.Employee);
        }

        public ShiftControl GetSingle(string shiftID, string employeeID, DateTime from)
        {
            return uow.ShiftControlRepo.GetSingle(x => x.Shift_ID == shiftID &&
            x.Employee_ID == employeeID &&
            x.ValidFrom == from);
        }
    }
}
