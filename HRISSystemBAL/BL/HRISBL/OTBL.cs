﻿using DomainModelHris;
using HRISSystemDAL;
using HRISSystemDAL.UnitOfWork;
using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystemBL.BL.HRISBL
{
    public interface IOTBL
    {
        void Add(string employeeID, DateTime otDate, string otType,
            string requestType, decimal? nCountOT, string remark, string payrollLot,
            string remarkFromAdmin, string user);
        void Update(OT o);
        void Delete(OT o);
        OT GetSingle(string employeeID, DateTime? otDate, string otType, string requestType);
        OT GetSingleByDate(string employeeID, DateTime otDate);
        List<OT> GetByDate(string employeeID, DateTime otDate);
        List<OT> GetHolidayOT(string employeeID, DateTime otDate);
        List<OT> GetExistingOT(string employeeID, DateTime endDate);
        List<OT> GetAll();
        List<OT> GetByFirstName(string nameFilter, DateTime fromDate, DateTime toDate);
        List<OT> GetByDepartment(string deptCode, DateTime fromDate, DateTime toDate);
        List<OT> GetPersonHolidayList(string employeeID, DateTime fromDate, DateTime toDate);
    }

    public class OTBL : IOTBL
    {
        IHRISUnitOfWork uow;
        public OTBL()
        {
            uow = new HRISUnitOfWork();
        }

        public void Add(string employeeID, DateTime otDate, string otType,
            string requestType, decimal? nCountOT, string remark, string payrollLot,
            string remarkFromAdmin, string user)
        {
            try
            {
                var model = GetSingle(employeeID, otDate, otType, requestType);
                if (model != null)
                    throw new Exception("โอทีประเภทนี้ได้มีการบันทึกอยู่ในระบบแล้ว กรุณาตรวจสอบ.");

                uow.OTRepo.Add(new OT
                {
                    Employee_ID = employeeID,
                    OTDate = otDate,
                    OTType = otType,
                    RequestType = requestType,
                    NcountOT = nCountOT,
                    PayrollLot = payrollLot,
                    Status = false,
                    AdminApproveStatus = null,
                    Remark = remark,
                    RemarkFromAdmin = remarkFromAdmin,
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = user
                });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(OT o)
        {
            DataAccessLayerServices.OTRepository().Remove(o);
        }

        public List<OT> GetAll()
        {
            return uow.OTRepo.Get();
        }

        public List<OT> GetByFirstName(string nameFilter, DateTime fromDate, DateTime toDate)
        {
            var otList = uow.OTRepo
                .Get(g => g.OTDate >= fromDate &&
                g.OTDate <= toDate &&
                g.Employee.Person.FirstNameTH.Contains(nameFilter),
                null,
                g => g.Employee,
                g => g.OTType1,
                g => g.OTRequestType,
                g => g.Employee.Person,
                g => g.Employee.Person.TitleName);

            return otList;
        }

        public OT GetSingle(string employeeID, DateTime? otDate, string otType, string requestType)
        {
            return DataAccessLayerServices
                .OTRepository()
                .GetSingle(g => g.Employee_ID == employeeID &&
                g.OTDate == otDate &&
                g.OTType == otType &&
                g.RequestType == requestType,
                g => g.OTType1,
                g => g.OTRequestType,
                g => g.Employee,
                g => g.Employee.Person,
                g => g.Employee.Person.TitleName);
        }

        public void Update(OT o)
        {
            DataAccessLayerServices.OTRepository().Update(o);
        }

        public List<OT> GetHolidayOT(string employeeID, DateTime otDate)
        {
            return DataAccessLayerServices.OTRepository()
                .GetList(g => g.Employee_ID == employeeID &&
                g.OTDate >= otDate &&
                g.OTDate <= otDate &&
                (g.OTType == "S" || g.OTType == "H"),
                g => g.OTType1,
                g => g.OTRequestType,
                g => g.Employee)
                .ToList();
        }

        public OT GetSingleByDate(string employeeID, DateTime otDate)
        {
            var otList = GetByDate(employeeID, otDate);
            var result = new OT { 
                Employee_ID = employeeID,
                OTDate = otDate, 
                NcountOT = otList.Sum(s => s.NcountOT) 
            };
            return result;
        }

        public List<OT> GetByDate(string employeeID, DateTime otDate)
        {
            return uow.OTRepo
                .Get(g => g.Employee_ID == employeeID &&
                g.OTDate == otDate,
                null,
                g => g.OTType1,
                g => g.OTRequestType);
        }

        public List<OT> GetExistingOT(string employeeID, DateTime endDate)
        {
            var otList = DataAccessLayerServices
                .OTRepository()
                .GetList(g => g.Employee_ID == employeeID &&
                g.OTDate > endDate)
                .ToList();
            return otList;
        }

        public List<OT> GetByDepartment(string deptCode, DateTime fromDate, DateTime toDate)
        {
            var otList = DataAccessLayerServices
                .OTRepository()
                .GetList(g => g.OTDate >= fromDate &&
                g.OTDate <= toDate &&
                g.Employee.DeptCode == deptCode)
                .ToList();
            return otList;
        }

        public List<OT> GetPersonHolidayList(string employeeID, DateTime fromDate, DateTime toDate)
        {
            var otList = DataAccessLayerServices
                .OTRepository()
                .GetList(g => g.OTDate >= fromDate &&
                g.OTDate <= toDate &&
                g.Employee.Noted == employeeID &&
                g.Employee.Staff_type != 1 &&
                g.OTType == "H")
                .ToList();
            return otList;
        }
    }
}
