﻿using DomainModelHris;
using HRISSystemDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystemBL.BL.HRISBL
{
    public interface IOTRequestTypeBL
    {
        List<OTRequestType> GetAll();
    }

    public class OTRequestTypeBL : IOTRequestTypeBL
    {
        IHRISUnitOfWork uow;
        public OTRequestTypeBL()
        {
            uow = new HRISUnitOfWork();
        }

        public List<OTRequestType> GetAll()
        {
            return uow.OTRequestTypeRepo.Get();
        }
    }
}
