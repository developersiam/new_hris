﻿using DomainModelHris;
using DomainModelTimeStec;
using HRISSystemDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystemBL.BL.HRISBL
{
    public interface IEmployeeBL
    {
        void EditInfo(string employeeID, string personID,
            string fingerID, string position, DateTime endDate,
            int? staffTypeID, int? staffStatusID, string shiftID, string email);
        void ChangeDefaultShift(string employeeID, string shiftID, string user);
        void ChangeDefaultShiftRange(List<Employee> employeeList, string shiftID, string user);
        Employee GetSingle(string employeeID);
        List<Employee> GetByDepartment(string departmentCode, int? staffStatus);
        List<Employee> GetAll(int? staffStatus);
        List<Employee> GetByDepartmentList(List<string> departments);
        List<Employee> GetByPersonIDList(List<string> personIDs);
    }

    public class EmployeeBL : IEmployeeBL
    {
        IHRISUnitOfWork uow;
        public EmployeeBL()
        {
            uow = new HRISUnitOfWork();
        }

        public void ChangeDefaultShift(string employeeID, string shiftID, string user)
        {
            try
            {
                var model = GetSingle(employeeID);
                if (model == null)
                    throw new Exception("ไม่พบข้อมูลพนักงานรหัส " + employeeID + " นี้ในระบบ");

                model.Shift_ID = shiftID;
                model.ModifiedDate = DateTime.Now;
                uow.EmployeeRepo.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ChangeDefaultShiftRange(List<Employee> employeeList, string shiftID, string user)
        {
            throw new NotImplementedException();
        }

        public void EditInfo(string employeeID, string personID,
            string fingerID, string positionID, DateTime endDate,
            int? staffTypeID, int? staffStatusID, string shiftID, string email)
        {
            try
            {
                if (staffStatusID == null)
                    throw new Exception("กรุณาระบุสถานะพนักงาน");

                if (staffTypeID == null)
                    throw new Exception("กรุณาระบุประเภทของพนักงาน");

                if (string.IsNullOrEmpty(employeeID) ||
                    string.IsNullOrEmpty(personID) ||
                    string.IsNullOrEmpty(fingerID))
                    throw new Exception("กรุณาระบุรหัสพนักงาน");

                if (string.IsNullOrEmpty(positionID))
                    throw new Exception("กรุณาระบุ Position");

                // Update Some field
                var oldItem = GetSingle(employeeID);

                // End staff status
                if (staffStatusID == 2)
                {
                    if (endDate == null)
                        throw new Exception("กรุณาระบุวันที่สิ้นสุดการทำงาน");

                    //var existingOTList = BusinessLayerServices.OTBL().GetExistingOT(EmployeeIDTextBox.Text, EndDatePicker.SelectedDate.Value);
                    //existingOTList = HRISBLServices.OTBL().GetExistingOT(employeeID, endDate);
                    //if (existingOTList.Any()) throw new Exception("พบข้อมูลการบันทึกโอทีหลังวันสิ้นสุดการทำงาน กรุณาตรวจสอบการทำโอทีก่อนสิ้นสุดการทำงาน");

                    //var result = MessageBoxResult.No;
                    //if (oldItem.Staff_type == 1) 
                    //    result = MessageBox.Show("ต้องการให้คงบัญชีถึงสิ้นเดือนใช่หรือไม่ ? (คงบัญชีถึงสิ้นเดือนหมายถึง ระบบเงินเดือนจะคำนวนเงินเดือนตั้งแต่วันที่ 1-สิ้นเดือนที่มาทำงานจริงรวมทั้งคำนวนวันทำโอทีตั้งแต่ 26-25 ให้ หากไม่แน่ใจกรุณาตรวจสอบกับฝ่ายบัญชีอีกครั้ง) \n หากต้องการให้คงบัญชีถึงสิ้นเดือนให้กดปุ่ม Yes , หากไม่ต้องการให้คงบัญชีถึงส้ินเดือนให้กดปุ่ม No , หากไม่แน่ใจและต้องการยกเลิกเพื่อสอบถามบัญชีให้กดปุ่ม Cancel", "Confirmation!", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                    //var payroll = BusinessLayerServices.PayrollBL().GetSinglePayrollByID(oldItem.Noted);
                    //if (result != MessageBoxResult.Cancel)
                    //{
                    //    if (result == MessageBoxResult.Yes) payroll.Staff_status = "3"; //คงบัญชี
                    //    else if (result == MessageBoxResult.No) payroll.Staff_status = "2"; //ไม่คงบัญชี
                    //    BusinessLayerServices.PayrollBL().UpdatePayrollEmpoyee(payroll);
                    //}
                }

                oldItem.Position_ID = positionID;
                oldItem.EndDate = endDate;
                oldItem.Staff_type = staffStatusID;
                oldItem.Staff_status = staffStatusID;
                oldItem.Shift_ID = shiftID;
                oldItem.Email = email;
                oldItem.ModifiedDate = DateTime.Now;

                uow.EmployeeRepo.Update(oldItem);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Employee> GetAll(int? staffStatus)
        {
            try
            {
                if (staffStatus == null)
                    return uow.EmployeeRepo.Get();
                else if (staffStatus == 1)
                    return uow.EmployeeRepo.Get(x => x.Staff_status == 1);
                else if (staffStatus == 2)
                    return uow.EmployeeRepo.Get(x => x.Staff_status == 1);
                else
                    return new List<Employee>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Employee> GetByDepartment(string departmentCode, int? staffStatus)
        {
            try
            {
                var list = uow.EmployeeRepo.Get(x => x.DeptCode == departmentCode);

                if (staffStatus == null)
                    return list;
                else if (staffStatus == 1)
                    return list.Where(x => x.Staff_status == 1).ToList();
                else if (staffStatus == 2)
                    return list.Where(x => x.Staff_status == 2).ToList();
                else
                    return new List<Employee>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Employee> GetByDepartmentList(List<string> departments)
        {
            return uow.EmployeeRepo
                .Get(x => departments.Contains(x.DeptCode)
                , null
                , y => y.Department
                , y => y.Person
                , y => y.Person.TitleName);
        }

        public List<Employee> GetByPersonIDList(List<string> personIDs)
        {
            return uow.EmployeeRepo
                .Get(x => personIDs.Contains(x.Person_ID)
                , null
                , y => y.Department
                , y => y.Person
                , y => y.Person.TitleName);
        }

        public Employee GetSingle(string employeeID)
        {
            return uow.EmployeeRepo
                .GetSingle(x => x.Employee_ID == employeeID
                , y => y.Department
                , y => y.Person
                , y => y.Person.TitleName);
        }
    }
}
