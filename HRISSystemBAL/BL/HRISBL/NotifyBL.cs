﻿using DomainModelHris;
using HRISSystemDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystemBL.BL.HRISBL
{
    public interface INotifyBL
    {
        List<sp_GetNotify_Result> GetNotifyList(DateTime fromDate, DateTime toDate);
    }
    public class NotifyBL : INotifyBL
    {
        public List<sp_GetNotify_Result> GetNotifyList(DateTime fromDate, DateTime toDate)
        {
            return DataAccessLayerServices.StoreProcedureRepository().sp_GetNotify(fromDate, toDate);
        }
    }
}
