﻿using DomainModelPayroll;
using HRISSystemDAL.UnitOfWork;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystemBL.BL.PayrollBL
{
    public interface ILotNumberBL
    {
        Lot_Number GetSingle(string lotYear, string lotMonth);
        List<Lot_Number> GetByStrartAndEndDate(DateTime start, DateTime end);
        List<Lot_Number> GetByBetweenStartAndEndDate(DateTime specifiedDate);
        bool IsLotNumberLocked(DateTime checkDate);
        bool IsLotNumberLockedByDateRange(DateTime from, DateTime to);
    }

    public class LotNumberBL : ILotNumberBL
    {
        IPayrollUnitOfWork uow;
        public LotNumberBL()
        {
            uow = new PayrollUnitOfWork();
        }

        public List<Lot_Number> GetByBetweenStartAndEndDate(DateTime specifiedDate)
        {
            return uow.LotNumberRepo
                .Get(x => x.Start_date == specifiedDate && 
                x.Finish_date == specifiedDate)
                .ToList();
        }

        public List<Lot_Number> GetByStrartAndEndDate(DateTime start, DateTime end)
        {
            return uow.LotNumberRepo
                .Get(x => x.Start_date == start &&
                x.Finish_date == end)
                .ToList();
        }

        public Lot_Number GetSingle(string lotYear, string lotMonth)
        {
            return uow.LotNumberRepo
                .GetSingle(x => x.Lot_Year == lotYear && x.Lot_Month == lotMonth);
        }

        public bool IsLotNumberLocked(DateTime checkDate)
        {
            try
            {
                var lotNumberList = PayrollServices.LotNumberBL().GetByBetweenStartAndEndDate(checkDate);
                if (lotNumberList
                    .Where(x => !string.IsNullOrEmpty(x.Lock_Acc) ||
                    !string.IsNullOrEmpty(x.Lock_Acc_Labor))
                    .Count() > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsLotNumberLockedByDateRange(DateTime from, DateTime to)
        {
            try
            {
                var lotNumberList = uow.LotNumberRepo
                    .Get(x=>x.Start_date >=from && 
                    x.Finish_date <= to &&
                    !string.IsNullOrEmpty(x.Lock_Acc));
                if (lotNumberList
                    .Count() > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
