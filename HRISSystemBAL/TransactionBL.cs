﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelHris;
using HRISSystemDAL;

namespace HRISSystemBL
{
    public interface ITransactionBL
    {
        void AddTransaction(TransactionLog item);
        decimal GetNewTransactionID();
    }
    public class TransactionBL : ITransactionBL
    {
        public void AddTransaction(TransactionLog item)
        {
            DataAccessLayerServices.TransactionLogRepository().Add(item);
        }

        public decimal GetNewTransactionID()
        {
            var x = DataAccessLayerServices.TransactionLogRepository().GetAll().Select(s => s.TransactionLog_ID).ToList().Max() + 1;
            return x;
        }
    }
}
