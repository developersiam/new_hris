﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelHris;
using HRISSystemDAL;

namespace HRISSystemBL
{
    public interface IEmployeeBL
    {
        void AddEmployee(Employee item);
        void UpdateEmployee(Employee item);
        //void DeleteEmployee(Employee item);
        string GetNewEmplyeeID();
        List<StaffStatu> GetAllStaffStatusList();
        List<StaffType> GetAllStaffTypeList();
        StaffType GetStaffTypeByID(int staffTypeID);
        List<sp_OT_GetEmployeeInformation_Result> GetEmployeeListByDate(DateTime? fromDate, DateTime? toDate);
        List<sp_OT_GetEmployeeInformation_Result> GetFilteredEmployeeList(string searchDepartment, string searchStatus, string searchStaff);
        List<sp_OT_GetEmployeeInformation_Result> GetFilteredEmployeeList(string searchType, string searchString, string searchStatus, string searchStaff);
        List<Employee> GetSearchedEmployee(int searchStaff, int searchStatus);
        List<Employee> GetEmployeeListByPersonID(string personID);
        Employee GetLastEmployeeByPersonID(string personID);
        Employee GetEmployeeByEmployeeID(string employeeID);
        Employee GetEmployeeByAccCode(string accCode);
        List<Employee> GetEmployeeListByDepartment(string deptCode);
        List<Employee> GetAvailableEmployeeByFingerID(string fingerID);
        List<Employee> GetEmployeeByDate(DateTime startDate);
        List<sp_GetEmployeeNOTScanInAndOutByDateRange_Result> sp_GetEmployeeNOTScanInAndOutByDateRange(DateTime fromDate, DateTime toDate);
        List<sp_GetEmployeeTimeAttendanceByDateAndEmployeeID_Result> sp_GetEmployeeTimeAttendanceByDateAndEmployeeID(DateTime fromDate, DateTime toDate, int? userID);
        List<sp_GetEmployeeTimeAttendanceByDateAndEmployee1_Result> sp_GetEmployeeTimeAttendanceByDateAndEmployee1(DateTime fromDate, DateTime toDate, int? userID);
        List<sp_GetEmployeeTimeAttendanceByDateAndEmployee2_Result> sp_GetEmployeeTimeAttendanceByDateAndEmployee2(DateTime fromDate, DateTime toDate, string employeeID);
        List<sp_GetTimeAttendance_Result> sp_GetTimeAttendance(DateTime fromDate, DateTime toDate);
        List<sp_GetTimeAttendanceByDept_Result> sp_GetTimeAttendanceByDept(DateTime fromDate, DateTime toDate, string deptCode);
        List<Employee> GetEmployeeListByStafftype(int staffTypeID);
        DateTime GetMinStartDateByPerson(string personID);
    }
    public class EmployeeBL : IEmployeeBL
    {
        public void AddEmployee(Employee item)
        {
            DataAccessLayerServices.EmployeeRepository().Add(item);
        }

        public List<StaffStatu> GetAllStaffStatusList()
        {
            return DataAccessLayerServices.StaffStatuRepository()
                .GetAll()
                .ToList();
        }

        public List<StaffType> GetAllStaffTypeList()
        {
            return DataAccessLayerServices.StaffTypeRepository()
                .GetAll()
                .OrderBy(o => o.StaffTypeID)
                .ToList();
        }

        public Employee GetEmployeeByEmployeeID(string employeeID)
        {
            return DataAccessLayerServices.EmployeeRepository()
                .GetSingle(g => g.Employee_ID == employeeID,
                g => g.Person,
                g => g.Person.TitleName,
                g => g.StaffType,
                g => g.Shift,
                g => g.Department,
                g => g.Position);
        }

        public Employee GetEmployeeByAccCode(string accCode)
        {
            return DataAccessLayerServices.EmployeeRepository()
                .GetSingle(g => g.Noted == accCode,
                g => g.Person,
                g => g.Person.TitleName,
                g => g.StaffType,
                g => g.Department);
        }

        public List<Employee> GetEmployeeListByPersonID(string personID)
        {
            return DataAccessLayerServices.EmployeeRepository()
                .GetList(g => g.Person_ID == personID,
                g => g.Person,
                g => g.Person.TitleName,
                g => g.Position)
                .ToList();
        }

        public Employee GetLastEmployeeByPersonID(string personID)
        {
            //return DataAccessLayerServices.EmployeeRepository().GetList(g => g.Person_ID == personID && g.Staff_status == 1, 
            //                                                                 g => g.Person, 
            //                                                                 g => g.Person.TitleName).ToList();
            return DataAccessLayerServices.EmployeeRepository()
                .GetList(g => g.Person_ID == personID,
                g => g.Person,
                g => g.Person.TitleName,
                g => g.Position)
                .OrderByDescending(o => o.EndDate)
                .First();
        }

        public List<sp_OT_GetEmployeeInformation_Result> GetFilteredEmployeeList(string searchType, string searchString, string searchStatus, string searchStaff)
        {
            var result = new List<sp_OT_GetEmployeeInformation_Result>();

            if (string.IsNullOrEmpty(searchType)) return result; // Return null

            var employeeList = DataAccessLayerServices.StoreProcedureRepository().sp_GetEmployeeInformation();
            if (searchType == "ค้นหาจากชื่อ (TH)") result = employeeList.Where(w => w.FirstNameTH.Contains(searchString)).ToList();
            else if (searchType == "ค้นหาจากชื่อ (EN)") result = employeeList.Where(w => !string.IsNullOrEmpty(w.FirstNameEN) &&
                                                                                      w.FirstNameEN.ToLower().Contains(searchString.ToLower()))
                                                                                      .ToList();
            else if (searchType == "ค้นหาจากนามสกุล (TH)") result = employeeList.Where(w => w.LastNameTH.Contains(searchString)).ToList();
            else if (searchType == "ค้นหาจากนามสกุล (EN)") result = employeeList.Where(w => !string.IsNullOrEmpty(w.LastNameEN) &&
                                                                                          w.LastNameEN.ToLower().Contains(searchString))
                                                                                          .ToList();
            else if (searchType == "ค้นหาจากรหัส HR") result = employeeList.Where(w => w.Employee_ID == searchString).ToList();
            else if (searchType == "ค้นหาจากรหัส Payroll") result = employeeList.Where(w => w.ACC_code == searchString).ToList();
            else result = employeeList.Where(w => w.Card_ID == searchString).ToList(); //(searchType == "ค้นหาจากรหัสบัตรประชาชน")

            if (!string.IsNullOrEmpty(searchStatus)) result = result.Where(w => w.STATUS2 == searchStatus).ToList();
            if (!string.IsNullOrEmpty(searchStaff)) result = result.Where(w => w.Staff_type1 == searchStaff).ToList();
            return result;
        }

        public List<Employee> GetSearchedEmployee(int searchStaff, int searchStatus)
        {
            var employeeList = DataAccessLayerServices.EmployeeRepository()
                .GetList(w => w.Staff_status == searchStatus,
                w => w.Person,
                w => w.Person.TitleName,
                w => w.StaffStatu,
                w => w.StaffType,
                w => w.Position,
                w => w.Shift)
                .ToList(); //Filter employee by STATUS

            if (searchStaff != 0) employeeList = employeeList.Where(w => w.Staff_type == searchStaff).ToList();
            return employeeList;
        }

        public string GetNewEmplyeeID()
        {
            string currentYear = DateTime.Now.Year.ToString().Substring(2, 2);
            string maxID = DataAccessLayerServices.EmployeeRepository().GetList(g => g.Employee_ID.Substring(0, 2) == currentYear).Max(m => m.Employee_ID);
            return BusinessLayerServices.DataCorrector().GetNewEmplyeeID(maxID);
        }

        public void UpdateEmployee(Employee item)
        {
            DataAccessLayerServices.EmployeeRepository().Update(item);
        }

        public List<sp_OT_GetEmployeeInformation_Result> GetFilteredEmployeeList(string searchDepartment, string searchStatus, string searchStaff)
        {
            var result = DataAccessLayerServices.StoreProcedureRepository().sp_GetEmployeeInformation();
            if (!string.IsNullOrEmpty(searchDepartment))
                result = result.Where(w => w.Deptcode == searchDepartment).ToList();
            if (!string.IsNullOrEmpty(searchStatus))
                result = result.Where(w => w.STATUS2 == searchStatus).ToList();
            if (!string.IsNullOrEmpty(searchStaff))
                result = result.Where(w => w.Staff_type1 == searchStaff).ToList();
            return result;
        }

        public List<Employee> GetAvailableEmployeeByFingerID(string fingerID)
        {
            var result = DataAccessLayerServices.EmployeeRepository()
                .GetList(g => g.FingerScanID == fingerID &&
                g.Staff_status == 1, g => g.Person)
                .ToList();
            return result;
        }

        public List<sp_OT_GetEmployeeInformation_Result> GetEmployeeListByDate(DateTime? fromDate, DateTime? toDate)
        {
            var result = DataAccessLayerServices.StoreProcedureRepository()
                .sp_GetEmployeeInformation();
            return result;
        }

        public List<Employee> GetEmployeeByDate(DateTime startDate)
        {
            return DataAccessLayerServices.EmployeeRepository()
                .GetList(g => g.StartDate <= startDate,
                g => g.Person,
                g => g.Person.TitleName,
                g => g.StaffStatu,
                g => g.StaffType)
                .ToList();
        }

        public List<sp_GetEmployeeNOTScanInAndOutByDateRange_Result> sp_GetEmployeeNOTScanInAndOutByDateRange(DateTime fromDate, DateTime toDate)
        {
            return DataAccessLayerServices.StoreProcedureRepository().sp_GetEmployeeNOTScanInAndOutByDateRange(fromDate, toDate);
        }

        public List<sp_GetEmployeeTimeAttendanceByDateAndEmployeeID_Result> sp_GetEmployeeTimeAttendanceByDateAndEmployeeID(DateTime fromDate, DateTime toDate, int? userID)
        {
            return DataAccessLayerServices.StoreProcedureRepository().sp_GetEmployeeTimeAttendanceByDateAndEmployeeID(fromDate, toDate, userID);
        }

        public List<sp_GetEmployeeTimeAttendanceByDateAndEmployee1_Result> sp_GetEmployeeTimeAttendanceByDateAndEmployee1(DateTime fromDate, DateTime toDate, int? userID)
        {
            return DataAccessLayerServices.StoreProcedureRepository().sp_GetEmployeeTimeAttendanceByDateAndEmployee1(fromDate, toDate, userID);
        }

        public List<sp_GetEmployeeTimeAttendanceByDateAndEmployee2_Result> sp_GetEmployeeTimeAttendanceByDateAndEmployee2(DateTime fromDate, DateTime toDate, string employeeID)
        {
            return DataAccessLayerServices.StoreProcedureRepository().sp_GetEmployeeTimeAttendanceByDateAndEmployee2(fromDate, toDate, employeeID);
        }

        public StaffType GetStaffTypeByID(int staffTypeID)
        {
            return DataAccessLayerServices.StaffTypeRepository().GetSingle(g => g.StaffTypeID == staffTypeID);
        }

        public List<Employee> GetEmployeeListByDepartment(string deptCode)
        {
            return DataAccessLayerServices.EmployeeRepository()
                .GetList(g => g.DeptCode == deptCode &&
                g.Staff_status == 1,
                g => g.Person,
                g => g.Person.TitleName)
                .ToList();
        }

        public List<sp_GetTimeAttendanceByDept_Result> sp_GetTimeAttendanceByDept(DateTime fromDate, DateTime toDate, string deptCode)
        {
            return DataAccessLayerServices.StoreProcedureRepository()
                .sp_GetTimeAttendanceByDept(fromDate, toDate, deptCode);
        }

        public List<sp_GetTimeAttendance_Result> sp_GetTimeAttendance(DateTime fromDate, DateTime toDate)
        {
            return DataAccessLayerServices.StoreProcedureRepository()
                .sp_GetTimeAttendance(fromDate, toDate);
        }

        public List<Employee> GetEmployeeListByStafftype(int staffTypeID)
        {
            var employeeList = DataAccessLayerServices.EmployeeRepository()
                .GetList(w => w.Staff_type == staffTypeID,
                w => w.Person,
                w => w.Person.TitleName,
                w => w.StaffStatu,
                w => w.StaffType,
                w => w.Position)
                .ToList(); //Filter employee by STATUS

            return employeeList;
        }

        public DateTime GetMinStartDateByPerson(string personID)
        {
            var empList = GetEmployeeListByPersonID(personID);
            var minStartDate = empList.OrderBy(o => o.StartDate).FirstOrDefault().StartDate;
            return minStartDate.GetValueOrDefault();
        }
    }
}
