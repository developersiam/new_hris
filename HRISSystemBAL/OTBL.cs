﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRISSystemDAL;
using DomainModelHris;

namespace HRISSystemBL
{
    public interface IOTBL
    {
        List<OT> GetAllOT();
        List<OT> GetFilterdOTList(string nameFilter, DateTime fromDate, DateTime toDate);
        List<OTType> GetOTType();
        List<OTRequestType> GetOTRequestType();
        OT GetSingleOT(string employeeID, DateTime? otDate, string otType, string requestType);
        OT GetSingleOTByDate(string employeeID, DateTime otDate);
        List<OT> GetListOTByDate(string employeeID, DateTime otDate);
        List<OT> GetHolidayOT(string employeeID, DateTime otDate);
        List<OT> GetExistingOT(string employeeID, DateTime endDate);
        void AddOT(OT o);
        void UpdateOT(OT o);
        void DeleteOT(OT o);
        List<OT> GetOTListbyDept(string deptCode, DateTime fromDate, DateTime toDate);
        List<OT> GetPersonHolidayList(string employeeID, DateTime fromDate, DateTime toDate);

    }
    public class OTBL : IOTBL
    {
        public void AddOT(OT o)
        {
            if (GetSingleOT(o.Employee_ID, o.OTDate, o.OTType, o.RequestType) != null) 
                throw new Exception("ข้อมูลนี้พบมามีการบันทึกแล้ว กรุณาตรวจสอบ");

            DataAccessLayerServices.OTRepository().Add(o);
        }

        public void DeleteOT(OT o)
        {
            DataAccessLayerServices.OTRepository().Remove(o);
        }

        public List<OT> GetAllOT()
        {
            return DataAccessLayerServices.OTRepository().GetAll().ToList();
        }

        public List<OT> GetFilterdOTList(string nameFilter, DateTime fromDate, DateTime toDate)
        {
            var otList = DataAccessLayerServices.OTRepository()
                .GetList(g => g.OTDate >= fromDate && 
                g.OTDate <= toDate, 
                g => g.Employee, 
                g => g.OTType1,
                g => g.OTRequestType,
                g => g.Employee.Person, 
                g => g.Employee.Person.TitleName)
                .ToList();

            if (!string.IsNullOrEmpty(nameFilter)) 
                otList = otList
                    .Where(w => w.Employee.Person.FirstNameTH.Contains(nameFilter))
                    .ToList();

            return otList
                .OrderByDescending(o => o.OTDate)
                .ThenBy(o => o.Employee.Person.FirstNameTH)
                .ToList();
        }

        public List<OTRequestType> GetOTRequestType()
        {
            return DataAccessLayerServices.OTRequestTypeRepository().GetAll().ToList();
        }

        public List<OTType> GetOTType()
        {
            return DataAccessLayerServices.OTTypeRepository().GetAll().ToList();
        }

        public OT GetSingleOT(string employeeID, DateTime? otDate, string otType, string requestType)
        {
            return DataAccessLayerServices
                .OTRepository()
                .GetSingle(g => g.Employee_ID == employeeID &&
                g.OTDate == otDate &&
                g.OTType == otType &&
                g.RequestType == requestType,
                g => g.OTType1,
                g => g.OTRequestType,
                g => g.Employee,
                g => g.Employee.Person,
                g => g.Employee.Person.TitleName);
        }

        public void UpdateOT(OT o)
        {
            DataAccessLayerServices.OTRepository().Update(o);
        }

        public List<OT> GetHolidayOT(string employeeID, DateTime otDate)
        {
            return DataAccessLayerServices.OTRepository()
                .GetList(g => g.Employee_ID == employeeID &&
                g.OTDate >= otDate &&
                g.OTDate <= otDate && 
                (g.OTType == "S" || g.OTType == "H"),
                g => g.OTType1,
                g => g.OTRequestType,
                g => g.Employee)
                .ToList();
        }

        public OT GetSingleOTByDate(string employeeID, DateTime otDate)
        {
            var otList = GetListOTByDate(employeeID, otDate);
            var result = new OT { Employee_ID = employeeID, OTDate = otDate, NcountOT = otList.Sum(s => s.NcountOT) };
            return result;
        }

        public List<OT> GetListOTByDate(string employeeID, DateTime otDate)
        {
            var otList = DataAccessLayerServices.OTRepository().GetList(g => g.Employee_ID == employeeID && g.OTDate == otDate, g => g.OTType1, g => g.OTRequestType).OrderBy(o => o.OTDate).ToList();
            return otList;
        }

        public List<OT> GetExistingOT(string employeeID, DateTime endDate)
        {
            var otList = DataAccessLayerServices
                .OTRepository()
                .GetList(g => g.Employee_ID == employeeID && 
                g.OTDate > endDate)
                .ToList();
            return otList;
        }

        public List<OT> GetOTListbyDept(string deptCode, DateTime fromDate, DateTime toDate)
        {
            var otList = DataAccessLayerServices
                .OTRepository()
                .GetList(g => g.OTDate >= fromDate &&
                g.OTDate <= toDate &&
                g.Employee.DeptCode == deptCode)
                .ToList();
            return otList;
        }

        public List<OT> GetPersonHolidayList(string employeeID, DateTime fromDate, DateTime toDate)
        {
            var otList = DataAccessLayerServices
                .OTRepository()
                .GetList(g => g.OTDate >= fromDate &&
                g.OTDate <= toDate &&
                g.Employee.Noted == employeeID &&
                g.Employee.Staff_type != 1 &&
                g.OTType == "H")
                .ToList();
            return otList;
        }
    }
}
