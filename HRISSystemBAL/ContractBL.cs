﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelHris;
using HRISSystemDAL;

namespace HRISSystemBL
{
    public interface IContractBL
    {
        void AddContract(EmployeeContract item);
        void UpdateContract(EmployeeContract item);
        void DeleteContract(EmployeeContract item);
        List<EmployeeContract> GetContractListByEmployeeID(string employeeID);
        EmployeeContract GetSingleEmployeeContract(string employeeID, string contractTypeID);
        void AddContractType(ContractType item);
        void UpdateContractType(ContractType item);
        void DeleteContractType(ContractType item);
        List<ContractType> GetAllContractTypeList();
        ContractType GetSingleContractType(string contractTypeID);
    }
    public class ContractBL : IContractBL
    {
        public void AddContract(EmployeeContract item)
        {
            DataAccessLayerServices.EmployeeContractRepository().Add(item);
        }

        public void AddContractType(ContractType item)
        {
            DataAccessLayerServices.ContractTypeRepository().Add(item);
        }

        public void DeleteContract(EmployeeContract item)
        {
            DataAccessLayerServices.EmployeeContractRepository().Remove(item);
        }

        public void DeleteContractType(ContractType item)
        {
            DataAccessLayerServices.ContractTypeRepository().Remove(item);
        }

        public List<ContractType> GetAllContractTypeList()
        {
            return DataAccessLayerServices.ContractTypeRepository().GetAll().ToList();
        }

        public List<EmployeeContract> GetContractListByEmployeeID(string employeeID)
        {
            return DataAccessLayerServices.EmployeeContractRepository().GetList(g => g.Employee_ID == employeeID, g => g.ContractType).ToList();
        }

        public ContractType GetSingleContractType(string contractTypeID)
        {
            return DataAccessLayerServices.ContractTypeRepository().GetSingle(g => g.ContractType_ID == contractTypeID);
        }

        public EmployeeContract GetSingleEmployeeContract(string employeeID, string contractTypeID)
        {
            return DataAccessLayerServices.EmployeeContractRepository().GetSingle(g => g.Employee_ID == employeeID && g.ContractType_ID == contractTypeID);
        }

        public void UpdateContract(EmployeeContract item)
        {
            DataAccessLayerServices.EmployeeContractRepository().Update(item);
        }

        public void UpdateContractType(ContractType item)
        {
            DataAccessLayerServices.ContractTypeRepository().Update(item);
        }
    }
}
