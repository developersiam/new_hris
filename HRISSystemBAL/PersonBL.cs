﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelHris;
using HRISSystemDAL;

namespace HRISSystemBL
{
    public interface IPersonBL
    {
        string GetNewPersonID();
        Person GetPersonByPersonID(string PersonID);
        List<TitleName> GetAllTitlenameList();
        List<Gender> GetAllGenderList();
        List<MaritalStatu> GetAllMaritalStatusList();
        List<Nationality> GetAllNationalityList();
        List<Religion> GetAllReligionList();
        List<Blood> GetAllBloodList();
        List<Race> GetAllRaceList();
        void AddPerson(Person item);
        void UpdatePerson(Person item);
        void DeletePerson(Person item);
    }
    public class PersonBL : IPersonBL
    {
        public void AddPerson(Person item)
        {
            DataAccessLayerServices.PersonRepository().Add(item);
        }

        public void DeletePerson(Person item)
        {
            throw new NotImplementedException();
        }

        public string GetNewPersonID()
        {
            //PersonRepository model = new PersonRepository();
            //string maxID = model.GetList(g => !string.IsNullOrEmpty(g.Person_ID))
            //                    .Select(s => s.Person_ID)
            //                    .OrderByDescending(o => o)
            //                    .FirstOrDefault();
            //int newID = Convert.ToInt32(maxID) + 1;
            //return newID.ToString();
            var listString = DataAccessLayerServices.PersonRepository().GetAll().Select(s => s.Person_ID).ToList();
            return BusinessLayerServices.DataCorrector().GetMaxIDFromListString(listString);
        }

        public Person GetPersonByPersonID(string PersonID)
        {
            return DataAccessLayerServices.PersonRepository().GetSingle(g => g.Person_ID == PersonID);
        }
        public List<TitleName> GetAllTitlenameList()
        {
            return DataAccessLayerServices.TitleNameRepository().GetAll().ToList();
        }
        public void UpdatePerson(Person item)
        {
            DataAccessLayerServices.PersonRepository().Update(item);
        }

        public List<Gender> GetAllGenderList()
        {
            return DataAccessLayerServices.GenderRepository().GetAll().ToList();
        }

        public List<MaritalStatu> GetAllMaritalStatusList()
        {
            return DataAccessLayerServices.MaritalStatuRepository().GetAll().ToList();
        }

        public List<Nationality> GetAllNationalityList()
        {
            return DataAccessLayerServices.NationalityRepository().GetAll().ToList();
        }

        public List<Religion> GetAllReligionList()
        {
            return DataAccessLayerServices.ReligionRepository().GetAll().ToList();
        }

        public List<Blood> GetAllBloodList()
        {
            return DataAccessLayerServices.BloodRepository().GetAll().ToList();
        }

        public List<Race> GetAllRaceList()
        {
            return DataAccessLayerServices.RaceRepository().GetAll().ToList();
        }
    }
}
