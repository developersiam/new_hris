﻿using DomainModelHris;
using HRISSystemDAL;
using HRISSystemDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystemBL
{
    public interface IShiftBL
    {
        List<Shift> GetAllShiftList();
        string GetMaxShiftID();
        Shift GetShiftByID(string shiftID);
        Shift GetShiftByName(string shiftName);
        void AddShift(Shift item);
        void UpdateShift(Shift item);
        void DeleteShift(Shift item);
        List<ShiftControl> GetShiftControl(string shiftID, DateTime fromDate, DateTime toDate);
        List<ShiftControl> GetShiftControlByDeptAndDateRange(DateTime fromDate, DateTime toDate, string deptCode);
        List<ShiftControl> GetShiftControlListByEmployeeID(string employeeID);
        ShiftControl GetSingleShiftControl(ShiftControl item);
        ShiftControl GetSingleShiftFromShiftControl(string employeeID, DateTime shiftDate);
        void AddShiftControl(ShiftControl item);
        void AddShiftControlList(List<ShiftControl> listItem, DateTime from, DateTime to);
        void UpdateShiftControl(ShiftControl item);
        void DeleteShiftControl(ShiftControl item);
    }

    public class ShiftBL : IShiftBL
    {
        public void AddShift(Shift item)
        {
            var existed = GetShiftByName(item.ShiftName);
            if (existed != null) throw new Exception("ชื้อกะถูกใช้ไปแล้ว กรุณาตรวจสอบ");
            DataAccessLayerServices.ShiftRepository().Add(item);
        }

        public void AddShiftControl(ShiftControl item)
        {
            DataAccessLayerServices.ShiftControlRepository().Add(item);
        }

        public void AddShiftControlList(List<ShiftControl> newList, DateTime from, DateTime to)
        {
            var oldList = new List<ShiftControl>();
            for (var day = from; day <= to; day = day.AddDays(1))
            {
                var _oldList = DataAccessLayerServices.ShiftControlRepository().GetList(g => g.ValidFrom == day).ToList();
                if (_oldList != null) oldList.AddRange(_oldList);
            }

            if (oldList != null)
            {
                foreach (var oldItem in oldList)
                {
                    DataAccessLayerServices.ShiftControlRepository().Remove(oldItem);
                }
            }

            if (newList != null)
            {
                foreach (var newItem in newList)
                {
                    newItem.Employee = null;
                    newItem.Shift = null;
                    DataAccessLayerServices.ShiftControlRepository().Add(newItem);
                }
            }
        }

        public void DeleteShift(Shift item)
        {
            DataAccessLayerServices.ShiftRepository().Remove(item);
        }

        public void DeleteShiftControl(ShiftControl item)
        {
            DataAccessLayerServices.ShiftControlRepository().Remove(item);
        }

        public List<Shift> GetAllShiftList()
        {
            return DataAccessLayerServices.ShiftRepository().GetAll().OrderBy(o => o.StartTime).ToList();
        }

        public string GetMaxShiftID()
        {
            var listString = DataAccessLayerServices.ShiftRepository()
                .GetAll().Select(s => s.Shift_ID)
                .ToList();
            return BusinessLayerServices.DataCorrector()
                .GetMaxIDFromListString(listString);
        }

        public Shift GetShiftByID(string shiftID)
        {
            return DataAccessLayerServices.ShiftRepository()
                .GetSingle(g => g.Shift_ID == shiftID);
        }

        public Shift GetShiftByName(string shiftName)
        {
            return DataAccessLayerServices.ShiftRepository()
                .GetSingle(g => g.ShiftName == shiftName);
        }

        public List<ShiftControl> GetShiftControl(string shiftID, DateTime fromDate, DateTime toDate)
        {
            return DataAccessLayerServices.ShiftControlRepository()
                .GetList(g => g.Shift_ID == shiftID &&
                g.ValidFrom >= fromDate &&
                g.ValidFrom <= toDate,
                g => g.Employee,
                g => g.Employee.Person,
                g => g.Employee.Person.TitleName,
                g => g.Employee.StaffType,
                g => g.Shift)
                .ToList();
        }

        public List<ShiftControl> GetShiftControlByDeptAndDateRange(DateTime fromDate, DateTime toDate, string deptCode)
        {
            return DataAccessLayerServices.ShiftControlRepository()
                .GetList(x => x.ValidFrom >= fromDate &&
                x.ValidFrom <= toDate &&
                x.Employee.DeptCode == deptCode
                , x => x.Employee)
                .ToList();
        }

        public List<ShiftControl> GetShiftControlListByEmployeeID(string employeeID)
        {
            return DataAccessLayerServices.ShiftControlRepository().GetList(g => g.Employee_ID == employeeID, g => g.Shift).OrderByDescending(o => o.ValidFrom).ToList();
        }

        public ShiftControl GetSingleShiftControl(ShiftControl item)
        {
            return DataAccessLayerServices.ShiftControlRepository().GetSingle(g => g.Employee_ID == item.Employee_ID &&
                                                                                   g.Shift_ID == item.Shift_ID &&
                                                                                   g.ValidFrom == item.ValidFrom &&
                                                                                   g.EndDate == item.EndDate);
        }

        public ShiftControl GetSingleShiftFromShiftControl(string employeeID, DateTime shiftDate)
        {
            return DataAccessLayerServices.ShiftControlRepository().GetSingle(g => g.Employee_ID == employeeID &&
                                                                                   g.ValidFrom == shiftDate &&
                                                                                   g.EndDate == shiftDate,
                                                                              g => g.Shift);
        }

        public void UpdateShift(Shift item)
        {
            DataAccessLayerServices.ShiftRepository().Update(item);
        }

        public void UpdateShiftControl(ShiftControl item)
        {
            DataAccessLayerServices.ShiftControlRepository().Update(item);
        }
    }
}
