﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelHris;
using HRISSystemDAL;

namespace HRISSystemBL
{
    public interface ITrainingBL
    {
        void AddTraining(Training item);
        void UpdateTraining(Training item);
        void DeleteTraining(Training item);
        List<Training> GetTrainingListByEmployeeID(string employeeID);
        List<Training> GetTraingListByCourseID(string courseID);
        Training GetSingleTraining(Training t);
        void AddTrainingCourse(TrainingCourse item);
        void UpdateTrainingCourse(TrainingCourse item);
        void DeleteTrainingCourse(TrainingCourse item);
        List<TrainingCourse> GetAllTrainingCourseList();
        TrainingCourse GetTrainingCourseByID(string trainingCourseID);
        string GetNewTrainingCourseID();
    }

    public class TrainingBL : ITrainingBL
    {
        public void AddTraining(Training item)
        {
            DataAccessLayerServices.TrainingRepository().Add(item);
        }

        public void AddTrainingCourse(TrainingCourse item)
        {
            DataAccessLayerServices.TrainingCourseRepository().Add(item);
        }

        public void DeleteTraining(Training item)
        {
            DataAccessLayerServices.TrainingRepository().Remove(item);
        }

        public void DeleteTrainingCourse(TrainingCourse item)
        {
            DataAccessLayerServices.TrainingCourseRepository().Remove(item);
        }

        public List<TrainingCourse> GetAllTrainingCourseList()
        {
            return DataAccessLayerServices.TrainingCourseRepository().GetAll().ToList();
        }

        public string GetNewTrainingCourseID()
        {
            var listString = DataAccessLayerServices.TrainingCourseRepository().GetAll().Select(s => s.TrainingCourse_ID).ToList();
            return BusinessLayerServices.DataCorrector().GetMaxIDFromListString(listString);
        }

        public Training GetSingleTraining(Training t)
        {
            return DataAccessLayerServices.TrainingRepository().GetSingle(g => g.BeginDate == t.BeginDate &&
                                                                               g.EndDate == t.EndDate &&
                                                                               g.TrainingCourse_ID == t.TrainingCourse_ID &&
                                                                               g.Employee_ID == t.Employee_ID);
        }

        public List<Training> GetTraingListByCourseID(string courseID)
        {
            return DataAccessLayerServices.TrainingRepository().GetList(g => g.TrainingCourse_ID == courseID, 
                                                                             g => g.TrainingCourse,
                                                                             g => g.Employee,
                                                                             g => g.Employee.Person,
                                                                             g => g.Employee.Person.TitleName).ToList();
        }

        public TrainingCourse GetTrainingCourseByID(string trainingCourseID)
        {
            return DataAccessLayerServices.TrainingCourseRepository().GetSingle(g => g.TrainingCourse_ID == trainingCourseID);
        }

        public List<Training> GetTrainingListByEmployeeID(string employeeID)
        {
            return DataAccessLayerServices.TrainingRepository().GetList(g => g.Employee_ID == employeeID, g => g.TrainingCourse).OrderByDescending(o => o.BeginDate).ToList();
        }

        public void UpdateTraining(Training item)
        {
            DataAccessLayerServices.TrainingRepository().Update(item);
        }

        public void UpdateTrainingCourse(TrainingCourse item)
        {
            DataAccessLayerServices.TrainingCourseRepository().Update(item);
        }
    }
}
