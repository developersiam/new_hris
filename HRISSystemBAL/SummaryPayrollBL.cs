﻿using DomainModelPayroll;
using HRISSystemDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystemBL
{
    public interface ISummaryPayrollBL
    {
        List<Payroll_Summary> GetPayrollSummaryList(int year);
        List<Payroll> GetCalculatePayrollList(int year);

    }
    public class SummaryPayrollBL : ISummaryPayrollBL
    {
        public List<Payroll_Summary> GetPayrollSummaryList(int year)
        {
            return DataAccessLayerServices.Payroll_SummaryRepository().GetList(g => g.Year == year).ToList();
        }

        public List<Payroll> GetCalculatePayrollList(int year)
        {
            var summaryList = new List<Payroll>();
            var payrollList = DataAccessLayerServices.PayrollRepository().GetList(g => g.Salary_paid_date.Value.Year == year).ToList();
            var employeeList = payrollList.GroupBy(g => g.Id_card).ToList().Select(s => new { idCard = s.Key }).ToList();

            employeeList.ForEach(e =>
            {
                var Id_card = e.idCard;
                var employeePayrollList = payrollList.Where(w => w.Id_card == Id_card);
                var payroll = payrollList.FirstOrDefault(s => s.Id_card == Id_card);

                //payroll.Lunch_sup = employeePayrollList.Sum(w => w.Lunch_sup);
                //payroll.Night_sup = employeePayrollList.Sum(w => w.Night_sup);
                var totalIncome = employeePayrollList.Sum(w => w.Total_Income);
                var totalIncomeLabour = employeePayrollList.Sum(w => w.Total_Income_Labour);

                payroll.Total_Income = totalIncome + totalIncomeLabour;
                payroll.Tax = employeePayrollList.Sum(w => w.Tax);
                payroll.Tax_Labour = employeePayrollList.Sum(w => w.Tax_Labour);
                payroll.SSO = employeePayrollList.Sum(w => w.SSO);
                payroll.Student_Loan = employeePayrollList.Sum(w => w.Student_Loan);
                payroll.Cut_wage = employeePayrollList.Sum(w => w.Cut_wage);
                payroll.Net_Income = employeePayrollList.Sum(w => w.Net_Income);
                payroll.NetSalary = employeePayrollList.Sum(w => w.NetSalary);

                summaryList.Add(payroll);
            });
            return summaryList;
        }
    }
}
