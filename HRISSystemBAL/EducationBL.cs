﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelHris;
using HRISSystemDAL;

namespace HRISSystemBL
{
    public interface IEducationBL
    {
        void AddEducation(Education item);
        void UpdateEducation(Education item);
        void DeleteEducation(Education item);
        List<Education> GetEducationListByPersonID(string personID);
        Education GetSingleEducation(string personID, string educationLevel, string educationQua);
        void AddEduLevel(EducationLevel item);
        void UpdateEduLevel(EducationLevel item);
        void DeleteEduLevel(EducationLevel item);
        List<EducationLevel> GetAllEduLevelList();
        EducationLevel GetSingleEducationLevel(string educationLevelID);
        void AddInstitute(Institute item);
        void UpdateInstitute(Institute item);
        void DeleteInstitute(Institute item);
        List<Institute> GetAllInstituteList();
        List<Institute> FilterInstituteList(string instituteTH);
        Institute GetSingleInstitute(string instituteID);
        void AddEduQualification(EducationQualification item);
        void UpdateEduQualification(EducationQualification item);
        void DeleteEduQualification(EducationQualification item);
        List<EducationQualification> GetAllEduQualificationList();
        EducationQualification GetSingleEducationQualification(string qualificationID);
        void AddEduMajor(Major item);
        void UpdateEduMajor(Major item);
        void DeleteEduMajor(Major item);
        List<Major> GetAllMajorList();
        Major GetSingleMajor(string majorID);
    }
    public class EducationBL : IEducationBL
    {
        public void AddEducation(Education item)
        {
            DataAccessLayerServices.EducationRepository().Add(item);
        }

        public void AddEduLevel(EducationLevel item)
        {
            DataAccessLayerServices.EducationLevelRepository().Add(item);
        }

        public void AddEduMajor(Major item)
        {
            DataAccessLayerServices.MajorRepository().Add(item);
        }

        public void AddEduQualification(EducationQualification item)
        {
            DataAccessLayerServices.EducationQualificationRepository().Add(item);
        }

        public void AddInstitute(Institute item)
        {
            DataAccessLayerServices.InstituteRepository().Add(item);
        }

        public void DeleteEducation(Education item)
        {
            DataAccessLayerServices.EducationRepository().Remove(item);
        }

        public void DeleteEduLevel(EducationLevel item)
        {
            DataAccessLayerServices.EducationLevelRepository().Remove(item);
        }

        public void DeleteEduMajor(Major item)
        {
            DataAccessLayerServices.MajorRepository().Remove(item);
        }

        public void DeleteEduQualification(EducationQualification item)
        {
            DataAccessLayerServices.EducationQualificationRepository().Remove(item);
        }

        public void DeleteInstitute(Institute item)
        {
            DataAccessLayerServices.InstituteRepository().Remove(item);
        }

        public List<Institute> FilterInstituteList(string instituteTH)
        {
            return DataAccessLayerServices.InstituteRepository().GetList(g => g.InstituteNameTH.Contains(instituteTH)).ToList();
        }

        public List<EducationLevel> GetAllEduLevelList()
        {
            return DataAccessLayerServices.EducationLevelRepository().GetAll().ToList();
        }

        public List<EducationQualification> GetAllEduQualificationList()
        {
            return DataAccessLayerServices.EducationQualificationRepository().GetAll().ToList();
        }

        public List<Institute> GetAllInstituteList()
        {
            return DataAccessLayerServices.InstituteRepository().GetAll().ToList();
        }

        public List<Major> GetAllMajorList()
        {
            return DataAccessLayerServices.MajorRepository().GetAll(g => g.EducationQualification).ToList();
        }

        public List<Education> GetEducationListByPersonID(string personID)
        {
            return DataAccessLayerServices.EducationRepository().GetList(g => g.Person_ID == personID, 
                                                                         g => g.Country, g => g.EducationLevel, 
                                                                         g => g.EducationQualification, g => g.Major, g => g.Institute).ToList();
        }

        public Education GetSingleEducation(string personID, string educationLevel, string educationQua)
        {
            //return DataAccessLayerServices.EducationRepository().GetSingle(g => g.Person_ID == personID && g.EducationLevel_ID == educationLevel && g.EducationQualification_ID == educationQua,
            //                                                               g => g.Country, g => g.EducationLevel,
            //                                                               g => g.EducationQualification, g => g.Major, g => g.Institute);
            return DataAccessLayerServices.EducationRepository().GetSingle(g => g.Person_ID == personID && g.EducationLevel_ID == educationLevel && g.EducationQualification_ID == educationQua);
        }

        public EducationLevel GetSingleEducationLevel(string educationLevelID)
        {
            return DataAccessLayerServices.EducationLevelRepository().GetSingle(g => g.EducationLevel_ID == educationLevelID);
        }

        public EducationQualification GetSingleEducationQualification(string qualificationID)
        {
            return DataAccessLayerServices.EducationQualificationRepository().GetSingle(g => g.EducationQualification_ID == qualificationID);
        }

        public Institute GetSingleInstitute(string instituteID)
        {
            return DataAccessLayerServices.InstituteRepository().GetSingle(g => g.Institute_ID == instituteID);
        }

        public Major GetSingleMajor(string majorID)
        {
            return DataAccessLayerServices.MajorRepository().GetSingle(g => g.Major_ID == majorID);
        }

        public void UpdateEducation(Education item)
        {
            DataAccessLayerServices.EducationRepository().Update(item);
        }

        public void UpdateEduLevel(EducationLevel item)
        {
            DataAccessLayerServices.EducationLevelRepository().Update(item);
        }

        public void UpdateEduMajor(Major item)
        {
            DataAccessLayerServices.MajorRepository().Update(item);
        }

        public void UpdateEduQualification(EducationQualification item)
        {
            DataAccessLayerServices.EducationQualificationRepository().Update(item);
        }

        public void UpdateInstitute(Institute item)
        {
            DataAccessLayerServices.InstituteRepository().Update(item);
        }
    }
}
