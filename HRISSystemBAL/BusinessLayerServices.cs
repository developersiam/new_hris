﻿namespace HRISSystemBL
{
    public static class BusinessLayerServices
    {
        public static IDataCorrector DataCorrector()
        {
            IDataCorrector obj = new DataCorrector();
            return obj;
        }
        public static ILeaveTypeBL LeaveTypeBL()
        {
            ILeaveTypeBL obj = new LeaveTypeBL();
            return obj;
        }
        public static IReferenceBL ReferenceBL()
        {
            IReferenceBL obj = new ReferenceBL();
            return obj;
        }
        public static IShiftBL ShiftBL()
        {
            IShiftBL obj = new ShiftBL();
            return obj;
        }
        public static IEmployeeBL EmployeeBL()
        {
            IEmployeeBL obj = new EmployeeBL();
            return obj;
        }
        public static IPersonBL PersonBL()
        {
            IPersonBL obj = new PersonBL();
            return obj;
        }
        public static IIDCardInfoBL IIDCardInfoBL()
        {
            IIDCardInfoBL obj = new IDCardInfoBL();
            return obj;
        }
        public static IAddressBL AddressBL()
        {
            IAddressBL obj = new AddressBL();
            return obj;
        }
        public static IDeptCodeBL DeptCodeBL()
        {
            IDeptCodeBL obj = new DeptCodeBL();
            return obj;
        }
        public static IContactPersonInfoBL ContactPersonInfoBL()
        {
            IContactPersonInfoBL obj = new ContactPersonInfoBL();
            return obj;
        }
        public static IFamilyBL FamilyBL()
        {
            IFamilyBL obj = new FamilyBL();
            return obj;
        }
        public static IOriginalWorkBL OriginalWorkBL()
        {
            IOriginalWorkBL obj = new OriginalWorkBL();
            return obj;
        }
        public static IEducationBL EducationBL()
        {
            IEducationBL obj = new EducationBL();
            return obj;
        }
        public static IContractBL ContractBL()
        {
            IContractBL obj = new ContractBL();
            return obj;
        }
        public static IPositionBL PositionBL()
        {
            IPositionBL obj = new PositionBL();
            return obj;
        }
        public static ITrainingBL TrainingBL()
        {
            ITrainingBL obj = new TrainingBL();
            return obj;
        }
        public static IPayrollFlagBL PayrollFlagBL()
        {
            IPayrollFlagBL obj = new PayrollFlagBL();
            return obj;
        }
        public static IPayrollBL PayrollBL()
        {
            IPayrollBL obj = new PayrollBL();
            return obj;
        }
        public static IManualWorkingBL ManualWorkingBL()
        {
            IManualWorkingBL obj = new ManualWorkingBL();
            return obj;
        }
        public static IOTBL OTBL()
        {
            IOTBL obj = new OTBL();
            return obj;
        }
        public static IOTOnlineBL OTOnlineBL()
        {
            IOTOnlineBL obj = new OTOnlineBL();
            return obj;
        }
        public static ITimeStecBL TimeStecBL()
        {
            ITimeStecBL obj = new TimeStecBL();
            return obj;
        }
        public static IOffSiteWorkBL OffSiteWorkBL()
        {
            IOffSiteWorkBL obj = new OffSiteWorkBL();
            return obj;
        }
        public static ITransactionBL TransactionBL()
        {
            ITransactionBL obj = new TransactionBL();
            return obj;
        }
        public static ITemporaryWorkerBL TemporaryWorkerBL()
        {
            ITemporaryWorkerBL obj = new TemporaryWorkerBL();
            return obj;
        }
        public static ISummaryPayrollBL SummaryPayrollBL()
        {
            ISummaryPayrollBL obj = new SummaryPayrollBL();
            return obj;
        }
    }
}
