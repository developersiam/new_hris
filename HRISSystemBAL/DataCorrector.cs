﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystemBL
{
    public interface IDataCorrector
    {
        string GetMaxIDFromListString(List<string> listString);
        string GetNewEmplyeeID(string maxID);
    }
    public class DataCorrector : IDataCorrector
    {
        public string GetMaxIDFromListString(List<string> listString)
        {
            if (listString == null || !listString.Any()) return "0";
            var listInt = new List<int>();
            listString.ForEach(f => listInt.Add(Convert.ToInt32(f)));
            int newID = listInt.Max() + 1;
            return newID.ToString();
        }

        public string GetNewEmplyeeID(string maxID)
        {
            string currentYear = DateTime.Now.Year.ToString().Substring(2, 2);
            if (string.IsNullOrEmpty(maxID)) return currentYear + "0000"; // No employee in current year yet.
            else return (Convert.ToInt32(maxID) + 1).ToString(); // Had employee in current year
        }
    }
}
