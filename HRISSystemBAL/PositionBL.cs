﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelHris;
using HRISSystemDAL;

namespace HRISSystemBL
{
    public interface IPositionBL
    {
        void AddPosition(Position item);
        void UpdatePosition(Position item);
        void DeletePosition(Position item);
        List<Position> GetAllList();
        List<Position> GetListByFilter(string searchText);
        List<Position> GetListByPositionID(string positionID);
        List<Position> GetPositionByDepartment(string deptCode);
        string GetNewPositionID();
        Position GetPositionByID(string positionID);

    }
    public class PositionBL : IPositionBL
    {
        public void AddPosition(Position item)
        {
            DataAccessLayerServices.PositionRepository().Add(item);
        }

        public void DeletePosition(Position item)
        {
            DataAccessLayerServices.PositionRepository().Remove(item);
        }

        public List<Position> GetAllList()
        {
            return DataAccessLayerServices.PositionRepository().GetAll().ToList();
        }

        public List<Position> GetListByFilter(string searchText)
        {
            return DataAccessLayerServices.PositionRepository().GetList(g => g.PositionNameTH.Contains(searchText)).ToList();
        }

        public List<Position> GetListByPositionID(string positionID)
        {
            return DataAccessLayerServices.PositionRepository().GetList(g => g.Position_ID == positionID).ToList();
        }

        public string GetNewPositionID()
        {
            var listString = DataAccessLayerServices.PositionRepository().GetAll().Select(s => s.Position_ID).ToList();
            return BusinessLayerServices.DataCorrector().GetMaxIDFromListString(listString);
        }

        public List<Position> GetPositionByDepartment(string deptCode)
        {
            throw new NotImplementedException();
        }

        public Position GetPositionByID(string positionID)
        {
            return DataAccessLayerServices.PositionRepository().GetSingle(g => g.Position_ID == positionID);
        }

        public void UpdatePosition(Position item)
        {
            DataAccessLayerServices.PositionRepository().Update(item);
        }
    }
}
