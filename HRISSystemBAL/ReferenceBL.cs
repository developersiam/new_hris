﻿using DomainModelHris;
using HRISSystemDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystemBL
{
    public interface IReferenceBL
    {
        //Branch Setup
        List<Branch> GetAllBranch();
        Branch GetBranchByID(string BranchID);
        void AddBranch(Branch item);
        void UpdateBranch(Branch item);
        void DeleteBranch(Branch item);
        // CropSetup
        List<CropSetup> GetAllCropSetup();
        CropSetup GetCropSetup(int Crop);
        void AddCropSetup(CropSetup item);
        void UpdateCropSetup(CropSetup item);
        void DeleteCropSetup(CropSetup item);
        // Holiday Setup
        List<Holiday> GetHolidayListByCrop(int Crop);
        List<Holiday> GetSpecialHolidayListByCrop(int Crop);
        Holiday GetHolidayByDate(DateTime HolidayDate);
        void AddHoliday(Holiday item);
        void UpdateHoliday(Holiday item);
        void DeleteHoliday(Holiday item);
    }
    class ReferenceBL : IReferenceBL
    {
        public void AddBranch(Branch item)
        {
            DataAccessLayerServices.BranchRepository().Add(item);
        }

        public void DeleteBranch(Branch item)
        {
            DataAccessLayerServices.BranchRepository().Remove(item);
        }

        public List<Branch> GetAllBranch()
        {
            return DataAccessLayerServices.BranchRepository().GetAll().ToList();
        }

        public Branch GetBranchByID(string BranchID)
        {
            return DataAccessLayerServices.BranchRepository().GetSingle(g => g.Branch_ID == BranchID);
        }

        public void UpdateBranch(Branch item)
        {
            DataAccessLayerServices.BranchRepository().Update(item);
        }
        public void AddCropSetup(CropSetup item)
        {
            DataAccessLayerServices.CropSetupRepository().Add(item);
        }

        public void DeleteCropSetup(CropSetup item)
        {
            DataAccessLayerServices.CropSetupRepository().Remove(item);
        }

        public List<CropSetup> GetAllCropSetup()
        {
            return DataAccessLayerServices.CropSetupRepository().GetAll().OrderByDescending(o => o.Crop).ToList();
        }

        public CropSetup GetCropSetup(int Crop)
        {
            return DataAccessLayerServices.CropSetupRepository().GetSingle(g => g.Crop == Crop);
        }

        public void UpdateCropSetup(CropSetup item)
        {
            DataAccessLayerServices.CropSetupRepository().Update(item);
        }
        public void AddHoliday(Holiday item)
        {
            DataAccessLayerServices.HolidayRepository().Add(item);
        }

        public void DeleteHoliday(Holiday item)
        {
            DataAccessLayerServices.HolidayRepository().Remove(item);
        }

        public List<Holiday> GetHolidayListByCrop(int Crop)
        {
            return DataAccessLayerServices.HolidayRepository().GetList(g => g.Crop == Crop).OrderBy(o => o.Date).ToList();
        }

        public Holiday GetHolidayByDate(DateTime HolidayDate)
        {
            return DataAccessLayerServices.HolidayRepository().GetSingle(s => s.Date == HolidayDate.Date);
            //DbFunctions.TruncateTime
        }

        public void UpdateHoliday(Holiday item)
        {
            DataAccessLayerServices.HolidayRepository().Update(item);
        }

        public List<Holiday> GetSpecialHolidayListByCrop(int Crop)
        {
            return DataAccessLayerServices.HolidayRepository().GetList(g => g.Crop == Crop && !g.HolidayFlag.Value).OrderBy(o => o.Date).ToList();
        }
    }
}
