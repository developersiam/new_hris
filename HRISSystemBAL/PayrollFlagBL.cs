﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelHris;
using HRISSystemDAL;

namespace HRISSystemBL
{
    public interface IPayrollFlagBL
    {
        void AddPayrollFlag(PayrollFlag item);
        void DeletePayrollFlag(PayrollFlag item);
        List<PayrollFlag> GetPayrollFlagListByEmployeeID(string employeeID);
        List<PayrollFlag> GetPayrollFlagListByDate(DateTime fromDate, DateTime toDate);
    }
    public class PayrollFlagBL : IPayrollFlagBL
    {
        public void AddPayrollFlag(PayrollFlag item)
        {
            DataAccessLayerServices.PayrollFlagRepository().Add(item);
        }

        public void DeletePayrollFlag(PayrollFlag item)
        {
            DataAccessLayerServices.PayrollFlagRepository().Remove(item);
        }

        public List<PayrollFlag> GetPayrollFlagListByDate(DateTime fromDate, DateTime toDate)
        {
            return DataAccessLayerServices.PayrollFlagRepository().GetList(g => g.ValidFrom >= fromDate && g.ValidFrom <= toDate).ToList();
        }

        public List<PayrollFlag> GetPayrollFlagListByEmployeeID(string employeeID)
        {
            return DataAccessLayerServices.PayrollFlagRepository().GetList(g => g.Employee_ID == employeeID).ToList();
        }
    }
}
