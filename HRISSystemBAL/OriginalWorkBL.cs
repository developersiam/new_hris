﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelHris;
using HRISSystemDAL;

namespace HRISSystemBL
{
    public interface IOriginalWorkBL
    {
        void AddOriginalWork(OriginalWorkHistory item);
        void UpdateOriginalWork(OriginalWorkHistory item);
        void DeleteOriginalWork(OriginalWorkHistory item);
        List<OriginalWorkHistory> GetOriginalWorkingListByPersonID(string personID);
        string GetNewOriginalWorkID();
        OriginalWorkHistory GetSingleOriginalWorkHistory(string originalWorkID);
    }
    public class OriginalWorkBL : IOriginalWorkBL
    {
        public void AddOriginalWork(OriginalWorkHistory item)
        {
            DataAccessLayerServices.OriginalWorkHistoryRepository().Add(item);
        }

        public void DeleteOriginalWork(OriginalWorkHistory item)
        {
            DataAccessLayerServices.OriginalWorkHistoryRepository().Remove(item);
        }

        public string GetNewOriginalWorkID()
        {
            var listString = DataAccessLayerServices.OriginalWorkHistoryRepository().GetAll().Select(s => s.OrginalWork_ID).ToList();
            return BusinessLayerServices.DataCorrector().GetMaxIDFromListString(listString);
        }

        public List<OriginalWorkHistory> GetOriginalWorkingListByPersonID(string personID)
        {
            return DataAccessLayerServices.OriginalWorkHistoryRepository().GetList(g => g.Person_ID == personID).ToList();
        }

        public OriginalWorkHistory GetSingleOriginalWorkHistory(string originalWorkID)
        {
            return DataAccessLayerServices.OriginalWorkHistoryRepository().GetSingle(g => g.OrginalWork_ID == originalWorkID);
        }

        public void UpdateOriginalWork(OriginalWorkHistory item)
        {
            DataAccessLayerServices.OriginalWorkHistoryRepository().Update(item);
        }
    }
}
