﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelTimeStec;
using HRISSystemDAL;

namespace HRISSystemBL
{
    public interface ITimeStecBL
    {
        void AddUserInfo(userinfo u);
        void UpdateUserInfo(userinfo u);
        void DeleteUserInfo(userinfo u);
        List<checkinout> GetCheckInOut(string fingerID, DateTime scanedDate);
        List<department> GetAllDepartment();
        department GetTimeStecDepartmentID(string departmentID);
        userinfo GetNewUserInfo();
        userinfo GetUserInfoByFingerID(string fingerID);
        userinfo GetUserInfoByPersonID(string personID);
        userinfo GetUserInfoByCard(string card);
    }
    public class TimeStecBL : ITimeStecBL
    {
        public void AddUserInfo(userinfo u)
        {
            DataAccessLayerServices.UserinfoRepository().Add(u);
        }

        public void DeleteUserInfo(userinfo u)
        {
            DataAccessLayerServices.UserinfoRepository().Remove(u);
        }

        public List<department> GetAllDepartment()
        {
            return DataAccessLayerServices.DepartmentTimeStecRepository().GetAll().ToList();
        }

        public List<checkinout> GetCheckInOut(string fingerID, DateTime scanedDate)
        {
            // BXUO190360022 IS BACKSCAN * BXUO190360026 IS FRONTSCAN
            var _userInfo = GetUserInfoByFingerID(fingerID);
            if (_userInfo == null) throw new NullReferenceException("ไม่พบข้อมูลผู่ใช้นี้ในฐานข้อมูล TimeStec, กรุณาตรวจสอบ");
            DateTime addedDate = scanedDate.AddDays(1);
            //return DataAccessLayerServices.CheckinoutRepository().GetList(g => g.userid == _userInfo.userid &&
            //                                                              g.checktime >= scanedDate &&
            //                                                              (g.sn_name != "BXUO190360026" && g.sn_name != "BXUO190360022") &&
            //                                                              g.checktime < addedDate).ToList();
            // 2021-01-06 All Scanner use as checkin-out scanner
            return DataAccessLayerServices.CheckinoutRepository().GetList(g => g.userid == _userInfo.userid &&
                                                                          g.checktime >= scanedDate &&
                                                                          g.checktime < addedDate).ToList();
        }

        public userinfo GetNewUserInfo()
        {
            var maxUserInfo = DataAccessLayerServices.UserinfoRepository().GetAll().OrderByDescending(o => o.userid).FirstOrDefault();
            int newUserID = maxUserInfo.userid + 1;
            string newBadgeNumber = (int.Parse(maxUserInfo.badgenumber) + 1).ToString();
            var newUserInfo = new userinfo { userid = newUserID, badgenumber = newBadgeNumber };
            return newUserInfo;
        }

        public department GetTimeStecDepartmentID(string departmentID)
        {
            return DataAccessLayerServices.DepartmentTimeStecRepository().GetSingle(g => g.code == departmentID);
        }

        public userinfo GetUserInfoByCard(string card)
        {
            return DataAccessLayerServices.UserinfoRepository().GetSingle(g => g.Card == card);
        }

        public userinfo GetUserInfoByFingerID(string fingerID)
        {
            string padFingerID = fingerID.PadLeft(13, '0');
            return DataAccessLayerServices.UserinfoRepository().GetSingle(g => g.badgenumber == padFingerID);
        }

        public userinfo GetUserInfoByPersonID(string personID)
        {
            var employee = BusinessLayerServices.EmployeeBL().GetLastEmployeeByPersonID(personID);
            return GetUserInfoByFingerID(employee.FingerScanID);
        }

        public void UpdateUserInfo(userinfo u)
        {
            DataAccessLayerServices.UserinfoRepository().Update(u);
        }
    }
}
