﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelHris;
using HRISSystemDAL;

namespace HRISSystemBL
{
    public interface IIDCardInfoBL
    {
        IDCardInfo GetSingleIDCard(string idCard);
        List<IDCardInfo> GetIDCardByPersonID(string PersonID);
        void Add(IDCardInfo item);
        void Update(IDCardInfo item);
        void Delete(IDCardInfo item);
        IDCardInfo GetSingleIDCardInfo(string cardID, DateTime expireDate);
    }
    public class IDCardInfoBL : IIDCardInfoBL
    {
        public void Add(IDCardInfo item)
        {
            DataAccessLayerServices.IDCardInfoRepository().Add(item);
        }

        public void Delete(IDCardInfo item)
        {
            DataAccessLayerServices.IDCardInfoRepository().Remove(item);
        }

        public List<IDCardInfo> GetIDCardByPersonID(string PersonID)
        {
            return DataAccessLayerServices.IDCardInfoRepository().GetList(g => g.Person_ID == PersonID, g => g.Province, g => g.District).ToList();
        }

        public IDCardInfo GetSingleIDCard(string idCard)
        {
            var recentCard = DataAccessLayerServices.IDCardInfoRepository().GetList(g => g.Card_ID == idCard, 
                                                                                         g => g.Person,
                                                                                         g => g.Person.TitleName)
                                                                           .ToList().OrderByDescending(o => o.ExpiredDate).ToList().FirstOrDefault();
            return recentCard;
        }

        public IDCardInfo GetSingleIDCardInfo(string cardID, DateTime expireDate)
        {
            return DataAccessLayerServices.IDCardInfoRepository().GetSingle(g => g.Card_ID == cardID && g.ExpiredDate == expireDate);
        }

        public void Update(IDCardInfo item)
        {
            DataAccessLayerServices.IDCardInfoRepository().Update(item);
        }
    }
}
