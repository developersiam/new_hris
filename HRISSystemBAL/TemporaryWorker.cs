﻿using DomainModelHris;
using DomainModelPayroll;
using HRISSystemDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystemBL
{
    public interface ITemporaryWorkerBL
    {
        void AddTemporaryWage(TemporaryWorkerWage item);
        void UpdateTemporaryWage(TemporaryWorkerWage item);
        void DeleteTemporaryWage(TemporaryWorkerWage item);
        List<TemporaryWorkerWage> GetTemporaryWageListByDate(string deptCode, DateTime fromDate, DateTime toDate);
        TemporaryWorkerWage GetSingleWage(string deptCode, string positionID, DateTime wageDate);
        List<sp_GetTemporaryWorkerWage_Result> sp_GetTemporaryWorkerWage(DateTime fromDate, DateTime toDate, string deptCode);
        decimal GetWagebyDateList(string deptCode, string positionID, DateTime fromDate, DateTime toDate);
    }

    public class TemporaryWorkerBL : ITemporaryWorkerBL
    {
        public void AddTemporaryWage(TemporaryWorkerWage item)
        {
            var selected = GetSingleWage(item.DepartmentCode, item.PositionID, item.WageDate);
            if (selected == null)
            {
                DataAccessLayerServices.TemporaryWorkerWageRepository().Add(item);
            }
            else throw new Exception("ข้อมูลนี้มีการบันทึกอยู่แล้ว กรุราตรวจสอบ");
        }

        public void DeleteTemporaryWage(TemporaryWorkerWage item)
        {
            var selected = GetSingleWage(item.DepartmentCode, item.PositionID, item.WageDate);
            if (selected != null)
            {
                DataAccessLayerServices.TemporaryWorkerWageRepository().Remove(selected);
            }
            else throw new Exception("ไม่พบข้อมูลที่ต้องการลบ กรุราตรวจสอบ");
        }

        public TemporaryWorkerWage GetSingleWage(string deptCode, string positionID, DateTime wageDate)
        {
            return DataAccessLayerServices.TemporaryWorkerWageRepository().GetSingle(g => g.DepartmentCode == deptCode && g.PositionID == positionID && g.WageDate == wageDate);
        }

        public List<TemporaryWorkerWage> GetTemporaryWageListByDate(string deptCode, DateTime fromDate, DateTime toDate)
        {
            return DataAccessLayerServices.TemporaryWorkerWageRepository().GetList(g => g.DepartmentCode == deptCode && g.WageDate >= fromDate && g.WageDate <= toDate,
                                                                                   g => g.Department,
                                                                                   g => g.Position).ToList();
        }

        public void UpdateTemporaryWage(TemporaryWorkerWage item)
        {
            DataAccessLayerServices.TemporaryWorkerWageRepository().Update(item);
        }

        public List<sp_GetTemporaryWorkerWage_Result> sp_GetTemporaryWorkerWage(DateTime fromDate, DateTime toDate, string deptCode)
        {
            return DataAccessLayerServices.StoreProcedureRepository().sp_GetTemporaryWorkerWage(fromDate, toDate, deptCode).ToList();
        }

        public decimal GetWagebyDateList(string deptCode, string positionID, DateTime fromDate, DateTime toDate)
        {
            decimal result = 0;
            for (var day = fromDate.Date; day.Date <= toDate.Date; day = day.AddDays(1))
            {
                var wage = GetSingleWage(deptCode, positionID, day);
                result += wage == null ? 0 : wage.Wage;
            }
            return result;
        }
    }
}
