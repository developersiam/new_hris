﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelHris;
using HRISSystemDAL;

namespace HRISSystemBL
{
    public interface IOffSiteWorkBL
    {
        List<WorkUpCountry> GetAll();
        List<WorkUpCountry> GetFilterdOffsiteList(string nameFilter, DateTime fromDate, DateTime toDate);
        WorkUpCountry GetSingleOffSite(DateTime workDate, string employeeID);
        void AddOffSiteWork(WorkUpCountry w);
        void UpdateOffSiteWork(WorkUpCountry w);
        void DeleteOffSiteWork(WorkUpCountry w);
        List<WorkUpCountryNoted> WorkUpCountryNotedList();
    }
    public class OffSiteWorkBL : IOffSiteWorkBL
    {
        public void AddOffSiteWork(WorkUpCountry w)
        {
            DataAccessLayerServices.WorkUpCountryRepository().Add(w);
        }

        public void DeleteOffSiteWork(WorkUpCountry w)
        {
            DataAccessLayerServices.WorkUpCountryRepository().Remove(w);
        }

        public List<WorkUpCountry> GetAll()
        {
            return DataAccessLayerServices.WorkUpCountryRepository().GetAll().ToList();
        }

        public List<WorkUpCountry> GetFilterdOffsiteList(string nameFilter, DateTime fromDate, DateTime toDate)
        {
            var osList = DataAccessLayerServices.WorkUpCountryRepository().GetList(g => g.WorkDate >= fromDate &&
                                                                                        g.WorkDate <= toDate,
                                                                                        g => g.WorkUpCountryNoted,
                                                                                        g => g.Employee, g => g.Employee.Person,
                                                                                                         g => g.Employee.Person.TitleName).ToList();
            //if (!string.IsNullOrEmpty(nameFilter)) osList = osList.Where(w => w.Employee.Person.FirstNameTH.Contains(nameFilter)).ToList();
            return osList.OrderByDescending(o => o.WorkDate).ThenBy(o => o.Employee.Person.FirstNameTH).ToList();
        }

        public WorkUpCountry GetSingleOffSite(DateTime workDate, string employeeID)
        {
            return DataAccessLayerServices.WorkUpCountryRepository().GetSingle(g => g.WorkDate == workDate && g.Employee_ID == employeeID);
        }

        public void UpdateOffSiteWork(WorkUpCountry w)
        {
            var item = GetSingleOffSite(w.WorkDate, w.Employee_ID);
            if (item != null) DeleteOffSiteWork(item);
            AddOffSiteWork(w);
            DataAccessLayerServices.WorkUpCountryRepository().Update(w);
        }

        public List<WorkUpCountryNoted> WorkUpCountryNotedList()
        {
            return DataAccessLayerServices.WorkUpCountryNotedRepository().GetAll().ToList();
        }
    }
}
