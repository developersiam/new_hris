﻿using System;
using System.Collections.Generic;
using System.Linq;
using DomainModelHris;
using HRISSystemDAL;

namespace HRISSystemBL
{
    public interface ILeaveTypeBL
    {
        List<LeaveType> GetAllLeaveType();
        List<LeaveType> GetLabourLeaveType();
        LeaveType GetLeaveTypeByID(string LeaveTypeID);
        string GetNewLeaveTypeID();
        void AddLeaveType(LeaveType item);
        void UpdateLeaveType(LeaveType item);
        void DeleteLeaveType(LeaveType item);
        decimal AddLeave(Leave l, decimal remaining);
        decimal AddHolidayLeave(Leave l);
        void DeleteLeave(Leave l);
        Leave GetSingleLeave(Leave l);
        List<Leave> GetLeaveList(DateTime fromDate, DateTime toDate);
        List<Leave> GetYearlyLeaveListByPerson(string personID, int year);
        List<LeaveSetup> GetAllLeaveSetup();
        List<LeaveSetup> GetLeaveSetupList(string deptCode, string employeeID, int Valid, int staffType);
        List<LeaveSetup> GetAvialableLeaveSetupListByDate(string employeeID, DateTime fromDate, DateTime toDate);
        LeaveSetup GetSingleLeaveSetup(string employeeID, string leaveType, DateTime validDate);
        LeaveSetup GetLeaveSetupByID(int leaveSetupID);
        //void UpdateLeaveSetupRemaining(Leave l);
        //void ReturnLeaveSetupRemaining(string Employee_ID, string LeaveType_ID, decimal? NcountLeave);
        void AddLeaveSetup(LeaveSetup item);
        void UpdateLeaveSetup(LeaveSetup item);
        void DeleteLeaveSetup(LeaveSetup item);
        List<Employee> GetLeaveSetupUpdateEmployeeList(string deptCode, string employeeID, int staffType);

        List<LeaveSetup> GetAll_AvialableLeaveSetupList_ByEmployeeID(string employeeID);
    }
    public class LeaveTypeBL : ILeaveTypeBL
    {
        public decimal AddLeave(Leave l, decimal remaining)
        {
            var existed = GetSingleLeave(l);
            var isOver = l.Status;
            if (existed == null)
            {
                var required = l.NcountLeave;
                l.LeaveSetup_ID = 1;
                if (l.LeaveType_ID == "L002" || l.LeaveType_ID == "L003")
                {
                    // L002 = ลากิจ/ลากิจพิเศษ L003 = ลาป่วย

                    l.NcountLeave = required > remaining ? (remaining > 0 ? remaining : required) : required; //ถ้าจำนวนที่ขอมากกว่าโควต้าที่เหลือ ให้บันทึกเป็นจำนวนโควต้าแทน แต่ถ้าคงเหลือเป็น 0 ให้จำนวนที่ขอเป็นลาเกิน
                    l.Noted = remaining == 0 ? "ลาเกิน" : l.Noted; // ถ้าโควต้าเป็น 0 จะบันทึกเป็น ลาเกิน
                    l.Status = remaining != 0 && isOver; // ถ้าสถานะเป็น ลาเกิน สถานะจะเป็น False -- isOver มาจาก checkbox
                    DataAccessLayerServices.LeaveRepository().Add(l);
                    remaining = required > remaining ? 0 : remaining - required.Value;
                }
                else DataAccessLayerServices.LeaveRepository().Add(l);
            }
            return remaining;
        }
        public decimal AddHolidayLeave(Leave l)
        {
            var required = l.NcountLeave;
            var existed = GetSingleLeave(l);
            var isOver = l.Status;
            if (existed == null)
            {
                do
                {
                    int LeaveSetup_ID = 0;
                    var remaining = GetAvialableLeaveSetupListByDate(l.Employee_ID, l.LeaveDate, l.LeaveDate).OrderBy(o => o.ValidFrom).FirstOrDefault(f => f.LeaveAmount > 0 && f.LeaveType_ID == l.LeaveType_ID);

                    if (remaining == null) LeaveSetup_ID = 1; // 1 is temporary index for old data and over leave quota
                    else
                    {
                        LeaveSetup_ID = remaining.LeaveSetup_ID;
                        var _remain = remaining.LeaveAmount;
                        var _difRemain = _remain - required; //decrease remain by require
                        var remain = _difRemain <= 0 ? 0 : _difRemain; //if remain less than zero set it 0
                        required = required - _remain; //decrease require leave amount
                        remaining.LeaveAmount = remain; //set model remain value
                        UpdateLeaveSetup(remaining); //update remaining

                        l.NcountLeave = required <= 0 ? _remain - _difRemain : _remain; //nleave = remain when require is more than remain
                    }

                    l.LeaveSetup_ID = LeaveSetup_ID;
                    l.Noted = LeaveSetup_ID == 1 ? "ลาเกิน" : l.Noted;
                    l.Status = LeaveSetup_ID != 1 && isOver; // ถ้าสถานะเป็น ลาเกิน สถานะจะเป็น False -- isOver มาจาก checkbox
                    DataAccessLayerServices.LeaveRepository().Add(l);

                    l.NcountLeave = required; // when required id over in this record and next record is null this value is OVER QUOTA
                    required = LeaveSetup_ID == 1 ? 0 : required;
                } while (required > 0);
                return 0;
            }
            return 1; // To decrease require amount
        }

        public void AddLeaveType(LeaveType item)
        {
            DataAccessLayerServices.LeaveTypeRepository().Add(item);
        }

        public void DeleteLeave(Leave l)
        {
            ReturnLeaveSetupRemaining(l);
            DataAccessLayerServices.LeaveRepository().Remove(l);
        }

        public void ReturnLeaveSetupRemaining(Leave l)
        {
            //2022-07-06 Return Leave Amount when leave delete
            // relation error
            //throw new NotImplementedException();
            if (l.LeaveSetup_ID == 1) return;
            var _return = l.NcountLeave;
            var leaveSetup = GetLeaveSetupByID(l.LeaveSetup_ID);
            var remain = leaveSetup.LeaveAmount + _return;
            leaveSetup.LeaveAmount = remain;
            UpdateLeaveSetup(leaveSetup);
        }

        public LeaveSetup GetLeaveSetupByID(int leaveSetupID)
        {
            return DataAccessLayerServices.LeaveSetupRepository().GetSingle(g => g.LeaveSetup_ID == leaveSetupID);
        }

        public void DeleteLeaveSetup(LeaveSetup item)
        {
            var leaveList = DataAccessLayerServices.LeaveRepository().GetList(g => g.LeaveSetup_ID == item.LeaveSetup_ID).ToList();
            if (leaveList.Any())
            {
                throw new Exception("ไม่สามารถลบได้ ตรวจพบการใช้งานโควต้านี้อยู่ " + leaveList.Count + " รายการ");
            }
            DataAccessLayerServices.LeaveSetupRepository().Remove(item);
        }

        public void DeleteLeaveType(LeaveType item)
        {
            DataAccessLayerServices.LeaveTypeRepository().Remove(item);
        }

        public List<LeaveSetup> GetAllLeaveSetup()
        {
            var itemList = DataAccessLayerServices.LeaveSetupRepository().GetAll().ToList();
            return itemList;
        }

        public List<LeaveType> GetAllLeaveType()
        {
            var result = DataAccessLayerServices.LeaveTypeRepository().GetAll().ToList();
            return result;
        }

        public List<LeaveType> GetLabourLeaveType()
        {
            var result = new List<LeaveType>();
            result.Add(new LeaveType { LeaveType_ID = "L001", LeaveTypeNameTH = "ลาพักผ่อน" });
            result.Add(new LeaveType { LeaveType_ID = "L002", LeaveTypeNameTH = "ลากิจพิเศษ" });
            result.Add(new LeaveType { LeaveType_ID = "L005", LeaveTypeNameTH = "ลากิจ" });
            return result;
        }

        public List<Leave> GetLeaveList(DateTime fromDate, DateTime toDate)
        {
            // relation error
            //throw new NotImplementedException();
            var leaveList = DataAccessLayerServices.LeaveRepository().GetList(g => g.LeaveDate >= fromDate && g.LeaveDate <= toDate,
                                                                              g => g.Employee,
                                                                              g => g.Employee.Person,
                                                                              g => g.Employee.Person.TitleName,
                                                                              g => g.LeaveType,
                                                                              g => g.LeaveSetup).ToList();
            return leaveList.OrderByDescending(o => o.LeaveDate).ThenBy(o => o.Employee.Person.FirstNameTH).ToList();
        }

        public List<Leave> GetYearlyLeaveListByPerson(string personID, int year)
        {
            var fromDate = new DateTime(year, 1, 1);
            var toDate = new DateTime(year, 12, 31);
            var leaveList = GetLeaveList(fromDate, toDate).Where(w => w.Employee.Person_ID == personID).ToList();
            return leaveList;
        }

        public List<Employee> GetLeaveSetupUpdateEmployeeList(string deptCode, string employeeID, int staffType)
        {
            var result = new List<Employee>();

            if (employeeID != "") result.Add(BusinessLayerServices.EmployeeBL().GetEmployeeByEmployeeID(employeeID));
            else if (deptCode != "") result = BusinessLayerServices.EmployeeBL().GetEmployeeListByDepartment(deptCode);
            else
            {
                var deptList = BusinessLayerServices.DeptCodeBL().GetAllDepartment();
                foreach (var dept in deptList)
                {
                    result.AddRange(BusinessLayerServices.EmployeeBL().GetEmployeeListByDepartment(deptCode));
                }
            }

            if (staffType != 0) result = result.Where(w => w.Staff_type == staffType).ToList();

            //if (staffType == 23) result = result.Where(w => w.Staff_type == 2 || w.Staff_type == 3).ToList();
            //else if (staffType != 0) result = result.Where(w => w.Staff_type == staffType).ToList();

            return result;
        }

        public List<LeaveSetup> GetLeaveSetupList(string deptCode, string employeeID, int Valid, int staffType)
        {
            // relation error
            //throw new NotImplementedException();
            var leaveList = new List<LeaveSetup>();
            if (employeeID == "") leaveList = GetLeaveSetupListByDepartment(deptCode);
            else leaveList = GetLeaveSetupListByEmployee(employeeID);

            if (Valid == 1) leaveList = leaveList.Where(w => w.ValidTo >= DateTime.Now.Date && w.ValidFrom <= DateTime.Now.Date).ToList(); // Avialable
            else if (Valid == 2) leaveList = leaveList.Where(w => w.ValidTo < DateTime.Now).ToList(); // Expired

            //if (staffType == 23) leaveList = leaveList.Where(w => w.Employee.Staff_type == 2 || w.Employee.Staff_type == 3).ToList(); // รายวัน/รายวันประจำ
            //else if (staffType != 0) leaveList = leaveList.Where(w => w.Employee.Staff_type == staffType).ToList(); // รายวันชั่วคราว/พนักงานประจำ

            if (staffType != 0) leaveList = leaveList.Where(w => w.Employee.Staff_type == staffType).ToList();

            leaveList.ForEach(f => { if (f.Employee.Staff_type != 1 && f.LeaveType_ID == "L002") f.LeaveType.LeaveTypeNameTH = "ลากิจพิเศษ"; });

            return leaveList;
        }

        public List<LeaveSetup> GetLeaveSetupListByDepartment(string deptCode)
        {
            // relation error
            //throw new NotImplementedException();
            var leaveList = new List<LeaveSetup>();
            if (deptCode == "")
            {
                leaveList = DataAccessLayerServices.LeaveSetupRepository().GetList(g => g.Year >= DateTime.Now.Year - 3,
                                                                                        g => g.Employee,
                                                                                        g => g.Employee.Person,
                                                                                        g => g.Employee.StaffType,
                                                                                        g => g.LeaveType).ToList();
            }
            else
            {
                leaveList = DataAccessLayerServices.LeaveSetupRepository().GetList(g => g.Employee.DeptCode == deptCode && g.Year >= DateTime.Now.Year - 3,
                                                                                   g => g.Employee,
                                                                                   g => g.Employee.Person,
                                                                                   g => g.Employee.StaffType,
                                                                                   g => g.LeaveType).ToList();
            }
            return leaveList;
        }

        public List<LeaveSetup> GetLeaveSetupListByEmployee(string employeeID)
        {
            // relation error
            //throw new NotImplementedException();
            var leaveList = DataAccessLayerServices.LeaveSetupRepository().GetList(g => g.Employee_ID == employeeID && g.Year >= DateTime.Now.Year - 3,
                                                                                   g => g.Employee,
                                                                                   g => g.Employee.Person,
                                                                                   g => g.Employee.StaffType,
                                                                                   g => g.LeaveType).ToList();
            return leaveList;
        }

        public LeaveType GetLeaveTypeByID(string LeaveTypeID)
        {
            return DataAccessLayerServices.LeaveTypeRepository().GetSingle(g => g.LeaveType_ID == LeaveTypeID);
        }

        public string GetNewLeaveTypeID()
        {
            string maxID = DataAccessLayerServices.LeaveTypeRepository().GetAll().Max(m => m.LeaveType_ID).ToString();
            string subMaxID = maxID.Substring(1, maxID.Length - 1);
            int intMaxID = int.Parse(subMaxID) + 1;
            return "L" + intMaxID.ToString().PadLeft(3, '0');
        }

        public LeaveSetup GetSingleLeaveSetup(string employeeID, string leaveType, DateTime validDate)
        {
            var item = DataAccessLayerServices.LeaveSetupRepository().GetSingle(g => g.Employee_ID == employeeID && g.LeaveType_ID == leaveType && g.ValidFrom == validDate,
                                                                                g => g.Employee,
                                                                                g => g.Employee.Person);
            return item;
        }

        public void AddLeaveSetup(LeaveSetup item)
        {
            DataAccessLayerServices.LeaveSetupRepository().Add(item);
        }

        public void UpdateLeaveSetup(LeaveSetup item)
        {
            var existed = GetSingleLeaveSetup(item.Employee_ID, item.LeaveType_ID, item.ValidFrom);
            if (existed == null) AddLeaveSetup(item); // Add new when data is not existed
            //else throw new Exception("ข้อมูลวันลาของพนักงานซ้ำ");
            else
            {
                item.LeaveSetup_ID = existed.LeaveSetup_ID;
                DataAccessLayerServices.LeaveSetupRepository().Update(item); // Update when found existing data
            }
        }

        public void UpdateLeaveType(LeaveType item)
        {
            DataAccessLayerServices.LeaveTypeRepository().Update(item);
        }

        public LeaveSetup GetFirstAvailableLeaveSetup(string employeeID, string leaveType)
        {
            var leaveSetuplist = GetLeaveSetupList("", employeeID, 1, 0);
            var remainingList = leaveSetuplist.Where(w => w.LeaveAmount > 0 && w.LeaveType_ID == leaveType).OrderBy(o => o.ValidFrom).ToList();
            if(!remainingList.Any()) remainingList = leaveSetuplist.Where(w => w.LeaveType_ID == leaveType).OrderByDescending(o => o.ValidFrom).ToList();
            var remaining = remainingList.FirstOrDefault();
            return remaining;
        }

        public Leave GetSingleLeave(Leave l)
        {
            return DataAccessLayerServices.LeaveRepository().GetSingle(g => g.Employee_ID == l.Employee_ID && g.LeaveDate == l.LeaveDate && 
                                                                            g.LeaveSetup_ID == l.LeaveSetup_ID && g.LeaveType_ID == l.LeaveType_ID && g.Status == l.Status);
        }

        public List<LeaveSetup> GetAvialableLeaveSetupListByDate(string employeeID, DateTime fromDate, DateTime toDate)
        {
            var leaveList = new List<LeaveSetup>();
            leaveList = GetLeaveSetupListByEmployee(employeeID);

            var fromList = leaveList.Where(w => w.ValidFrom <= fromDate && w.ValidTo >= fromDate).ToList();
            var toList = leaveList.Where(w => w.ValidFrom <= toDate && w.ValidTo >= toDate).ToList();

            var result = fromList.Union(toList).ToList();

            result.ForEach(f => { if (f.Employee.Staff_type != 1 && f.LeaveType_ID == "L002") f.LeaveType.LeaveTypeNameTH = "ลากิจพิเศษ"; });

            return result;
        }



        public List<LeaveSetup> GetAll_AvialableLeaveSetupList_ByEmployeeID(string employeeID)
        {
            var leaveList = new List<LeaveSetup>();
            leaveList = GetLeaveSetupListByEmployee(employeeID);

            var availableList = leaveList.Where(w => w.LeaveAmount > 0 && w.ValidTo >= DateTime.Now).ToList();

            return availableList;
        }
    }
}
