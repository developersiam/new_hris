﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelPayroll;
using HRISSystemDAL;
using DomainModelHris;

namespace HRISSystemBL
{
    public interface IPayrollBL
    {
        List<Lot_Number> GetAllLot();
        sp_GetCurrent_Lot_Result GetCurrentDateLot();
        List<Lot_Number> GetLotByDateRange(DateTime fromDate, DateTime toDate);
        Lot_Number GetSingleLot(string lotMonth, string lotYear);
        Lot_Number GetSingleLotByDate(DateTime lotDate);
        DomainModelPayroll.Employee GetSinglePayrollByID(string employeeID);
        DomainModelPayroll.Employee GetEmployeeByHrisID(string employeeID);
        //DomainModelPayroll.Employee GetEmployeeListByDepartment(string deptCode);
        void AddLotnumber(Lot_Number i);
        void UpdateLotnumber(Lot_Number item);
        void AddPayrollEmployee(DomainModelPayroll.Employee i);
        void UpdatePayrollEmpoyee(DomainModelPayroll.Employee i);
        string GetBankAccountByPersonID(string personID);
        string GetBankAccountByHrisID(DomainModelHris.Employee HrisID);
        OT_Detail GetSingleOTDetail(OT_Detail o);
        void AddOTDetail(OT_Detail o);
        void DeleteOTDetail(OT_Detail o);
        List<OT_Detail> GetOTDetailByLot(string lotMonth, string lotYear);
        List<sp_GetWorkingDateToPayroll_Result> GetWorkingDateToPayroll(DateTime fromDate, DateTime toDate);
        List<sp_GetLabourWorkAndOTFromSTEC_payroll_Result> GetLabourWorkAndOTFromPayroll(string lotMonth, string lotYear);
        void AddWorkDate(Labour_work l);
        void DeleteWorkDate(Labour_work l);
        List<Labour_work> GetWorkDateByLot(string lotMonth, string lotYear);
        List<Payroll> GetPrintSlip(string payrollLot, string department, string staffType);
        List<Payroll> GetPayrollByLot(string payrollLot);
        List<Payroll> GetPayrollByLotAndDepartment(string payrollLot, string department, int staffTypeID);
        List<sp_GetPayRollForPrintSlip_Result> GetPrintSlipResult(string payrollLot, string departmentCode);
        List<sp_GetToPayrollByDepartment_Result> sp_GetToPayrollByDepartment(string lotMonth, string lotYear, string deptCode, string username);
        void UpdatePayroll(List<Payroll> currentList, List<Payroll> payrollList);
        List<sp_GetPayrollSummaryReport_Result> sp_GetPayrollSummaryReport(string payrollLot, int staffType);
        List<sp_GetPayrollEmployee_Result> sp_GetPayrollEmployee(int Stafftype, int Status);
        List<sp_NEWGet_Result> sp_Newget(string lotMonth, string lotYear, string deptCode, int staffType);
        List<sp_GetSSOReport_Result> sp_GetSSOReport(string lotNumber);
        List<sp_GetActualWorkingDate_Result> sp_GetActualWorkingDate(DateTime fromDate, DateTime toDate);
        List<sp_GetSupportByDateAndEmployee_Result> sp_GetSupportByDateAndEmployee(DateTime fromDate, DateTime toDate, int fingerID);
        List<sp_GetSummarySupportByDateAndEmployee_Result> sp_GetSummarySupportByDate(DateTime fromDate, DateTime toDate);
        void AddSupportDetail(Support_Detail s);
        void DeleteSupportDetail(Support_Detail s);
        Support_Detail GetSingleSupportDetail(string lotMonth, string lotYear, string employeeID);
        List<Support_Detail> GetListSupportDetailByLot(string lotMonth, string lotYear);
    }
    public class PayrollBL : IPayrollBL
    {
        public void AddOTDetail(OT_Detail o)
        {
            DataAccessLayerServices.OTDetailRepository().Add(o);
        }

        public void AddPayrollEmployee(DomainModelPayroll.Employee i)
        {
            DataAccessLayerServices.PayrollEmployeeRepository().Add(i);
        }

        public void AddWorkDate(Labour_work l)
        {
            DataAccessLayerServices.LabourWorkRepository().Add(l);
        }

        public void DeleteOTDetail(OT_Detail o)
        {
            if (GetSingleOTDetail(o) != null) DataAccessLayerServices.OTDetailRepository().Remove(o);
        }

        public void DeleteWorkDate(Labour_work l)
        {
            DataAccessLayerServices.LabourWorkRepository().Remove(l);
        }

        public List<Lot_Number> GetAllLot()
        {
            return DataAccessLayerServices.Lot_NumberRepository().GetAll()
                                                                 .OrderByDescending(o => o.Lot_Year)
                                                                 .ThenByDescending(o => o.Lot_Month).ToList();
        }

        public string GetBankAccountByHrisID(DomainModelHris.Employee HrisID)
        {
            var employee = DataAccessLayerServices.PayrollEmployeeRepository().GetSingle(g => g.EmployeeID == HrisID.Employee_ID); 
            if(employee == null) employee = DataAccessLayerServices.PayrollEmployeeRepository().GetSingle(g => g.EmployeeID == HrisID.Noted);
            var result = employee == null? "" : employee.Bank_acc;
            return result;
        }

        public string GetBankAccountByPersonID(string personID)
        {
            var lastEmployee = BusinessLayerServices.EmployeeBL().GetEmployeeListByPersonID(personID).OrderByDescending(o => o.EndDate).FirstOrDefault();
            var lastPayroll = DataAccessLayerServices.PayrollEmployeeRepository().GetSingle(g => g.Employee_id == lastEmployee.Noted);
            return lastPayroll != null ? lastPayroll.Bank_acc : "";
        }

        public sp_GetCurrent_Lot_Result GetCurrentDateLot()
        {
            //string lotYear = DateTime.Now.Year.ToString();
            //string lotMonth = DateTime.Now.Month.ToString("D2");
            //var x = DataAccessLayerServices.Lot_NumberRepository().GetSingle(g => g.Lot_Year == lotYear && g.Lot_Month == lotMonth);

            //var todaydate = DateTime.Now;
            //var y = DataAccessLayerServices.Lot_NumberRepository().GetSingle(g => g.Start_date <= todaydate && g.Finish_date >= todaydate);
            //return y;
            var currentLot = DataAccessLayerServices.StoreProcedureRepository().sp_GetCurrent_Lot().SingleOrDefault();
            return currentLot;
            //var _lotNumber = currentLot.Select(s => new Lot_Number
            //{
            //    Lot_Month = s.Lot_Month,
            //    Lot_Year = s.Lot_Year,
            //    Start_date = s.Start_date,
            //    Finish_date = s.Finish_date,
            //    Lock_Hr = s.Lock_Hr,
            //    Lock_Acc = s.Lock_Acc,
            //    Salary_Paid_date = s.Salary_Paid_date,
            //    Ot_Paid_date = s.Ot_Paid_date,
            //    Lock_Acc_Labor = s.Lock_Acc_Labor,
            //    Lock_Hr_Labor = s.Lock_Hr_Labor,
            //    LockedAll = s.LockedAll
            //}).SingleOrDefault();
            //return _lotNumber;
        }

        public List<Lot_Number> GetLotByDateRange(DateTime fromDate, DateTime toDate)
        {
            return GetAllLot().Where(w => w.Start_date <= fromDate && w.Finish_date >= toDate).ToList();
        }

        public List<OT_Detail> GetOTDetailByLot(string lotMonth, string lotYear)
        {
            return DataAccessLayerServices.OTDetailRepository().GetList(g => g.Lot_Month == lotMonth && g.Lot_Year == lotYear).ToList();
        }

        public DomainModelPayroll.Employee GetSinglePayrollByID(string employeeID)
        {
            //var x = BusinessLayerServices.EmployeeBL().GetEmployeeByEmployeeID
            return DataAccessLayerServices.PayrollEmployeeRepository().GetSingle(g => g.Employee_id == employeeID);
        }

        public List<Labour_work> GetWorkDateByLot(string lotMonth, string lotYear)
        {
            return DataAccessLayerServices.LabourWorkRepository().GetList(g => g.Lot_Month == lotMonth && g.Lot_Year == lotYear).ToList();
        }

        public List<sp_GetWorkingDateToPayroll_Result> GetWorkingDateToPayroll(DateTime fromDate, DateTime toDate)
        {
            return DataAccessLayerServices.StoreProcedureRepository().sp_GetWorkingDateToPayroll(fromDate, toDate);
        }

        public List<sp_GetLabourWorkAndOTFromSTEC_payroll_Result> GetLabourWorkAndOTFromPayroll(string lotMonth, string lotYear)
        {
            return DataAccessLayerServices.StoreProcedureRepository().sp_GetLabourWorkAndOTFromSTEC_payroll(lotMonth, lotYear);
        }

        public void AddLotnumber(Lot_Number i)
        {
            DataAccessLayerServices.Lot_NumberRepository().Add(i);
        }

        public void UpdateLotnumber(Lot_Number item)
        {
            DataAccessLayerServices.Lot_NumberRepository().Update(item);
        }

        public void UpdatePayrollEmpoyee(DomainModelPayroll.Employee i)
        {
            DataAccessLayerServices.PayrollEmployeeRepository().Update(i);
        }

        public List<Payroll> GetPrintSlip(string payrollLot, string department, string staffType)
        {
            var result = new List<Payroll>();
            result = DataAccessLayerServices.PayrollRepository().GetList(g => g.Payroll_date == payrollLot).ToList();
            if (department != "") result = result.Where(w => w.Dept_code == department).ToList();

            if (staffType == "1") result = result.Where(w => w.Staff_type == "1").ToList();
            else if (staffType == "4") result = result.Where(w => w.Staff_type == "4").ToList();
            else result = result.Where(w => w.Staff_type == "2" || w.Staff_type == "3").ToList();

            //if (employeeID != "") result = result.Where(w => w.EmployeeID == employeeID).ToList(); //Search by HR Employee ID
            return result;
        }

        public List<sp_GetPayRollForPrintSlip_Result> GetPrintSlipResult(string payrollLot, string departmentCode)
        {
            var result = new List<sp_GetPayRollForPrintSlip_Result>();
            result = DataAccessLayerServices.StoreProcedureRepository().sp_GetPayRollForPrintSlip(payrollLot);
            if (departmentCode != "") result = result.Where(w => w.DepartmentCode == departmentCode).ToList();
            return result;
        }

        public List<Payroll> GetPayrollByLot(string payrollLot)
        {
            return DataAccessLayerServices.PayrollRepository().GetList(g => g.Payroll_date == payrollLot).ToList();
        }

        public DomainModelPayroll.Employee GetEmployeeByHrisID(string employeeID)
        {
            var x = BusinessLayerServices.EmployeeBL().GetEmployeeByEmployeeID(employeeID);
            return DataAccessLayerServices.PayrollEmployeeRepository().GetSingle(g => g.Employee_id == x.Noted);
        }

        public List<sp_GetToPayrollByDepartment_Result> sp_GetToPayrollByDepartment(string lotMonth, string lotYear, string deptCode, string username)
        {
            var result = new List<sp_GetToPayrollByDepartment_Result>();
            result = DataAccessLayerServices.StoreProcedureRepository().sp_GetToPayrollByDepartment(lotMonth, lotYear, deptCode);

            if (username == "m.tanainan") result = result.Where(w => w.Staff_type != "1").ToList();
            else result = result.Where(w => w.Staff_type == "1").ToList();

            return result;
        }

        public void UpdatePayroll(List<Payroll> currentList, List<Payroll> payrollList)
        {
            foreach (var current in currentList)
            {
                if (current != null) DataAccessLayerServices.PayrollRepository().Remove(current);
            }
            foreach (var _payroll in payrollList)
            {
                DataAccessLayerServices.PayrollRepository().Add(_payroll);
            }
        }

        public List<Payroll> GetPayrollByLotAndDepartment(string payrollLot, string department, int staffTypeID)
        {
            var result = new List<Payroll>();

            if (department != "") result = DataAccessLayerServices.PayrollRepository().GetList(g => g.Payroll_date == payrollLot && g.Dept_code == department).ToList();
            else result = DataAccessLayerServices.PayrollRepository().GetList(g => g.Payroll_date == payrollLot).ToList(); //All department

            if (staffTypeID == 1) result = result.Where(w => w.Staff_type == "1").ToList();
            else if (staffTypeID == 4) result = result.Where(w => w.Staff_type == "4").ToList();
            else result = result.Where(w => w.Staff_type == "2" || w.Staff_type == "3").ToList();

            result = result.OrderBy(o => o.Dept_code).ThenBy(t => t.Employee_id).ToList();

            return result;
        }

        public Lot_Number GetSingleLot(string lotMonth, string lotYear)
        {
            return DataAccessLayerServices.Lot_NumberRepository().GetSingle(g => g.Lot_Month == lotMonth && g.Lot_Year == lotYear);
        }

        public List<sp_GetPayrollSummaryReport_Result> sp_GetPayrollSummaryReport(string payrollLot, int staffType)
        {
            var result = DataAccessLayerServices.StoreProcedureRepository().sp_GetPayrollSummaryReport(payrollLot);
            //if (staffType == 1) result = result.Where(w => w.Staff_type == 1).ToList();
            //else if (staffType == 2) result = result.Where(w => w.Staff_type != 1).ToList();
            if (staffType == 4) result = result.Where(w => w.Staff_type != 1).ToList();
            else if (staffType != 0 && staffType != 4) result = result.Where(w => w.Staff_type == staffType).ToList();
            return result;
        }

        public List<sp_GetPayrollEmployee_Result> sp_GetPayrollEmployee(int Stafftype, int Status)
        {
            //var result = DataAccessLayerServices.StoreProcedureRepository().sp_GetPayrollEmployee().Where(w => w.Staff_type != 4).ToList();
            //if (Stafftype != 0) result = result.Where(w => w.Staff_type == Stafftype).ToList();
            var _status = Status == 1 ? "ทำงาน" : "สิ้นสุด";
            var result = new List<sp_GetPayrollEmployee_Result>();
            if (Status == 0) result = DataAccessLayerServices.StoreProcedureRepository().sp_GetPayrollEmployee().ToList();
            else result = DataAccessLayerServices.StoreProcedureRepository().sp_GetPayrollEmployee().Where(w => w.StaffStatus == _status).ToList();
            if (Stafftype != 0) result = result.Where(w => w.Staff_type == Stafftype).ToList();
            //else return result;
            return result;
        }

        public List<sp_NEWGet_Result> sp_Newget(string lotMonth, string lotYear, string deptCode, int staffType)
        {
            var result = new List<sp_NEWGet_Result>();
            if (deptCode != "") result = DataAccessLayerServices.StoreProcedureRepository().sp_Newget(lotMonth, lotYear, deptCode);
            else
            {
                var deptList = BusinessLayerServices.DeptCodeBL().GetAllDepartment();
                foreach (var dept in deptList)
                {
                    result.AddRange(DataAccessLayerServices.StoreProcedureRepository().sp_Newget(lotMonth, lotYear, dept.DeptCode));
                }
            }

            if (staffType == 1) result = result.Where(w => w.Staff_type == "1").ToList();
            else if (staffType == 4) result = result.Where(w => w.Staff_type == "4").ToList();
            else result = result.Where(w => w.Staff_type == "2" || w.Staff_type == "3").ToList();

            result = result.OrderBy(o => o.Dept_code).ThenBy(t => t.Employee_id).ToList();

            return result;
        }

        public List<sp_GetSSOReport_Result> sp_GetSSOReport(string lotNumber)
        {
            return DataAccessLayerServices.StoreProcedureRepository().sp_GetSSOReport(lotNumber).ToList();
        }

        public OT_Detail GetSingleOTDetail(OT_Detail o)
        {
            return DataAccessLayerServices.OTDetailRepository().GetSingle(g => g.Employee_id == o.Employee_id && g.Lot_Month == o.Lot_Month && g.Lot_Year == o.Lot_Year);
        }

        public List<Payroll> PND1ForReport009(int year, int staffType)
        {
            //var groupDepartment = listOT.GroupBy(g => g.Employee.DeptCode).ToList().Select(s => new { departmentCode = s.Key }).ToList();
            var payrollList = DataAccessLayerServices.PayrollRepository().GetList(g => g.Payroll_date.EndsWith(year.ToString())).ToList();
            if (staffType == 1) payrollList = payrollList.Where(w => w.Staff_type == "1").ToList();
            else payrollList = payrollList.Where(w => w.Staff_type != "1").ToList();

            var employeeList = payrollList.GroupBy(g => g.Id_card).ToList().Select(s => new { idCard = s.Key }).ToList();

            foreach (var _idCard in employeeList)
            {
                var i = BusinessLayerServices.IIDCardInfoBL().GetSingleIDCard(_idCard.idCard);
                var adr = BusinessLayerServices.AddressBL().GetAddressListByPersonID(i.Person_ID).SingleOrDefault(s => s.AddressType_ID == "1");

                if (adr == null) throw new Exception(string.Format("ไม่พบที่อยปัจจุบันของ บัตร {0} ชื่อ {1} {2}", i.Card_ID, i.Person.FirstNameTH, i.Person.LastNameTH));

                var eachPayroll = payrollList.Where(w => w.Id_card == _idCard.idCard).OrderByDescending(o => o.Salary_paid_date);

                var titl = i.Person.TitleName.TitleNameTH;
                var fnam = i.Person.FirstNameTH;
                var lnam = i.Person.LastNameTH;
                var hmno = adr.HomeNumber;
                var moo = adr.Moo;
                var tumb = adr.SubDistrict.SubDistrictNameTH;
                var amph = adr.SubDistrict.District.DistrictNameTH;
                var prov = adr.SubDistrict.District.Province.ProvinceNameTH;
                var zipc = adr.Postcode;
                var addr = (hmno == "" ? "" : hmno + " ") + (moo == null ? "" : "ม." + moo) + "ต." + tumb + "อ." + amph;
                var vyea = year + 543;
                var maxp = eachPayroll.SingleOrDefault(s => s.Payroll_date.StartsWith("12"));
                var pdat = maxp.Salary_paid_date;
                var neti = eachPayroll.Sum(s => s.Net_Income);
                var stax = eachPayroll.Sum(s => s.Tax);

            }

            return null;
        }

        public List<sp_GetActualWorkingDate_Result> sp_GetActualWorkingDate(DateTime fromDate, DateTime toDate)
        {
            return DataAccessLayerServices.StoreProcedureRepository().sp_GetActualWorkingDate(fromDate, toDate);
        }

        public List<sp_GetSupportByDateAndEmployee_Result> sp_GetSupportByDateAndEmployee(DateTime fromDate, DateTime toDate, int fingerID)
        {
            return DataAccessLayerServices.StoreProcedureRepository().sp_GetSupportByDateAndEmployee(fromDate, toDate, fingerID);
        }

        public List<sp_GetSummarySupportByDateAndEmployee_Result> sp_GetSummarySupportByDate(DateTime fromDate, DateTime toDate)
        {
            return DataAccessLayerServices.StoreProcedureRepository().sp_GetSummarySupportByDate(fromDate, toDate);
        }

        public void AddSupportDetail(Support_Detail s)
        {
            if (GetSingleSupportDetail(s.Lot_Month, s.Lot_Year, s.Employee_id) == null)
            {
                DataAccessLayerServices.Support_DetailRepository().Add(s);
            }
        }

        public void DeleteSupportDetail(Support_Detail s)
        {
            if (GetSingleSupportDetail(s.Lot_Month, s.Lot_Year, s.Employee_id) != null)
            {
                DataAccessLayerServices.Support_DetailRepository().Remove(s);
            }
        }

        public Support_Detail GetSingleSupportDetail(string lotMonth, string lotYear, string employeeID)
        {
            return DataAccessLayerServices.Support_DetailRepository().GetSingle(g => g.Lot_Month == lotMonth && g.Lot_Year == lotYear && g.Employee_id == employeeID);
        }

        public List<Support_Detail> GetListSupportDetailByLot(string lotMonth, string lotYear)
        {
            return DataAccessLayerServices.Support_DetailRepository().GetList(g => g.Lot_Month == lotMonth && g.Lot_Year == lotYear).ToList();
        }

        public Lot_Number GetSingleLotByDate(DateTime lotDate)
        {
            var x = DataAccessLayerServices.Lot_NumberRepository().GetSingle(g => g.Start_date <= lotDate && g.Finish_date >= lotDate);
            return x;
        }

        
    }
}
