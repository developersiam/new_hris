﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelHris;
using HRISSystemDAL;

namespace HRISSystemBL
{
    public interface IManualWorkingBL
    {
        List<ManualWorkDate> GetEachEmployeeManualWorkDateList(string employeeID, DateTime fromDate, DateTime toDate);
        List<ManualWorkDate> GetManualDateByDateAndID(DateTime searchDate, string employeeID);
        List<ManualWorkDate> GetManualDateByDateAndDepartment(DateTime dateFrom, DateTime dateTo, string departmentCode);
        ManualWorkDate GetSingleManualWorkDate(DateTime manualDate, string manualType, string employeeID);

        void AddManuaWorkingDate(ManualWorkDate i);
        void UpdateManuaWorkingDate(ManualWorkDate i);
        void DeleteManuaWorkingDate(ManualWorkDate i);
        List<ManualWorkDateType> GetManualWorkTypeList();
    }
    public class ManualWorkingBL : IManualWorkingBL
    {
        public void AddManuaWorkingDate(ManualWorkDate i)
        {
            DataAccessLayerServices.ManualWorkDateRepository().Add(i);
        }

        public void DeleteManuaWorkingDate(ManualWorkDate i)
        {
            DataAccessLayerServices.ManualWorkDateRepository().Remove(i);
        }

        public List<ManualWorkDate> GetManualDateByDateAndDepartment(DateTime dateFrom, DateTime dateTo, string departmentCode)
        {
            return DataAccessLayerServices.ManualWorkDateRepository().GetList(g => g.ManualWorkDateDate >= dateFrom &&
                                                                                   g.ManualWorkDateDate <= dateTo &&
                                                                                   g.Employee.DeptCode == departmentCode).ToList();
        }

        public List<ManualWorkDate> GetManualDateByDateAndID(DateTime searchDate, string employeeID)
        {
            return DataAccessLayerServices.ManualWorkDateRepository().GetList(e => e.Employee_ID == employeeID && 
                                                                              e.ManualWorkDateDate >= searchDate && 
                                                                              e.ManualWorkDateDate <= searchDate, e => e.ManualWorkDateType).ToList();
        }

        public ManualWorkDate GetSingleManualWorkDate(DateTime manualDate, string manualType, string employeeID)
        {
            return DataAccessLayerServices.ManualWorkDateRepository().GetSingle(g => g.Employee_ID == employeeID &&
                                                                                     g.ManualWorkDateDate == manualDate &&
                                                                                     g.ManualWorkDateType_ID == manualType);
        }

        public List<ManualWorkDate> GetEachEmployeeManualWorkDateList(string employeeID, DateTime fromDate, DateTime toDate)
        {
            return DataAccessLayerServices.ManualWorkDateRepository().GetList(g => g.Employee_ID == employeeID && 
                                                                                   g.ManualWorkDateDate >= fromDate && 
                                                                                   g.ManualWorkDateDate <= toDate, 
                                                                              g => g.ManualWorkDateType)
                                                                     .OrderByDescending(o => o.ManualWorkDateDate)
                                                                     .ThenBy(t => t.Times).ToList();
        }

        public List<ManualWorkDateType> GetManualWorkTypeList()
        {
            return DataAccessLayerServices.ManualWorkDateTypeRepository().GetAll().ToList();
        }

        public void UpdateManuaWorkingDate(ManualWorkDate i)
        {
            DataAccessLayerServices.ManualWorkDateRepository().Update(i);
        }
    }
}
