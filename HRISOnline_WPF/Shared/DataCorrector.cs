﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DomainModelPayroll;
using HRISSystemBL;
using DomainModelHris;
using HRISOnline_WPF.ViewModel;

namespace HRISOnline_WPF.Shared
{
    public interface IDataCorrector
    {
        bool NumericCheck(string value, int digit, int? digitRange);
        string FormatNumber(int decimalDigit, string inputString, bool Nullable);
        string FormatTime(string inputString);
        string OTRateCalculate(string income, string staffType);
        string OTHolidayCalculate(string income, string staffType);
        string OTHolidayRateCalculate(string income, string staffType);
        List<LotNumberViewModel> LotNumberList(List<Lot_Number> lotNumber);
        string FirstCapital(string inputString);
        List<decimal> GetOTHour();
        List<OTMinutes> GetOTMinute(string _formState);
        string ConvertOTAmountFromListOT(List<OT> listOTData);
        List<OTViewModel> ConvertToOTViewmodel(List<OT> listOTData);
    }
    public class DataCorrector : IDataCorrector
    {
        public string FormatNumber(int decimalDigit, string inputString, bool Nullable)
        {
            // Digit 0 for Integer type
            Regex regex = new Regex(@"^[0-9]+(\.[0-9]{1,6})?$");
            Match match = regex.Match(inputString);
            if (match.Success)
            {
                decimal x = decimal.Parse(inputString);
                x = Math.Round(x, decimalDigit);
                return x.ToString();
            }
            else if (Nullable) return null; 
            else return "0";
        }

        public string FormatTime(string inputString)
        {
            try
            {
                if (string.IsNullOrEmpty(inputString)) return "00:00:00";

                int hour = 0;
                int minute = 0;
                string[] splited;

                if (inputString.Length <= 2)
                {
                    if (!int.TryParse(inputString, out hour) || hour >= 24) return "00:00:00";
                }
                else
                {
                    inputString = inputString.Replace('.', ':');

                    if (inputString.IndexOf(":") > 0) splited = inputString.Split(':'); //Split with ':'
                    else return "00:00:00";

                    if (splited.Length > 0) int.TryParse(splited[0], out hour);
                    if (splited.Length > 1) int.TryParse(splited[1], out minute);
                }

                TimeSpan result = new TimeSpan(hour, minute, 0);
                return result.ToString();
            }
            catch (Exception)
            {
                return "00:00:00";
            }
        }

        public string OTRateCalculate(string income, string staffType)
        {
            if (string.IsNullOrEmpty(income)) return "";
            decimal _income = decimal.Parse(income);

            decimal result = 0;
            if (staffType == "1") result = decimal.Divide(_income, 30); // พนักงานประจำ
            result = decimal.Divide(result, 8);
            result = decimal.Multiply(result, decimal.Parse("1.5"));
            //result = Math.Round(result, 2);
            return Math.Round(result, 2).ToString();
        }

        public string OTHolidayCalculate(string income, string staffType)
        {
            if (string.IsNullOrEmpty(income)) return "";
            decimal _income = decimal.Parse(income);

            decimal result = 0;
            if (staffType == "1") result = decimal.Divide(_income, 30); // พนักงานประจำ
            else result = _income;
            return Math.Round(result, 2).ToString();
        }

        public string OTHolidayRateCalculate(string income, string staffType)
        {
            if (string.IsNullOrEmpty(income)) return "";
            decimal _income = decimal.Parse(income);

            decimal result = 0;
            if (staffType == "1") result = decimal.Divide(_income, 30); // พนักงานประจำ
            result = decimal.Divide(result, 8);
            result = decimal.Multiply(result, 3);
            return Math.Round(result, 2).ToString();
        }

        public List<LotNumberViewModel> LotNumberList(List<Lot_Number> lotNumber)
        {
            var result = new List<LotNumberViewModel>();
            foreach (var i in lotNumber)
            {
                result.Add(new LotNumberViewModel
                {
                    LotNumber = i.Lot_Month + "/" + i.Lot_Year,
                    Lot_Month = i.Lot_Month,
                    Lot_Year = i.Lot_Year,
                    Start_date = i.Start_date,
                    Finish_date = i.Finish_date,
                    Lock_Hr = i.Lock_Hr,
                    Lock_Hr_Bool = i.Lock_Hr == "2" ? true : false,
                    Lock_Acc = i.Lock_Acc,
                    Lock_Acc_Bool = i.Lock_Acc == "2" ? true : false,
                    Salary_Paid_date = i.Salary_Paid_date,
                    Ot_Paid_date = i.Ot_Paid_date,
                    Lock_Acc_Labor = i.Lock_Acc_Labor,
                    Lock_Acc_Labor_Bool = i.Lock_Acc_Labor == "2" ? true : false,
                    Lock_Hr_Labor = i.Lock_Hr_Labor,
                    Lock_Hr_Labor_Bool = i.Lock_Hr_Labor == "2" ? true : false,
                    LockedAll = i.LockedAll
                });
            }
            return result;
        }

        public bool NumericCheck(string value, int digit, int? digitRange)
        {
            string _digitRange = digitRange == null ? "" : "," + digitRange;
            string expression = @"^\d{"+ digit + _digitRange + "}$";

            //Regex regex = new Regex(@"^\d{0,13}$");
            Regex regex = new Regex(expression);
            Match match = regex.Match(value);
            return match.Success;
        }

        public string FirstCapital(string inputString)
        {
            inputString = inputString.ToLower();
            if (string.IsNullOrEmpty(inputString)) return string.Empty;
            // convert to char array of the string
            char[] letters = inputString.ToCharArray();
            // upper case the first char
            letters[0] = char.ToUpper(letters[0]);
            // return the array made of the new char array
            return new string(letters);
        }

        public List<decimal> GetOTHour()
        {
            List<decimal> otHourList = new List<decimal>();
            for (decimal i = 0; i < 13; i++)
            {
                otHourList.Add(i);
            }
            return otHourList;
        }

        public List<OTMinutes> GetOTMinute(string _formState)
        {
            List<OTMinutes> otMinuteList = new List<OTMinutes>();
            otMinuteList.Add(new OTMinutes { Minutes = "0", Multiplier = 0m });
            if (_formState != "Insert") otMinuteList.Add(new OTMinutes { Minutes = "10", Multiplier = 0.167m });
            if (_formState != "Insert") otMinuteList.Add(new OTMinutes { Minutes = "20", Multiplier = 0.33m });
            otMinuteList.Add(new OTMinutes { Minutes = "30", Multiplier = 0.50m });
            if (_formState != "Insert") otMinuteList.Add(new OTMinutes { Minutes = "40", Multiplier = 0.67m });
            if (_formState != "Insert") otMinuteList.Add(new OTMinutes { Minutes = "50", Multiplier = 0.83m });
            return otMinuteList;
        }

        public string ConvertOTAmountFromListOT(List<OT> listOTData)
        {
            decimal sumMinute = 0;
            foreach (var ot in listOTData)
            {
                var otAmount = ot.NcountOT.GetValueOrDefault();
                if (otAmount > 0)
                {
                    var _otHour = Math.Truncate(otAmount);
                    var _otMinuteMultiplyer = otAmount - _otHour;
                    var _otMinute = Convert.ToDecimal(GetOTMinute("Update").SingleOrDefault(s => s.Multiplier == _otMinuteMultiplyer).Minutes);
                    sumMinute = sumMinute + (_otHour * 60) + _otMinute;
                }
            }
            TimeSpan span = TimeSpan.FromMinutes((double)sumMinute);
            string result = string.Format("{0} hours {1} mins", Math.Truncate(span.TotalHours), span.Minutes);
            return result;
        }

        public List<OTViewModel> ConvertToOTViewmodel(List<OT> listOTData)
        {
            var result = listOTData.Select(s => new OTViewModel
            {
                OTs = s,
                _AdminStatus = s.AdminApproveStatus == "A",
                _ApproverStatus = s.ManagerApproveStatus == "A",
                _FinalApproverStatus = s.FinalApproverStatus == "A",
                _HrApproverStatus = s.HRApproveStatus == "A",
                _isReject = (s.ManagerApproveStatus == "R" || s.FinalApproverStatus == "R" || s.HRApproveStatus == "R"),
                _strReject = (s.ManagerApproveStatus == "R" || s.FinalApproverStatus == "R" || s.HRApproveStatus == "R") ? "Reject" : ""
            }).ToList();

            return result;
        }
    }
}
