﻿using DomainModelHris;
using HRISOnline_WPF.Shared;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace HRISOnline_WPF.Service
{
    public interface IEmailSender
    {
        void SendApproveEmail(List<OT> listOT, string requestFrom);
        void SendRejectEmail(OT model, string requestFrom);
    }
    public class EmailSender : IEmailSender
    {
        public void SendApproveEmail(List<OT> listOT, string requestFrom)
        {
            string subject = "The OT request for approve from OT Online system.";

            var destinationEmail = BusinessLayerServices.OTOnlineBL().GetDestinationEmail(listOT, requestFrom, "Approve");
            if (destinationEmail == null || !destinationEmail.Any()) return;

            string messages = "<table>" +
                                    "<tr><td>ข้อความจากระบบ OT Online </td></tr>" +
                                    "<tr> <td>*** อีเมลฉบับนี้ถูกส่งผ่านระบบอัตโนมัติ กรุณาอย่าตอบกลับ ***</td></tr>" +
                                    "<tr> <td>คุณได้รับคำร้องให้พิจารณาการอนุมัติการขอทำโอทีจาก '" + requestFrom + "' จำนวน " + listOT.Count.ToString() + " รายการ </td></tr>" +
                                    "<tr> <td>กรุณาตรวจสอบข้อมูลและอนุมัติการขอทำโอทีของพนักงานจากระบบ OT Online </td></tr>" +
                              "</table>" +
                              "<br />" +
                              "<table>" +
                                    "<tr><td>Message from OT Online System.</td></tr>" +
                                    "<tr><td>*** This is an automatically generated email, please do not reply ***</td></tr>" +
                                    "<tr> <td>You have received " + listOT.Count.ToString() + " new request(s) OT online from '" + requestFrom + "' </td></tr>" +
                                    "<tr> <td>, please consider to approve on the OT Online system. </td></tr>" +
                              "</table>" +
                              "<br />" +
                              "<table>" +
                                    "<tr><td>Kind Regards,</td></tr>" +
                                    "<tr> <td>OT Online System</td></tr>" +
                              "</table>" +
                              "<br />";
            SendEmail(subject, messages, destinationEmail);
        }

        public void SendRejectEmail(OT ots, string requestFrom)
        {
            string subject = "The OT request was rejected from OT Online system.";

            var listOT = new List<OT>() { ots };
            var destinationEmail = BusinessLayerServices.OTOnlineBL().GetDestinationEmail(listOT, requestFrom, "Reject");
            if (destinationEmail == null || !destinationEmail.Any()) return;
            
            string messages = "<table>" +
                                    "<tr> <td>ข้อความจากระบบ OT Online </td> </tr>" +
                                    "<tr> </tr>" +
                                    "<tr> <td>*** อีเมลฉบับนี้ถูกส่งผ่านระบบอัตโนมัติ กรุณาอย่าตอบกลับ ***</td> </tr>" +
                                    "<tr> <td>คำร้องการพิจารณาการอนุมัติการขอทำโอทีถูกปฏิเสธจาก '" + requestFrom + "' </td> </tr>" +
                                    "<tr> <td>Name : '" + ots.Employee.Person.TitleName.TitleNameTH + " " + ots.Employee.Person.FirstNameTH + " " + ots.Employee.Person.LastNameTH + "' </td> </tr>" +
                                    "<tr> <td>OT Date : '" + ots.OTDate.ToString("dd/MM/yyyy") + "' </td> </tr>" +
                                    "<tr> <td>OT Type : '" + ots.OTType1.OTTypeName + "' </td> </tr>" +
                                    "<tr> <td>Request Type : '" + ots.OTRequestType.RequestFlagName + "' </td> </tr>" +
                                    "<tr> <td>OT Amount : '" + ots.NcountOT.Value.ToString() + "' </td> </tr>" +
                                    "<tr> <td>Remark : '" + ots.Remark + "' </td> </tr>" +
                                    "<tr> <td>Admin Remark : '" + ots.RemarkFromAdmin + "' </td> </tr>" +
                                    "<tr> <td>Approver Remark : '" + ots.RemarkFromManager + "' </td> </tr>" +
                                    "<tr> <td>Final Approver Remark : '" + ots.RemarkFromFinalApprover + "' </td> </tr>" +
                                    "<tr> <td>HR Remark : '" + ots.RemarkFromHR + "' </td> </tr>" +
                              "</table>" +
                              "<br />" +
                              "<table>" +
                                    "<tr> <td>Message from OT Online System.</td> </tr>" +
                                    "<tr> </tr>" +
                                    "<tr> <td>*** This is an automatically generated email, please do not reply ***</td> </tr>" +
                                    "<tr> <td> OT request was rejected by '" + requestFrom + "'</td> </tr>" +
                                    "<tr> <td>Name : '" + ots.Employee.Person.TitleName.TitleNameEN + " " + ots.Employee.Person.FirstNameEN + " " + ots.Employee.Person.LastNameEN + "' </td> </tr>" +
                                    "<tr> <td>OT Date : '" + ots.OTDate.ToString("dd/MM/yyyy") + "' </td> </tr>" +
                                    "<tr> <td>OT Type : '" + ots.OTType1.OTTypeName + "' </td> </tr>" +
                                    "<tr> <td>Request Type : '" + ots.OTRequestType.RequestFlagName + "' </td> </tr>" +
                                    "<tr> <td>OT Amount : '" + ots.NcountOT.Value.ToString() + "' </td> </tr>" +
                                    "<tr> <td>Remark : '" + ots.Remark + "' </td> </tr>" +
                                    "<tr> <td>Admin Remark : '" + ots.RemarkFromAdmin + "' </td> </tr>" +
                                    "<tr> <td>Approver Remark : '" + ots.RemarkFromManager + "' </td> </tr>" +
                                    "<tr> <td>Final Approver Remark : '" + ots.RemarkFromFinalApprover + "' </td> </tr>" +
                                    "<tr> <td>HR Remark : '" + ots.RemarkFromHR + "' </td> </tr>" +
                              "</table>" +
                              "<br />" +
                              "<table>" +
                                    "<tr> <td>Kind Regards,</td> </tr>" +
                                    "<tr> <td>OT Online System</td> </tr>" +
                              "</table>";
            SendEmail(subject, messages, destinationEmail);
        }

        private void SendEmail(string subject, string messages, List<string> destinationEmail)
        {
            try
            {
                string fromEmail = "it-info@siamtobacco.com";
                string fromPassword = "FvUfUp2uywvmu$tecE#";

                MailMessage mailmessage = new MailMessage();
                SmtpClient client = new SmtpClient();
                client.Credentials = new System.Net.NetworkCredential(fromEmail, fromPassword);
                client.Port = 587;
                client.Host = "smtp.emailsrvr.com";
                mailmessage.From = new MailAddress("it-info@siamtobacco.com");
                foreach (var address in destinationEmail)
                {
                    if (!string.IsNullOrEmpty(address)) mailmessage.To.Add(address);
                }
                mailmessage.CC.Add(SingletonConfiguration.getInstance().Email);
                mailmessage.Subject = subject;
                mailmessage.Body = messages;
                mailmessage.IsBodyHtml = true;
                client.Send(mailmessage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
