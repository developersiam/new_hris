﻿using HRISOnline_WPF.Service;

namespace HRISOnline_WPF.Shared
{
    public static class HRISOnlineService
    {
        public static IDataCorrector DataCorrector()
        {
            IDataCorrector obj = new DataCorrector();
            return obj;
        }
        public static IUserAuthentication UserAuthentication()
        {
            IUserAuthentication obj = new UserAuthentication();
            return obj;
        }
        public static IEmailSender EmailSender()
        {
            IEmailSender obj = new EmailSender();
            return obj;
        }

    }
}
