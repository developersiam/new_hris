﻿using System;

namespace HRISOnline_WPF.Shared
{
    class SingletonConfiguration
    {
        private static SingletonConfiguration instance = new SingletonConfiguration();

        public int? Admin_ID { get; set; }
        public int? Approver_ID { get; set; }
        public int? FinalApprover_ID { get; set; }
        public int? HRApprover_ID { get; set; }
        public string Username { get; set; }
        public DateTime LoggedIn { get; set; }
        public string DepartmentCode { get; set; }
        public string Email { get; set; }
        //public string permissionLevel { get; set; }
        //public string singleton_employee_ID { get; set; }

        private SingletonConfiguration() { }

        public static SingletonConfiguration getInstance()
        {
            return instance;
        }
    }
}
