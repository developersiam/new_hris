﻿using HRISOnline_WPF.Form.OTOnline;
using HRISOnline_WPF.Form.OTOnline.Setup;
using HRISOnline_WPF.Shared;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HRISOnline_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OTOnlinetMenu_Click(object sender, RoutedEventArgs e)
        {
            var username = SingletonConfiguration.getInstance().Username;
            var admin = BusinessLayerServices.OTOnlineBL().GetListOTOnlineAdmin(username).FirstOrDefault();
            SingletonConfiguration.getInstance().Admin_ID = admin == null? (int?)null : admin.Admin_ID;
            var approver = BusinessLayerServices.OTOnlineBL().GetListOTOnlineApprover(username).FirstOrDefault();
            SingletonConfiguration.getInstance().Approver_ID = approver == null ? (int?)null : approver.Approver_ID;
            var fapprover = BusinessLayerServices.OTOnlineBL().GetListOTOnlineFinalApprover(username).FirstOrDefault();
            SingletonConfiguration.getInstance().FinalApprover_ID = fapprover == null ? (int?)null : fapprover.FinalApprover_ID;
            var hrapprover = BusinessLayerServices.OTOnlineBL().GetSingleOTOnlineHR(username);
            SingletonConfiguration.getInstance().HRApprover_ID = hrapprover == null ? (int?)null : hrapprover.HRApprover_ID;

            EnableOTOnline(Visibility.Visible);
            EnableLeaveOnline(Visibility.Collapsed);

            //MainFrame.Content = "OT Online";
            this.MainFrame.Navigate(new Form.OTOnline.Report.RPT001Page());
        }

        private void LeaveOnlineMenu_Click(object sender, RoutedEventArgs e)
        {
            EnableOTOnline(Visibility.Collapsed);
            EnableLeaveOnline(Visibility.Visible);

            MainFrame.Content = "Leave Online";
        }

        private void EnableOTOnline(Visibility visibility)
        {
            try
            {
                var loggedIn = SingletonConfiguration.getInstance();
                //string department = SingletonConfiguration.getInstance().DepartmentCode;
                //bool isRequest = SingletonConfiguration.getInstance().Admin_ID != null || department == "ITC";
                OT_OTRequestMenu.Visibility = visibility;
                OT_OTRequestMenu.Visibility = loggedIn.Admin_ID == null ? Visibility.Collapsed : Visibility.Visible;
                OT_ApproverMenu.Visibility = visibility;
                OT_ApproverMenu.Visibility = loggedIn.Approver_ID == null ? Visibility.Collapsed : Visibility.Visible;
                OT_FinalApproverMenu.Visibility = visibility;
                OT_FinalApproverMenu.Visibility = loggedIn.FinalApprover_ID == null ? Visibility.Collapsed : Visibility.Visible;
                OT_HRApproveMenu.Visibility = visibility;
                OT_HRApproveMenu.Visibility = loggedIn.HRApprover_ID == null ? Visibility.Collapsed : Visibility.Visible;
                OT_ReportMenu.Visibility = visibility;
                OT_ReportMenu.Visibility = (loggedIn.Admin_ID != null || loggedIn.Approver_ID != null || loggedIn.FinalApprover_ID != null || loggedIn.HRApprover_ID != null) ? Visibility.Visible : Visibility.Collapsed;
                OT_SetupMenu.Visibility = Visibility;
                OT_SetupMenu.Visibility = loggedIn.HRApprover_ID == null ? Visibility.Collapsed : Visibility.Visible;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EnableLeaveOnline(Visibility visibility)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void OT_OTRequestMenu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new OTRequestPage());
        }

        private void OT_ApproverMenu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new ApproverPage());
        }

        private void OT_FinalApproverMenu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new FinalApproverPage());
        }

        private void OT_HRApproveMenu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new HRApproverPage());
        }

        private void OT_RPT001Menu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Form.OTOnline.Report.RPT001Page());
        }

        private void OT_AdminSetup_Click(object sender, RoutedEventArgs e)
        {
            var s = new SetupAdminWindow();
            s.ShowDialog();
        }

        private void OT_ApproverSetup_Click(object sender, RoutedEventArgs e)
        {
            var s = new SetupApproverWindow();
            s.ShowDialog();
        }

        private void OT_FinalApproverSetup_Click(object sender, RoutedEventArgs e)
        {
            var s = new SetupFinalApproverWindow();
            s.ShowDialog();
        }

        private void OT_HRApproverSetup_Click(object sender, RoutedEventArgs e)
        {
            var s = new SetupHrWindow();
            s.ShowDialog();
        }

        private void ExitMenu_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("ต้องการออกจากโปรแกรมหรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes) Close();
        }
    }
}
