﻿using DomainModelHris;
using DomainModelPayroll;
using HRISOnline_WPF.Shared;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace HRISOnline_WPF.Form.OTOnline
{
    /// <summary>
    /// Interaction logic for OTDetailWindow.xaml
    /// </summary>
    public partial class OTDetailWindow : Window
    {
        public string _formState = "";
        public string _actionFrom = "";
        public OT OTdetail;

        public OTDetailWindow()
        {
            InitializeComponent();
        }

        public OTDetailWindow(OT ot, string actionfrom, string formState)
        {
            InitializeComponent();
            OTdetail = ot;
            _formState = formState;
            _actionFrom = actionfrom;
            BindCombobox();
            Clear();
            BindOTDetail();
            BindControl();
        }

        private void BindCombobox()
        {
            OTHourCombobox.ItemsSource = HRISOnlineService.DataCorrector().GetOTHour();
            OTMinuteCombobox.ItemsSource = HRISOnlineService.DataCorrector().GetOTMinute(_formState);
            TitleCombobox.ItemsSource = BusinessLayerServices.PersonBL().GetAllTitlenameList();
            OTTypeCombobox.ItemsSource = BusinessLayerServices.OTBL().GetOTType();
            OTRequestCombobox.ItemsSource = BusinessLayerServices.OTBL().GetOTRequestType();
            StaffTypeCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
            var payrollLot = HRISOnlineService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);
            LotCombobox.ItemsSource = payrollLot;
            LotCombobox.SelectedValue = "06/2018";
            ShiftCombobox.ItemsSource = BusinessLayerServices.ShiftBL().GetAllShiftList();
            //LotCombobox.SelectedValue = DateTime.Now.Month.ToString("D2") + "/" + DateTime.Now.Year;
        }

        private void BindOTDetail()
        {
            try
            {
                if (OTdetail != null)
                {
                    var employee = BusinessLayerServices.EmployeeBL().GetEmployeeByEmployeeID(OTdetail.Employee_ID);
                    EmployeeIDTextBox.Text = employee.Employee_ID;
                    FingerIDTextBox.Text = employee.FingerScanID;
                    TitleCombobox.SelectedValue = employee.Person.TitleName_ID;
                    NameTHTextBox.Text = employee.Person.FirstNameTH;
                    LastnameTHTextBox.Text = employee.Person.LastNameTH;
                    StaffTypeCombobox.SelectedValue = employee.Staff_type;

                    LotCombobox.SelectedValue = OTdetail.PayrollLot;
                    OTDateDatePicker.SelectedDate = OTdetail.OTDate;
                    OTTypeCombobox.SelectedValue = OTdetail.OTType;
                    OTRequestCombobox.SelectedValue = OTdetail.RequestType;
                    string otAmount = OTdetail.NcountOT.ToString();
                    decimal otHour = Math.Truncate(Convert.ToDecimal(otAmount));
                    decimal otMinute = Convert.ToDecimal(otAmount) - otHour;
                    OTHourCombobox.SelectedValue = otHour;
                    OTMinuteCombobox.SelectedValue = otMinute;
                    RemarkAdminTextBox.Text = OTdetail.RemarkFromAdmin;
                    RemarkManagerTextBox.Text = OTdetail.RemarkFromManager;
                    RemarkFinalApvrTextBox.Text = OTdetail.RemarkFromFinalApprover;
                    RemarkHRTextBox.Text = OTdetail.RemarkFromHR;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindControl()
        {
            EmployeeIDTextBox.IsEnabled = _formState != "Insert" ? false : true;
            //ShiftCombobox.IsEnabled = _formState != "Insert" ? false : true; 
            ShiftCombobox.IsEnabled = false;
            OTTypeCombobox.IsEnabled = _formState != "Insert" ? false : true; // Disable if not Insert
            OTRequestCombobox.IsEnabled = _formState != "Insert" ? false : true;
            //OTTextBox.IsEnabled = (_formState == "Insert" || _formState == "Update") ? true : false;
            OTHourCombobox.IsEnabled = (_formState == "Insert" || _formState == "Update") ? true : false;
            OTMinuteCombobox.IsEnabled = (_formState == "Insert" || _formState == "Update") ? true : false;
            RemarkAdminTextBox.IsEnabled = _actionFrom != "Admin" ? false : true; // Disable is not Admin
            //RemarkAdminTextBox.Background = RemarkAdminTextBox.IsEnabled ? Brushes.LightCyan : Brushes.White;
            RemarkManagerTextBox.IsEnabled = (_actionFrom == "Approver" && _formState == "Reject") ? true : false;
            RemarkManagerTextBox.Background = RemarkManagerTextBox.IsEnabled ? Brushes.SeaShell : Brushes.White;
            RemarkFinalApvrTextBox.IsEnabled = (_actionFrom == "FinalApprover" && _formState == "Reject") ? true : false;
            RemarkFinalApvrTextBox.Background = RemarkFinalApvrTextBox.IsEnabled ? Brushes.SeaShell : Brushes.White;
            RemarkHRTextBox.IsEnabled = (_actionFrom == "HR" && _formState == "Reject") ? true : false;
            RemarkHRTextBox.Background = RemarkHRTextBox.IsEnabled ? Brushes.SeaShell : Brushes.White;

            SearchButton.Visibility = _formState == "Insert" ? Visibility.Visible : Visibility.Collapsed;
            //ApproveDetailButton.Visibility = ((_formState == "Update" || _formState == "View") && _actionFrom != "Report") ? Visibility.Visible : Visibility.Collapsed;
            ApproveDetailButton.Visibility = (_formState == "View" && _actionFrom != "Report") ? Visibility.Visible : Visibility.Collapsed;
            SaveButton.Visibility = (_formState == "Update" || _formState == "Insert") ? Visibility.Visible : Visibility.Collapsed;
            RejectButton.Visibility = (_formState == "Reject") ? Visibility.Visible : Visibility.Collapsed;
        }

        private void LotCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var selected = (Lot_Number)LotCombobox.SelectedItem;
                if (selected != null)
                {
                    LotFromDatePicker.SelectedDate = selected.Start_date;
                    LotToDatePicker.SelectedDate = selected.Finish_date;
                    OTDateDatePicker.DisplayDateStart = selected.Start_date;
                    OTDateDatePicker.DisplayDateEnd = selected.Finish_date;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EmployeeIDTextBox_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Search();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void Search()
        {
            if (_formState != "Insert") return;
            var window = new SearchEmployee_Window();
            window._requestFrom = "Admin";
            window.ShowDialog();
            if (window.selectedEmployee != null)
            {
                var s = window.selectedEmployee;
                EmployeeIDTextBox.Text = s.Employee_ID;
                FingerIDTextBox.Text = s.FingerScanID;
                TitleCombobox.SelectedValue = s.Person.TitleName_ID;
                NameTHTextBox.Text = s.Person.FirstNameTH;
                LastnameTHTextBox.Text = s.Person.LastNameTH;
                StaffTypeCombobox.SelectedValue = s.Staff_type;
                OTDateDatePicker.IsEnabled = true;

                OTDateDatePicker.SelectedDate = null;
            }
        }

        private void OTDateDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Clear();
                if (OTDateDatePicker.SelectedDate != null)
                {
                    CheckDateType();
                    if (string.IsNullOrEmpty(EmployeeIDTextBox.Text) || string.IsNullOrEmpty(FingerIDTextBox.Text)) throw new Exception("กรุณาเลือกพนักงาน.");
                    CheckINOut();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Clear()
        {
            ShiftCombobox.SelectedIndex = -1;
            OTTypeCombobox.SelectedIndex = -1;
            OTRequestCombobox.SelectedIndex = -1;
            OTHourCombobox.SelectedIndex = 0;
            OTMinuteCombobox.SelectedIndex = 0;
            HolidayTextBox.Text = "";
            ScanInTextBox.Text = "";
            ScanOutTextBox.Text = "";
            TotalWorkTextBox.Text = "";
            CalculatedOTTextBox.Text = "";
            ShiftCombobox.SelectedIndex = -1;
        }

        private void CheckDateType()
        {
            try
            {
                DateTime otDate = OTDateDatePicker.SelectedDate.Value;
                if (otDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    HolidayTextBox.Text = "วันอาทิตย์";
                    //DateTypeTextBlock.Foreground = Brushes.OrangeRed;
                    OTTypeCombobox.SelectedValue = "H";
                }
                else
                {
                    //เช็คว่าเป็น holiday ที่กำหนดไว้หรือไม่  
                    var holiday = BusinessLayerServices.ReferenceBL().GetHolidayByDate(otDate);
                    if (holiday == null)
                    {
                        HolidayTextBox.Text = "วันธรรมดา";
                        //DateTypeTextBlock.Foreground = Brushes.Black;
                        OTTypeCombobox.SelectedValue = "N";
                    }
                    else
                    {
                        if (holiday.HolidayFlag == true)//ตามประกาศบริษัทฯ
                        {
                            HolidayTextBox.Text = holiday.HolidayNameTH.ToString() + " (วันหยุดตามประกาศบริษัท)";
                            //DateTypeTextBlock.Foreground = Brushes.Blue;
                            OTTypeCombobox.SelectedValue = "H";
                        }
                        else //ถ้าเป็นวันหยุดพิเศษ เช่น งานเลี้ยง คริตส์มาส 
                        {
                            HolidayTextBox.Text = holiday.HolidayNameTH.ToString() + " (วันหยุดพิเศษ)";
                            //DateTypeTextBlock.Foreground = Brushes.OrangeRed;
                            OTTypeCombobox.SelectedValue = "H";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CheckINOut()
        {
            var shiftControl = BusinessLayerServices.ShiftBL().GetSingleShiftFromShiftControl(EmployeeIDTextBox.Text, OTDateDatePicker.SelectedDate.Value);
            ShiftCombobox.SelectedValue = shiftControl == null ? "0" : shiftControl.Shift_ID;

            TimeSpan? scanIn, scanOut;

            var otDate = OTDateDatePicker.SelectedDate.Value;
            var scanedDate = BusinessLayerServices.TimeStecBL().GetCheckInOut(FingerIDTextBox.Text, otDate);
            var manualDate = BusinessLayerServices.ManualWorkingBL().GetManualDateByDateAndID(otDate, EmployeeIDTextBox.Text);

            if (manualDate.Count() <= 0 && scanedDate.Count() <= 0)
            {
                ScanInTextBox.Text = "";
                ScanOutTextBox.Text = "";
                TotalWorkTextBox.Text = "";
                CalculatedOTTextBox.Text = "";
                ShiftCombobox.SelectedIndex = -1;
                MessageBox.Show("ไม่พบเวลาการทำงานในวันที่ " + otDate, "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                //เช็คเวลาเข้าทำงาน
                if (manualDate.Where(e => e.ManualWorkDateType_ID == "A01").Count() > 0) scanIn = manualDate.Where(e => e.ManualWorkDateType_ID == "A01").FirstOrDefault().Times;
                else if (scanedDate.Count > 0) scanIn = scanedDate.Min(c => c.checktime.TimeOfDay);
                else scanIn = null;
                ScanInTextBox.Text = scanIn.ToString();

                //เช็คเวลาออกงาน
                if (manualDate.Where(e => e.ManualWorkDateType_ID == "A02").Count() > 0) scanOut = manualDate.Where(e => e.ManualWorkDateType_ID == "A02").FirstOrDefault().Times;
                else if (scanedDate.Count > 0) scanOut = scanedDate.Max(c => c.checktime.TimeOfDay);
                else scanOut = null;
                ScanOutTextBox.Text = scanOut.ToString();

                //คำนวนเวลาทำงาน
                if (scanIn != null && scanOut != null)
                {
                    int breakTime = shiftControl.Shift_ID == "0" ? 60 : 30; // shift (0) (8-17) subtact 60min, other shift subtract 30min
                    TimeSpan span = scanOut.GetValueOrDefault() - scanIn.GetValueOrDefault();
                    span = (scanOut.GetValueOrDefault() != scanIn.GetValueOrDefault()) ? span.Subtract(new TimeSpan(0, breakTime, 0)) : span; 
                    TotalWorkTextBox.Text = string.Format("{0} hours , {1} minutes", span.Hours, span.Minutes);
                }
                else TotalWorkTextBox.Text = "";
            }
            var av = avialableOT();
            var avt = TimeSpan.FromMinutes((double)av);
            CalculatedOTTextBox.Text = string.Format("{0} hours , {1} minutes", avt.Hours, avt.Minutes);
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("ยืนยันการบันทึกหรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes) Save();
        }

        private void ApproveDetailButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("ยืนยันการ Approve หรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                _formState = "Approve";
                Save();
            }
        }

        private void RejectButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("ยืนยันการ Reject หรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;
            if (_actionFrom == "Approver" && !string.IsNullOrEmpty(RemarkManagerTextBox.Text)) Save();
            else if (_actionFrom == "FinalApprover" && !string.IsNullOrEmpty(RemarkFinalApvrTextBox.Text)) Save();
            else if (_actionFrom == "HR" && !string.IsNullOrEmpty(RemarkHRTextBox.Text)) Save();

            else MessageBox.Show("กรุณาใส่ Remark การปฏิเสธ", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void Save()
        {
            try
            {
                ConditionChecking();
                if (OTAmountChecking())
                {
                    // Create document by Admin
                    if (_formState == "Insert") AddOT();
                    // Update - Approve - Reject
                    else UpdateOT();
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ConditionChecking()
        {
            if (string.IsNullOrEmpty(EmployeeIDTextBox.Text)) throw new Exception("กรุณาระบุพนักงาน");
            if (LotCombobox.Text == "" || LotFromDatePicker.Text == "" || LotToDatePicker.Text == "") throw new Exception("กรุณาระบุ Lot");
            if (OTDateDatePicker.Text == "") throw new Exception("กรุณาระบุวันที่ทำ OT");
            //21 June 2018 ไม่อนุญาตให้คีย์ล่วงหน้าได้
            if (Convert.ToDateTime(OTDateDatePicker.Text) > Convert.ToDateTime(DateTime.Now)) throw new Exception("ไม่สามารถบันทึกวันที่ทำโอทีล่วงหน้าได้ กรุณาตรวจสอบข้อมูลอีกครั้ง");
            // 29Oct2018 ไม่อนุญาติให้คีย์ข้อมูลย้อนหลัง (OT ไม่ตรง Lot)
            if (Convert.ToDateTime(OTDateDatePicker.Text) < Convert.ToDateTime(LotFromDatePicker.Text)) throw new Exception("ไม่สามารถบันทึกการทำโอทีได้ เนื่องจากคุณระบุวันที่ทำโอทีย้อนหลังจาก Lot ปัจจุบัน");

            //8 Aug 2018 ให้เช็คการอนุญาตให้คีย์ในช่วง lot ที่เลือกไว้ และ lot นั้นจะต้องไม่ lock จากบัญชีและ HRM ด้วย
            //var _lotNumber = (LotNumberViewModel)LotCombobox.SelectedItem;
            //if (_lotNumber.Lock_Acc == "2" || _lotNumber.Lock_Acc_Labor == "2") throw new Exception("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก");
            //if (_lotNumber.Lock_Hr_Labor == "2") throw new Exception("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก");
            //if (ScanInTextBox.Text == "" || ScanOutTextBox.Text == "") throw new Exception("เนื่องจากพนักงานท่านนี้ไม่มีข้อมูลการสแกนนิ้วในวันที่ดังกล่าวจึงไม่สามารถบันทึกโอทีได้ กรุณาตรวจสอบ");

            DateTime otDate = OTDateDatePicker.SelectedDate.Value;
            TimeSpan span = (Convert.ToDateTime(ScanOutTextBox.Text) - Convert.ToDateTime(ScanInTextBox.Text));
            //วันธรรมดาที่ไม่ใช่วันหยุดประจำปีของบริษัท หากทำงานไม่เกิน 8 ชั่วโมง ไม่อนุญาตให้ทำการบันทึกโอที
            var holiday = BusinessLayerServices.ReferenceBL().GetHolidayByDate(otDate);
            if (holiday == null && span.Hours < 8) throw new Exception("ไม่สามารถบันทึกการขอทำโอที เนื่องจากมีการมีการทำงานน้อยกว่า 8 ชัวโมง");

            if (OTHourCombobox.Text == "" || OTMinuteCombobox.Text == "") throw new Exception("กรุณาระบุเวลาทำ OT ให้ครบถ้วน");
            if (OTTypeCombobox.Text == "") throw new Exception("กรุณาเลือกประเภท OT");
            decimal otAmount = Convert.ToDecimal(OTHourCombobox.Text.ToString()) + Convert.ToDecimal(OTMinuteCombobox.Text.ToString());
            if (OTTypeCombobox.SelectedValue.ToString() == "H" && otAmount > 2) throw new Exception("จำนวนวันทำงาน Holiday ไม่ถูกต้อง กรุณาตรวจสอบข้อมูล");
            if (OTRequestCombobox.Text == "") throw new Exception("กรุณาเลือกช่วงเวลาทำ OT");
        }

        private bool OTAmountChecking()
        {
            if (_formState == "Reject") return true; // Reject case only update status and remark not ot amount
            DateTime otDate = OTDateDatePicker.SelectedDate.Value;
            var holiday = BusinessLayerServices.ReferenceBL().GetHolidayByDate(otDate);
            if (holiday != null) return true;

            decimal availableTime = avialableOT();
            decimal _requestedOT = 0;

            var oTByDate = BusinessLayerServices.OTBL().GetSingleOTByDate(EmployeeIDTextBox.Text, otDate).NcountOT.GetValueOrDefault();
            if (_formState == "Insert")
            {
                if (oTByDate > 0)
                {
                    var _otHour = Math.Truncate(oTByDate);
                    var _otMinuteMultiplyer = oTByDate - _otHour;
                    var _otMinute = HRISOnlineService.DataCorrector().GetOTMinute("Update").SingleOrDefault(w => w.Multiplier == _otMinuteMultiplyer).Minutes;
                    _requestedOT = (_otHour * 60) + Convert.ToDecimal(_otMinute);
                }
            }

            var otHour = Convert.ToDecimal(OTHourCombobox.Text.ToString());
            var otMinute = Convert.ToDecimal(OTMinuteCombobox.Text.ToString()); // Minute unit
            decimal requestedTime = ((otHour * 60) + otMinute) + _requestedOT;

            if (requestedTime > availableTime)
            {
                MessageBox.Show("จำนวนโอทีรวมในวันที่ " + otDate.ToString("dd/MM/yyyy") + " มากกว่าที่ระบบคำนวนได้ \n " + 
                                "เวลาที่ขอได้ " + availableTime + " นาที < รวม OT ที่ขอ " + requestedTime + " นาที", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                if (oTByDate > 0)
                {
                    var w = new OTByDateWindow(EmployeeIDTextBox.Text, otDate);
                    w.HeaderTextBlock.Text = string.Format("[ {0} {1} ]'s OT List in [ {2} ]", NameTHTextBox.Text, LastnameTHTextBox.Text, otDate.ToString("dd/MM/yyyy"));
                    w.ShowDialog();
                }
                return false;
            }

            return true;
        }

        private decimal avialableOT()
        {
            var shiftControl = BusinessLayerServices.ShiftBL().GetSingleShiftFromShiftControl(EmployeeIDTextBox.Text, OTDateDatePicker.SelectedDate.Value);

            int breakMinute = shiftControl.Shift_ID == "0" ? 60 : 30;
            int workHour = shiftControl.Shift_ID == "9" ? 7 : 8; // shift (9) (2-10) works 7.30 hour
            int workMimute = shiftControl.Shift_ID == "9" ? 30 : 0; // shift (9) (2-10) works 7.30 hour

            TimeSpan span = (Convert.ToDateTime(ScanOutTextBox.Text) - Convert.ToDateTime(ScanInTextBox.Text));
            decimal availableTime = (((span.Hours - workHour) * 60) + (span.Minutes)) - workMimute - breakMinute; // shift (0) (8-17) subtact 60min, other shift subtract 30min

            return availableTime;
        }

        private void AddOT()
        {
            var _ot = BusinessLayerServices.OTBL().GetSingleOT(EmployeeIDTextBox.Text, OTDateDatePicker.SelectedDate, OTTypeCombobox.SelectedValue.ToString(), OTRequestCombobox.SelectedValue.ToString());
            if (_ot != null) throw new Exception("โอทีประเภทนี้ได้มีการบันทึกอยู่ในระบบแล้ว กรุณาตรวจสอบ.");

            var otHour = Convert.ToDecimal(OTHourCombobox.Text.ToString());
            var otMinute = Convert.ToDecimal(OTMinuteCombobox.SelectedValue.ToString()); // Multiplier value
            decimal otAmount = otHour + otMinute;
            var newItem = new OT
            {
                Employee_ID = EmployeeIDTextBox.Text,
                OTDate = OTDateDatePicker.SelectedDate.Value,
                OTType = (string)OTTypeCombobox.SelectedValue,
                RequestType = (string)OTRequestCombobox.SelectedValue,
                NcountOT = otAmount,
                Status = false,
                ModifiedDate = DateTime.Now,
                Remark = "Create from OT Online",
                PayrollLot = LotCombobox.SelectedValue.ToString(),
                RemarkFromAdmin = RemarkAdminTextBox.Text
            };
            BusinessLayerServices.OTBL().AddOT(newItem);
        }

        private void UpdateOT()
        {
            var otHour = Convert.ToDecimal(OTHourCombobox.Text.ToString());
            var otMinute = Convert.ToDecimal(OTMinuteCombobox.SelectedValue.ToString()); // Multiplier value
            decimal otAmount = otHour + otMinute;

            var newItem = BusinessLayerServices.OTBL().GetSingleOT(EmployeeIDTextBox.Text, OTDateDatePicker.SelectedDate.Value, OTTypeCombobox.SelectedValue.ToString(), OTRequestCombobox.SelectedValue.ToString());
            newItem.NcountOT = otAmount;
            newItem.RemarkFromAdmin = RemarkAdminTextBox.Text;
            newItem.RemarkFromManager = RemarkManagerTextBox.Text;
            newItem.RemarkFromFinalApprover = RemarkFinalApvrTextBox.Text;
            newItem.RemarkFromHR = RemarkHRTextBox.Text;
            newItem.ModifiedDate = DateTime.Now;
            newItem.ModifiedUser = SingletonConfiguration.getInstance().Username;
            BusinessLayerServices.OTOnlineBL().UpdateOTOnline(newItem, _actionFrom, _formState, SingletonConfiguration.getInstance().Username);

            var listOT = new List<OT>() { newItem };
            if (_formState == "Approve") HRISOnlineService.EmailSender().SendApproveEmail(listOT, _actionFrom);
            if (_formState == "Reject") HRISOnlineService.EmailSender().SendRejectEmail(newItem, _actionFrom);
        }
    }
}
