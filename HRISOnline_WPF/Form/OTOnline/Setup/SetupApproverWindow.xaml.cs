﻿using DomainModelHris;
using HRISOnline_WPF.Shared;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISOnline_WPF.Form.OTOnline.Setup
{
    /// <summary>
    /// Interaction logic for SetupApproverWindow.xaml
    /// </summary>
    public partial class SetupApproverWindow : Window
    {
        public SetupApproverWindow()
        {
            InitializeComponent();
            TitleCombobox.ItemsSource = BusinessLayerServices.PersonBL().GetAllTitlenameList();
            DepartmentCombobox.ItemsSource = BusinessLayerServices.DeptCodeBL().GetAllDepartment();
            ReloadDatagrid();
        }

        private void EmployeeIDTextBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left) Search();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void Search()
        {
            try
            {
                var window = new SearchEmployee_Window();
                window._requestFrom = "HR Approver";
                window.ShowDialog();
                if (window.selectedEmployee != null) BindSelectedEmployee(window.selectedEmployee);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindSelectedEmployee(Employee e)
        {
            Clear();
            EmployeeIDTextBox.Text = e.Employee_ID;
            TitleCombobox.SelectedValue = e.Person.TitleName_ID;
            NameTHTextBox.Text = e.Person.FirstNameTH;
            LastnameTHTextBox.Text = e.Person.LastNameTH;
            EmailTextBox.Text = e.Email;
            if (EmailTextBox.Text == "") MessageBox.Show("ไม่พบอีเมลผู้ใช้ ไม่สามารถบันทึกได้.", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void ReloadDatagrid()
        {
            try
            {
                Clear();
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = BusinessLayerServices.OTOnlineBL().GetAllOTOnlineApprover();
                TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", InformationDataGrid.Items.Count);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            try
            {
                EmployeeIDTextBox.Text = "";
                TitleCombobox.SelectedIndex = -1;
                NameTHTextBox.Text = "";
                LastnameTHTextBox.Text = "";
                EmailTextBox.Text = "";
                UsernameTextBox.Text = "";
                UsernameTextBox.IsEnabled = true;
                DepartmentCombobox.SelectedIndex = -1;

                AddButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(EmployeeIDTextBox.Text)) throw new Exception("กรุณาเลือกพนักงาน");
                    if (string.IsNullOrEmpty(NameTHTextBox.Text)) throw new Exception("กรุณาเลือกพนักงาน");
                    if (string.IsNullOrEmpty(TitleCombobox.Text)) throw new Exception("กรุณาเลือกพนักงาน");
                    if (string.IsNullOrEmpty(EmailTextBox.Text)) throw new Exception("ไม่พบอีเมลผู้ใช้ ไม่สามารถบันทึกได้");
                    if (string.IsNullOrEmpty(UsernameTextBox.Text)) throw new Exception("กรุณาระบุชื่อผู้ใช้พนักงาน");
                    if (string.IsNullOrEmpty(DepartmentCombobox.Text)) throw new Exception("กรุณาเลือกแผนก");

                    var approver = BusinessLayerServices.OTOnlineBL().GetSingleOTOnlineApprover(UsernameTextBox.Text, EmployeeIDTextBox.Text);
                    if (approver == null)
                    {
                        AddApprover();
                        approver = BusinessLayerServices.OTOnlineBL().GetSingleOTOnlineApprover(UsernameTextBox.Text, EmployeeIDTextBox.Text);
                    }

                    var newItem = new OTOnlineSetupApproverDetail
                    {
                        Approver_ID = approver.Approver_ID,
                        Dept_code = DepartmentCombobox.SelectedValue.ToString()
                    };
                    BusinessLayerServices.OTOnlineBL().AddOTOnlineApproverDetail(newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddApprover()
        {
            try
            {
                var newItem = new OTOnlineSetupApprover
                {
                    EmployeeID = EmployeeIDTextBox.Text,
                    StecUsername = UsernameTextBox.Text
                };
                BusinessLayerServices.OTOnlineBL().AddOTOnlineApprover(newItem);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var oldItem = (OTOnlineSetupApproverDetail)InformationDataGrid.SelectedItem;
                    if (oldItem == null) throw new Exception("กรุณาเลือกข้อมูลที่ต้องการ");
                    BusinessLayerServices.OTOnlineBL().DeleteOTOnlineApproverDetail(oldItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UsernameTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(UsernameTextBox.Text)) UsernameTextBox.Text = HRISOnlineService.DataCorrector().FirstCapital(UsernameTextBox.Text);
        }
    }
}
