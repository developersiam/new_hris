﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DomainModelHris;
using HRISSystemBL;
using HRISOnline_WPF.Shared;

namespace HRISOnline_WPF.Form.OTOnline.Setup
{
    /// <summary>
    /// Interaction logic for SetupAdminWindow.xaml
    /// </summary>
    public partial class SetupAdminWindow : Window
    {
        public SetupAdminWindow()
        {
            InitializeComponent();
            TitleCombobox.ItemsSource = BusinessLayerServices.PersonBL().GetAllTitlenameList();
            DepartmentCombobox.ItemsSource = BusinessLayerServices.DeptCodeBL().GetAllDepartment();
            ReloadDatagrid();
        }

        private void EmployeeIDTextBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left) Search();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void Search()
        {
            try
            {
                var window = new SearchEmployee_Window();
                window._requestFrom = "HR Approver";
                window.ShowDialog();
                if (window.selectedEmployee != null) BindSelectedEmployee(window.selectedEmployee);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindSelectedEmployee(Employee e)
        {
            Clear();
            EmployeeIDTextBox.Text = e.Employee_ID;
            TitleCombobox.SelectedValue = e.Person.TitleName_ID;
            NameTHTextBox.Text = e.Person.FirstNameTH;
            LastnameTHTextBox.Text = e.Person.LastNameTH;
            EmailTextBox.Text = e.Email;
            if (EmailTextBox.Text == "") MessageBox.Show("ไม่พบอีเมลผู้ใช้ ไม่สามารถบันทึกได้.", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void ReloadDatagrid()
        {
            try
            {
                Clear();
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = BusinessLayerServices.OTOnlineBL().GetAllOTOnlineAdmin();
                TotalTextBlock.Text = string.Format("ทั้งหมด {0} รายการ", InformationDataGrid.Items.Count);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            try
            {
                EmployeeIDTextBox.Text = "";
                TitleCombobox.SelectedIndex = -1;
                NameTHTextBox.Text = "";
                LastnameTHTextBox.Text = "";
                EmailTextBox.Text = "";
                UsernameTextBox.Text = "";
                UsernameTextBox.IsEnabled = true;
                DepartmentCombobox.SelectedIndex = -1;

                AddButton.IsEnabled = true;
                UpdateButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                //OTOnlineSetupAdminDetail selected = (OTOnlineSetupAdminDetail)InformationDataGrid.SelectedItem;
                //var admin = BusinessLayerServices.OTOnlineBL().GetSingleOTOnlineAdminDetail(selected.Admin_ID, selected.DeptID);
                //EmployeeIDTextBox.Text = admin.OTOnlineSetupAdmin.Employee.Employee_ID;
                //TitleCombobox.SelectedValue = admin.OTOnlineSetupAdmin.Employee.Person.TitleName_ID;
                //NameTHTextBox.Text = admin.OTOnlineSetupAdmin.Employee.Person.FirstNameTH;
                //LastnameTHTextBox.Text = admin.OTOnlineSetupAdmin.Employee.Person.LastNameTH;
                //EmailTextBox.Text = admin.OTOnlineSetupAdmin.Employee.Email;
                //UsernameTextBox.Text = admin.OTOnlineSetupAdmin.AdName;
                //DepartmentCombobox.SelectedValue = admin.DeptID;

                //AddButton.IsEnabled = false;
                //UpdateButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(EmployeeIDTextBox.Text)) throw new Exception("กรุณาเลือกพนักงาน");
                    if (string.IsNullOrEmpty(NameTHTextBox.Text)) throw new Exception("กรุณาเลือกพนักงาน");
                    if (string.IsNullOrEmpty(TitleCombobox.Text)) throw new Exception("กรุณาเลือกพนักงาน");
                    if (string.IsNullOrEmpty(EmailTextBox.Text)) throw new Exception("ไม่พบอีเมลผู้ใช้ ไม่สามารถบันทึกได้");
                    if (string.IsNullOrEmpty(UsernameTextBox.Text)) throw new Exception("กรุณาระบุชื่อผู้ใช้พนักงาน");
                    if (string.IsNullOrEmpty(DepartmentCombobox.Text)) throw new Exception("กรุณาเลือกแผนก");

                    //var admin = BusinessLayerServices.OTBL().GetListOTOnlineAdmin(UsernameTextBox.Text).FirstOrDefault();
                    var admin = BusinessLayerServices.OTOnlineBL().GetSingleOTOnlineAdmin(UsernameTextBox.Text, EmployeeIDTextBox.Text);
                    if (admin == null)
                    {
                        AddAdmin();
                        admin = BusinessLayerServices.OTOnlineBL().GetSingleOTOnlineAdmin(UsernameTextBox.Text, EmployeeIDTextBox.Text);
                    }

                    var newItem = new OTOnlineSetupAdminDetail
                    {
                        Admin_ID = admin.Admin_ID,
                        DeptID = DepartmentCombobox.SelectedValue.ToString(),
                        CreatedDate = DateTime.Now,
                        ValidFrom = DateTime.Now,
                        EndDate = DateTime.MaxValue
                    };
                    BusinessLayerServices.OTOnlineBL().AddOTOnlineAdminDetail(newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddAdmin()
        {
            try
            {
                var newItem = new OTOnlineSetupAdmin
                {
                    Employee_ID = EmployeeIDTextBox.Text,
                    AdName = UsernameTextBox.Text,
                    CreateDate = DateTime.Now,
                    ValidFrom = DateTime.Now,
                    EndDate = DateTime.MaxValue
                };
                BusinessLayerServices.OTOnlineBL().AddOTOnlineAdmin(newItem);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (string.IsNullOrEmpty(EmployeeIDTextBox.Text)) throw new Exception("กรุณาเลือกพนักงาน");
                    if (string.IsNullOrEmpty(NameTHTextBox.Text)) throw new Exception("กรุณาเลือกพนักงาน");
                    if (string.IsNullOrEmpty(TitleCombobox.Text)) throw new Exception("กรุณาเลือกพนักงาน");
                    if (string.IsNullOrEmpty(EmailTextBox.Text)) throw new Exception("ไม่พบอีเมลผู้ใช้ ไม่สามารถบันทึกได้");
                    if (string.IsNullOrEmpty(UsernameTextBox.Text)) throw new Exception("กรุณาระบุชื่อผู้ใช้พนักงาน");
                    if (string.IsNullOrEmpty(DepartmentCombobox.Text)) throw new Exception("กรุณาเลือกแผนก");

                    var admin = BusinessLayerServices.OTOnlineBL().GetListOTOnlineAdmin(UsernameTextBox.Text).FirstOrDefault();
                    var oldItem = BusinessLayerServices.OTOnlineBL().GetSingleOTOnlineAdminDetail(admin.Admin_ID, DepartmentCombobox.SelectedValue.ToString());

                    var newItem = new OTOnlineSetupAdminDetail
                    {
                        Admin_ID = oldItem.Admin_ID,
                        DeptID = DepartmentCombobox.SelectedValue.ToString(),
                        CreatedDate = oldItem.CreatedDate,
                        ModifiedDate = DateTime.Now,
                        ValidFrom = oldItem.ValidFrom,
                        EndDate = oldItem.EndDate
                    };
                    BusinessLayerServices.OTOnlineBL().UpdateOTOnlineAdminDetail(newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var oldItem = (OTOnlineSetupAdminDetail)InformationDataGrid.SelectedItem;
                    if (oldItem == null) throw new Exception("กรุณาเลือกข้อมูลที่ต้องการ");
                    BusinessLayerServices.OTOnlineBL().DeleteOTOnlineAdminDetail(oldItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UsernameTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(UsernameTextBox.Text)) UsernameTextBox.Text = HRISOnlineService.DataCorrector().FirstCapital(UsernameTextBox.Text);
        }
    }
}
