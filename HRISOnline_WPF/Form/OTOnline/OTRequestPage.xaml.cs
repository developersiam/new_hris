﻿using DomainModelHris;
using DomainModelPayroll;
using HRISOnline_WPF.Shared;
using HRISOnline_WPF.ViewModel;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HRISOnline_WPF.Form.OTOnline
{
    /// <summary>
    /// Interaction logic for OTRequestPage.xaml
    /// </summary>
    public partial class OTRequestPage : Page
    {
        public OTRequestPage()
        {
            InitializeComponent();
            BindPayrollLot();
            ReloadDataGrid();
        }

        private void BindPayrollLot()
        {
            var payrollLot = HRISOnlineService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);
            LotCombobox.ItemsSource = payrollLot;
            LotCombobox.SelectedValue = "06/2018";
            //LotCombobox.SelectedValue = DateTime.Now.Month.ToString("D2") + "/" + DateTime.Now.Year;
        }

        private void LotCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                OTDataGrid.ItemsSource = null;
                var selectedLot = (Lot_Number)LotCombobox.SelectedItem;
                DateTime displayDateStart = selectedLot.Start_date.GetValueOrDefault();
                DateTime displayDateEnd = selectedLot.Finish_date.GetValueOrDefault();
                LotFromDateDatePicker.DisplayDateStart = displayDateStart;
                LotFromDateDatePicker.DisplayDateEnd = displayDateEnd;
                LotFromDateDatePicker.SelectedDate = displayDateStart;
                LotToDateDatePicker.DisplayDateStart = displayDateStart;
                LotToDateDatePicker.DisplayDateEnd = displayDateEnd;
                LotToDateDatePicker.SelectedDate = displayDateEnd;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDataGrid();
        }

        private void NameFilterTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            ReloadDataGrid();
        }

        private void ReloadDataGrid()
        {
            var fromDate = LotFromDateDatePicker.SelectedDate.GetValueOrDefault();
            var toDate = LotToDateDatePicker.SelectedDate.GetValueOrDefault();
            if (fromDate != null && toDate != null)
            {
                var listOTData = BusinessLayerServices.OTOnlineBL().GetOTOnlineList(fromDate, toDate, "Admin", SingletonConfiguration.getInstance().Username);
                if (NameFilterTextBox.Text.Trim() != "") listOTData = listOTData.Where(w => w.Employee.Person.FirstNameTH.Contains(NameFilterTextBox.Text)).ToList();

                List<OTViewModel> listOTView = HRISOnlineService.DataCorrector().ConvertToOTViewmodel(listOTData);

                OTDataGrid.ItemsSource = null;
                OTDataGrid.ItemsSource = listOTView;
                TotalDocumentTextBox.Text = listOTView.Count().ToString("N0");
            }
        }

        private void RequestNewOTButton_Click(object sender, RoutedEventArgs e)
        {
            GetOTDetailWindow(null, "Insert");
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            GetOTDetailWindow((OTViewModel)OTDataGrid.SelectedItem, "Update");
        }

        private void OTDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            GetOTDetailWindow((OTViewModel)OTDataGrid.SelectedItem, "Update");
        }

        private void GetOTDetailWindow(OTViewModel otView, string formState)
        {
            try
            {
                var window = new OTDetailWindow(otView == null ? null : otView.OTs, "Admin", formState);
                window.ShowDialog();
                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ApproveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการยืนยันการส่งคำร้องขอโอทีหรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;
                var selected = (OTViewModel)OTDataGrid.SelectedItem;
                BusinessLayerServices.OTOnlineBL().UpdateOTOnline(selected.OTs, "Admin", "Approve", SingletonConfiguration.getInstance().Username);
                //    //Send Email
                //    List<OT> listOT = new List<OT>();
                //    listOT.Add(model);
                //    OTOnlineService.EmailSender().SendApproveEmail(listOT, "Admin");
                MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;
                var selected = (OTViewModel)OTDataGrid.SelectedItem;
                BusinessLayerServices.OTBL().DeleteOT(selected.OTs);
                MessageBox.Show("ลบข้อมูลสำเร็จ", "Information");
                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
