﻿using DomainModelHris;
using DomainModelPayroll;
using HRISOnline_WPF.Shared;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HRISOnline_WPF.Form.OTOnline
{
    /// <summary>
    /// Interaction logic for ApproverPage.xaml
    /// </summary>
    public partial class ApproverPage : Page
    {
        public ApproverPage()
        {
            InitializeComponent();
            BindPayrollLot();
            ReloadDataGrid();
        }

        private void BindPayrollLot()
        {
            var payrollLot = HRISOnlineService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);
            LotCombobox.ItemsSource = payrollLot;
            LotCombobox.SelectedValue = "06/2018";
            //LotCombobox.SelectedValue = DateTime.Now.Month.ToString("D2") + "/" + DateTime.Now.Year;
        }

        private void LotCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                OTDataGrid.ItemsSource = null;
                var selectedLot = (Lot_Number)LotCombobox.SelectedItem;
                DateTime displayDateStart = selectedLot.Start_date.GetValueOrDefault();
                DateTime displayDateEnd = selectedLot.Finish_date.GetValueOrDefault();
                LotFromDateDatePicker.DisplayDateStart = displayDateStart;
                LotFromDateDatePicker.DisplayDateEnd = displayDateEnd;
                LotFromDateDatePicker.SelectedDate = displayDateStart;
                LotToDateDatePicker.DisplayDateStart = displayDateStart;
                LotToDateDatePicker.DisplayDateEnd = displayDateEnd;
                LotToDateDatePicker.SelectedDate = displayDateEnd;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDataGrid();
        }

        private void NameFilterTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            ReloadDataGrid();
        }

        private void ReloadDataGrid()
        {
            var fromDate = LotFromDateDatePicker.SelectedDate.GetValueOrDefault();
            var toDate = LotToDateDatePicker.SelectedDate.GetValueOrDefault();
            if (fromDate != null && toDate != null)
            {
                var listOTData = BusinessLayerServices.OTOnlineBL().GetOTOnlineList(fromDate, toDate, "Approver", SingletonConfiguration.getInstance().Username);
                if (NameFilterTextBox.Text.Trim() != "")
                {
                    listOTData = listOTData.Where(w => w.Employee.Person.FirstNameTH.Contains(NameFilterTextBox.Text)).ToList();
                }
                OTDataGrid.ItemsSource = null;
                OTDataGrid.ItemsSource = listOTData;
                TotalDocumentTextBox.Text = listOTData.Count().ToString("N0");
                DatagridSelectedItems();
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            GetOTDetailWindow("View");
        }

        private void OTDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            GetOTDetailWindow("View");
        }

        private void RejectButton_Click(object sender, RoutedEventArgs e)
        {
            GetOTDetailWindow("Reject");
        }

        private void GetOTDetailWindow(string formState)
        {
            try
            {
                var otDetail = (OT)OTDataGrid.SelectedItem;
                var window = new OTDetailWindow(otDetail, "Approver", formState);
                window.ShowDialog();
                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ApproveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการยืนยันการส่งคำร้องขอโอทีหรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;
                var selected = (OT)OTDataGrid.SelectedItem;
                BusinessLayerServices.OTOnlineBL().UpdateOTOnline(selected, "Approver", "Approve", SingletonConfiguration.getInstance().Username);
                //    //Send Email
                //    List<OT> listOT = new List<OT>();
                //    listOT.Add(selected);
                //    OTOnlineService.EmailSender().SendApproveEmail(listOT, "Approver");
                MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void OTDataGrid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            DatagridSelectedItems();
        }

        private void OTDataGrid_KeyUp(object sender, KeyEventArgs e)
        {
            DatagridSelectedItems();
        }

        private void DatagridSelectedItems()
        {
            try
            {
                int? selectedCount = OTDataGrid.SelectedItems.Count;
                MultiApproveTextblock.Text = string.Format("อนุมัติที่เลือก ({0})", selectedCount);
                MultiApproveButton.IsEnabled = selectedCount > 0 ? true : false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void MultiApproveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var selected = OTDataGrid.SelectedItems;
                if (selected.Count <= 0) throw new Exception("ไม่พบข้อมูล OT ที่เลือก");
                if (MessageBox.Show("ต้องการยืนยันการส่งคำร้องขอโอที (" + selected.Count + ") รายการใช่หรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;

                foreach (var item in selected)
                {
                    var ot = (OT)item;
                    BusinessLayerServices.OTOnlineBL().UpdateOTOnline(ot, "Approver", "Approve", SingletonConfiguration.getInstance().Username);
                }

                //    //Send Email
                //    OTOnlineService.EmailSender().SendApproveEmail(listOTData, "Approver");
                MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
