﻿using DomainModelHris;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISOnline_WPF.Form.OTOnline
{
    /// <summary>
    /// Interaction logic for OTByDateWindow.xaml
    /// </summary>
    public partial class OTByDateWindow : Window
    {
        public OTByDateWindow()
        {
            InitializeComponent();
        }
        public OTByDateWindow(string employeeID, DateTime otDate)
        {
            InitializeComponent();
            InformationDataGrid.ItemsSource = BusinessLayerServices.OTBL().GetListOTByDate(employeeID, otDate);
        }
    }
}
