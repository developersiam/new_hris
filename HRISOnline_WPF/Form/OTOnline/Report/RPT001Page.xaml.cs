﻿using DomainModelHris;
using DomainModelPayroll;
using HRISOnline_WPF.Shared;
using HRISOnline_WPF.ViewModel;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HRISOnline_WPF.Form.OTOnline.Report
{
    /// <summary>
    /// Interaction logic for RPT001Page.xaml
    /// </summary>
    public partial class RPT001Page : Page
    {
        public RPT001Page()
        {
            InitializeComponent();
            BindPayrollLot();
            StatusCombobox.ItemsSource = GetStatus();
            DepartmentCombobox.ItemsSource = BusinessLayerServices.DeptCodeBL().GetAllDepartment();
            ReloadDataGrid();
        }

        private void BindPayrollLot()
        {
            var payrollLot = HRISOnlineService.DataCorrector().LotNumberList(BusinessLayerServices.PayrollBL().GetAllLot()).Take(24);
            LotCombobox.ItemsSource = payrollLot;
            LotCombobox.SelectedValue = "06/2018";
            //LotCombobox.SelectedValue = DateTime.Now.Month.ToString("D2") + "/" + DateTime.Now.Year;
        }

        private List<string> GetStatus()
        {
            var result = new List<string>();
            result.Add("All");
            result.Add("Reject");
            result.Add("Finished");
            result.Add("Not Finished");
            result.Add("Approve by Admin");
            result.Add("Approve by Approver");
            result.Add("Approve by Final Approver");
            return result;
        }

        private void LotCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var selectedLot = (Lot_Number)LotCombobox.SelectedItem;
                DateTime displayDateStart = selectedLot.Start_date.GetValueOrDefault();
                DateTime displayDateEnd = selectedLot.Finish_date.GetValueOrDefault();
                LotFromDateDatePicker.DisplayDateStart = displayDateStart;
                LotFromDateDatePicker.DisplayDateEnd = displayDateEnd;
                LotFromDateDatePicker.SelectedDate = displayDateStart;
                LotToDateDatePicker.DisplayDateStart = displayDateStart;
                LotToDateDatePicker.DisplayDateEnd = displayDateEnd;
                LotToDateDatePicker.SelectedDate = displayDateEnd;
                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDataGrid();
        }

        private void NameFilterTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            ReloadDataGrid();
        }

        private void DepartmentCombobox_DropDownClosed(object sender, EventArgs e)
        {
           if(DepartmentCombobox.Text != "") ReloadDataGrid();
        }

        private void StatusCombobox_DropDownClosed(object sender, EventArgs e)
        {
            if (StatusCombobox.Text != "") ReloadDataGrid();
        }

        private void ReloadDataGrid()
        {
            try
            {
                var fromDate = LotFromDateDatePicker.SelectedDate.GetValueOrDefault();
                var toDate = LotToDateDatePicker.SelectedDate.GetValueOrDefault();
                if (fromDate == null || toDate == null) throw new Exception("กรุณาเลือกวันที่ให้ถูกต้อง");
                if (fromDate > toDate) throw new Exception("กรุณาเลือกวันที่ให้ถูกต้อง");

                var status = StatusCombobox.Text;
                var listOTData = BusinessLayerServices.OTOnlineBL().GetOTOnlineList(fromDate, toDate, "Report", SingletonConfiguration.getInstance().Username);
                if (NameFilterTextBox.Text.Trim() != "") listOTData = listOTData.Where(w => w.Employee.Person.FirstNameTH.Contains(NameFilterTextBox.Text)).ToList();
                if (DepartmentCombobox.Text != "") listOTData = listOTData.Where(w => w.Employee.DeptCode == DepartmentCombobox.SelectedValue.ToString()).ToList();
                if (status == "Reject") listOTData = listOTData.Where(w => w.ManagerApproveStatus == "R" || w.FinalApproverStatus == "R" || w.HRApproveStatus == "R").ToList();
                else if (status == "Finished") listOTData = listOTData.Where(w => w.Status == true).ToList();
                else if (status == "Not Finished") listOTData = listOTData.Where(w => w.Status == false).ToList();
                else if (status == "Approve by Admin") listOTData = listOTData.Where(w => w.AdminApproveStatus == "A" &&
                                                                                          w.ManagerApproveStatus != "A" &&
                                                                                          w.FinalApproverStatus != "A" &&
                                                                                          w.HRApproveStatus != "A").ToList();
                else if (status == "Approve by Approver") listOTData = listOTData.Where(w => w.AdminApproveStatus == "A" &&
                                                                                             w.ManagerApproveStatus == "A" &&
                                                                                             w.FinalApproverStatus != "A" &&
                                                                                             w.HRApproveStatus != "A").ToList();
                else if (status == "Approve by Final Approver") listOTData = listOTData.Where(w => w.AdminApproveStatus == "A" &&
                                                                                                   w.ManagerApproveStatus == "A" &&
                                                                                                   w.FinalApproverStatus == "A" &&
                                                                                                   w.HRApproveStatus != "A").ToList();

                var listOTView = HRISOnlineService.DataCorrector().ConvertToOTViewmodel(listOTData);

                OTDataGrid.ItemsSource = null;
                OTDataGrid.ItemsSource = listOTView;
                TotalOTTextBox.Text = HRISOnlineService.DataCorrector().ConvertOTAmountFromListOT(listOTData);
                TotalDocumentTextBox.Text = listOTView.Count().ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            GetOTDetailWindow("View");
        }

        private void OTDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            GetOTDetailWindow("View");
        }

        private void GetOTDetailWindow(string formState)
        {
            try
            {
                var otDetail = (OTViewModel)OTDataGrid.SelectedItem;
                var window = new OTDetailWindow(otDetail.OTs, "Report", formState);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            NameFilterTextBox.Text = "";
            DepartmentCombobox.Text = "";
            StatusCombobox.Text = "";
            OTDataGrid.ItemsSource = null;
            TotalDocumentTextBox.Text = "0";
            TotalOTTextBox.Text = string.Format("{0} hours {1} mins", 0, 0);
        }
    }
}
