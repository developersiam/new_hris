﻿using DomainModelHris;
using HRISOnline_WPF.Shared;
using HRISSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HRISOnline_WPF.Form
{
    /// <summary>
    /// Interaction logic for SearchEmployee_Window.xaml
    /// </summary>
    public partial class SearchEmployee_Window : Window
    {
        public string _requestFrom;
        public DomainModelHris.Employee selectedEmployee;
        public SearchEmployee_Window()
        {
            InitializeComponent();
            SearchTypeCombobox.ItemsSource = BindSearchEmployeeTypeList();
            SearchTypeCombobox.SelectedIndex = 1;
            StatusCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffStatusList();
            StatusCombobox.SelectedValue = "1";
            StaffTypeCombobox.ItemsSource = BusinessLayerServices.EmployeeBL().GetAllStaffTypeList();
        }

        public List<string> BindSearchEmployeeTypeList()
        {
            List<string> result = new List<string>();
            result.Add("ค้นหาจากชื่อ (TH)");
            result.Add("ค้นหาจากชื่อ (EN)");
            result.Add("ค้นหาจากนามสกุล (TH)");
            result.Add("ค้นหาจากนามสกุล (EN)");
            result.Add("ค้นหาจากรหัสพนักงาน");
            result.Add("ค้นหาจากรหัสบัตรประชาชน");
            return result;
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.Key == Key.Enter && EmployeeInformationDataGrid.Items.Count == 1) DataSelected(); //After enter press data will Selectedd if remain item is = 1
            ReloadDatagrid();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                var searchStatus = (StaffStatu)StatusCombobox.SelectedItem;
                var searchStaff = (StaffType)StaffTypeCombobox.SelectedItem;

                string searchText = SearchTextBox.Text.Trim();
                string searchType = SearchTypeCombobox.Text;
                var indexStatus = searchStatus == null ? (int?)null : searchStatus.StaffStatusID;
                var indexStaff = searchStaff == null ? (int?)null : searchStaff.StaffTypeID;

                var employeeList = BusinessLayerServices.OTOnlineBL().GetOTOnlineEmployee(searchType, indexStaff, indexStatus, searchText, _requestFrom, SingletonConfiguration.getInstance().Username);

                EmployeeInformationDataGrid.ItemsSource = null;
                EmployeeInformationDataGrid.ItemsSource = employeeList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            DataSelected();
        }

        private void EmployeeInformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left) DataSelected();
        }

        private void DataSelected()
        {
            try
            {
                var employee = (DomainModelHris.Employee)EmployeeInformationDataGrid.SelectedItem;
                if (employee != null) selectedEmployee = employee; //Define data to public model for Passing value to mainpage.
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchTypeCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SearchTextBox.Focus();
        }

        private void StatusCombobox_DropDownClosed(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(StatusCombobox.Text)) ReloadDatagrid();
        }

        private void StaffTypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(StaffTypeCombobox.Text)) ReloadDatagrid();
        }
    }
}
