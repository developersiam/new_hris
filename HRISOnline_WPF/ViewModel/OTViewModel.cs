﻿using DomainModelHris;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISOnline_WPF.ViewModel
{
    public class OTViewModel
    {
        public OT OTs { get; set; }
        public bool _AdminStatus { get; set; }
        public bool _ApproverStatus { get; set; }
        public bool _FinalApproverStatus { get; set; }
        public bool _HrApproverStatus { get; set; }
        public bool _isReject { get; set; }
        public string _strReject { get; set; }
    }
}
