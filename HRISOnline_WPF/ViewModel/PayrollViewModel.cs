﻿using DomainModelPayroll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISOnline_WPF.ViewModel
{
    public class PayrollViewModel : Payroll
    {
        public double? otHourAmount { get; set; }
        public double? otHolidayAmount { get; set; }
        public double? otHourHolidayAmount { get; set; }
        public double? otSpecialAmount { get; set; }
    }
}
