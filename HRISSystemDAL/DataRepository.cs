﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelHris;
using DomainModelPayroll;
using DomainModelTimeStec;
using HRISSystemDAL.GenericRepository;

namespace HRISSystemDAL
{
    public interface ILeaveTypeRepository : IGenericDataRepository<LeaveType> { }
    public class LeaveTypeRepository : GenericDataRepository<LeaveType>, ILeaveTypeRepository { public LeaveTypeRepository(DbContext db) : base(db) { } }
    public interface ILeaveRepository : IGenericDataRepository<Leave> { }
    public class LeaveRepository : GenericDataRepository<Leave>, ILeaveRepository { public LeaveRepository(DbContext db) : base(db) { } }
    public interface IBranchRepository : IGenericDataRepository<Branch> { }
    public class BranchRepository : GenericDataRepository<Branch>, IBranchRepository { public BranchRepository(DbContext db) : base(db) { } }
    public interface ICropSetupRepository : IGenericDataRepository<CropSetup> { }
    public class CropSetupRepository : GenericDataRepository<CropSetup>, ICropSetupRepository { public CropSetupRepository(DbContext db) : base(db) { } }
    public interface IHolidayRepository : IGenericDataRepository<Holiday> { }
    public class HolidayRepository : GenericDataRepository<Holiday>, IHolidayRepository { public HolidayRepository(DbContext db) : base(db) { } }
    public interface IShiftRepository : IGenericDataRepository<Shift> { }
    public class ShiftRepository : GenericDataRepository<Shift>, IShiftRepository { public ShiftRepository(DbContext db) : base(db) { } }
    public interface ITitleNameRepository : IGenericDataRepository<TitleName> { }
    public class TitleNameRepository : GenericDataRepository<TitleName>, ITitleNameRepository { public TitleNameRepository(DbContext db) : base(db) { } }
    public interface IBloodRepository : IGenericDataRepository<Blood> { }
    public class BloodRepository : GenericDataRepository<Blood>, IBloodRepository { public BloodRepository(DbContext db) : base(db) { } }
    public interface IGenderRepository : IGenericDataRepository<Gender> { }
    public class GenderRepository : GenericDataRepository<Gender>, IGenderRepository { public GenderRepository(DbContext db) : base(db) { } }
    public interface IMaritalStatuRepository : IGenericDataRepository<MaritalStatu> { }
    public class MaritalStatuRepository : GenericDataRepository<MaritalStatu>, IMaritalStatuRepository { public MaritalStatuRepository(DbContext db) : base(db) { } }
    public interface IReligionRepository : IGenericDataRepository<Religion> { }
    public class ReligionRepository : GenericDataRepository<Religion>, IReligionRepository { public ReligionRepository(DbContext db) : base(db) { } }
    public interface INationalityRepository : IGenericDataRepository<Nationality> { }
    public class NationalityRepository : GenericDataRepository<Nationality>, INationalityRepository { public NationalityRepository(DbContext db) : base(db) { } }
    public interface IRaceRepository : IGenericDataRepository<Race> { }
    public class RaceRepository : GenericDataRepository<Race>, IRaceRepository { public RaceRepository(DbContext db) : base(db) { } }
    public interface IPersonRepository : IGenericDataRepository<Person> { }
    public class PersonRepository : GenericDataRepository<Person>, IPersonRepository { public PersonRepository(DbContext db) : base(db) { } }
    public interface IIDCardInfoRepository : IGenericDataRepository<IDCardInfo> { }
    public class IDCardInfoRepository : GenericDataRepository<IDCardInfo>, IIDCardInfoRepository { public IDCardInfoRepository(DbContext db) : base(db) { } }
    public interface IProvinceRepository : IGenericDataRepository<Province> { }
    public class ProvinceRepository : GenericDataRepository<Province>, IProvinceRepository { public ProvinceRepository(DbContext db) : base(db) { } }
    public interface IDistrictRepository : IGenericDataRepository<District> { }
    public class DistrictRepository : GenericDataRepository<District>, IDistrictRepository { public DistrictRepository(DbContext db) : base(db) { } }
    public interface ISubDistrictRepository : IGenericDataRepository<SubDistrict> { }
    public class SubDistrictRepository : GenericDataRepository<SubDistrict>, ISubDistrictRepository { public SubDistrictRepository(DbContext db) : base(db) { } }
    public interface IAddressTypeRepository : IGenericDataRepository<AddressType> { }
    public class AddressTypeRepository : GenericDataRepository<AddressType>, IAddressTypeRepository { public AddressTypeRepository(DbContext db) : base(db) { } }
    public interface IAddressRepository : IGenericDataRepository<Address> { }
    public class AddressRepository : GenericDataRepository<Address>, IAddressRepository { public AddressRepository(DbContext db) : base(db) { } }
    public interface ICountryRepository : IGenericDataRepository<Country> { }
    public class CountryRepository : GenericDataRepository<Country>, ICountryRepository { public CountryRepository(DbContext db) : base(db) { } }
    public interface IEmployeeRepository : IGenericDataRepository<DomainModelHris.Employee> { }
    public class EmployeeRepository : GenericDataRepository<DomainModelHris.Employee>, IEmployeeRepository { public EmployeeRepository(DbContext db) : base(db) { } }
    public interface IDepartmentRepository : IGenericDataRepository<Department> { }
    public class DepartmentRepository : GenericDataRepository<Department>, IDepartmentRepository { public DepartmentRepository(DbContext db) : base(db) { } }
    public interface IStaffStatuRepository : IGenericDataRepository<StaffStatu> { }
    public class StaffStatuRepository : GenericDataRepository<StaffStatu>, IStaffStatuRepository { public StaffStatuRepository(DbContext db) : base(db) { } }
    public interface IStaffTypeRepository : IGenericDataRepository<StaffType> { }
    public class StaffTypeRepository : GenericDataRepository<StaffType>, IStaffTypeRepository { public StaffTypeRepository(DbContext db) : base(db) { } }
    public interface IContactPersonInfoRepository : IGenericDataRepository<ContactPersonInfo> { }
    public class ContactPersonInfoRepository : GenericDataRepository<ContactPersonInfo>, IContactPersonInfoRepository { public ContactPersonInfoRepository(DbContext db) : base(db) { } }
    public interface IFamilyMemberTypeRepository : IGenericDataRepository<FamilyMemberType> { }
    public class FamilyMemberTypeRepository : GenericDataRepository<FamilyMemberType>, IFamilyMemberTypeRepository { public FamilyMemberTypeRepository(DbContext db) : base(db) { } }
    public interface IFamilyMemberRepository : IGenericDataRepository<FamilyMember> { }
    public class FamilyMemberRepository : GenericDataRepository<FamilyMember>, IFamilyMemberRepository { public FamilyMemberRepository(DbContext db) : base(db) { } }
    public interface IVitalStauRepository : IGenericDataRepository<VitalStau> { }
    public class VitalStauRepository : GenericDataRepository<VitalStau>, IVitalStauRepository { public VitalStauRepository(DbContext db) : base(db) { } }
    public interface IOriginalWorkHistoryRepository : IGenericDataRepository<OriginalWorkHistory> { }
    public class OriginalWorkHistoryRepository : GenericDataRepository<OriginalWorkHistory>, IOriginalWorkHistoryRepository { public OriginalWorkHistoryRepository(DbContext db) : base(db) { } }
    public interface IEducationRepository : IGenericDataRepository<Education> { }
    public class EducationRepository : GenericDataRepository<Education>, IEducationRepository { public EducationRepository(DbContext db) : base(db) { } }
    public interface IEducationLevelRepository : IGenericDataRepository<EducationLevel> { }
    public class EducationLevelRepository : GenericDataRepository<EducationLevel>, IEducationLevelRepository { public EducationLevelRepository(DbContext db) : base(db) { } }
    public interface IInstituteRepository : IGenericDataRepository<Institute> { }
    public class InstituteRepository : GenericDataRepository<Institute>, IInstituteRepository { public InstituteRepository(DbContext db) : base(db) { } }
    public interface IEducationQualificationRepository : IGenericDataRepository<EducationQualification> { }
    public class EducationQualificationRepository : GenericDataRepository<EducationQualification>, IEducationQualificationRepository { public EducationQualificationRepository(DbContext db) : base(db) { } }
    public interface IMajorRepository : IGenericDataRepository<Major> { }
    public class MajorRepository : GenericDataRepository<Major>, IMajorRepository { public MajorRepository(DbContext db) : base(db) { } }
    public interface IPositionRepository : IGenericDataRepository<Position> { }
    public class PositionRepository : GenericDataRepository<Position>, IPositionRepository { public PositionRepository(DbContext db) : base(db) { } }
    public interface IEmployeeContractRepository : IGenericDataRepository<EmployeeContract> { }
    public class EmployeeContractRepository : GenericDataRepository<EmployeeContract>, IEmployeeContractRepository { public EmployeeContractRepository(DbContext db) : base(db) { } }
    public interface IContractTypeRepository : IGenericDataRepository<ContractType> { }
    public class ContractTypeRepository : GenericDataRepository<ContractType>, IContractTypeRepository { public ContractTypeRepository(DbContext db) : base(db) { } }
    public interface IShiftControlRepository : IGenericDataRepository<ShiftControl> { }
    public class ShiftControlRepository : GenericDataRepository<ShiftControl>, IShiftControlRepository { public ShiftControlRepository(DbContext db) : base(db) { } }
    public interface ITrainingRepository : IGenericDataRepository<Training> { }
    public class TrainingRepository : GenericDataRepository<Training>, ITrainingRepository { public TrainingRepository(DbContext db) : base(db) { } }
    public interface ITrainingCourseRepository : IGenericDataRepository<TrainingCourse> { }
    public class TrainingCourseRepository : GenericDataRepository<TrainingCourse>, ITrainingCourseRepository { public TrainingCourseRepository(DbContext db) : base(db) { } }
    public interface IPayrollFlagRepository : IGenericDataRepository<PayrollFlag> { }
    public class PayrollFlagRepository : GenericDataRepository<PayrollFlag>, IPayrollFlagRepository { public PayrollFlagRepository(DbContext db) : base(db) { } }
    public interface ILot_NumberRepository : IGenericDataRepository<Lot_Number> { }
    public class Lot_NumberRepository : GenericDataRepository<Lot_Number>, ILot_NumberRepository { public Lot_NumberRepository(DbContext db) : base(db) { } }
    public interface IManualWorkDateRepository : IGenericDataRepository<ManualWorkDate> { }
    public class ManualWorkDateRepository : GenericDataRepository<ManualWorkDate>, IManualWorkDateRepository { public ManualWorkDateRepository(DbContext db) : base(db) {} }
    public interface IManualWorkDateTypeRepository : IGenericDataRepository<ManualWorkDateType> { }
    public class ManualWorkDateTypeRepository : GenericDataRepository<ManualWorkDateType>, IManualWorkDateTypeRepository { public ManualWorkDateTypeRepository(DbContext db) : base(db) { } }
    public interface IPayrollRepository : IGenericDataRepository<Payroll> { }
    public class PayrollRepository : GenericDataRepository<Payroll>, IPayrollRepository { public PayrollRepository(DbContext db) : base(db) { } }
    //public interface IPayrollEmployeeRepository : IGenericDataRepository<PayrollEmployee> { }
    //public class PayrollEmployeeRepository : GenericDataRepository<PayrollEmployee>, IPayrollEmployeeRepository { public PayrollEmployeeRepository(DbContext db) : base(db) { } }
    public interface IOTRepository : IGenericDataRepository<OT> { }
    public class OTRepository : GenericDataRepository<OT>, IOTRepository { public OTRepository(DbContext db) : base(db) { } }
    public interface IOTTypeRepository : IGenericDataRepository<OTType> { }
    public class OTTypeRepository : GenericDataRepository<OTType>, IOTTypeRepository { public OTTypeRepository(DbContext db) : base(db) { } }
    public interface IOTRequestTypeRepository : IGenericDataRepository<OTRequestType> { }
    public class OTRequestTypeRepository : GenericDataRepository<OTRequestType>, IOTRequestTypeRepository { public OTRequestTypeRepository(DbContext db) : base(db) { } }
    public interface IOTOnlineSetupAdminRepository : IGenericDataRepository<OTOnlineSetupAdmin> { }
    public class OTOnlineSetupAdminRepository : GenericDataRepository<OTOnlineSetupAdmin>, IOTOnlineSetupAdminRepository { public OTOnlineSetupAdminRepository(DbContext db) : base(db) { } }
    public interface IOTOnlineSetupAdminDetailRepository : IGenericDataRepository<OTOnlineSetupAdminDetail> { }
    public class OTOnlineSetupAdminDetailRepository : GenericDataRepository<OTOnlineSetupAdminDetail>, IOTOnlineSetupAdminDetailRepository { public OTOnlineSetupAdminDetailRepository(DbContext db) : base(db) { } }
    public interface IOTOnlineSetupApproverRepository : IGenericDataRepository<OTOnlineSetupApprover> { }
    public class OTOnlineSetupApproverRepository : GenericDataRepository<OTOnlineSetupApprover>, IOTOnlineSetupApproverRepository { public OTOnlineSetupApproverRepository(DbContext db) : base(db) { } }
    public interface IOTOnlineSetupApproverDetailRepository : IGenericDataRepository<OTOnlineSetupApproverDetail> { }
    public class OTOnlineSetupApproverDetailRepository : GenericDataRepository<OTOnlineSetupApproverDetail>, IOTOnlineSetupApproverDetailRepository { public OTOnlineSetupApproverDetailRepository(DbContext db) : base(db) { } }
    public interface IOTOnlineSetupFinalApproverRepository : IGenericDataRepository<OTOnlineSetupFinalApprover> { }
    public class OTOnlineSetupFinalApproverRepository : GenericDataRepository<OTOnlineSetupFinalApprover>, IOTOnlineSetupFinalApproverRepository { public OTOnlineSetupFinalApproverRepository(DbContext db) : base(db) { } }
    public interface IOTOnlineSetupFinalApproverDetailRepository : IGenericDataRepository<OTOnlineSetupFinalApproverDetail> { }
    public class OTOnlineSetupFinalApproverDetailRepository : GenericDataRepository<OTOnlineSetupFinalApproverDetail>, IOTOnlineSetupFinalApproverDetailRepository { public OTOnlineSetupFinalApproverDetailRepository(DbContext db) : base(db) { } }
    public interface IOTOnlineSetupHRApproverRepository : IGenericDataRepository<OTOnlineSetupHRApprover> { }
    public class OTOnlineSetupHRApproverRepository : GenericDataRepository<OTOnlineSetupHRApprover>, IOTOnlineSetupHRApproverRepository { public OTOnlineSetupHRApproverRepository(DbContext db) : base(db) { } }
    public interface ICheckinoutRepository : IGenericDataRepository<checkinout> { }
    public class CheckinoutRepository : GenericDataRepository<checkinout>, ICheckinoutRepository { public CheckinoutRepository(DbContext db) : base(db) { } }
    public interface IUserinfoRepository : IGenericDataRepository<userinfo> { }
    public class UserinfoRepository : GenericDataRepository<userinfo>, IUserinfoRepository { public UserinfoRepository(DbContext db) : base(db) { } }
    public interface IDepartmentTimeStecRepository: IGenericDataRepository<department> { }
    public class DepartmentTimeStecRepository : GenericDataRepository<department>, IDepartmentTimeStecRepository { public DepartmentTimeStecRepository(DbContext db) : base(db) { } }
    public interface IWorkUpCountryRepository : IGenericDataRepository<WorkUpCountry> { }
    public class WorkUpCountryRepository : GenericDataRepository<WorkUpCountry>, IWorkUpCountryRepository { public WorkUpCountryRepository(DbContext db) : base(db) { } }
    public interface IOTDetailRepository : IGenericDataRepository<OT_Detail> { }
    public class OTDetailRepository : GenericDataRepository<OT_Detail>, IOTDetailRepository { public OTDetailRepository(DbContext db) : base(db) { } }
    public interface ILabourWorkRepository : IGenericDataRepository<Labour_work> { }
    public class LabourWorkRepository : GenericDataRepository<Labour_work>, ILabourWorkRepository { public LabourWorkRepository(DbContext db) : base(db) { } }
    public interface ITransactionLogRepository : IGenericDataRepository<TransactionLog> { }
    public class TransactionLogRepository : GenericDataRepository<TransactionLog>, ITransactionLogRepository { public TransactionLogRepository(DbContext db) : base(db) { } }
    public interface IPayrollEmployeeRepository : IGenericDataRepository<DomainModelPayroll.Employee> { }
    public class PayrollEmployeeRepository : GenericDataRepository<DomainModelPayroll.Employee>, IPayrollEmployeeRepository { public PayrollEmployeeRepository(DbContext db) : base(db) { } }
    public interface IFinalApprover_TypeRepository : IGenericDataRepository<FinalApprover_Type> { }
    public class FinalApprover_TypeRepository : GenericDataRepository<FinalApprover_Type>, IFinalApprover_TypeRepository { public FinalApprover_TypeRepository(DbContext db) : base(db) { } }
    public interface ITemporaryWorkerWageRepository : IGenericDataRepository<TemporaryWorkerWage> { }
    public class TemporaryWorkerWageRepository : GenericDataRepository<TemporaryWorkerWage>, ITemporaryWorkerWageRepository { public TemporaryWorkerWageRepository(DbContext db) : base(db) { } }
    public interface ILeaveSetupRepository : IGenericDataRepository<LeaveSetup> { }
    public class LeaveSetupRepository : GenericDataRepository<LeaveSetup>, ILeaveSetupRepository { public LeaveSetupRepository(DbContext db) : base(db) { } }
    public interface IWorkUpCountryNotedRepository : IGenericDataRepository<WorkUpCountryNoted> { }
    public class WorkUpCountryNotedRepository : GenericDataRepository<WorkUpCountryNoted>, IWorkUpCountryNotedRepository { public WorkUpCountryNotedRepository(DbContext db) : base(db) { } }
    public interface ISupport_DetailRepository : IGenericDataRepository<Support_Detail> { }
    public class Support_DetailRepository : GenericDataRepository<Support_Detail>, ISupport_DetailRepository { public Support_DetailRepository(DbContext db) : base(db) { } }
    public interface IPayroll_SummaryRepository : IGenericDataRepository<Payroll_Summary> { }
    public class Payroll_SummaryRepository : GenericDataRepository<Payroll_Summary>, IPayroll_SummaryRepository { public Payroll_SummaryRepository(DbContext db) : base(db) { } }
    //public interface IRepository: IGenericDataRepository<AA> { }
    //public class Repository : GenericDataRepository<AA>, IRepository {  public Repository(DbContext db) : base(db) {} }
    //public interface IRepository: IGenericDataRepository<AA> { }
    //public class Repository : GenericDataRepository<AA>, IRepository {  public Repository(DbContext db) : base(db) {} }
    //public interface IRepository: IGenericDataRepository<AA> { }
    //public class Repository : GenericDataRepository<AA>, IRepository {  public Repository(DbContext db) : base(db) {} }
    //public interface IRepository: IGenericDataRepository<AA> { }
    //public class Repository : GenericDataRepository<AA>, IRepository {  public Repository(DbContext db) : base(db) {} }
    //public interface IRepository: IGenericDataRepository<AA> { }
    //public class Repository : GenericDataRepository<AA>, IRepository {  public Repository(DbContext db) : base(db) {} }
    //public interface IRepository: IGenericDataRepository<AA> { }
    //public class Repository : GenericDataRepository<AA>, IRepository {  public Repository(DbContext db) : base(db) {} }
    //public interface IRepository: IGenericDataRepository<AA> { }
    //public class Repository : GenericDataRepository<AA>, IRepository {  public Repository(DbContext db) : base(db) {} }

}
