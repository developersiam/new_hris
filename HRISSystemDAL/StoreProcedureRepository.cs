﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelHris;
using DomainModelPayroll;
using HRISSystemDAL.EDMX;

namespace HRISSystemDAL
{
    public interface IStoreProcedureRepository
    {
        List<sp_OT_GetEmployeeInformation_Result> sp_GetEmployeeInformation();
        List<sp_GetWorkingDateToPayroll_Result> sp_GetWorkingDateToPayroll(DateTime fromDate, DateTime toDate);
        List<sp_GetLabourWorkAndOTFromSTEC_payroll_Result> sp_GetLabourWorkAndOTFromSTEC_payroll(string lotMonth, string lotYear);
        List<sp_GetPayRollForPrintSlip_Result> sp_GetPayRollForPrintSlip(string payrollLot);
        List<sp_GetEmployeeNOTScanInAndOutByDateRange_Result> sp_GetEmployeeNOTScanInAndOutByDateRange(DateTime fromDate, DateTime toDate);
        List<sp_GetEmployeeTimeAttendanceByDateAndEmployeeID_Result> sp_GetEmployeeTimeAttendanceByDateAndEmployeeID(DateTime fromDate, DateTime toDate, int? userID);
        List<sp_GetEmployeeTimeAttendanceByDateAndEmployee1_Result> sp_GetEmployeeTimeAttendanceByDateAndEmployee1(DateTime fromDate, DateTime toDate, int? userID);
        List<sp_GetEmployeeTimeAttendanceByDateAndEmployee2_Result> sp_GetEmployeeTimeAttendanceByDateAndEmployee2(DateTime fromDate, DateTime toDate, string employeeID);
        List<sp_GetToPayrollByDepartment_Result> sp_GetToPayrollByDepartment(string lotMonth, string lotYear, string deptCode);
        List<sp_GetPayrollSummaryReport_Result> sp_GetPayrollSummaryReport(string payrollLot);
        List<sp_GetPayrollEmployee_Result> sp_GetPayrollEmployee();
        List<sp_NEWGet_Result> sp_Newget(string lotMonth, string lotYear, string deptCode);
        List<sp_GetSSOReport_Result> sp_GetSSOReport(string lotNumber);
        List<sp_GetTimeAttendance_Result> sp_GetTimeAttendance(DateTime fromDate, DateTime toDate);
        List<sp_GetTimeAttendanceByDept_Result> sp_GetTimeAttendanceByDept(DateTime fromDate, DateTime toDate, string deptCode);
        List<sp_GetTemporaryWorkerWage_Result> sp_GetTemporaryWorkerWage(DateTime fromDate, DateTime toDate, string deptCode);
        List<sp_GetActualWorkingDate_Result> sp_GetActualWorkingDate(DateTime fromDate, DateTime toDate);
        List<sp_GetCurrent_Lot_Result> sp_GetCurrent_Lot();
        List<sp_GetSummarySupportByDateAndEmployee_Result> sp_GetSummarySupportByDate(DateTime fromDate, DateTime toDate);
        List<sp_GetSupportByDateAndEmployee_Result> sp_GetSupportByDateAndEmployee(DateTime fromDate, DateTime toDate, int fingerID);
        List<sp_GetNotify_Result> sp_GetNotify(DateTime fromDate, DateTime toDate);
    }
    public class StoreProcedureRepository : IStoreProcedureRepository
    {
        public List<sp_OT_GetEmployeeInformation_Result> sp_GetEmployeeInformation()
        {
            try
            {
                using (HRISSystemEntities _context = new HRISSystemEntities())
                {
                    return _context.sp_OT_GetEmployeeInformation().ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<sp_GetWorkingDateToPayroll_Result> sp_GetWorkingDateToPayroll(DateTime fromDate, DateTime toDate)
        {
            try
            {
                using (STEC_PayrollEntities _context = new STEC_PayrollEntities())
                {
                    return _context.sp_GetWorkingDateToPayroll(fromDate, toDate).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<sp_GetLabourWorkAndOTFromSTEC_payroll_Result> sp_GetLabourWorkAndOTFromSTEC_payroll(string lotMonth, string lotYear)
        {
            try
            {
                using (HRISSystemEntities _context = new HRISSystemEntities())
                {
                    return _context.sp_GetLabourWorkAndOTFromSTEC_payroll(lotYear, lotMonth).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<sp_GetPayRollForPrintSlip_Result> sp_GetPayRollForPrintSlip(string payrollLot)
        {
            try
            {
                using (HRISSystemEntities _context = new HRISSystemEntities())
                {
                    return _context.sp_GetPayRollForPrintSlip(payrollLot).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<sp_GetEmployeeNOTScanInAndOutByDateRange_Result> sp_GetEmployeeNOTScanInAndOutByDateRange(DateTime fromDate, DateTime toDate)
        {
            try
            {
                using (HRISSystemEntities _context = new HRISSystemEntities())
                {
                    return _context.sp_GetEmployeeNOTScanInAndOutByDateRange(fromDate, toDate).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<sp_GetEmployeeTimeAttendanceByDateAndEmployeeID_Result> sp_GetEmployeeTimeAttendanceByDateAndEmployeeID(DateTime fromDate, DateTime toDate, int? userID)
        {
            try
            {
                using (HRISSystemEntities _context = new HRISSystemEntities())
                {
                    return _context.sp_GetEmployeeTimeAttendanceByDateAndEmployeeID(fromDate, toDate, userID).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<sp_GetEmployeeTimeAttendanceByDateAndEmployee1_Result> sp_GetEmployeeTimeAttendanceByDateAndEmployee1(DateTime fromDate, DateTime toDate, int? userID)
        {
            try
            {
                using (HRISSystemEntities _context = new HRISSystemEntities())
                {
                    return _context.sp_GetEmployeeTimeAttendanceByDateAndEmployee1(fromDate, toDate, userID).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<sp_GetEmployeeTimeAttendanceByDateAndEmployee2_Result> sp_GetEmployeeTimeAttendanceByDateAndEmployee2(DateTime fromDate, DateTime toDate, string employeeID)
        {
            try
            {
                using (HRISSystemEntities _context = new HRISSystemEntities())
                {
                    return _context.sp_GetEmployeeTimeAttendanceByDateAndEmployee2(fromDate, toDate, employeeID).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<sp_GetToPayrollByDepartment_Result> sp_GetToPayrollByDepartment(string lotMonth, string lotYear, string deptCode)
        {
            try
            {
                using (STEC_PayrollEntities _context = new STEC_PayrollEntities())
                {
                    return _context.sp_GetToPayrollByDepartment(lotMonth, lotYear, deptCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<sp_GetPayrollSummaryReport_Result> sp_GetPayrollSummaryReport(string payrollLot)
        {
            try
            {
                using (STEC_PayrollEntities _context = new STEC_PayrollEntities())
                {
                    return _context.sp_GetPayrollSummaryReport(payrollLot).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<sp_GetPayrollEmployee_Result> sp_GetPayrollEmployee()
        {
            using (STEC_PayrollEntities _context = new STEC_PayrollEntities())
            {
                return _context.sp_GetPayrollEmployee().ToList();
            }
        }

        public List<sp_NEWGet_Result> sp_Newget(string lotMonth, string lotYear, string deptCode)
        {
            using (STEC_PayrollEntities _context = new STEC_PayrollEntities())
            {
                return _context.sp_NEWGet(lotMonth, lotYear, deptCode).ToList();
            }
        }

        public List<sp_GetSSOReport_Result> sp_GetSSOReport(string lotNumber)
        {
            using (STEC_PayrollEntities _context = new STEC_PayrollEntities())
            {
                return _context.sp_GetSSOReport(lotNumber).ToList();
            }
        }

        public List<sp_GetTimeAttendanceByDept_Result> sp_GetTimeAttendanceByDept(DateTime fromDate, DateTime toDate, string deptCode)
        {
            using (HRISSystemEntities _context = new HRISSystemEntities())
            {
                return _context.sp_GetTimeAttendanceByDept(fromDate, toDate, deptCode).ToList();
            }
        }

        public List<sp_GetTimeAttendance_Result> sp_GetTimeAttendance(DateTime fromDate, DateTime toDate)
        {
            using (HRISSystemEntities _context = new HRISSystemEntities())
            {
                return _context.sp_GetTimeAttendance(fromDate, toDate).ToList();
            }
        }
        public List<sp_GetTemporaryWorkerWage_Result> sp_GetTemporaryWorkerWage(DateTime fromDate, DateTime toDate, string deptCode)
        {
            using (STEC_PayrollEntities _context = new STEC_PayrollEntities())
            {
                return _context.sp_GetTemporaryWorkerWage(fromDate, toDate, deptCode).ToList();
            }
        }
        public List<sp_GetActualWorkingDate_Result> sp_GetActualWorkingDate(DateTime fromDate, DateTime toDate)
        {
            using (STEC_PayrollEntities _context = new STEC_PayrollEntities())
            {
                return _context.sp_GetActualWorkingDate(fromDate, toDate).ToList();
            }
        }

        public List<sp_GetCurrent_Lot_Result> sp_GetCurrent_Lot()
        {
            using (STEC_PayrollEntities _context = new STEC_PayrollEntities())
            {
                return _context.sp_GetCurrent_Lot().ToList();
            }
        }

        public List<sp_GetSummarySupportByDateAndEmployee_Result> sp_GetSummarySupportByDate(DateTime fromDate, DateTime toDate)
        {
            using (HRISSystemEntities _context = new HRISSystemEntities())
            {
                return _context.sp_GetSummarySupportByDateAndEmployee(fromDate, toDate).ToList();
            }
        }

        public List<sp_GetSupportByDateAndEmployee_Result> sp_GetSupportByDateAndEmployee(DateTime fromDate, DateTime toDate, int fingerID)
        {
            using (HRISSystemEntities _context = new HRISSystemEntities())
            {
                return _context.sp_GetSupportByDateAndEmployee(fromDate, toDate, fingerID).ToList();
            }
        }

        public List<sp_GetNotify_Result> sp_GetNotify(DateTime fromDate, DateTime toDate)
        {
            using (HRISSystemEntities _context = new HRISSystemEntities())
            {
                return _context.sp_GetNotify(fromDate, toDate).ToList();
            }
        }
    }
}
