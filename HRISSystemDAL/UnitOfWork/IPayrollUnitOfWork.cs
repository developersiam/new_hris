﻿using DomainModelPayroll;
using HRISSystemDAL.GenericRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystemDAL.UnitOfWork
{
    public interface IPayrollUnitOfWork
    {
        GenericRepository.IGenericDataRepository<Lot_Number> LotNumberRepo { get; }
        void Save();
    }
}
