﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using DomainModelHris;
using HRISSystemDAL.EDMX;
using HRISSystemDAL.GenericRepository;

namespace HRISSystemDAL.UnitOfWork
{
    public class HRISUnitOfWork : IHRISUnitOfWork, System.IDisposable
    {
        private readonly HRISSystemEntities _context;
        private HRISSystemDAL.GenericRepository.IGenericDataRepository<Department> _departmentRepo;
        private HRISSystemDAL.GenericRepository.IGenericDataRepository<Employee> _employeeRepo;
        private HRISSystemDAL.GenericRepository.IGenericDataRepository<Shift> _shiftRepo;
        private HRISSystemDAL.GenericRepository.IGenericDataRepository<ShiftControl> _shiftControlRepo;
        private HRISSystemDAL.GenericRepository.IGenericDataRepository<OT> _otRepo;
        private HRISSystemDAL.GenericRepository.IGenericDataRepository<OTType> _otTypeRepo;
        private HRISSystemDAL.GenericRepository.IGenericDataRepository<OTRequestType> _otRequestTypeRepo;

        public HRISUnitOfWork()
        {
            _context = new HRISSystemEntities();
        }

        public HRISSystemDAL.GenericRepository.IGenericDataRepository<Shift> ShiftRepo
        {
            get
            {
                return _shiftRepo ?? (_shiftRepo = new HRISSystemDAL.GenericRepository.GenericDataRepository<Shift>(_context));
            }
        }

        public HRISSystemDAL.GenericRepository.IGenericDataRepository<ShiftControl> ShiftControlRepo
        {
            get
            {
                return _shiftControlRepo ?? (_shiftControlRepo = new HRISSystemDAL.GenericRepository.GenericDataRepository<ShiftControl>(_context));
            }
        }

        public GenericRepository.IGenericDataRepository<Department> DepartmentRepo
        {
            get
            {
                return _departmentRepo ?? (_departmentRepo = new HRISSystemDAL.GenericRepository.GenericDataRepository<Department>(_context));
            }
        }

        public GenericRepository.IGenericDataRepository<Employee> EmployeeRepo
        {
            get
            {
                return _employeeRepo ?? (_employeeRepo = new HRISSystemDAL.GenericRepository.GenericDataRepository<Employee>(_context));
            }
        }

        public GenericRepository.IGenericDataRepository<OT> OTRepo
        {
            get
            {
                return _otRepo ?? (_otRepo = new HRISSystemDAL.GenericRepository.GenericDataRepository<OT>(_context));
            }
        }

        public GenericRepository.IGenericDataRepository<OTRequestType> OTRequestTypeRepo
        {
            get
            {
                return _otRequestTypeRepo ?? (_otRequestTypeRepo = new HRISSystemDAL.GenericRepository.GenericDataRepository<OTRequestType>(_context));
            }
        }

        public GenericRepository.IGenericDataRepository<OTType> OTTypeRepo
        {
            get
            {
                return _otTypeRepo ?? (_otTypeRepo = new HRISSystemDAL.GenericRepository.GenericDataRepository<OTType>(_context));
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }
    }
}