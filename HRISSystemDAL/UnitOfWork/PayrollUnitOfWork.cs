﻿using DomainModelPayroll;
using HRISSystemDAL.EDMX;
using HRISSystemDAL.GenericRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystemDAL.UnitOfWork
{
    public class PayrollUnitOfWork : IPayrollUnitOfWork, System.IDisposable
    {
        private readonly STEC_PayrollEntities _context;
        private GenericRepository.IGenericDataRepository<Lot_Number> _lotNumberRepo;

        public PayrollUnitOfWork()
        {
            _context = new STEC_PayrollEntities();
        }

        public GenericRepository.IGenericDataRepository<Lot_Number> LotNumberRepo
        {
            get
            {
                return _lotNumberRepo ?? (_lotNumberRepo = new GenericRepository.GenericDataRepository<Lot_Number>(_context));
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }
    }
}
