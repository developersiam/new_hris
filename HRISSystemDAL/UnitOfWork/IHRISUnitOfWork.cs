﻿using DomainModelHris;
using HRISSystemDAL.GenericRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystemDAL.UnitOfWork
{
    public interface IHRISUnitOfWork
    {
        HRISSystemDAL.GenericRepository.IGenericDataRepository<Department> DepartmentRepo { get; }
        HRISSystemDAL.GenericRepository.IGenericDataRepository<Employee> EmployeeRepo { get; }
        HRISSystemDAL.GenericRepository.IGenericDataRepository<Shift> ShiftRepo { get; }
        HRISSystemDAL.GenericRepository.IGenericDataRepository<ShiftControl> ShiftControlRepo { get; }
        HRISSystemDAL.GenericRepository.IGenericDataRepository<OT> OTRepo { get; }
        HRISSystemDAL.GenericRepository.IGenericDataRepository<OTType> OTTypeRepo { get; }
        HRISSystemDAL.GenericRepository.IGenericDataRepository<OTRequestType> OTRequestTypeRepo { get; }
        void Save();
    }
}
