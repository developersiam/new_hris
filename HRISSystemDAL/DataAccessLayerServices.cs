﻿using HRISSystemDAL.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRISSystemDAL
{
    public static class DataAccessLayerServices
    {
        public static IStoreProcedureRepository StoreProcedureRepository()
        {
            IStoreProcedureRepository obj = new StoreProcedureRepository();
            return obj;
        }
        public static ILeaveTypeRepository LeaveTypeRepository()
        {
            ILeaveTypeRepository obj = new LeaveTypeRepository(new HRISSystemEntities());
            return obj;
        }
        public static ILeaveRepository LeaveRepository()
        {
            ILeaveRepository obj = new LeaveRepository(new HRISSystemEntities());
            return obj;
        }
        public static IBranchRepository BranchRepository()
        {
            IBranchRepository obj = new BranchRepository(new HRISSystemEntities());
            return obj;
        }
        public static ICropSetupRepository CropSetupRepository()
        {
            ICropSetupRepository obj = new CropSetupRepository(new HRISSystemEntities());
            return obj;
        }
        public static IHolidayRepository HolidayRepository()
        {
            IHolidayRepository obj = new HolidayRepository(new HRISSystemEntities());
            return obj;
        }
        public static IShiftRepository ShiftRepository()
        {
            IShiftRepository obj = new ShiftRepository(new HRISSystemEntities());
            return obj;
        }
        public static ITitleNameRepository TitleNameRepository()
        {
            ITitleNameRepository obj = new TitleNameRepository(new HRISSystemEntities());
            return obj;
        }
        public static IBloodRepository BloodRepository()
        {
            IBloodRepository obj = new BloodRepository(new HRISSystemEntities());
            return obj;
        }
        public static IGenderRepository GenderRepository()
        {
            IGenderRepository obj = new GenderRepository(new HRISSystemEntities());
            return obj;
        }
        public static IMaritalStatuRepository MaritalStatuRepository()
        {
            IMaritalStatuRepository obj = new MaritalStatuRepository(new HRISSystemEntities());
            return obj;
        }
        public static IReligionRepository ReligionRepository()
        {
            IReligionRepository obj = new ReligionRepository(new HRISSystemEntities());
            return obj;
        }
        public static INationalityRepository NationalityRepository()
        {
            INationalityRepository obj = new NationalityRepository(new HRISSystemEntities());
            return obj;
        }
        public static IRaceRepository RaceRepository()
        {
            IRaceRepository obj = new RaceRepository(new HRISSystemEntities());
            return obj;
        }
        public static IPersonRepository PersonRepository()
        {
            IPersonRepository obj = new PersonRepository(new HRISSystemEntities());
            return obj;
        }
        public static IIDCardInfoRepository IDCardInfoRepository()
        {
            IIDCardInfoRepository obj = new IDCardInfoRepository(new HRISSystemEntities());
            return obj;
        }
        public static IProvinceRepository ProvinceRepository()
        {
            IProvinceRepository obj = new ProvinceRepository(new HRISSystemEntities());
            return obj;
        }
        public static IDistrictRepository DistrictRepository()
        {
            IDistrictRepository obj = new DistrictRepository(new HRISSystemEntities());
            return obj;
        }
        public static ISubDistrictRepository SubDistrictRepository()
        {
            ISubDistrictRepository obj = new SubDistrictRepository(new HRISSystemEntities());
            return obj;
        }
        public static IAddressTypeRepository AddressTypeRepository()
        {
            IAddressTypeRepository obj = new AddressTypeRepository(new HRISSystemEntities());
            return obj;
        }
        public static IAddressRepository AddressRepository()
        {
            IAddressRepository obj = new AddressRepository(new HRISSystemEntities());
            return obj;
        }
        public static ICountryRepository CountryRepository()
        {
            ICountryRepository obj = new CountryRepository(new HRISSystemEntities());
            return obj;
        }
        public static IEmployeeRepository EmployeeRepository()
        {
            IEmployeeRepository obj = new EmployeeRepository(new HRISSystemEntities());
            return obj;
        }
        public static IDepartmentRepository DepartmentRepository()
        {
            IDepartmentRepository obj = new DepartmentRepository(new HRISSystemEntities());
            return obj;
        }
        public static IStaffStatuRepository StaffStatuRepository()
        {
            IStaffStatuRepository obj = new StaffStatuRepository(new HRISSystemEntities());
            return obj;
        }
        public static IStaffTypeRepository StaffTypeRepository()
        {
            IStaffTypeRepository obj = new StaffTypeRepository(new HRISSystemEntities());
            return obj;
        }
        public static IContactPersonInfoRepository ContactPersonInfoRepository()
        {
            IContactPersonInfoRepository obj = new ContactPersonInfoRepository(new HRISSystemEntities());
            return obj;
        }
        public static IFamilyMemberTypeRepository FamilyMemberTypeRepository()
        {
            IFamilyMemberTypeRepository obj = new FamilyMemberTypeRepository(new HRISSystemEntities());
            return obj;
        }
        public static IFamilyMemberRepository FamilyMemberRepository()
        {
            IFamilyMemberRepository obj = new FamilyMemberRepository(new HRISSystemEntities());
            return obj;
        }
        public static IVitalStauRepository VitalStauRepository()
        {
            IVitalStauRepository obj = new VitalStauRepository(new HRISSystemEntities());
            return obj;
        }
        public static IOriginalWorkHistoryRepository OriginalWorkHistoryRepository()
        {
            IOriginalWorkHistoryRepository obj = new OriginalWorkHistoryRepository(new HRISSystemEntities());
            return obj;
        }
        public static IEducationRepository EducationRepository()
        {
            IEducationRepository obj = new EducationRepository(new HRISSystemEntities());
            return obj;
        }
        public static IEducationLevelRepository EducationLevelRepository()
        {
            IEducationLevelRepository obj = new EducationLevelRepository(new HRISSystemEntities());
            return obj;
        }
        public static IInstituteRepository InstituteRepository()
        {
            IInstituteRepository obj = new InstituteRepository(new HRISSystemEntities());
            return obj;
        }
        public static IEducationQualificationRepository EducationQualificationRepository()
        {
            IEducationQualificationRepository obj = new EducationQualificationRepository(new HRISSystemEntities());
            return obj;
        }
        public static IMajorRepository MajorRepository()
        {
            IMajorRepository obj = new MajorRepository(new HRISSystemEntities());
            return obj;
        }
        public static IPositionRepository PositionRepository()
        {
            IPositionRepository obj = new PositionRepository(new HRISSystemEntities());
            return obj;
        }
        public static IEmployeeContractRepository EmployeeContractRepository()
        {
            IEmployeeContractRepository obj = new EmployeeContractRepository(new HRISSystemEntities());
            return obj;
        }
        public static IContractTypeRepository ContractTypeRepository()
        {
            IContractTypeRepository obj = new ContractTypeRepository(new HRISSystemEntities());
            return obj;
        }
        public static IShiftControlRepository ShiftControlRepository()
        {
            IShiftControlRepository obj = new ShiftControlRepository(new HRISSystemEntities());
            return obj;
        }
        public static ITrainingRepository TrainingRepository()
        {
            ITrainingRepository obj = new TrainingRepository(new HRISSystemEntities());
            return obj;
        }
        public static ITrainingCourseRepository TrainingCourseRepository()
        {
            ITrainingCourseRepository obj = new TrainingCourseRepository(new HRISSystemEntities());
            return obj;
        }
        public static IPayrollFlagRepository PayrollFlagRepository()
        {
            IPayrollFlagRepository obj = new PayrollFlagRepository(new HRISSystemEntities());
            return obj;
        }
        public static ILot_NumberRepository Lot_NumberRepository()
        {
            ILot_NumberRepository obj = new Lot_NumberRepository(new STEC_PayrollEntities());
            return obj;
        }
        public static IManualWorkDateRepository ManualWorkDateRepository()
        {
            IManualWorkDateRepository obj = new ManualWorkDateRepository(new HRISSystemEntities());
            return obj;
        }
        public static IManualWorkDateTypeRepository ManualWorkDateTypeRepository()
        {
            IManualWorkDateTypeRepository obj = new ManualWorkDateTypeRepository(new HRISSystemEntities());
            return obj;
        }
        public static IPayrollRepository PayrollRepository()
        {
            IPayrollRepository obj = new PayrollRepository(new STEC_PayrollEntities());
            return obj;
        }
        //public static IPayrollEmployeeRepository PayrollEmployeeRepository()
        //{
        //    IPayrollEmployeeRepository obj = new PayrollEmployeeRepository(new STEC_PayrollEntities());
        //    return obj;
        //}
        public static IOTRepository OTRepository()
        {
            IOTRepository obj = new OTRepository(new HRISSystemEntities());
            return obj;
        }
        public static IOTTypeRepository OTTypeRepository()
        {
            IOTTypeRepository obj = new OTTypeRepository(new HRISSystemEntities());
            return obj;
        }
        public static IOTRequestTypeRepository OTRequestTypeRepository()
        {
            IOTRequestTypeRepository obj = new OTRequestTypeRepository(new HRISSystemEntities());
            return obj; 
        }
        public static IOTOnlineSetupAdminRepository OTOnlineSetupAdminRepository()
        {
            IOTOnlineSetupAdminRepository obj = new OTOnlineSetupAdminRepository(new HRISSystemEntities());
            return obj;
        }
        public static IOTOnlineSetupAdminDetailRepository OTOnlineSetupAdminDetailRepository()
        {
            IOTOnlineSetupAdminDetailRepository obj = new OTOnlineSetupAdminDetailRepository(new HRISSystemEntities());
            return obj;
        }
        public static IOTOnlineSetupApproverRepository OTOnlineSetupApproverRepository()
        {
            IOTOnlineSetupApproverRepository obj = new OTOnlineSetupApproverRepository(new HRISSystemEntities());
            return obj;
        }
        public static IOTOnlineSetupApproverDetailRepository OTOnlineSetupApproverDetailRepository()
        {
            IOTOnlineSetupApproverDetailRepository obj = new OTOnlineSetupApproverDetailRepository(new HRISSystemEntities());
            return obj;
        }
        public static IOTOnlineSetupFinalApproverRepository OTOnlineSetupFinalApproverRepository()
        {
            IOTOnlineSetupFinalApproverRepository obj = new OTOnlineSetupFinalApproverRepository(new HRISSystemEntities());
            return obj;
        }
        public static IOTOnlineSetupFinalApproverDetailRepository OTOnlineSetupFinalApproverDetailRepository()
        {
            IOTOnlineSetupFinalApproverDetailRepository obj = new OTOnlineSetupFinalApproverDetailRepository(new HRISSystemEntities());
            return obj;
        }
        public static IOTOnlineSetupHRApproverRepository OTOnlineSetupHRApproverRepository()
        {
            IOTOnlineSetupHRApproverRepository obj = new OTOnlineSetupHRApproverRepository(new HRISSystemEntities());
            return obj;
        }
        public static ICheckinoutRepository CheckinoutRepository()
        {
            ICheckinoutRepository obj = new CheckinoutRepository(new TimeSTECEntities());
            return obj;
        }
        public static IUserinfoRepository UserinfoRepository()
        {
            IUserinfoRepository obj = new UserinfoRepository(new TimeSTECEntities());
            return obj;
        }
        public static IDepartmentTimeStecRepository DepartmentTimeStecRepository()
        {
            IDepartmentTimeStecRepository obj = new DepartmentTimeStecRepository(new TimeSTECEntities());
            return obj;
        }
        public static IWorkUpCountryRepository WorkUpCountryRepository()
        {
            IWorkUpCountryRepository obj = new WorkUpCountryRepository(new HRISSystemEntities());
            return obj;
        }
        public static IOTDetailRepository OTDetailRepository()
        {
            IOTDetailRepository obj = new OTDetailRepository(new STEC_PayrollEntities());
            return obj;
        }
        public static ILabourWorkRepository LabourWorkRepository()
        {
            ILabourWorkRepository obj = new LabourWorkRepository(new STEC_PayrollEntities());
            return obj;
        }
        public static ITransactionLogRepository TransactionLogRepository()
        {
            ITransactionLogRepository obj = new TransactionLogRepository(new HRISSystemEntities());
            return obj;
        }
        public static IPayrollEmployeeRepository PayrollEmployeeRepository()
        {
            IPayrollEmployeeRepository obj = new PayrollEmployeeRepository(new STEC_PayrollEntities());
            return obj;
        }
        public static IFinalApprover_TypeRepository FinalApproverTypeRepository()
        {
            IFinalApprover_TypeRepository obj = new FinalApprover_TypeRepository(new HRISSystemEntities());
            return obj;
        }
        public static ITemporaryWorkerWageRepository TemporaryWorkerWageRepository()
        {
            ITemporaryWorkerWageRepository obj = new TemporaryWorkerWageRepository(new HRISSystemEntities());
            return obj;
        }
        public static ILeaveSetupRepository LeaveSetupRepository()
        {
            ILeaveSetupRepository obj = new LeaveSetupRepository(new HRISSystemEntities());
            return obj;
        }
        public static IWorkUpCountryNotedRepository WorkUpCountryNotedRepository()
        {
            IWorkUpCountryNotedRepository obj = new WorkUpCountryNotedRepository(new HRISSystemEntities());
            return obj;
        }
        public static ISupport_DetailRepository Support_DetailRepository()
        {
            ISupport_DetailRepository obj = new Support_DetailRepository(new STEC_PayrollEntities());
            return obj;
        }

        public static IPayroll_SummaryRepository Payroll_SummaryRepository()
        {
            IPayroll_SummaryRepository obj = new Payroll_SummaryRepository(new STEC_PayrollEntities());
            return obj;
        }
    }
}
