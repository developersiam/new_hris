﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HRISSystemDAL.EDMX
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    using DomainModelPayroll;

    public partial class STEC_PayrollEntities : DbContext
    {
        public STEC_PayrollEntities()
            : base("name=STEC_PayrollEntities")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Labour_work> Labour_work { get; set; }
        public virtual DbSet<NotSSO> NotSSOes { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<OT_Detail> OT_Detail { get; set; }
        public virtual DbSet<Support_Detail> Support_Detail { get; set; }
        public virtual DbSet<Payroll> Payrolls { get; set; }
        public virtual DbSet<Leave_Detail> Leave_Detail { get; set; }
        public virtual DbSet<Lot_Number> Lot_Number { get; set; }
        public virtual DbSet<Payroll_Summary> Payroll_Summary { get; set; }
    
        public virtual ObjectResult<sp_GetToPayrollByDepartment_Result> sp_GetToPayrollByDepartment(string lotMonth, string lotYear, string deptCode)
        {
            var lotMonthParameter = lotMonth != null ?
                new ObjectParameter("LotMonth", lotMonth) :
                new ObjectParameter("LotMonth", typeof(string));
    
            var lotYearParameter = lotYear != null ?
                new ObjectParameter("LotYear", lotYear) :
                new ObjectParameter("LotYear", typeof(string));
    
            var deptCodeParameter = deptCode != null ?
                new ObjectParameter("DeptCode", deptCode) :
                new ObjectParameter("DeptCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetToPayrollByDepartment_Result>("sp_GetToPayrollByDepartment", lotMonthParameter, lotYearParameter, deptCodeParameter);
        }
    
        public virtual ObjectResult<sp_GetPayrollSummaryReport_Result> sp_GetPayrollSummaryReport(string lotNumber)
        {
            var lotNumberParameter = lotNumber != null ?
                new ObjectParameter("LotNumber", lotNumber) :
                new ObjectParameter("LotNumber", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetPayrollSummaryReport_Result>("sp_GetPayrollSummaryReport", lotNumberParameter);
        }
    
        public virtual ObjectResult<sp_GetWorkingDateToPayroll_Result> sp_GetWorkingDateToPayroll(Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate)
        {
            var fromDateParameter = fromDate.HasValue ?
                new ObjectParameter("FromDate", fromDate) :
                new ObjectParameter("FromDate", typeof(System.DateTime));
    
            var toDateParameter = toDate.HasValue ?
                new ObjectParameter("ToDate", toDate) :
                new ObjectParameter("ToDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetWorkingDateToPayroll_Result>("sp_GetWorkingDateToPayroll", fromDateParameter, toDateParameter);
        }
    
        public virtual ObjectResult<sp_GetTemporaryWorkerWage_Result> sp_GetTemporaryWorkerWage(Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, string deptCode)
        {
            var fromDateParameter = fromDate.HasValue ?
                new ObjectParameter("FromDate", fromDate) :
                new ObjectParameter("FromDate", typeof(System.DateTime));
    
            var toDateParameter = toDate.HasValue ?
                new ObjectParameter("ToDate", toDate) :
                new ObjectParameter("ToDate", typeof(System.DateTime));
    
            var deptCodeParameter = deptCode != null ?
                new ObjectParameter("DeptCode", deptCode) :
                new ObjectParameter("DeptCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetTemporaryWorkerWage_Result>("sp_GetTemporaryWorkerWage", fromDateParameter, toDateParameter, deptCodeParameter);
        }
    
        public virtual ObjectResult<sp_GetActualWorkingDate_Result> sp_GetActualWorkingDate(Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate)
        {
            var fromDateParameter = fromDate.HasValue ?
                new ObjectParameter("FromDate", fromDate) :
                new ObjectParameter("FromDate", typeof(System.DateTime));
    
            var toDateParameter = toDate.HasValue ?
                new ObjectParameter("ToDate", toDate) :
                new ObjectParameter("ToDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetActualWorkingDate_Result>("sp_GetActualWorkingDate", fromDateParameter, toDateParameter);
        }
    
        public virtual ObjectResult<sp_GetCurrent_Lot_Result> sp_GetCurrent_Lot()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetCurrent_Lot_Result>("sp_GetCurrent_Lot");
        }
    
        public virtual ObjectResult<sp_NEWGet_Result> sp_NEWGet(string lotMonth, string lotYear, string deptCode)
        {
            var lotMonthParameter = lotMonth != null ?
                new ObjectParameter("LotMonth", lotMonth) :
                new ObjectParameter("LotMonth", typeof(string));
    
            var lotYearParameter = lotYear != null ?
                new ObjectParameter("LotYear", lotYear) :
                new ObjectParameter("LotYear", typeof(string));
    
            var deptCodeParameter = deptCode != null ?
                new ObjectParameter("DeptCode", deptCode) :
                new ObjectParameter("DeptCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_NEWGet_Result>("sp_NEWGet", lotMonthParameter, lotYearParameter, deptCodeParameter);
        }
    
        public virtual ObjectResult<sp_GetSSOReport_Result> sp_GetSSOReport(string lotNumber)
        {
            var lotNumberParameter = lotNumber != null ?
                new ObjectParameter("LotNumber", lotNumber) :
                new ObjectParameter("LotNumber", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetSSOReport_Result>("sp_GetSSOReport", lotNumberParameter);
        }
    
        public virtual ObjectResult<sp_GetPayrollEmployee_Result> sp_GetPayrollEmployee()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetPayrollEmployee_Result>("sp_GetPayrollEmployee");
        }
    }
}
