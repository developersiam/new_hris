﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace HRISSystemDAL.GenericRepository
{
    public class GenericDataRepository<T> : IGenericDataRepository<T> where T : class
    {
        private DbContext context;
        private DbSet<T> dbSet;

        public GenericDataRepository(DbContext context)
        {
            this.context = context;
            this.dbSet = context.Set<T>();
        }

        public virtual List<T> Get(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = dbSet;
            foreach (Expression<Func<T, object>> include in includes)
                query = query.Include(include);

            if (filter != null)
                query = query.Where(filter);

            if (orderBy != null)
                query = orderBy(query);

            return query.ToList();
        }

        public virtual IQueryable<T> Query(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = dbSet;

            if (filter != null)
                query = query.Where(filter);

            if (orderBy != null)
                query = orderBy(query);

            foreach (Expression<Func<T, object>> include in includes)
                query = query.Include(include);

            return query;
        }

        public virtual T GetSingle(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] includes)
        {
            T item = null;
            IQueryable<T> dbQuery = context.Set<T>().Where(where);

            foreach (Expression<Func<T, object>> include in includes)
                dbQuery = dbQuery.Include<T, object>(include);

            item = dbQuery
                //.AsNoTracking()
                .FirstOrDefault(where);
            return item;
        }

        public void Add(T entity)
        {
            dbSet.Add(entity);
        }

        public void Update(T entity)
        {
            dbSet.Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }

        public void Remove(T entity)
        {
            if (context.Entry(entity).State == EntityState.Detached)
                dbSet.Attach(entity);

            dbSet.Remove(entity);
        }

        public void UpdateRange(IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                context.Entry(item).State = EntityState.Modified;
            }            
        }

        //public virtual IList<T> GetAll(params Expression<Func<T, object>>[] navigationProperties)
        //{
        //    List<T> list;
        //    using (var context = new BuyingSystemEntities())
        //    {
        //        IQueryable<T> dbQuery = context.Set<T>();

        //        //Apply eager loading
        //        foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
        //            dbQuery = dbQuery.Include<T, object>(navigationProperty);

        //        list = dbQuery
        //            .AsNoTracking()
        //            .ToList<T>();
        //    }
        //    return list;
        //}

        //public virtual IList<T> GetList(System.Linq.Expressions.Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] navigationProperties)
        //{
        //    List<T> list;
        //    using (var context = new BuyingSystemEntities())
        //    {
        //        IQueryable<T> dbQuery = context.Set<T>().Where(where);

        //        //Apply eager loading
        //        foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
        //            dbQuery = dbQuery.Include<T, object>(navigationProperty);

        //        list = dbQuery
        //            .AsNoTracking()
        //            .Where(where)
        //            .ToList<T>();
        //    }
        //    return list;
        //}

        //public virtual T GetSingle(System.Linq.Expressions.Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] navigationProperties)
        //{
        //    T item = null;
        //    using (var context = new BuyingSystemEntities())
        //    {
        //        IQueryable<T> dbQuery = context.Set<T>().Where(where);

        //        //Apply eager loading
        //        foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
        //            dbQuery = dbQuery.Include<T, object>(navigationProperty);

        //        item = dbQuery
        //            .AsNoTracking() //Don't track any changes for the selected item
        //            .FirstOrDefault(where); //Apply where clause
        //    }
        //    return item;
        //}

        ///* rest of code omitted */

        //public void Add(params T[] items)
        //{
        //    using (var context = new BuyingSystemEntities())
        //    {
        //        foreach (T item in items)
        //        {
        //            context.Entry(item).State = System.Data.Entity.EntityState.Added;
        //        }
        //        context.SaveChanges();
        //    }
        //}

        //public void Update(params T[] items)
        //{
        //    using (var context = new BuyingSystemEntities())
        //    {
        //        foreach (T item in items)
        //        {
        //            context.Entry(item).State = System.Data.Entity.EntityState.Modified;
        //        }
        //        context.SaveChanges();
        //    }
        //}

        //public void Remove(params T[] items)
        //{
        //    using (var context = new BuyingSystemEntities())
        //    {
        //        foreach (T item in items)
        //        {
        //            context.Entry(item).State = System.Data.Entity.EntityState.Deleted;
        //        }
        //        context.SaveChanges();
        //    }
        //}
    }
}
